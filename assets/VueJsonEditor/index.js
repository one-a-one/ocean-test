import VueJsonEditor from './VueJsonEditor'

VueJsonEditor.install = function (Vue) {
  Vue.component(VueJsonEditor.name, VueJsonEditor)
}

export default VueJsonEditor
