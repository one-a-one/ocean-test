import util from './core/utils/util'
import time from './core/utils/time'

import clickoutside from './core/utils/clickoutside'
import { getLodop } from './core/utils/LodopFuncs'
import LodopBuilder from './core/utils/LodopBuilder'
// import processingXlsx from './core/utils/processingXlsx'

import CssProp from './core/style/CssProp'

// import oceVideo from './oce-video'
// import oceMap from './oce-map'
import oceBase from './oce-base'
// import oceAvue from './oce-avue'

import XnyBase from './XnyBase'
import XnyEcharts from './XnyEcharts'

import VueJsonEditor from '../assets/VueJsonEditor'
import Avue from '../assets/avue/avue2.9.12'
import '../assets/avue/index2.9.12.css'

const VERSION = '1.1.1'

const ocean = {
  ...util,
  ...time,
  // VERSION: process.env.VERSION,
  VERSION,
  // ...processingXlsx,
  getLodop,
  ...LodopBuilder,
  CssProp,
}

const install = function (Vue, option = {}) {
  // const modules = option.modules
  // const isOption = util.validatenull(modules) // 空值

  // if (isOption || modules?.includes('core'))
  Vue.prototype.$ocean = ocean

  // if (isOption || modules?.includes('oceMap')) {
  //   import('./oce-map').then((res) => {
  //     const oceMap = res.default
  //     Vue.use(oceMap)
  //   })
  // }

  // if (isOption || modules?.includes('oceBase')) {
  //   import('./oce-base').then((res) => {
  //     const oceBase = res.default
  //     Vue.use(oceBase)
  //   })
  // }

  // if (isOption || modules?.includes('oceVideo')) {
  //   import('./oce-video').then((res) => {
  //     const oceVideo = res.default
  //     Vue.use(oceVideo)
  //   })
  // }

  // if (isOption || modules?.includes('oceAvue')) {
  //   import('./oce-avue').then((res) => {
  //     const oceAvue = res.default
  //     Vue.use(oceAvue)
  //   })
  // }

  // if (isOption || modules?.includes('XnyBase')) {
  //   import('./XnyBase').then((res) => {
  //     const XnyBase = res.default
  //     Vue.use(XnyBase)
  //   })
  // }

  // Vue.use(oceMap)
  // Vue.use(oceVideo)

  // Vue.use(oceAvue)
  Vue.use(oceBase)
  Vue.use(XnyBase)
  Vue.use(XnyEcharts)
  Vue.use(VueJsonEditor)
  Vue.use(Avue)
  Vue.directive('clickoutside', clickoutside)
}

export default {
  // version: process.env.VERSION,
  version: VERSION,
  install,
}
