import OceSelect from './src/select';

OceSelect.install = function(Vue) {
  Vue.component(OceSelect.name, OceSelect);
};

export default OceSelect;
