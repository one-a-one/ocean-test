import OceFormDrawer from './src/formDrawer';

OceFormDrawer.install = function(Vue) {
  Vue.component(OceFormDrawer.name, OceFormDrawer);
};

export default OceFormDrawer;
