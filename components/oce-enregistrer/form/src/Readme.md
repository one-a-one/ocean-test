### Option Attributes

|  参数   | 说明  | 类型  | 默认值  |
|  ----  | ----  | ----  | ----  |
| ____  | ________________________ | ____  | ____ |

### Column Attributes

|  参数   | 说明  | 类型  | 默认值  |
|  ----  | ----  | ----  | ----  |
| label  | 表单列标题 | String  | ____ |
| prop  | 表单列字段 | String  | ____ |
| type  | 列类型<br>(目前支持input、select、date类型） | String  | input |
| comType  | 列子组件类型<br>(如：tupe=date时，可选daterange、datetime等，具体详见element-ui对应类型文档） | String  | input |
