import OceForm from './src/form';

OceForm.install = function(Vue) {
  Vue.component(OceForm.name, OceForm);
};

export default OceForm;
