import OceDate from './src/date';

OceDate.install = function(Vue) {
  Vue.component(OceDate.name, OceDate);
};

export default OceDate;
