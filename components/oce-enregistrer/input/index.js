import OceInput from './src/input';

OceInput.install = function(Vue) {
  Vue.component(OceInput.name, OceInput);
};

export default OceInput;
