
import Form from './form/index'
import Select from './select/index'
import Input from './input/index'
import Date from './date/index'
import FormDrawer from './formDrawer/index'

const components = [
  Form,
  Select,
  Input,
  Date,
  FormDrawer,
]

const install = function (Vue,) {
  // 注册组件------------------------------------------
  components.forEach(component => {
    Vue.component(component.name, component)
  })
}

let componentsObj = {}
components.forEach(component => {
  componentsObj[component.name] = component
})

export default {
  version: '0.0.1',
  install,
  components: {
    ...componentsObj
  }
}
