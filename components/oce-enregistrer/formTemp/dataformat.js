// 支持表单类型
const OCE_FORM_LIST = ['select','input','date']

/**
 * 动态获取组件
 */
export const getComponent = (type) => {
  let result = 'oce-input'
  if(OCE_FORM_LIST.includes(type)) {
    result = 'oce-' + type
  }
  return result
}
