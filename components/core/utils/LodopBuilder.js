/* eslint-disable no-unused-vars */
/* eslint-disable no-eval */
import { getLodop } from './LodopFuncs'
// 当前样式
const defaultStyle = {
  fontName: '宋体', // 设定纯文本打印项的字体名称。
  // fontSize: 9, // 设定纯文本打印项的字体大小,单位是pt，缺省值是9。
  fontSize: 3.175, // 3.175mm = 9pt
  fontColor: '#000000', // 设定纯文本打印项的字体颜色。
  bold: 0, // 设定纯文本打印项是否粗体。
  italic: 0, // 设定纯文本打印项是否斜体。
  underline: 0, // 设定纯文本打印项是否下滑线。
  alignment: 1, // 设定纯文本打印项的内容左右靠齐方式。
  angle: 0, // 设定纯文本打印项的旋转角度。
  itemType: 0, // 设定打印项的基本属性。
  hOrient: 0, // 设定打印项在纸张范围内的水平方向的位置锁定方式。
  vOrient: 0, // 设定打印项在纸张范围内的垂直方向的位置锁定方式。
  penWidth: 1, // 线条宽度。
  penStyle: 0, // 线条风格。
  stretch: 0, // 图片截取缩放模式。
  previewOnly: 0, // 内容仅仅用来预览。
  readOnly: true, // 纯文本内容在打印维护时，是否禁止修改。
}

const nowStyle = {}
// 初始化样式
function initStyle() {
  for (const key in defaultStyle) {
    nowStyle[key] = -1
  }
}

// 毫米转换磅 72pt = 25.4mm
function mmToPoint(num) {
  return num * (72 / 25.4)
}

// 设置样式
function setStyle(style) {
  let sytleTemplate = ''
  // console.log(style)
  // eslint-disable-next-line no-restricted-syntax
  for (const key in nowStyle) {
    // console.log(nowStyle[key], style[key], nowStyle[key] != style[key])
    if (nowStyle[key] == -1 || nowStyle[key] != style[key]) {
      // console.log(key)
      nowStyle[key] = style[key]
      sytleTemplate += `
  LODOP.SET_PRINT_STYLE('${key}', '${
        key == 'fontSize' ? mmToPoint(style[key]) : style[key]
      }')`
    }
  }
  // console.log(sytleTemplate)
  return sytleTemplate
}

// 获取lodep模板
export const getLodopTemplate = (globalOption, itemOption) => {
  // console.log(globalOption, itemOption)
  let template = ''
  template += `  LODOP.PRINT_INIT('${globalOption.strTaskName}')
  LODOP.SET_PRINT_PAGESIZE(${globalOption.intOrient || 0}, ${
    globalOption.pageWidth || 0
  }, ${globalOption.pageHeight || 0}, '${globalOption.strPageName || ''}')`
  // 样式初始化
  initStyle()
  template += setStyle(defaultStyle)
  itemOption.forEach((item) => {
    template += setStyle(item.style)
    // console.log(item)
    const { content } = item
    switch (item.type) {
      case 'text':
        template += `
  LODOP.ADD_PRINT_TEXT('${content.top}mm', '${content.left}mm', '${content.width}mm', '${content.height}mm', ( form && form['${content.key}'] ) ? form['${content.key}'] : '${content.default}')`
        break
      case 'line':
        template += `
  LODOP.ADD_PRINT_LINE('${content.top1}mm', '${content.left1}mm', '${content.top2}mm', '${content.left2}mm', '${content.intLineStyle}', '${content.intLineWidth}')`
        break
      case 'rectangle':
        template += `
  LODOP.ADD_PRINT_RECT('${content.top}mm', '${content.left}mm', '${content.width}mm', '${content.height}mm', '${content.intLineStyle}', '${content.intLineWidth}')`
        break
      case 'barcode':
        template += `
  LODOP.ADD_PRINT_BARCODE('${content.top}mm', '${content.left}mm', '${content.width}mm', '${content.height}mm', '${content.codeType}', ( form && form['${content.key}'] ) ? form['${content.key}'] : '${content.default}')
  LODOP.SET_PRINT_STYLEA(0,"DataCharset","UTF-8");`
        break
      default:
        throw new Error(`类型${item.type}暂不支持。`)
    }
  })
  return template
}

// lodop预览
export const lodopPreview = (template, form = {}) => {
  const LODOP = getLodop()
  // console.log(form && form[title] ? form[title] : 'L700过程标签')
  const temp = `${template}
      LODOP.PREVIEW()`
  eval(temp)
}
// lodop打印
export const lodopPrint = (template, form = {}) => {
  const LODOP = getLodop()
  const temp = `${template}
      LODOP.PRINT()`
  eval(temp)
}

export default { getLodopTemplate, lodopPreview, lodopPrint }
