﻿/* eslint-disable no-undef */
/* eslint-disable consistent-return */
/* eslint-disable camelcase */
//= =本JS是加载Lodop插件或Web打印服务CLodop/Lodop7的综合示例，可直接使用，建议理解后融入自己程序==

let CreatedOKLodopObject
let CLodopIsLocal
let CLodopJsState

//= =判断是否需要CLodop(那些不支持插件的浏览器):==
function needCLodop() {
  try {
    const ua = navigator.userAgent
    if (ua.match(/Windows\sPhone/i)) return true
    if (ua.match(/iPhone|iPod|iPad/i)) return true
    if (ua.match(/Android/i)) return true
    if (ua.match(/Edge\D?\d+/i)) return true

    const verTrident = ua.match(/Trident\D?\d+/i)
    const verIE = ua.match(/MSIE\D?\d+/i)
    let verOPR = ua.match(/OPR\D?\d+/i)
    let verFF = ua.match(/Firefox\D?\d+/i)
    const x64 = ua.match(/x64/i)
    if (!verTrident && !verIE && x64) return true
    if (verFF) {
      verFF = verFF[0].match(/\d+/)
      if (verFF[0] >= 41 || x64) return true
    } else if (verOPR) {
      verOPR = verOPR[0].match(/\d+/)
      if (verOPR[0] >= 32) return true
    } else if (!verTrident && !verIE) {
      let verChrome = ua.match(/Chrome\D?\d+/i)
      if (verChrome) {
        verChrome = verChrome[0].match(/\d+/)
        if (verChrome[0] >= 41) return true
      }
    }
    return false
  } catch (err) {
    return true
  }
}

// 加载CLodop时用双端口(http是8000/18000,而https是8443/8444)以防其中某端口被占,
// 主JS文件名“CLodopfuncs.js”是固定名称，其内容是动态的，与其链接的打印环境有关:
function loadCLodop() {
  if (CLodopJsState == 'loading' || CLodopJsState == 'complete') return
  CLodopJsState = 'loading'
  const head =
    document.head ||
    document.getElementsByTagName('head')[0] ||
    document.documentElement
  const JS1 = document.createElement('script')
  const JS2 = document.createElement('script')

  if (window.location.protocol == 'https:') {
    JS1.src = 'https://localhost.lodop.net:8443/CLodopfuncs.js'
    JS2.src = 'https://localhost.lodop.net:8444/CLodopfuncs.js'
  } else {
    JS1.src = 'http://localhost:8000/CLodopfuncs.js'
    JS2.src = 'http://localhost:18000/CLodopfuncs.js'
  }
  JS1.onload = JS2.onload = function () {
    CLodopJsState = 'complete'
  }
  JS1.onerror = JS2.onerror = function (evt) {
    CLodopJsState = 'complete'
  }
  head.insertBefore(JS1, head.firstChild)
  head.insertBefore(JS2, head.firstChild)
  CLodopIsLocal = !!(JS1.src + JS2.src).match(/\/\/localho|\/\/127.0.0./i)
}

if (needCLodop()) {
  loadCLodop()
} // 开始加载

//= =获取LODOP对象主过程,判断是否安装、需否升级:==
export const getLodop = (oOBJECT, oEMBED) => {
  const strFontTag = "<br><font color='#FF00FF'>打印控件"
  const strLodopInstall = `${strFontTag}未安装!点击这里<a href='http://www.lodop.net/download/Lodop6.226Clodop4.158.zip' target='_self'>执行安装</a>`
  const strLodopUpdate = `${strFontTag}需要升级!点击这里<a href='http://www.lodop.net/download/Lodop6.226Clodop4.158.zip' target='_self'>执行升级</a>`
  const strLodop64Install = `${strFontTag}未安装!点击这里<a href='http://www.lodop.net/download/Lodop6.226Clodop4.158.zip' target='_self'>执行安装</a>`
  const strLodop64Update = `${strFontTag}需要升级!点击这里<a href='http://www.lodop.net/download/Lodop6.226Clodop4.158.zip' target='_self'>执行升级</a>`
  const strCLodopInstallA =
    "<font color='#FF00FF'>Web打印服务CLodop未安装启动，点击这里<a href='http://www.lodop.net/download/Lodop6.226Clodop4.158.zip' target='_self'>下载执行安装</a>"
  const strCLodopInstallB =
    "<br>（若此前已安装过，可<a href='CLodop.protocol:setup' target='_self'>点这里直接再次启动</a>）"
  const strCLodopUpdate =
    "<font color='#FF00FF'>Web打印服务CLodop需升级!点击这里<a href='http://www.lodop.net/download/Lodop6.226Clodop4.158.zip' target='_self'>执行升级</a>"
  const strLodop7FontTag = "<br><font color='#FF00FF'>Web打印服务Lodop7"
  const strLodop7HrefX86 =
    "点击这里<a href='Lodop7_Linux_X86_64.tar.gz' target='_self'>下载安装</a>(下载后解压，点击lodop文件开始执行)"
  const strLodop7HrefARM =
    "点击这里<a href='Lodop7_Linux_ARM64.tar.gz'  target='_self'>下载安装</a>(下载后解压，点击lodop文件开始执行)"
  const strLodop7Install_X86 = `${strLodop7FontTag}未安装启动，${strLodop7HrefX86}`
  const strLodop7Install_ARM = `${strLodop7FontTag}未安装启动，${strLodop7HrefARM}`
  const strLodop7Update_X86 = `${strLodop7FontTag}需升级，${strLodop7HrefX86}`
  const strLodop7Update_ARM = `${strLodop7FontTag}需升级，${strLodop7HrefARM}`
  const strInstallOK = '，成功后请刷新本页面或重启浏览器。</font>'
  let LODOP
  // try {
  const isWinIE =
    /MSIE/i.test(navigator.userAgent) || /Trident/i.test(navigator.userAgent)
  const isWinIE64 = isWinIE && /x64/i.test(navigator.userAgent)
  const isLinuxX86 =
    /Linux/i.test(navigator.platform) && /x86/i.test(navigator.platform)
  const isLinuxARM =
    /Linux/i.test(navigator.platform) && /aarch/i.test(navigator.platform)

  if (needCLodop() || isLinuxX86 || isLinuxARM) {
    try {
      LODOP = getCLodop()
    } catch (err) {}
    if (!LODOP && CLodopJsState !== 'complete') {
      if (CLodopJsState == 'loading') {
        throw new Error('网页还没下载完毕，请稍等一下再操作.')
      } else throw new Error('未曾加载Lodop主JS文件，请先调用loadCLodop过程.')
      return
    }
    let strAlertMessage
    if (!LODOP) {
      if (isLinuxX86) strAlertMessage = strLodop7Install_X86
      else if (isLinuxARM) strAlertMessage = strLodop7Install_ARM
      else {
        strAlertMessage =
          strCLodopInstallA + (CLodopIsLocal ? strCLodopInstallB : '')
      }
      // document.body.innerHTML =
      //   strAlertMessage + strInstallOK + document.body.innerHTML
      throw new Error(strAlertMessage + strInstallOK)
    }
    if (isLinuxX86 && LODOP.CVERSION < '7.0.3.9') {
      strAlertMessage = strLodop7Update_X86
    } else if (isLinuxARM && LODOP.CVERSION < '7.0.3.9') {
      strAlertMessage = strLodop7Update_ARM
    } else if (CLODOP.CVERSION < '4.1.5.5') strAlertMessage = strCLodopUpdate

    if (strAlertMessage) {
      // document.body.innerHTML =
      //   strAlertMessage + strInstallOK + document.body.innerHTML
      throw new Error(strAlertMessage + strInstallOK)
    }
  } else {
    //= =如果页面有Lodop插件就直接使用,否则新建:==
    if (oOBJECT || oEMBED) {
      if (isWinIE) LODOP = oOBJECT
      else LODOP = oEMBED
    } else if (!CreatedOKLodopObject) {
      LODOP = document.createElement('object')
      LODOP.setAttribute('width', 0)
      LODOP.setAttribute('height', 0)
      LODOP.setAttribute(
        'style',
        'position:absolute;left:0px;top:-100px;width:0px;height:0px;'
      )
      if (isWinIE) {
        LODOP.setAttribute(
          'classid',
          'clsid:2105C259-1E0C-4534-8141-A753534CB4CA'
        )
      } else LODOP.setAttribute('type', 'application/x-print-lodop')
      document.documentElement.appendChild(LODOP)
      CreatedOKLodopObject = LODOP
    } else LODOP = CreatedOKLodopObject
    //= =Lodop插件未安装时提示下载地址:==
    if (!LODOP || !LODOP.VERSION) {
      // document.body.innerHTML =
      //   (isWinIE64 ? strLodop64Install : strLodopInstall) +
      //   strInstallOK +
      //   document.body.innerHTML
      throw new Error(
        (isWinIE64 ? strLodop64Install : strLodopInstall) + strInstallOK
      )
      // return LODOP
    }
    if (LODOP.VERSION < '6.2.2.6') {
      // document.body.innerHTML =
      //   (isWinIE64 ? strLodop64Update : strLodopUpdate) +
      //   strInstallOK +
      //   document.body.innerHTML
      throw new Error(
        (isWinIE64 ? strLodop64Update : strLodopUpdate) + strInstallOK
      )
    }
  }
  //= ==如下空白位置适合调用统一功能(如注册语句、语言选择等):=======================

  LODOP.SET_LICENSES(
    '',
    '13528A153BAEE3A0254B9507DCDE2839',
    'EDE92F75B6A3D917F65910',
    'D60BC84D7CF2DE18156A6F88987304CB6D8'
  )

  //= ==============================================================================
  return LODOP
  // } catch (err) {
  //   alert(`getLodop出错:${err}`)
  // }
}

export default { getLodop }
