/**
 * 时间转换
 */

// 秒数转换为时分秒格式
const timeFormat = (time, connector = ':') => {
  let timeStr = '';
  let stringFormat = (i) => {
    return i < 10 ? `0${i}` : `${i}`;
  }
  let minuteTime = 0;
  let secondTime = 0;
  let hourTime = 0;
  if (time < 60) {
    timeStr = `00${connector}00${connector}${stringFormat(time)}`
  } else if (time >= 60 && time < 3600) {
    minuteTime = parseInt(time / 60);
    secondTime = parseInt(time % 60);
    timeStr = `00${connector}${stringFormat(minuteTime)}${connector}${stringFormat(secondTime)}`;
  } else if (time >= 3600) {
    let _t = parseInt(time % 3600);
    hourTime = parseInt(time / 3600);
    minuteTime = parseInt(_t / 60);
    secondTime = parseInt(_t % 60);
    timeStr = `${stringFormat(hourTime)}${connector}${stringFormat(minuteTime)}${connector}${stringFormat(secondTime)}`
  }
  return timeStr;
}

// 时间转换秒数
const secondsCounter = (dateStr = '',isUtc = true,isData = true) => {
  let date = dateStr
  if(!isData) date = '2020-01-01 ' + dateStr
  if(isUtc) date = utcToLocal(dateStr)
  date = new Date(date)
  let hours = date.getHours()
  let minutes = date.getMinutes()
  let seconds = date.getSeconds()
  return hours * 3600 + minutes * 60 + seconds
}

export default { timeFormat, secondsCounter }