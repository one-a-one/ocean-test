/* eslint-disable no-underscore-dangle */
import { props as defaultProps, preinstallSytle } from './defaultProps'
import { parseCamelCase, validatenull, vaildData } from '../utils/util'

const defaultOption = {
  prefix: 'Ocean', // css前缀
}

export class CssProp {
  // let props = {
  //     model: 'default',
  //   }
  constructor(option = {}) {
    this.docStyle = document.documentElement.style
    this.prefix = option.prefix || defaultOption.prefix
    this.props = {
      model: 'default',
    }
    this.defaultProps = defaultProps
    this.init(option.initProps || defaultProps)
  }

  // 设置模式
  setModel(model) {
    if (validatenull(model)) return false
    const style = preinstallSytle[model]
    if (model == 'default') {
      defaultProps.forEach((item) => {
        if (!item.prop) return
        this.setProp(item.prop, item.default + (item.suffix || ''))
      })
      this.props.model = model
    } else if (style) {
      defaultProps.forEach((item) => {
        if (!item.prop && !style[item.prop]) return
        this.setProp(item.prop, style[item.prop] + (item.suffix || ''))
      })
      this.props.model = model
    }
    return true
  }

  // 获取默认配置项
  // eslint-disable-next-line class-methods-use-this
  getDefaultProp(propertyname) {
    if (validatenull(propertyname)) return false
    return defaultProps.find((item) => item.prop == propertyname)
  }

  // 初始化
  init(props) {
    const storageProps = JSON.parse(localStorage.getItem('Ocean-css-prop'))
    // this.setModel(storageProps.model)
    this.props.model = storageProps?.model || this.props.model
    props.forEach((item) => {
      if (!item.prop) return
      this.setProp(
        item.prop,
        storageProps
          ? vaildData(
              storageProps[item.prop],
              item.default + (item.suffix || '')
            )
          : item.default + (item.suffix || '')
      )
    })
  }

  /**
   * 设置css属性
   * @param {*} propertyname 例：searchPadding
   * @param {*} value
   * @param {*} priority
   * @returns
   */
  setProp(propertyname, value, priority) {
    if (this.props[propertyname] == value) return
    this.props[propertyname] = value
    const res = this.docStyle.setProperty(
      `--${this.prefix}-${parseCamelCase(propertyname, '-')}`,
      value,
      priority
    )
    localStorage.setItem('Ocean-css-prop', JSON.stringify(this.props))
    return res
  }

  getProp(propertyname) {
    return this.docStyle.getPropertyValue(
      `--${parseCamelCase(propertyname, '-')}`
    )
  }

  removeProp(propertyname) {
    delete this.props.propertyname
    return this.docStyle.removeProperty(propertyname)
  }
}

const temp = new CssProp()

export default temp
