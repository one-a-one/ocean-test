
import Video from './video/index'

const components = [
  Video,
]

const install = function (Vue) {
  // 注册组件------------------------------------------
  components.forEach(component => {
    Vue.component(component.name, component)
  })
}

// if (typeof window !== 'undefined' && window.Vue) {
//   install(window.Vue)
// }

let componentsObj = {}
components.forEach(component => {
  componentsObj[component.name] = component
})

export default {
  version: '0.0.1',
  install,
  components: {
    ...componentsObj
  }
}
