import "xgplayer"
import FlvPlayer from "xgplayer-flv"
// import FlvJsPlayer from "xgplayer-flv.js";

// console.log(FlvPlayer)
// let connectTimeoutFlag = null
export default {
  methods: {
    xgPlayerInitDPlayer(container, url) {
      if(!container || !url) return
      let flvPlayer = new FlvPlayer({
        el: container,
        url,
        isLive: true,
        playsinline: true,
        hasAudio: false,
        height: '100%',
        width: '100%',
        // fitVideoSize: 'auto',
        controls: false,
        autoplay: true
      });
      let connectTimeoutFlag
      flvPlayer.on('error', (e) => {
        console.log('报错了' + this.infoText);
        // this.xgPlayerRelink()
        if (connectTimeoutFlag || !this.$refs.video || !this.realIsPlay) {
          clearTimeout(connectTimeoutFlag)
          connectTimeoutFlag = null
          return
        }
        this.destroy()
        connectTimeoutFlag = setTimeout(() => {
          console.log('回连-------------' + this.infoText)
          if (this.isLive && !this.isPlay && !this.player) {
            this.playVideo()
          } else {
            clearTimeout(connectTimeoutFlag)
            connectTimeoutFlag = null
          }
        }, 5000)
        //-------------------------------------------
      })
      // 播放
      flvPlayer.on('play', () => {
        // console.log('播放')
        this.isPlay = true
      })
      // 继续播放
      flvPlayer.on('playing', () => {
        this.isPlay = true
      })
      // 结束
      flvPlayer.on('ended', () => {
        console.log('结束' + this.infoText);
        // this.xgPlayerRelink()
        if (connectTimeoutFlag || !this.$refs.video  || !this.realIsPlay) {
          clearTimeout(connectTimeoutFlag)
          connectTimeoutFlag = null
          return
        }
        this.destroy()
        connectTimeoutFlag = setTimeout(() => {
          console.log('回连-------------' + this.infoText)
          if (this.isLive && !this.isPlay && !this.player) {
            this.playVideo()
          } else {
            clearTimeout(connectTimeoutFlag)
            connectTimeoutFlag = null
          }
        }, 5000)
        //-------------------------------------------
      })
      return flvPlayer
    },
    // 重新连接
    xgPlayerRelink() {
      let connectTimeoutFlag
      console.log('connectTimeoutFlag' + this.infoText, connectTimeoutFlag)
      if (connectTimeoutFlag) {
        clearTimeout(connectTimeoutFlag)
        connectTimeoutFlag = null
        return
      }
      this.destroy()
      connectTimeoutFlag = setTimeout(() => {
        console.log('回连-------------' + this.infoText)
        if (this.isLive && !this.isPlay && !this.player) {
          this.playVideo()
        } else {
          clearTimeout(connectTimeoutFlag)
          connectTimeoutFlag = null
        }
      }, 5000)
    },
    // 切换播放状态播放
    xgPlayerPlayVideo(isPlay) {
      if (typeof isPlay === "boolean") {
        this.realIsPlay = isPlay
        if (!isPlay) {
          this.destroy();
        } else {
          this.player = this.initDPlayer(this.$refs.video, this.url);
        }
        return;
      }
      if (this.isPlay) {
        this.destroy();
      } else {
        this.player = this.initDPlayer(this.$refs.video, this.url);
      }
    },
    // 销毁
    xgPlayerDestroy() {
      this.isPlay = false
      if (!this.player || !this.player.destroy) return;
      this.player.destroy();
      this.assistantNum = 0;
      this.duration = 0;
      this.player = null
    },
    // 切换显示比例 speed 4:3 16:9 filling
    xgPlayerUpdateDisplayScale() {
      let scale = this.displayScale;
      let videoEl = this.$refs.video.getElementsByTagName("video")[0];
      let elWidth = this.$refs.video.clientWidth;
      let elHeight = this.$refs.video.clientHeight;
      let ratio = elWidth / elHeight;
      console.log(this.$refs.video, videoEl);
      console.log(elWidth, elHeight, ratio);
      if (scale == 'speed') {
        videoEl.style.objectFit = ''
      } else {
        videoEl.style.objectFit = 'fill'
      }
      switch (scale) {
        case "4:3":
          if (ratio > 4 / 3) {
            videoEl.style.height = "100%";
            videoEl.style.width = elHeight * (4 / 3) + "px";
          } else {
            videoEl.style.height = elWidth * (3 / 4) + "px";
            videoEl.style.width = "100%";
          }
          break;
        case "16:9":
          if (ratio > 16 / 9) {
            videoEl.style.height = "100%";
            videoEl.style.width = elHeight * (16 / 9) + "px";
          } else {
            videoEl.style.height = elWidth * (9 / 16) + "px";
            videoEl.style.width = "100%";
          }
          break;
        default:
          videoEl.style.height = "100%";
          videoEl.style.width = "100%";
      }
    }
  }
}