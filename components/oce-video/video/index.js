import OceVideo from './src/video';

OceVideo.install = function(Vue) {
  Vue.component(OceVideo.name, OceVideo);
};

export default OceVideo;
