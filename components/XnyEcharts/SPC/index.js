import XnySPC from './src/SPC.vue'

XnySPC.install = function (Vue) {
  Vue.component(XnySPC.name, XnySPC)
}

export default XnySPC
