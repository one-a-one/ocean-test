
const comList = require.context('./', true, /index.js$/)
const components = []
comList.keys().map((fileName) => {
  const comp = comList(fileName).default
  if (comp) components.push(comp)
})

const install = function (Vue) {
  // 注册组件------------------------------------------
  components.forEach((component) => {
    Vue.component(component.name, component)
  })
}

const componentsObj = {}
components.forEach((component) => {
  componentsObj[component.name] = component
})

export default {
  version: '0.0.1',
  install,
  components: {
    ...componentsObj,
  },
}
