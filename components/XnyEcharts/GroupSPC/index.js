import XnyGroupSPC from './src/GroupSPC.vue'

XnyGroupSPC.install = function (Vue) {
  Vue.component(XnyGroupSPC.name, XnyGroupSPC)
}

export default XnyGroupSPC
