import OceProgress from './src/progress';

OceProgress.install = function(Vue) {
  Vue.component(OceProgress.name, OceProgress);
};

export default OceProgress;
