import OceInputTree from './src/inputTree.vue'

OceInputTree.install = function (Vue) {
  Vue.component(OceInputTree.name, OceInputTree)
}

export default OceInputTree
