import OceGrid from './src/grid';

OceGrid.install = function(Vue) {
  Vue.component(OceGrid.name, OceGrid);
};

export default OceGrid;