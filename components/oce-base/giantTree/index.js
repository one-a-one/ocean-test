import OceGiantTree from './src/giantTree.vue'

OceGiantTree.install = function (Vue) {
  Vue.component(OceGiantTree.name, OceGiantTree)
}

export default OceGiantTree
