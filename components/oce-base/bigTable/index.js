import OceBigTable from './src/bigTable.vue'
import vbtTable from './src/bigTreeTable/table.vue'
import vbtTableColumn from './src/bigTreeTable/table-column'

OceBigTable.install = function (Vue) {
  Vue.component(OceBigTable.name, OceBigTable)
}

// export default OceBigTable;

vbtTable.install = function (Vue) {
  Vue.component(vbtTable.name, vbtTable)
}

vbtTableColumn.install = function (Vue) {
  Vue.component(vbtTableColumn.name, vbtTableColumn)
}

export { vbtTable, vbtTableColumn, OceBigTable }
