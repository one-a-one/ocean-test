import OceTable from './src/table';

OceTable.install = function(Vue) {
  Vue.component(OceTable.name, OceTable);
};

export default OceTable;
