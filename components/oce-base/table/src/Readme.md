### Option Attributes

|  参数   | 说明  | 类型  | 默认值  |
|  ----  | ----  | ----  | ----  |
| height  | 表格高度 | String  | ____ |
| selection  | 行可勾选 | Boolean  | false |
| delBtn  | 行内删除按钮 | Boolean  | true |
| editBtn  | 行内编辑按钮 | Boolean  | true |
| menu  | 是否显示操作菜单栏 | Boolean  | false |
| menuFixed  | 操作菜单栏是否固定在左侧或者右侧<br>(可选值：left, right) | String  | ____ |

### Column Attributes

|  参数   | 说明  | 类型  | 默认值  |
|  ----  | ----  | ----  | ----  |
| label  | 列标题 | String  | ____ |
| prop  | 列字段 | String  | ____ |
| minWidth  | 对应列的最小宽度 | String  | ____ |
| sortable  | 对应列是否可以排序 | boolean  | false |
| fixed  | 列是否固定在左侧或者右侧<br>(可选值：left, right) | String  | ____ |
