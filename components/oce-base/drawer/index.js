import OceDrawer from './src/drawer';

OceDrawer.install = function(Vue) {
  Vue.component(OceDrawer.name, OceDrawer);
};

export default OceDrawer;
