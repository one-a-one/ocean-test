import OceSearch from './src/search';

OceSearch.install = function(Vue) {
  Vue.component(OceSearch.name, OceSearch);
};

export default OceSearch;
