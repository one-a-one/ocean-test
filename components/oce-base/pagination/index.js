import OcePagination from './src/pagination';

OcePagination.install = function(Vue) {
  Vue.component(OcePagination.name, OcePagination);
};

export default OcePagination;
