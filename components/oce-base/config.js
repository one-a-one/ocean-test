export default {
  size: 'mini',
  plain: true,
  search: {
    searchMenuSpan: 4, // 搜索框按钮栅列
    searchSpan: 6, // 搜索框栅列
  },
  toolbar: {
    addBtn: true,
    editBtn: false,
    delBtn: true,
    configLocalDisplay: true, // 表格配置栏显示方式
  },
}
