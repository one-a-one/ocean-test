import Search from './search/index'
// import Table from './table/index'
import Toolbar from './toolbar/index'
import Drawer from './drawer/index'
// import Grid from './grid/index'
// import Progress from './progress/index'
// import { vbtTable, vbtTableColumn, OceBigTable } from './bigTable/index'
// import { OceBigTable } from './bigTable/index'
import Pagination from './pagination/index'
// import GiantTree from './giantTree/index'
// import InputTree from './inputTree/index'

const components = [
  Search,
  // Table,
  Toolbar,
  Drawer,
  // Grid,
  // Progress,
  // vbtTable,
  // vbtTableColumn,
  // OceBigTable,
  Pagination,
  // GiantTree,
  // InputTree,
]

const install = function (Vue) {
  // 注册组件------------------------------------------
  components.forEach((component) => {
    Vue.component(component.name, component)
  })
}

// if (typeof window !== 'undefined' && window.Vue) {
//   install(window.Vue)
// }

const componentsObj = {}
components.forEach((component) => {
  componentsObj[component.name] = component
})

export default {
  version: '0.0.1',
  install,
  components: {
    ...componentsObj,
  },
}
