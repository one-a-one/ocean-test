import OceToolbar from './src/toolbar';

OceToolbar.install = function(Vue) {
  Vue.component(OceToolbar.name, OceToolbar);
};

export default OceToolbar;
