import OceDrawShapes from './src/drawShapes';

OceDrawShapes.install = function(Vue) {
  Vue.component(OceDrawShapes.name, OceDrawShapes);
};

export default OceDrawShapes;