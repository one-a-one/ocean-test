import OceCluster from './src/cluster';

OceCluster.install = function(Vue) {
  Vue.component(OceCluster.name, OceCluster);
};

export default OceCluster;
