/* eslint-disable no-undef */
// 异步加载高德地图
export default function loadBMap(ak) {
  return new Promise(function (resolve, reject) {
    if (typeof AMap !== 'undefined') {
      resolve(AMap)
      return true
    }
    window.onLoad  = function () {
      resolve(AMap)
    }
    let script = document.createElement('script')
    script.type = 'text/javascript'
    script.src = 'https://webapi.amap.com/maps?v=2.0&key=' + ak + '&callback=onLoad&plugin=AMap.MouseTool,AMap.Geocoder'
    // script.async = 'async'
    script.onerror = reject
    document.head.appendChild(script)
  })
}
