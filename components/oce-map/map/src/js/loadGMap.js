// 异步加载google地图
export default function loadBMap(ak) {
  return new Promise(function (resolve, reject) {
    if (typeof google !== 'undefined') {
      resolve(google)
      return true
    }
    window.initMap  = function () {
      resolve(google)
    }
    let script = document.createElement('script')
    script.type = 'text/javascript'
    script.src = 'https://maps.googleapis.com/maps/api/js?key=' + ak + '&callback=initMap&libraries=&v=weekly'
    script.defer = true
    script.onerror = reject
    document.head.appendChild(script)
  })
}
