import OceMap from './src/map';

OceMap.install = function(Vue) {
  Vue.component(OceMap.name, OceMap);
};

export default OceMap;
