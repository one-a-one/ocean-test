/**
 *
 * Using [data URI] to build worker URL,
 * mdn:https://developer.mozilla.org/zh-CN/docs/Web/API/Worker/Worker
 * mdn Note:浏览器厂商对于 data URI 是否同源存在分歧。尽管 Gecko 10.0 (Firefox 10.0 / Thunderbird 10.0 / SeaMonkey 2.7)
 * 和之后的版本接受 data URIs，但在所有其他浏览器中并非如此。
 *
 * @param fn
 * @constructor
 */
const localWorker = function (fn, options) {
  let op = options || {}
  if (!fn) throw new Error('fn not be null')
  if (Object.prototype.toString.call(fn) !== '[object Function]') throw new Error('fn mu')
  let url = (window.URL||webkitURL).createObjectURL(new Blob([`(${fn.toString()})()`], {type:"text/javascript"}));
  const worker = new Worker(url, op);
  (window.URL||webkitURL).revokeObjectURL(url)
  return worker
}

localWorker.prototype.getWorker = function () {
  return localWorker()
}

export default {localWorker}
