import coordinateTranslate from './utils/coordinateTranslate'
import olUtil from './utils/olutil'
import localWorker from './utils/localWorker'

import Map from './map/index'
import Cluster from './myMap/cluster/index'
import DrawShapes from './myMap/drawShapes/index'

const components = [
  Map,
  Cluster,
  DrawShapes,
]
let ocean = {
  ...coordinateTranslate,
  olUtil,
  ...localWorker,
}

// 加载百度地图插件------------------------------------------------------
// let libs = [
//   'http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js'
// ]
// const head = document.getElementsByTagName('head')[0]

// let count = 0

// function loadLib(src) {
//   const lib = document.createElement('script');
//   lib.setAttribute('type', 'text/javascript');
//   lib.setAttribute('src', src);
//   lib.onerror = function () {
//     window.console.log('百度地图插件加载失败.');
//   }
//   lib.onload = function () {
//     count += 1;
//     if (count === libs.length) { // libs全部加载成功才返回
//       window.console.log('百度地图加载成功.')
//     }
//   }
//   head.appendChild(lib);
// }

// install------------------------------------------------------------
const install = function (Vue, opts = {}) {
  const { AMapAK, BMapAK } = opts
  ocean = Object.assign({
    AMapAK,
    BMapAK
  },ocean)
  if(!Vue.prototype.$ocean) Vue.prototype.$ocean = {}
  for (let key in ocean) {
    Vue.prototype.$ocean[key] = ocean[key]
  }
  // Vue.prototype.$ocean = ocean
  
  // // 加载百度地图----------------------------------
  // if (window.BMap && window.BMapLib) {
  //   // 已经加载则直接返回
  //   window.console.log('已加载百度地图')
  // } else if (BMapAK) {
  //   const bmap = document.createElement('script');
  //   bmap.type = 'text/javascript';
  //   bmap.src = 'http://api.map.baidu.com/api?v=3.0&ak=' + BMapAK + '&callback=MapLoadSuccess';
  //   head.appendChild(bmap);
  //   window.MapLoadSuccess = function () {
  //     // BMap加载完成，开始加载libs
  //     libs.forEach(lib => {
  //       loadLib(lib)
  //     })
  //   }
  //   bmap.onerror = function () {
  //     window.console.log('百度地图加载失败.');
  //   }
  // }

  // // 加载高德地图------------------------------------
  // if (typeof AMap !== 'undefined') {
  //   window.console.log('已加载高德地图')
  //   return true
  // } else if (AMapAK)  {
  //   window.onLoad  = function () {
  //     window.console.log('高德地图加载成功.')
  //   }
  //   let script = document.createElement('script')
  //   script.type = 'text/javascript'
  //   script.src = 'https://webapi.amap.com/maps?v=2.0&key=' + AMapAK + '&callback=onLoad&plugin=AMap.MouseTool,AMap.Geocoder'
  //   script.onerror = function () {
  //     window.console.log('高德地图加载失败.');
  //   }
  //   document.head.appendChild(script)
  // }

  // 注册组件------------------------------------------
  components.forEach(component => {
    Vue.component(component.name, component)
  })
}

// if (typeof window !== 'undefined' && window.Vue && !window.Vue.prototype.$ocean) {
//   install(window.Vue)
// }

let componentsObj = {}
components.forEach(component => {
  componentsObj[component.name] = component
})

export default {
  version: '0.0.1',
  install,
  components: {
    ...componentsObj
  }
}
