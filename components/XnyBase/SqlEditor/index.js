import XnySqlEditor from './src/SqlEditor.vue'

XnySqlEditor.install = function (Vue) {
  Vue.component(XnySqlEditor.name, XnySqlEditor)
}

export default XnySqlEditor
