import XnyGeneralTest from './src/Test.vue'

XnyGeneralTest.install = function (Vue) {
  Vue.component(XnyGeneralTest.name, XnyGeneralTest)
}

export default XnyGeneralTest
