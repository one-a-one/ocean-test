export default {
  searchForm: {
    IntScope: {
      mapComponent: 'XnyNumberPicker',
    },
    DoubleScope: {
      mapComponent: 'XnyNumberPicker',
    },
  },
  avueForm: {
    MultiLike: {
      type: 'textarea',
      minRows: 1,
      maxRows: 4,
    },
    SingleSelection: {
      type: 'select',
      filterable: true,
    },
    MultiSelection: {
      type: 'select',
      filterable: true,
      multiple: true,
    },
    // 日期选择器
    DateScope: {
      type: 'daterange',
      format: 'yyyy-MM-dd',
      valueFormat: 'yyyy-MM-dd',
      startPlaceholder: '日期开始范围自定义',
      endPlaceholder: '日期结束范围自定义',
      dataType: 'string',
    },
    // 时间日期选择器
    TimeScope: {
      type: 'datetimerange',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'yyyy-MM-dd HH:mm:ss',
      startPlaceholder: '时间日期开始范围自定义',
      endPlaceholder: '时间日期结束范围自定义',
      dataType: 'string',
    },
  },
}
