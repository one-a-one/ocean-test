export default {
  code: 200,
  msg: '操作成功',
  data: {
    tableName: 'testTable',
    tabList: [
      {
        tableName: 'testTable',
        tabName: 'test_tab',
        totalRowCount: 11,
        rowCountPerPage: 10,
        pageIndex: 1,
        pageCount: 2,
        rowList: [
          {
            rowKey: '[id=1]',
            fieldList: [
              {
                fieldName: 'name',
                textValue: '张三1',
                displayText: '张三1',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '1',
                displayText: '1',
                hrefValue: null,
              },
              {
                fieldName: 'age',
                textValue: '11',
                displayText: '11',
                hrefValue: null,
              },
            ],
          },
          {
            rowKey: '[id=2]',
            fieldList: [
              {
                fieldName: 'name',
                textValue: '张三2',
                displayText: '张三2',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '2',
                displayText: '2',
                hrefValue: null,
              },
              {
                fieldName: 'age',
                textValue: '11',
                displayText: '11',
                hrefValue: null,
              },
            ],
          },
          {
            rowKey: '[id=3]',
            fieldList: [
              {
                fieldName: 'name',
                textValue: '张三3',
                displayText: '张三3',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '3',
                displayText: '3',
                hrefValue: null,
              },
              {
                fieldName: 'age',
                textValue: '11',
                displayText: '11',
                hrefValue: null,
              },
            ],
          },
          {
            rowKey: '[id=4]',
            fieldList: [
              {
                fieldName: 'name',
                textValue: '张三4',
                displayText: '张三4',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '4',
                displayText: '4',
                hrefValue: null,
              },
              {
                fieldName: 'age',
                textValue: '3',
                displayText: '3',
                hrefValue: null,
              },
            ],
          },
          {
            rowKey: '[id=5]',
            fieldList: [
              {
                fieldName: 'name',
                textValue: '李四1',
                displayText: '李四1',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '5',
                displayText: '5',
                hrefValue: null,
              },
              {
                fieldName: 'age',
                textValue: '22',
                displayText: '22',
                hrefValue: null,
              },
            ],
          },
          {
            rowKey: '[id=6]',
            fieldList: [
              {
                fieldName: 'name',
                textValue: '李四2',
                displayText: '李四2',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '6',
                displayText: '6',
                hrefValue: null,
              },
              {
                fieldName: 'age',
                textValue: '22',
                displayText: '22',
                hrefValue: null,
              },
            ],
          },
          {
            rowKey: '[id=7]',
            fieldList: [
              {
                fieldName: 'name',
                textValue: '李四3',
                displayText: '李四3',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '7',
                displayText: '7',
                hrefValue: null,
              },
              {
                fieldName: 'age',
                textValue: '3',
                displayText: '3',
                hrefValue: null,
              },
            ],
          },
          {
            rowKey: '[id=8]',
            fieldList: [
              {
                fieldName: 'name',
                textValue: '李四4',
                displayText: '李四4',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '8',
                displayText: '8',
                hrefValue: null,
              },
              {
                fieldName: 'age',
                textValue: '44',
                displayText: '44',
                hrefValue: null,
              },
            ],
          },
          {
            rowKey: '[id=9]',
            fieldList: [
              {
                fieldName: 'name',
                textValue: '王二1',
                displayText: '王二1',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '9',
                displayText: '9',
                hrefValue: null,
              },
              {
                fieldName: 'age',
                textValue: '44',
                displayText: '44',
                hrefValue: null,
              },
            ],
          },
          {
            rowKey: '[id=10]',
            fieldList: [
              {
                fieldName: 'name',
                textValue: '王二2',
                displayText: '王二2',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '10',
                displayText: '10',
                hrefValue: null,
              },
              {
                fieldName: 'age',
                textValue: '44',
                displayText: '44',
                hrefValue: null,
              },
            ],
          },
        ],
      },
      {
        tableName: 'testTable',
        tabName: 'test_tab2',
        totalRowCount: 4,
        rowCountPerPage: 30,
        pageIndex: 1,
        pageCount: 1,
        rowList: [
          {
            rowKey: '[id=1]',
            fieldList: [
              {
                fieldName: 'a',
                textValue: 'G216000230',
                displayText: 'G216000230',
                hrefValue: null,
              },
              {
                fieldName: 'b',
                textValue: 'GD0001',
                displayText: 'GD0001',
                hrefValue: null,
              },
              {
                fieldName: 'c',
                textValue: '111214LALC9A98',
                displayText: '111214LALC9A98',
                hrefValue: null,
              },
              {
                fieldName: 'd',
                textValue: '5',
                displayText: '5',
                hrefValue: null,
              },
              {
                fieldName: 'e',
                textValue: 'LA',
                displayText: 'LA',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '1',
                displayText: '1',
                hrefValue: null,
              },
            ],
          },
          {
            rowKey: '[id=2]',
            fieldList: [
              {
                fieldName: 'a',
                textValue: 'G216000231',
                displayText: 'G216000231',
                hrefValue: null,
              },
              {
                fieldName: 'b',
                textValue: 'GD0002',
                displayText: 'GD0002',
                hrefValue: null,
              },
              {
                fieldName: 'c',
                textValue: '111214LALC9A98',
                displayText: '111214LALC9A98',
                hrefValue: null,
              },
              {
                fieldName: 'd',
                textValue: '5',
                displayText: '5',
                hrefValue: null,
              },
              {
                fieldName: 'e',
                textValue: 'LC',
                displayText: 'LC',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '2',
                displayText: '2',
                hrefValue: null,
              },
            ],
          },
          {
            rowKey: '[id=3]',
            fieldList: [
              {
                fieldName: 'a',
                textValue: 'G216000232',
                displayText: 'G216000232',
                hrefValue: null,
              },
              {
                fieldName: 'b',
                textValue: 'GD0003',
                displayText: 'GD0003',
                hrefValue: null,
              },
              {
                fieldName: 'c',
                textValue: '111214LALC9A98',
                displayText: '111214LALC9A98',
                hrefValue: null,
              },
              {
                fieldName: 'd',
                textValue: '10',
                displayText: '10',
                hrefValue: null,
              },
              {
                fieldName: 'e',
                textValue: 'LA',
                displayText: 'LA',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '3',
                displayText: '3',
                hrefValue: null,
              },
            ],
          },
          {
            rowKey: '[id=4]',
            fieldList: [
              {
                fieldName: 'a',
                textValue: 'G216000233',
                displayText: 'G216000233',
                hrefValue: null,
              },
              {
                fieldName: 'b',
                textValue: 'GD0004',
                displayText: 'GD0004',
                hrefValue: null,
              },
              {
                fieldName: 'c',
                textValue: '111214LALC9A98',
                displayText: '111214LALC9A98',
                hrefValue: null,
              },
              {
                fieldName: 'd',
                textValue: '10',
                displayText: '10',
                hrefValue: null,
              },
              {
                fieldName: 'e',
                textValue: 'LC',
                displayText: 'LC',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '4',
                displayText: '4',
                hrefValue: null,
              },
            ],
          },
        ],
      },
      {
        tableName: 'testTable',
        tabName: 'test_tab3',
        totalRowCount: 2,
        rowCountPerPage: 30,
        pageIndex: 1,
        pageCount: 1,
        rowList: [
          {
            rowKey: '[id=1]',
            fieldList: [
              {
                fieldName: 'address',
                textValue: '中国',
                displayText: '中国',
                hrefValue: null,
              },
              {
                fieldName: 'name',
                textValue: '李四',
                displayText: '李四',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '1',
                displayText: '1',
                hrefValue: null,
              },
            ],
          },
          {
            rowKey: '[id=2]',
            fieldList: [
              {
                fieldName: 'address',
                textValue: '日本',
                displayText: '日本',
                hrefValue: null,
              },
              {
                fieldName: 'name',
                textValue: '王五',
                displayText: '王五',
                hrefValue: null,
              },
              {
                fieldName: 'id',
                textValue: '2',
                displayText: '2',
                hrefValue: null,
              },
            ],
          },
        ],
      },
    ],
  },
}
