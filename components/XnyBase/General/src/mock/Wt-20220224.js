export default {
  code: 200,
  msg: '操作成功',
  data: {
    buttonList: [],
    dictList: [
      {
        dictCode: 'tcm_prd_class',
        dictName: '产品大类',
        valueList: [
          {
            valueCode: 'LOC',
            valueName: '钴酸锂',
          },
          {
            valueCode: 'NCM',
            valueName: '三元材料',
          },
          {
            valueCode: 'SYHSG',
            valueName: '四氧化三钴',
          },
          {
            valueCode: 'NCMQQ',
            valueName: '三元前驱',
          },
        ],
      },
      {
        dictCode: 'tcm_step_code',
        dictName: '工序',
        valueList: [
          {
            valueCode: '1',
            valueName: '1',
          },
          {
            valueCode: 'HL0',
            valueName: '零次混料',
          },
          {
            valueCode: 'SJ0',
            valueName: '零次烧结',
          },
          {
            valueCode: 'FS0',
            valueName: '零次制粉',
          },
          {
            valueCode: 'DHL',
            valueName: '大粒度一次混料',
          },
          {
            valueCode: 'DSJ',
            valueName: '大粒度一次烧结',
          },
          {
            valueCode: 'DQF',
            valueName: '大粒度一次制粉',
          },
          {
            valueCode: 'XHL',
            valueName: '小粒度一次混料',
          },
          {
            valueCode: 'XSJ',
            valueName: '小粒度一次烧结',
          },
          {
            valueCode: 'XQF',
            valueName: '小粒度一次制粉',
          },
          {
            valueCode: 'HL1',
            valueName: '一次混料',
          },
          {
            valueCode: 'SJ1',
            valueName: '一次烧结',
          },
          {
            valueCode: 'FS1',
            valueName: '一次制粉',
          },
          {
            valueCode: 'HL2',
            valueName: '二次混料',
          },
          {
            valueCode: 'SJ2',
            valueName: '二次烧结',
          },
          {
            valueCode: 'FS2',
            valueName: '二次制粉',
          },
          {
            valueCode: 'HL3',
            valueName: '三次混料',
          },
          {
            valueCode: 'SJ3',
            valueName: '三次烧结',
          },
          {
            valueCode: 'FS3',
            valueName: '三次制粉',
          },
          {
            valueCode: 'HPL',
            valueName: '合批',
          },
          {
            valueCode: 'CTL',
            valueName: '除铁',
          },
          {
            valueCode: 'BZL',
            valueName: '包装',
          },
          {
            valueCode: 'SXL',
            valueName: '水洗',
          },
          {
            valueCode: 'SCT',
            valueName: '湿法除铁',
          },
          {
            valueCode: 'QPA',
            valueName: '氨水配料',
          },
          {
            valueCode: 'QPC',
            valueName: '氯化钴配料',
          },
          {
            valueCode: 'QPG',
            valueName: '钴盐配料',
          },
          {
            valueCode: 'QPT',
            valueName: '碳酸氢铵配料',
          },
          {
            valueCode: 'QPS',
            valueName: '三元料液配料',
          },
          {
            valueCode: 'QPX',
            valueName: '稀碱配料',
          },
          {
            valueCode: 'QPM',
            valueName: '硫酸镁添加剂配料',
          },
          {
            valueCode: 'QPL',
            valueName: '硫酸铝添加剂配料',
          },
          {
            valueCode: 'QCA',
            valueName: '氨水储料',
          },
          {
            valueCode: 'QCC',
            valueName: '氯化钴储料',
          },
          {
            valueCode: 'QCG',
            valueName: '钴盐储料',
          },
          {
            valueCode: 'QCJ',
            valueName: '液碱储料',
          },
          {
            valueCode: 'QCS',
            valueName: '三元料液储料',
          },
          {
            valueCode: 'QCX',
            valueName: '稀碱储料',
          },
          {
            valueCode: 'QTC',
            valueName: '碳酸氢铵储料',
          },
          {
            valueCode: 'QCL',
            valueName: '硫酸铝添加剂储料',
          },
          {
            valueCode: 'QCM',
            valueName: '硫酸镁添加剂储料',
          },
          {
            valueCode: 'QCD',
            valueName: '晶种沉淀',
          },
          {
            valueCode: 'QGY',
            valueName: '钴盐长大沉淀',
          },
          {
            valueCode: 'QSY',
            valueName: '三元长大沉淀',
          },
          {
            valueCode: 'QCH',
            valueName: '陈化',
          },
          {
            valueCode: 'QXD',
            valueName: '洗涤',
          },
          {
            valueCode: 'QHG',
            valueName: '烘干',
          },
          {
            valueCode: 'QDS',
            valueName: '煅烧',
          },
          {
            valueCode: 'QCT',
            valueName: '除铁',
          },
          {
            valueCode: 'QHP',
            valueName: '合批',
          },
          {
            valueCode: 'QBZ',
            valueName: '包装',
          },
          {
            valueCode: 'QMY',
            valueName: '母液处理',
          },
          {
            valueCode: 'QSR',
            valueName: '酸溶',
          },
          {
            valueCode: 'QSN',
            valueName: '室内',
          },
          {
            valueCode: 'QSW',
            valueName: '室外',
          },
        ],
      },
      {
        dictCode: 'tcm_wb_code',
        dictName: '基地编码',
        valueList: [
          {
            valueCode: 'WX',
            valueName: '外协基地',
          },
          {
            valueCode: 'HC',
            valueName: '海沧基地',
          },
          {
            valueCode: 'HJ',
            valueName: '海璟基地',
          },
          {
            valueCode: 'SM',
            valueName: '三明基地',
          },
          {
            valueCode: 'ND',
            valueName: '宁德基地',
          },
          {
            valueCode: 'QQ',
            valueName: '前驱基地',
          },
        ],
      },
      {
        dictCode: 'tcm_ws_code',
        dictName: '车间编码',
        valueList: [
          {
            valueCode: 'QQ1',
            valueName: '海沧前驱体一车间',
          },
          {
            valueCode: 'QQ2',
            valueName: '海沧前驱体二车间',
          },
          {
            valueCode: 'QQ3',
            valueName: '海沧前驱体三车间',
          },
          {
            valueCode: 'HCA',
            valueName: '海沧A车间',
          },
          {
            valueCode: 'HCB',
            valueName: '海沧B车间',
          },
          {
            valueCode: 'HCC',
            valueName: '海沧C车间',
          },
          {
            valueCode: 'SMA',
            valueName: '三明A车间',
          },
          {
            valueCode: 'SMB',
            valueName: '三明B车间',
          },
          {
            valueCode: 'SMC',
            valueName: '三明C车间',
          },
          {
            valueCode: 'NDA',
            valueName: '宁德A车间',
          },
          {
            valueCode: 'NDB',
            valueName: '宁德B车间',
          },
          {
            valueCode: 'NDC',
            valueName: '宁德C车间',
          },
          {
            valueCode: 'HJA',
            valueName: '海璟A车间',
          },
          {
            valueCode: 'HJB',
            valueName: '海璟B车间',
          },
          {
            valueCode: 'HJC',
            valueName: '海璟C车间',
          },
          {
            valueCode: 'WMD',
            valueName: '明德外协',
          },
          {
            valueCode: 'WCT',
            valueName: '昌泰外协',
          },
        ],
      },
      {
        dictCode: 'tpg_cap_mode_code',
        dictName: '设备能力模式',
        valueList: [
          {
            valueCode: 'PCGD',
            valueName: '批次固定',
          },
          {
            valueCode: 'PCLXG',
            valueName: '批次量相关',
          },
        ],
      },
      {
        dictCode: 'tpg_fac_class',
        dictName: '设备类型',
        valueList: [
          {
            valueCode: 'HLJ',
            valueName: '混料机',
          },
          {
            valueCode: 'CHC',
            valueName: '陈化槽',
          },
          {
            valueCode: 'LXJ',
            valueName: '离心机',
          },
          {
            valueCode: 'HZY',
            valueName: '回转窑',
          },
          {
            valueCode: 'SJL',
            valueName: '烧结炉',
          },
          {
            valueCode: 'FSJ',
            valueName: '粉碎机',
          },
          {
            valueCode: 'HPJ',
            valueName: '合批机',
          },
          {
            valueCode: 'CTJ',
            valueName: '除铁机',
          },
          {
            valueCode: 'BZJ',
            valueName: '包装机',
          },
          {
            valueCode: 'PZC',
            valueName: '配制槽',
          },
          {
            valueCode: 'CLG',
            valueName: '储料罐',
          },
          {
            valueCode: 'FYF',
            valueName: '反应釜',
          },
        ],
      },
      {
        dictCode: 'tpg_mat_form',
        dictName: '物料形态',
        valueList: [
          {
            valueCode: 'GT',
            valueName: '固态',
          },
          {
            valueCode: 'YT',
            valueName: '液态',
          },
        ],
      },
    ],
    filterList: [],
    hasBlockSearch: true,
    linkColumnList: [],
    tabList: [
      {
        buttonList: [],
        fieldList: [
          {
            dictCode: 'tcm_wb_code',
            fieldDisplayName: '基地编码',
            fieldName: 'wb_code',
            fieldNameCamel: 'wbCode',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            dictCode: 'tcm_ws_code',
            fieldDisplayName: '车间编码',
            fieldName: 'ws_code',
            fieldNameCamel: 'wsCode',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            dictCode: 'tcm_prd_class',
            fieldDisplayName: '产品大类',
            fieldName: 'prd_class_code',
            fieldNameCamel: 'prdClassCode',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            dictCode: 'tcm_step_code',
            fieldDisplayName: '工序编码',
            fieldName: 'step_code',
            fieldNameCamel: 'stepCode',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            dictCode: 'tpg_fac_class',
            fieldDisplayName: '设备类型',
            fieldName: 'fac_class',
            fieldNameCamel: 'facClass',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '顺序号',
            fieldName: 'serial_no',
            fieldNameCamel: 'serialNo',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            dictCode: 'tpg_cap_mode_code',
            fieldDisplayName: '能力模式',
            fieldName: 'cap_mode_code',
            fieldNameCamel: 'capModeCode',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            dictCode: 'tpg_mat_form',
            fieldDisplayName: '产出物料形态',
            fieldName: 'mat_form',
            fieldNameCamel: 'matForm',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '默认批次值',
            fieldName: 'batch_val',
            fieldNameCamel: 'batchVal',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '默认准备时间',
            fieldName: 'time_prepare',
            fieldNameCamel: 'timePrepare',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '默认占用时间',
            fieldName: 'time_occupy',
            fieldNameCamel: 'timeOccupy',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '默认批次耗时',
            fieldName: 'time_batch',
            fieldNameCamel: 'timeBatch',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '默认通过时间',
            fieldName: 'time_pass',
            fieldNameCamel: 'timePass',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '基地名称',
            fieldName: 'wb_name',
            fieldNameCamel: 'wbName',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '车间名称',
            fieldName: 'ws_name',
            fieldNameCamel: 'wsName',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '产品大类名称',
            fieldName: 'prd_class_name',
            fieldNameCamel: 'prdClassName',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '工序名称',
            fieldName: 'step_name',
            fieldNameCamel: 'stepName',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '设备类型名称',
            fieldName: 'fac_class_name',
            fieldNameCamel: 'facClassName',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '备注',
            fieldName: 'remark',
            fieldNameCamel: 'remark',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
        ],
        filterList: [
          {
            defaultValueList: [],
            filterDisplayName: '产品大类',
            filterId: 11610101,
            filterType: 'MultiSelection',
            heightPixelInput: 60,
            heightPixelLabel: 60,
            optionList: [
              {
                optionDisp: '钴酸锂',
                optionValue: 'LOC',
              },
              {
                optionDisp: '三元材料',
                optionValue: 'NCM',
              },
              {
                optionDisp: '四氧化三钴',
                optionValue: 'SYHSG',
              },
              {
                optionDisp: '三元前驱',
                optionValue: 'NCMQQ',
              },
            ],
            widthPixelInput: 100,
            widthPixelLabel: 100,
          },
          {
            defaultValueList: [],
            filterDisplayName: '工序',
            filterId: 11610102,
            filterType: 'MultiSelection',
            heightPixelInput: 60,
            heightPixelLabel: 60,
            optionList: [
              {
                optionDisp: '1',
                optionValue: '1',
              },
              {
                optionDisp: '零次混料',
                optionValue: 'HL0',
              },
              {
                optionDisp: '零次烧结',
                optionValue: 'SJ0',
              },
              {
                optionDisp: '零次制粉',
                optionValue: 'FS0',
              },
              {
                optionDisp: '大粒度一次混料',
                optionValue: 'DHL',
              },
              {
                optionDisp: '大粒度一次烧结',
                optionValue: 'DSJ',
              },
              {
                optionDisp: '大粒度一次制粉',
                optionValue: 'DQF',
              },
              {
                optionDisp: '小粒度一次混料',
                optionValue: 'XHL',
              },
              {
                optionDisp: '小粒度一次烧结',
                optionValue: 'XSJ',
              },
              {
                optionDisp: '小粒度一次制粉',
                optionValue: 'XQF',
              },
              {
                optionDisp: '一次混料',
                optionValue: 'HL1',
              },
              {
                optionDisp: '一次烧结',
                optionValue: 'SJ1',
              },
              {
                optionDisp: '一次制粉',
                optionValue: 'FS1',
              },
              {
                optionDisp: '二次混料',
                optionValue: 'HL2',
              },
              {
                optionDisp: '二次烧结',
                optionValue: 'SJ2',
              },
              {
                optionDisp: '二次制粉',
                optionValue: 'FS2',
              },
              {
                optionDisp: '三次混料',
                optionValue: 'HL3',
              },
              {
                optionDisp: '三次烧结',
                optionValue: 'SJ3',
              },
              {
                optionDisp: '三次制粉',
                optionValue: 'FS3',
              },
              {
                optionDisp: '合批',
                optionValue: 'HPL',
              },
              {
                optionDisp: '除铁',
                optionValue: 'CTL',
              },
              {
                optionDisp: '包装',
                optionValue: 'BZL',
              },
              {
                optionDisp: '水洗',
                optionValue: 'SXL',
              },
              {
                optionDisp: '湿法除铁',
                optionValue: 'SCT',
              },
              {
                optionDisp: '氨水配料',
                optionValue: 'QPA',
              },
              {
                optionDisp: '氯化钴配料',
                optionValue: 'QPC',
              },
              {
                optionDisp: '钴盐配料',
                optionValue: 'QPG',
              },
              {
                optionDisp: '碳酸氢铵配料',
                optionValue: 'QPT',
              },
              {
                optionDisp: '三元料液配料',
                optionValue: 'QPS',
              },
              {
                optionDisp: '稀碱配料',
                optionValue: 'QPX',
              },
              {
                optionDisp: '硫酸镁添加剂配料',
                optionValue: 'QPM',
              },
              {
                optionDisp: '硫酸铝添加剂配料',
                optionValue: 'QPL',
              },
              {
                optionDisp: '氨水储料',
                optionValue: 'QCA',
              },
              {
                optionDisp: '氯化钴储料',
                optionValue: 'QCC',
              },
              {
                optionDisp: '钴盐储料',
                optionValue: 'QCG',
              },
              {
                optionDisp: '液碱储料',
                optionValue: 'QCJ',
              },
              {
                optionDisp: '三元料液储料',
                optionValue: 'QCS',
              },
              {
                optionDisp: '稀碱储料',
                optionValue: 'QCX',
              },
              {
                optionDisp: '碳酸氢铵储料',
                optionValue: 'QTC',
              },
              {
                optionDisp: '硫酸铝添加剂储料',
                optionValue: 'QCL',
              },
              {
                optionDisp: '硫酸镁添加剂储料',
                optionValue: 'QCM',
              },
              {
                optionDisp: '晶种沉淀',
                optionValue: 'QCD',
              },
              {
                optionDisp: '钴盐长大沉淀',
                optionValue: 'QGY',
              },
              {
                optionDisp: '三元长大沉淀',
                optionValue: 'QSY',
              },
              {
                optionDisp: '陈化',
                optionValue: 'QCH',
              },
              {
                optionDisp: '洗涤',
                optionValue: 'QXD',
              },
              {
                optionDisp: '烘干',
                optionValue: 'QHG',
              },
              {
                optionDisp: '煅烧',
                optionValue: 'QDS',
              },
              {
                optionDisp: '除铁',
                optionValue: 'QCT',
              },
              {
                optionDisp: '合批',
                optionValue: 'QHP',
              },
              {
                optionDisp: '包装',
                optionValue: 'QBZ',
              },
              {
                optionDisp: '母液处理',
                optionValue: 'QMY',
              },
              {
                optionDisp: '酸溶',
                optionValue: 'QSR',
              },
              {
                optionDisp: '室内',
                optionValue: 'QSN',
              },
              {
                optionDisp: '室外',
                optionValue: 'QSW',
              },
            ],
            widthPixelInput: 100,
            widthPixelLabel: 100,
          },
          {
            defaultValueList: [],
            filterDisplayName: '设备类型',
            filterId: 11610103,
            filterType: 'MultiSelection',
            heightPixelInput: 60,
            heightPixelLabel: 60,
            optionList: [
              {
                optionDisp: '混料机',
                optionValue: 'HLJ',
              },
              {
                optionDisp: '陈化槽',
                optionValue: 'CHC',
              },
              {
                optionDisp: '离心机',
                optionValue: 'LXJ',
              },
              {
                optionDisp: '回转窑',
                optionValue: 'HZY',
              },
              {
                optionDisp: '烧结炉',
                optionValue: 'SJL',
              },
              {
                optionDisp: '粉碎机',
                optionValue: 'FSJ',
              },
              {
                optionDisp: '合批机',
                optionValue: 'HPJ',
              },
              {
                optionDisp: '除铁机',
                optionValue: 'CTJ',
              },
              {
                optionDisp: '包装机',
                optionValue: 'BZJ',
              },
              {
                optionDisp: '配制槽',
                optionValue: 'PZC',
              },
              {
                optionDisp: '储料罐',
                optionValue: 'CLG',
              },
              {
                optionDisp: '反应釜',
                optionValue: 'FYF',
              },
            ],
            widthPixelInput: 100,
            widthPixelLabel: 100,
          },
        ],
        hasBlockPage: true,
        hasBlockSelect: true,
        linkedList: [],
        radioFlag: false,
        rowCountPerPage: 20,
        rowCountPerPageList: [10, 20, 30, 50, 100],
        tabDisplayName: '能力批次定义表',
        tabName: 'pgFacCapacityA01',
        tabType: '1',
      },
      {
        buttonList: [],
        fieldList: [
          {
            dictCode: 'tcm_wb_code',
            fieldDisplayName: '基地编码',
            fieldName: 'wb_code',
            fieldNameCamel: 'wbCode',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            dictCode: 'tcm_ws_code',
            fieldDisplayName: '车间编码',
            fieldName: 'ws_code',
            fieldNameCamel: 'wsCode',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            dictCode: 'tcm_prd_class',
            fieldDisplayName: '产品大类',
            fieldName: 'prd_class_code',
            fieldNameCamel: 'prdClassCode',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            dictCode: 'tcm_step_code',
            fieldDisplayName: '工序编码',
            fieldName: 'step_code',
            fieldNameCamel: 'stepCode',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            dictCode: 'tpg_fac_class',
            fieldDisplayName: '设备类型',
            fieldName: 'fac_class',
            fieldNameCamel: 'facClass',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '明细ID',
            fieldName: 'detail_id',
            fieldNameCamel: 'detailId',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '设备列表',
            fieldName: 'fac_code_list',
            fieldNameCamel: 'facCodeList',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '路线列表',
            fieldName: 'route_code_list',
            fieldNameCamel: 'routeCodeList',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '产品列表',
            fieldName: 'mat_code_list',
            fieldNameCamel: 'matCodeList',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '质量维列表',
            fieldName: 'qdc_list',
            fieldNameCamel: 'qdcList',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '配方码列表',
            fieldName: 'bom_code_list',
            fieldNameCamel: 'bomCodeList',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '配方值矩阵号列表',
            fieldName: 'matrix_no_list',
            fieldNameCamel: 'matrixNoList',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '顺序号',
            fieldName: 'serial_no',
            fieldNameCamel: 'serialNo',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '批次值',
            fieldName: 'batch_val',
            fieldNameCamel: 'batchVal',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '准备时间',
            fieldName: 'time_prepare',
            fieldNameCamel: 'timePrepare',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '占用时间',
            fieldName: 'time_occupy',
            fieldNameCamel: 'timeOccupy',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '批次耗时',
            fieldName: 'time_batch',
            fieldNameCamel: 'timeBatch',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '通过时间',
            fieldName: 'time_pass',
            fieldNameCamel: 'timePass',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
          {
            fieldDisplayName: '备注',
            fieldName: 'remark',
            fieldNameCamel: 'remark',
            fieldType: 'ColumnData',
            frozenFlag: false,
            hiddenFlag: false,
            hrefFlag: false,
            widthPixel: 150,
          },
        ],
        filterList: [
          {
            defaultValueList: [],
            filterDisplayName: '产品大类',
            filterId: 11610201,
            filterType: 'MultiSelection',
            heightPixelInput: 60,
            heightPixelLabel: 60,
            optionList: [
              {
                optionDisp: '钴酸锂',
                optionValue: 'LOC',
              },
              {
                optionDisp: '三元材料',
                optionValue: 'NCM',
              },
              {
                optionDisp: '四氧化三钴',
                optionValue: 'SYHSG',
              },
              {
                optionDisp: '三元前驱',
                optionValue: 'NCMQQ',
              },
            ],
            widthPixelInput: 100,
            widthPixelLabel: 100,
          },
          {
            defaultValueList: [],
            filterDisplayName: '工序',
            filterId: 11610202,
            filterType: 'MultiSelection',
            heightPixelInput: 60,
            heightPixelLabel: 60,
            optionList: [
              {
                optionDisp: '1',
                optionValue: '1',
              },
              {
                optionDisp: '零次混料',
                optionValue: 'HL0',
              },
              {
                optionDisp: '零次烧结',
                optionValue: 'SJ0',
              },
              {
                optionDisp: '零次制粉',
                optionValue: 'FS0',
              },
              {
                optionDisp: '大粒度一次混料',
                optionValue: 'DHL',
              },
              {
                optionDisp: '大粒度一次烧结',
                optionValue: 'DSJ',
              },
              {
                optionDisp: '大粒度一次制粉',
                optionValue: 'DQF',
              },
              {
                optionDisp: '小粒度一次混料',
                optionValue: 'XHL',
              },
              {
                optionDisp: '小粒度一次烧结',
                optionValue: 'XSJ',
              },
              {
                optionDisp: '小粒度一次制粉',
                optionValue: 'XQF',
              },
              {
                optionDisp: '一次混料',
                optionValue: 'HL1',
              },
              {
                optionDisp: '一次烧结',
                optionValue: 'SJ1',
              },
              {
                optionDisp: '一次制粉',
                optionValue: 'FS1',
              },
              {
                optionDisp: '二次混料',
                optionValue: 'HL2',
              },
              {
                optionDisp: '二次烧结',
                optionValue: 'SJ2',
              },
              {
                optionDisp: '二次制粉',
                optionValue: 'FS2',
              },
              {
                optionDisp: '三次混料',
                optionValue: 'HL3',
              },
              {
                optionDisp: '三次烧结',
                optionValue: 'SJ3',
              },
              {
                optionDisp: '三次制粉',
                optionValue: 'FS3',
              },
              {
                optionDisp: '合批',
                optionValue: 'HPL',
              },
              {
                optionDisp: '除铁',
                optionValue: 'CTL',
              },
              {
                optionDisp: '包装',
                optionValue: 'BZL',
              },
              {
                optionDisp: '水洗',
                optionValue: 'SXL',
              },
              {
                optionDisp: '湿法除铁',
                optionValue: 'SCT',
              },
              {
                optionDisp: '氨水配料',
                optionValue: 'QPA',
              },
              {
                optionDisp: '氯化钴配料',
                optionValue: 'QPC',
              },
              {
                optionDisp: '钴盐配料',
                optionValue: 'QPG',
              },
              {
                optionDisp: '碳酸氢铵配料',
                optionValue: 'QPT',
              },
              {
                optionDisp: '三元料液配料',
                optionValue: 'QPS',
              },
              {
                optionDisp: '稀碱配料',
                optionValue: 'QPX',
              },
              {
                optionDisp: '硫酸镁添加剂配料',
                optionValue: 'QPM',
              },
              {
                optionDisp: '硫酸铝添加剂配料',
                optionValue: 'QPL',
              },
              {
                optionDisp: '氨水储料',
                optionValue: 'QCA',
              },
              {
                optionDisp: '氯化钴储料',
                optionValue: 'QCC',
              },
              {
                optionDisp: '钴盐储料',
                optionValue: 'QCG',
              },
              {
                optionDisp: '液碱储料',
                optionValue: 'QCJ',
              },
              {
                optionDisp: '三元料液储料',
                optionValue: 'QCS',
              },
              {
                optionDisp: '稀碱储料',
                optionValue: 'QCX',
              },
              {
                optionDisp: '碳酸氢铵储料',
                optionValue: 'QTC',
              },
              {
                optionDisp: '硫酸铝添加剂储料',
                optionValue: 'QCL',
              },
              {
                optionDisp: '硫酸镁添加剂储料',
                optionValue: 'QCM',
              },
              {
                optionDisp: '晶种沉淀',
                optionValue: 'QCD',
              },
              {
                optionDisp: '钴盐长大沉淀',
                optionValue: 'QGY',
              },
              {
                optionDisp: '三元长大沉淀',
                optionValue: 'QSY',
              },
              {
                optionDisp: '陈化',
                optionValue: 'QCH',
              },
              {
                optionDisp: '洗涤',
                optionValue: 'QXD',
              },
              {
                optionDisp: '烘干',
                optionValue: 'QHG',
              },
              {
                optionDisp: '煅烧',
                optionValue: 'QDS',
              },
              {
                optionDisp: '除铁',
                optionValue: 'QCT',
              },
              {
                optionDisp: '合批',
                optionValue: 'QHP',
              },
              {
                optionDisp: '包装',
                optionValue: 'QBZ',
              },
              {
                optionDisp: '母液处理',
                optionValue: 'QMY',
              },
              {
                optionDisp: '酸溶',
                optionValue: 'QSR',
              },
              {
                optionDisp: '室内',
                optionValue: 'QSN',
              },
              {
                optionDisp: '室外',
                optionValue: 'QSW',
              },
            ],
            widthPixelInput: 100,
            widthPixelLabel: 100,
          },
          {
            defaultValueList: [],
            filterDisplayName: '设备类型',
            filterId: 11610203,
            filterType: 'MultiSelection',
            heightPixelInput: 60,
            heightPixelLabel: 60,
            optionList: [
              {
                optionDisp: '混料机',
                optionValue: 'HLJ',
              },
              {
                optionDisp: '陈化槽',
                optionValue: 'CHC',
              },
              {
                optionDisp: '离心机',
                optionValue: 'LXJ',
              },
              {
                optionDisp: '回转窑',
                optionValue: 'HZY',
              },
              {
                optionDisp: '烧结炉',
                optionValue: 'SJL',
              },
              {
                optionDisp: '粉碎机',
                optionValue: 'FSJ',
              },
              {
                optionDisp: '合批机',
                optionValue: 'HPJ',
              },
              {
                optionDisp: '除铁机',
                optionValue: 'CTJ',
              },
              {
                optionDisp: '包装机',
                optionValue: 'BZJ',
              },
              {
                optionDisp: '配制槽',
                optionValue: 'PZC',
              },
              {
                optionDisp: '储料罐',
                optionValue: 'CLG',
              },
              {
                optionDisp: '反应釜',
                optionValue: 'FYF',
              },
            ],
            widthPixelInput: 100,
            widthPixelLabel: 100,
          },
        ],
        hasBlockPage: true,
        hasBlockSelect: true,
        linkedList: [],
        radioFlag: false,
        rowCountPerPage: 20,
        rowCountPerPageList: [10, 20, 30, 50, 100],
        tabDisplayName: '能力批次明细表',
        tabName: 'pgFacCapacityA02',
        tabType: '1',
      },
    ],
    tableDisplayName: '设备能力批次数据',
    tableName: 'pgFacCapacityA',
    tableType: 'Normal',
  },
}
