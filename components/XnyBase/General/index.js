import XnyGeneral from './src/General.vue'

XnyGeneral.install = function (Vue) {
  Vue.component(XnyGeneral.name, XnyGeneral)
}

export default XnyGeneral
