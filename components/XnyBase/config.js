export default {
  size: 'mini',
  addBtn: true,
  delBtn: true,
  editBtn: true,
  refreshBtn: true,
  crud: {
    showSearch: true,
    showToolbar: true,
    showPage: true,
  },
  general: {
    props: { label: 'optionDisp', value: 'optionValue' },
    tableTabMenu: true, // 显示表格tab切换菜单
  },
  vxeTable: {
    resizable: true, // 启用列宽调整
    stripe: true,
    menuWidth: 200, // 操作列宽度
    menuFixed: 'right', // 操作列冻结方式
    menu: true, // 显示操作列
    isHover: true, // 当鼠标移到行时，是否要高亮当前行
    isCurrent: true, // 当鼠标点击行时，是否要高亮当前行
    align: 'center', // 内容对齐方式
    headerAlign: 'center', // 表头对齐方式
    seq: true, // 显示序号列
    seqWidth: 100, // 序号列宽
    lineHeight: 40, // 行高
    height: 'auto', // 表格高度 auto, %, px
    showHeaderOverflow: 'tooltip', // 标题溢出省略
    showOverflow: 'tooltip', // 单元格溢出省略
    treeTransform: false, // 自动将列表转为树结构
    treeRowField: 'id', // 树节点的字段名
    treeParentField: 'parentId', // 树父节点的字段名
    treeChildren: 'children', // 树子节点的字段名
    treeIndent: 20, // 树节点的缩进
    treeAccordion: false, // 对于同一级的节点，每次只能展开一个
    checkbox: true, // 显示多选列
    radio: false, // 显示单选列,当开启单选列时默认隐藏多选列
    radioStrict: true, // 严格模式，选中后不能取消
    radioReserve: false, // 是否保留勾选状态
    radioVisibleMethod: () => true, // 是否允许勾选的方法，该方法，的返回值用来决定这一行的 radio 是否显示
    radioCheckMethod: () => true, // 是否允许选中的方法，该方法的返回值用来决定这一行的 radio 是否可以选中
    radioTrigger: 'default', // 触发方式
    radioHighlight: false, // 高亮选中行
  },
  crudFormDialog: {
    width: '50%',
  },
}
