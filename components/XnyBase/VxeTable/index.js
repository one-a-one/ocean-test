import XnyVxeTable from './src/VxeTable.vue'

XnyVxeTable.install = function (Vue) {
  Vue.component(XnyVxeTable.name, XnyVxeTable)
}

export default XnyVxeTable
