import XnyNumberPicker from './src/NumberPicker.vue'

XnyNumberPicker.install = function (Vue) {
  Vue.component(XnyNumberPicker.name, XnyNumberPicker)
}

export default XnyNumberPicker
