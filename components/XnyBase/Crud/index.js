import XnyCrud from './src/Crud.vue'

XnyCrud.install = function (Vue) {
  Vue.component(XnyCrud.name, XnyCrud)
}

export default XnyCrud
