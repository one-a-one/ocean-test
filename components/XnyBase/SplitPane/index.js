import XnySplitPane from './src/SplitPane.vue'

XnySplitPane.install = function (Vue) {
  Vue.component(XnySplitPane.name, XnySplitPane)
}

export default XnySplitPane
