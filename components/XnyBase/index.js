// 三方依赖需要npm安装
import 'xe-utils'
import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'

const comList = require.context('./', true, /index.js$/)
const components = []
comList.keys().map((fileName) => {
  const comp = comList(fileName).default
  if (comp) components.push(comp)
})

const install = function (Vue) {
  // 注册组件------------------------------------------
  components.forEach((component) => {
    Vue.component(component.name, component)
  })
  Vue.use(VXETable)
  Vue.prototype.$XModal = VXETable.modal
}

const componentsObj = {}
components.forEach((component) => {
  componentsObj[component.name] = component
})

export default {
  version: '0.0.1',
  install,
  components: {
    ...componentsObj,
  },
}
