import OceCrud from './src/crud.vue'

OceCrud.install = function (Vue) {
  Vue.component('OceCrud', OceCrud)
}

export default OceCrud
