import Avue from '../../assets/avue/avue'
// import Avue from '../../assets/avue/avue2.8.23'
import '../../assets/avue/index.css'
import './style/index.scss'

// import Crud from './crud'

const components = [
  // Crud
]

// install------------------------------------------------------------
const install = function (Vue) {
  Vue.use(Avue)
  // console.log(Avue)

  // 注册组件------------------------------------------
  components.forEach((component) => {
    // Vue.component(component.name, component)
    component.install(Vue)
  })
}

// if (typeof window !== 'undefined' && window.Vue && !window.Vue.prototype.$ocean) {
//   install(window.Vue)
// }

const componentsObj = {}
components.forEach((component) => {
  componentsObj[component.name] = component
})

export default {
  version: '0.0.1',
  install,
  components: {
    ...Avue,
    ...componentsObj,
  },
}
