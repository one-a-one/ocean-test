const path = require('path')
const Client = require('ssh2-sftp-client');

(async () => {
  const sftp = new Client()
  const distPath = path.resolve(__dirname, '../docs/.vuepress/dist')
  try {
    await sftp.connect({
      host: 'ocean-v2.top',
      port: '30022',
      username: 'joe',
      password: 'wyz1993313nasmm',
      // username: 'root',
      // password: '123456',
    })
    console.log('连接成功')
    await sftp.rmdir('/OceanDoc', true)
    await sftp.mkdir('/OceanDoc')
    await sftp.uploadDir(distPath, '/OceanDoc')
    console.log('上传成功')
  } catch (e) {
    console.log('错误', e)
    console.log('sftp服务器连接失败')
  } finally {
    try {
      await sftp.end()
    } catch (e) {
      console.log('连接失败，无法关闭连接')
    }
  }
})()
