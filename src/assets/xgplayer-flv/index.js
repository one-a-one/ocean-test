(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('xgplayer')) :
  typeof define === 'function' && define.amd ? define(['xgplayer'], factory) :
  (global = global || self, global.FlvPlayer = factory(global.Player));
}(this, (function (Player) { 'use strict';

  Player = Player && Object.prototype.hasOwnProperty.call(Player, 'default') ? Player['default'] : Player;

  /**
   * @param {number} num
   * @param {number} fixed
   * @return {number}
   */
  var debounce = (function (fn, wait) {
    var lastTime = Date.now();
    var timer = null;
    var isFirstTime = true;

    return function () {
      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      var now = Date.now();
      if (isFirstTime) {
        lastTime = Date.now();
        isFirstTime = false;
        fn.apply(undefined, args);
      }
      if (now - lastTime > wait) {
        lastTime = now;
        fn.apply(undefined, args);
      } else {
        if (timer) {
          window.clearTimeout(timer);
        }
        timer = setTimeout(function () {
          fn.apply(undefined, args);
        }, wait);
      }
    };
  });

  var BROWSER_EVENTS = {
    VISIBILITY_CHANGE: 'VISIBILITY_CHANGE'
  };
  var PLAYER_EVENTS = {
    SEEK: 'SEEK'
  };

  var LOADER_EVENTS = {
    LADER_START: 'LOADER_START',
    LOADER_DATALOADED: 'LOADER_DATALOADED',
    LOADER_COMPLETE: 'LOADER_COMPLETE',
    LOADER_RESPONSE_HEADERS: 'LOADER_RESPONSE_HEADERS',
    LOADER_ERROR: 'LOADER_ERROR',
    LOADER_RETRY: 'LOADER_RETRY',
    LOADER_TTFB: 'LOADER_TTFB'
  };

  var DEMUX_EVENTS = {
    DEMUX_START: 'DEMUX_START',
    DEMUX_COMPLETE: 'DEMUX_COMPLETE',
    DEMUX_ERROR: 'DEMUX_ERROR',
    METADATA_PARSED: 'METADATA_PARSED',
    SEI_PARSED: 'SEI_PARSED',
    VIDEO_METADATA_CHANGE: 'VIDEO_METADATA_CHANGE',
    AUDIO_METADATA_CHANGE: 'AUDIO_METADATA_CHANGE',
    MEDIA_INFO: 'MEDIA_INFO',
    ISKEYFRAME: 'ISKEYFRAME'
  };

  var REMUX_EVENTS = {
    REMUX_METADATA: 'REMUX_METADATA',
    REMUX_MEDIA: 'REMUX_MEDIA',
    MEDIA_SEGMENT: 'MEDIA_SEGMENT',
    REMUX_ERROR: 'REMUX_ERROR',
    INIT_SEGMENT: 'INIT_SEGMENT',
    DETECT_CHANGE_STREAM: 'DETECT_CHANGE_STREAM',
    DETECT_CHANGE_STREAM_DISCONTINUE: 'DETECT_CHANGE_STREAM_DISCONTINUE',
    DETECT_AUDIO_GAP: 'DETECT_AUDIO_GAP',
    DETECT_LARGE_GAP: 'DETECT_LARGE_GAP',
    DETECT_AUDIO_OVERLAP: 'DETECT_AUDIO_OVERLAP',
    RANDOM_ACCESS_POINT: 'RANDOM_ACCESS_POINT',
    DETECT_FRAG_ID_DISCONTINUE: 'DETECT_FRAG_ID_DISCONTINUE'
  };

  var MSE_EVENTS = {
    SOURCE_UPDATE_END: 'SOURCE_UPDATE_END',
    MSE_ERROR: 'MSE_ERROR',
    MSE_WRONG_TRACK_ADD: 'MSE_WRONG_TRACK_ADD'

    // hls专有events
  };var HLS_EVENTS = {
    RETRY_TIME_EXCEEDED: 'RETRY_TIME_EXCEEDED'
  };

  var CRYPTO_EVENTS = {
    START_DECRYPTOO: 'START_DECRYPTO',
    DECRYPTED: 'DECRYPTED'
  };
  var ALLEVENTS = Object.assign({}, LOADER_EVENTS, DEMUX_EVENTS, REMUX_EVENTS, MSE_EVENTS, HLS_EVENTS, PLAYER_EVENTS, BROWSER_EVENTS);

  var FlvAllowedEvents = [];
  var HlsAllowedEvents = [];

  for (var key in ALLEVENTS) {
    if (ALLEVENTS.hasOwnProperty(key)) {
      FlvAllowedEvents.push(ALLEVENTS[key]);
    }
  }

  for (var _key in ALLEVENTS) {
    if (ALLEVENTS.hasOwnProperty(_key)) {
      HlsAllowedEvents.push(ALLEVENTS[_key]);
    }
  }

  var EVENTS = {
    ALLEVENTS: ALLEVENTS,
    HLS_EVENTS: HLS_EVENTS,
    REMUX_EVENTS: REMUX_EVENTS,
    DEMUX_EVENTS: DEMUX_EVENTS,
    MSE_EVENTS: MSE_EVENTS,
    LOADER_EVENTS: LOADER_EVENTS,
    FlvAllowedEvents: FlvAllowedEvents,
    HlsAllowedEvents: HlsAllowedEvents,
    CRYPTO_EVENTS: CRYPTO_EVENTS,
    PLAYER_EVENTS: PLAYER_EVENTS,
    BROWSER_EVENTS: BROWSER_EVENTS
  };

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var RemuxedBuffer = function RemuxedBuffer() {
    _classCallCheck(this, RemuxedBuffer);

    /** @type{string} */
    this.mimetype = '';
    /** @type{Uint8Array|null} */
    this.init = null;
    /** @type{Uint8Array[]} */
    this.data = [];
    /** @type{number} */
    this.bufferDuration = 0;
  };

  var RemuxedBufferManager = function () {
    function RemuxedBufferManager() {
      _classCallCheck(this, RemuxedBufferManager);

      /** @type{Object.<string, RemuxedBuffer>} */
      this.sources = {};
    }

    /**
     *
     * @param {string} name
     * @return {RemuxedBuffer}
     */

    _createClass(RemuxedBufferManager, [{
      key: 'getSource',
      value: function getSource(name) {
        return this.sources[name];
      }

      /**
       * @param {string} name
       * @return {RemuxedBuffer}
       */

    }, {
      key: 'createSource',
      value: function createSource(name) {
        this.sources[name] = new RemuxedBuffer();
        return this.sources[name];
      }
    }, {
      key: 'clear',
      value: function clear() {
        this.sources = {};
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        this.clear();
      }
    }]);

    return RemuxedBufferManager;
  }();

  var _createClass$1 = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$1(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var XGDataView = function () {
    function XGDataView(buffer) {
      _classCallCheck$1(this, XGDataView);

      if (buffer instanceof ArrayBuffer) {
        /** @type {ArrayBuffer} */
        this.buffer = buffer;
        /** @type {DataView} */
        this.dataview = new DataView(buffer);
        /** @type {number} */
        this.dataview.position = 0;
      } else {
        throw new Error('data is invalid');
      }
    }

    /**
     * byffer length
     * @return {number}
     */

    _createClass$1(XGDataView, [{
      key: 'back',

      /**
       * move read position backward
       * @param count
       */
      value: function back(count) {
        this.position -= count;
      }

      /**
       * move read position forward
       * @param count
       */

    }, {
      key: 'skip',
      value: function skip(count) {
        var loop = Math.floor(count / 4);
        var last = count % 4;
        for (var i = 0; i < loop; i++) {
          XGDataView.readByte(this.dataview, 4);
        }
        if (last > 0) {
          XGDataView.readByte(this.dataview, last);
        }
      }

      /**
       * [readByte 从DataView中读取数据]
       * @param  {DataView} buffer [DataView实例]
       * @param  {Number} size   [读取字节数]
       * @return {Number}        [整数]
       */

    }, {
      key: 'readUint8',

      /**
       * @return {Number}
       */
      value: function readUint8() {
        return XGDataView.readByte(this.dataview, 1);
      }

      /**
       * @return {Number}
       */

    }, {
      key: 'readUint16',
      value: function readUint16() {
        return XGDataView.readByte(this.dataview, 2);
      }

      /**
       * @return {Number}
       */

    }, {
      key: 'readUint24',
      value: function readUint24() {
        return XGDataView.readByte(this.dataview, 3);
      }

      /**
       * @return {Number}
       */

    }, {
      key: 'readUint32',
      value: function readUint32() {
        return XGDataView.readByte(this.dataview, 4);
      }

      /**
       * @return {Number}
       */

    }, {
      key: 'readUint64',
      value: function readUint64() {
        return XGDataView.readByte(this.dataview, 8);
      }

      /**
       * @return {Number}
       */

    }, {
      key: 'readInt8',
      value: function readInt8() {
        return XGDataView.readByte(this.dataview, 1, true);
      }

      /**
       * @return {Number}
       */

    }, {
      key: 'readInt16',
      value: function readInt16() {
        return XGDataView.readByte(this.dataview, 2, true);
      }

      /**
       * @return {Number}
       */

    }, {
      key: 'readInt32',
      value: function readInt32() {
        return XGDataView.readByte(this.dataview, 4, true);
      }

      /**
       * @return {Uint8Array}
       */

    }, {
      key: 'writeUint32',
      value: function writeUint32(value) {
        return new Uint8Array([value >>> 24 & 0xff, value >>> 16 & 0xff, value >>> 8 & 0xff, value & 0xff]);
      }
    }, {
      key: 'length',
      get: function get() {
        return this.buffer.byteLength;
      }

      /**
       * set current read position of data-view
       * @param value
       */

    }, {
      key: 'position',
      set: function set(value) {
        this.dataview.position = value;
      }

      /**
       * set current read position of data-view
       * @param value
       */

      , get: function get() {
        return this.dataview.position;
      }
    }], [{
      key: 'readByte',
      value: function readByte(buffer, size, sign) {
        var res = void 0;
        switch (size) {
          case 1:
            if (sign) {
              res = buffer.getInt8(buffer.position);
            } else {
              res = buffer.getUint8(buffer.position);
            }
            break;
          case 2:
            if (sign) {
              res = buffer.getInt16(buffer.position);
            } else {
              res = buffer.getUint16(buffer.position);
            }
            break;
          case 3:
            if (sign) {
              throw new Error('not supported for readByte 3');
            } else {
              res = buffer.getUint8(buffer.position) << 16;
              res |= buffer.getUint8(buffer.position + 1) << 8;
              res |= buffer.getUint8(buffer.position + 2);
            }
            break;
          case 4:
            if (sign) {
              res = buffer.getInt32(buffer.position);
            } else {
              res = buffer.getUint32(buffer.position);
            }
            break;
          case 8:
            if (sign) {
              throw new Error('not supported for readBody 8');
            } else {
              res = buffer.getUint32(buffer.position) << 32;
              res |= buffer.getUint32(buffer.position + 4);
            }
            break;
          default:
            res = '';
        }
        buffer.position += size;
        return res;
      }
    }]);

    return XGDataView;
  }();

  var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

  var _createClass$2 = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  function _classCallCheck$2(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var Track = function () {
    function Track() {
      _classCallCheck$2(this, Track);

      /** @type {number} */
      this.id = -1;
      /** @type {number} */
      this.sequenceNumber = 0;
      /** @type {*} */
      this.samples = [];
      /** @type {*} */
      this.droppedSamples = [];
      /** @type {number} */
      this.length = 0;
    }

    _createClass$2(Track, [{
      key: 'reset',
      value: function reset() {
        this.sequenceNumber = 0;
        this.samples = [];
        this.length = 0;
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        this.reset();
        this.id = -1;
      }
    }]);

    return Track;
  }();

  var AudioTrack = function (_Track) {
    _inherits(AudioTrack, _Track);

    function AudioTrack() {
      _classCallCheck$2(this, AudioTrack);

      /** @type {string} */
      var _this = _possibleConstructorReturn(this, (AudioTrack.__proto__ || Object.getPrototypeOf(AudioTrack)).call(this));

      _this.TAG = 'AudioTrack';
      /** @type {string} */
      _this.type = 'audio';
      return _this;
    }

    return AudioTrack;
  }(Track);

  var VideoTrack = function (_Track2) {
    _inherits(VideoTrack, _Track2);

    function VideoTrack() {
      _classCallCheck$2(this, VideoTrack);

      /** @type {string} */
      var _this2 = _possibleConstructorReturn(this, (VideoTrack.__proto__ || Object.getPrototypeOf(VideoTrack)).call(this));

      _this2.TAG = 'VideoTrack';
      /** @type {string} */
      _this2.type = 'video';
      /** @type {number} */
      _this2.dropped = 0;
      /** @type {number} */
      _this2.sequenceNumber = 0;
      return _this2;
    }

    _createClass$2(VideoTrack, [{
      key: 'reset',
      value: function reset() {
        this.sequenceNumber = 0;
        this.samples = [];
        this.length = 0;
        this.dropped = 0;
      }
    }]);

    return VideoTrack;
  }(Track);

  var Tracks = function () {
    function Tracks() {
      _classCallCheck$2(this, Tracks);

      this.audioTrack = null;
      this.videoTrack = null;
    }

    _createClass$2(Tracks, [{
      key: 'destroy',
      value: function destroy() {
        this.audioTrack = null;
        this.videoTrack = null;
      }
    }]);

    return Tracks;
  }();

  var _createClass$3 = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$3(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var XgBuffer = function () {
    /**
     * A buffer to store loaded data.
     *
     * @class LoaderBuffer
     * @param {number} length - Optional the buffer size
     */
    function XgBuffer(length) {
      _classCallCheck$3(this, XgBuffer);

      this.length = length || 0;
      this.historyLen = length || 0;
      this.array = [];
      this.offset = 0;
    }

    /**
     * The function to push data.
     *
     * @param {Uint8Array} data - The data to push into the buffer
     */

    _createClass$3(XgBuffer, [{
      key: "push",
      value: function push(data) {
        this.array.push(data);
        this.length += data.byteLength;
        this.historyLen += data.byteLength;
      }

      /**
       * The function to shift data.
       *
       * @param {number} length - The size of shift.
       */

    }, {
      key: "shift",
      value: function shift(length) {
        if (this.array.length < 1) {
          return new Uint8Array(0);
        }

        if (length === undefined) {
          return this._shiftBuffer();
        }
        if (this.offset + length === this.array[0].length) {
          var _ret = this.array[0].slice(this.offset, this.offset + length);
          this.offset = 0;
          this.array.shift();
          this.length -= length;
          return _ret;
        }

        if (this.offset + length < this.array[0].length) {
          var _ret2 = this.array[0].slice(this.offset, this.offset + length);
          this.offset += length;
          this.length -= length;
          return _ret2;
        }

        var ret = new Uint8Array(length);
        var tmpoff = 0;
        while (this.array.length > 0 && length > 0) {
          if (this.offset + length < this.array[0].length) {
            var tmp = this.array[0].slice(this.offset, this.offset + length);
            ret.set(tmp, tmpoff);
            this.offset += length;
            this.length -= length;
            length = 0;
            break;
          } else {
            // console.log('mark1')
            var templength = this.array[0].length - this.offset;
            ret.set(this.array[0].slice(this.offset, this.array[0].length), tmpoff);
            this.array.shift();
            this.offset = 0;
            tmpoff += templength;
            this.length -= templength;
            length -= templength;
          }
        }
        return ret;
      }

      /**
       * Function to clear the buffer.
       */

    }, {
      key: "clear",
      value: function clear() {
        this.array = [];
        this.length = 0;
        this.offset = 0;
      }
    }, {
      key: "destroy",
      value: function destroy() {
        this.clear();
        this.historyLen = 0;
      }

      /**
       * Function to shift one unit8Array.
       */

    }, {
      key: "_shiftBuffer",
      value: function _shiftBuffer() {
        this.length -= this.array[0].length;
        this.offset = 0;
        return this.array.shift();
      }

      /**
       * Convert uint8 data to number.
       *
       * @param {number} start - the start postion.
       * @param {number} length - the length of data.
       */

    }, {
      key: "toInt",
      value: function toInt(start, length) {
        var retInt = 0;
        var i = this.offset + start;
        while (i < this.offset + length + start) {
          if (i < this.array[0].length) {
            retInt = retInt * 256 + this.array[0][i];
          } else if (this.array[1]) {
            retInt = retInt * 256 + this.array[1][i - this.array[0].length];
          }

          i++;
        }
        return retInt;
      }
    }]);

    return XgBuffer;
  }();

  var _createClass$4 = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$4(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var AudioTrackMeta = function () {
    function AudioTrackMeta(meta) {
      _classCallCheck$4(this, AudioTrackMeta);

      var _default = {
        sampleRate: 48000,
        channelCount: 2,
        codec: 'mp4a.40.2',
        config: [41, 401, 136, 0],
        duration: 0,
        id: 2,
        refSampleDuration: 21,
        sampleRateIndex: 3,
        timescale: 1000,
        type: 'audio'
      };
      if (meta) {
        return Object.assign({}, _default, meta);
      }
      return _default;
    }

    _createClass$4(AudioTrackMeta, [{
      key: 'destroy',
      value: function destroy() {
        this.init = null;
      }
    }]);

    return AudioTrackMeta;
  }();

  var VideoTrackMeta = function () {
    function VideoTrackMeta(meta) {
      _classCallCheck$4(this, VideoTrackMeta);

      var _default = {
        avcc: null,
        sps: new Uint8Array(0),
        pps: new Uint8Array(0),
        chromaFormat: 420,
        codec: 'avc1.640020',
        codecHeight: 720,
        codecWidth: 1280,
        duration: 0,
        frameRate: {
          fixed: true,
          fps: 25,
          fps_num: 25000,
          fps_den: 1000
        },
        id: 1,
        level: '3.2',
        presentHeight: 720,
        presentWidth: 1280,
        profile: 'High',
        refSampleDuration: 40,
        parRatio: {
          height: 1,
          width: 1
        },
        timescale: 1000,
        type: 'video'
      };

      if (meta) {
        return Object.assign({}, _default, meta);
      }
      return _default;
    }

    _createClass$4(VideoTrackMeta, [{
      key: 'destroy',
      value: function destroy() {
        this.init = null;
        this.sps = null;
        this.pps = null;
      }
    }]);

    return VideoTrackMeta;
  }();

  var _createClass$5 = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$5(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  /**
   * @typedef {Object} TAudioSample
   * @property {number} dts
   * @property {number} pts
   * @property {number} originDts
   * @property {Uint8Array} data
   */

  /**
   * @typedef {Object} TVideoSample
   * @property {number} dts
   * @property {number} pts
   * @property {boolean} isKeyframe
   * @property {number} originDts
   * @property {Uint8Array} data
   * @property {Array<any>} nals
   */

  var AudioSample = function () {
    function AudioSample(info) {
      _classCallCheck$5(this, AudioSample);

      var _default = AudioSample.getDefault();
      if (!info) {
        return _default;
      }
      return Object.assign({}, _default, info);
    }

    /**
     * default audio sample
     * @return {TAudioSample}
     */

    _createClass$5(AudioSample, null, [{
      key: "getDefault",
      value: function getDefault() {
        return {
          dts: -1,
          pts: -1,
          originDts: -1,
          data: new Uint8Array()
        };
      }
    }]);

    return AudioSample;
  }();

  var VideoSample = function () {
    /**
     * @constructor
     * @param info
     * @return {TVideoSample}
     */
    function VideoSample(info) {
      _classCallCheck$5(this, VideoSample);

      var _default = VideoSample.getDefault();

      if (!info) {
        return _default;
      }
      var sample = Object.assign({}, _default, info);

      return sample;
    }

    /**
     * default video sample
     * @return {TVideoSample}
     */

    _createClass$5(VideoSample, null, [{
      key: "getDefault",
      value: function getDefault() {
        return {
          dts: -1,
          pts: undefined,
          isKeyframe: false, // is Random access point
          originDts: -1,
          data: new Uint8Array(),
          nals: []
        };
      }
    }]);

    return VideoSample;
  }();

  var _createClass$6 = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$6(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  /**
   * @typedef {Object} VideoMediaInfo
   * @property {string|null} codec
   * @property {number|null} width
   * @property {number|null} height
   * @property {string|null} profile
   * @property {{fixed: boolean,fps: number,fps_num: number,fps_den: number}} frameRate
   * @property {string|null} chromaFormat
   * @property {{width:number,height:number}} parRatio
   */

  /**
   * @typedef {Object} AudioMediaInfo
   * @property {string|null} codec
   * @property {number|null} sampleRate
   * @property {number|null} sampleRateIndex
   * @property {number|null} channelCount
   */

  /**
   * @param {Object} obj
   * @return {boolean}
   */
  var isObjectFilled = function isObjectFilled(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        if (obj[key] === null) {
          return false;
        }
      }
    }
    return true;
  };

  var MediaInfo = function () {
    function MediaInfo() {
      _classCallCheck$6(this, MediaInfo);

      this.mimeType = null;
      this.duration = null;

      /** @type {boolean} */
      this.hasVideo = false;
      /**
       * video media info
       * @type {VideoMediaInfo}
       */
      this.video = {
        codec: null,
        width: null,
        height: null,
        profile: null,
        level: null,
        frameRate: {
          fixed: true,
          fps: 25,
          fps_num: 25000,
          fps_den: 1000
        },
        chromaFormat: null,
        parRatio: {
          width: 1,
          height: 1
          /** @type {boolean} */
        } };this.hasAudio = false;

      /**
       * video media info
       * @type {AudioMediaInfo}
       */
      this.audio = {
        codec: null,
        sampleRate: null,
        sampleRateIndex: null,
        channelCount: null
      };
    }

    _createClass$6(MediaInfo, [{
      key: "isComplete",
      value: function isComplete() {
        return MediaInfo.isBaseInfoReady(this) && MediaInfo.isVideoReady(this) && MediaInfo.isAudioReady(this);
      }
    }], [{
      key: "isBaseInfoReady",
      value: function isBaseInfoReady(mediaInfo) {
        return isObjectFilled(mediaInfo);
      }
    }, {
      key: "isVideoReady",
      value: function isVideoReady(mediaInfo) {
        if (!mediaInfo.hasVideo) {
          return true;
        }

        return isObjectFilled(mediaInfo.video);
      }
    }, {
      key: "isAudioReady",
      value: function isAudioReady(mediaInfo) {
        if (!mediaInfo.hasAudio) {
          return true;
        }

        return isObjectFilled(mediaInfo.video);
      }
    }]);

    return MediaInfo;
  }();

  function _classCallCheck$7(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var FlvTag = function FlvTag() {
    _classCallCheck$7(this, FlvTag);

    /** @type {number} */
    this.filtered = -1;
    /** @type {number} */
    this.tagType = -1;
    /** @type {number} */
    this.datasize = -1;
    /** @type {number} */
    this.dts = -1;
  };

  function unwrapExports (x) {
  	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
  }

  function createCommonjsModule(fn, module) {
  	return module = { exports: {} }, fn(module, module.exports), module.exports;
  }

  var eventemitter3 = createCommonjsModule(function (module) {

  var has = Object.prototype.hasOwnProperty,
      prefix = '~';

  /**
   * Constructor to create a storage for our `EE` objects.
   * An `Events` instance is a plain object whose properties are event names.
   *
   * @constructor
   * @private
   */
  function Events() {}

  //
  // We try to not inherit from `Object.prototype`. In some engines creating an
  // instance in this way is faster than calling `Object.create(null)` directly.
  // If `Object.create(null)` is not supported we prefix the event names with a
  // character to make sure that the built-in object properties are not
  // overridden or used as an attack vector.
  //
  if (Object.create) {
    Events.prototype = Object.create(null);

    //
    // This hack is needed because the `__proto__` property is still inherited in
    // some old browsers like Android 4, iPhone 5.1, Opera 11 and Safari 5.
    //
    if (!new Events().__proto__) prefix = false;
  }

  /**
   * Representation of a single event listener.
   *
   * @param {Function} fn The listener function.
   * @param {*} context The context to invoke the listener with.
   * @param {Boolean} [once=false] Specify if the listener is a one-time listener.
   * @constructor
   * @private
   */
  function EE(fn, context, once) {
    this.fn = fn;
    this.context = context;
    this.once = once || false;
  }

  /**
   * Add a listener for a given event.
   *
   * @param {EventEmitter} emitter Reference to the `EventEmitter` instance.
   * @param {(String|Symbol)} event The event name.
   * @param {Function} fn The listener function.
   * @param {*} context The context to invoke the listener with.
   * @param {Boolean} once Specify if the listener is a one-time listener.
   * @returns {EventEmitter}
   * @private
   */
  function addListener(emitter, event, fn, context, once) {
    if (typeof fn !== 'function') {
      throw new TypeError('The listener must be a function');
    }

    var listener = new EE(fn, context || emitter, once),
        evt = prefix ? prefix + event : event;

    if (!emitter._events[evt]) emitter._events[evt] = listener, emitter._eventsCount++;else if (!emitter._events[evt].fn) emitter._events[evt].push(listener);else emitter._events[evt] = [emitter._events[evt], listener];

    return emitter;
  }

  /**
   * Clear event by name.
   *
   * @param {EventEmitter} emitter Reference to the `EventEmitter` instance.
   * @param {(String|Symbol)} evt The Event name.
   * @private
   */
  function clearEvent(emitter, evt) {
    if (--emitter._eventsCount === 0) emitter._events = new Events();else delete emitter._events[evt];
  }

  /**
   * Minimal `EventEmitter` interface that is molded against the Node.js
   * `EventEmitter` interface.
   *
   * @constructor
   * @public
   */
  function EventEmitter() {
    this._events = new Events();
    this._eventsCount = 0;
  }

  /**
   * Return an array listing the events for which the emitter has registered
   * listeners.
   *
   * @returns {Array}
   * @public
   */
  EventEmitter.prototype.eventNames = function eventNames() {
    var names = [],
        events,
        name;

    if (this._eventsCount === 0) return names;

    for (name in events = this._events) {
      if (has.call(events, name)) names.push(prefix ? name.slice(1) : name);
    }

    if (Object.getOwnPropertySymbols) {
      return names.concat(Object.getOwnPropertySymbols(events));
    }

    return names;
  };

  /**
   * Return the listeners registered for a given event.
   *
   * @param {(String|Symbol)} event The event name.
   * @returns {Array} The registered listeners.
   * @public
   */
  EventEmitter.prototype.listeners = function listeners(event) {
    var evt = prefix ? prefix + event : event,
        handlers = this._events[evt];

    if (!handlers) return [];
    if (handlers.fn) return [handlers.fn];

    for (var i = 0, l = handlers.length, ee = new Array(l); i < l; i++) {
      ee[i] = handlers[i].fn;
    }

    return ee;
  };

  /**
   * Return the number of listeners listening to a given event.
   *
   * @param {(String|Symbol)} event The event name.
   * @returns {Number} The number of listeners.
   * @public
   */
  EventEmitter.prototype.listenerCount = function listenerCount(event) {
    var evt = prefix ? prefix + event : event,
        listeners = this._events[evt];

    if (!listeners) return 0;
    if (listeners.fn) return 1;
    return listeners.length;
  };

  /**
   * Calls each of the listeners registered for a given event.
   *
   * @param {(String|Symbol)} event The event name.
   * @returns {Boolean} `true` if the event had listeners, else `false`.
   * @public
   */
  EventEmitter.prototype.emit = function emit(event, a1, a2, a3, a4, a5) {
    var evt = prefix ? prefix + event : event;

    if (!this._events[evt]) return false;

    var listeners = this._events[evt],
        len = arguments.length,
        args,
        i;

    if (listeners.fn) {
      if (listeners.once) this.removeListener(event, listeners.fn, undefined, true);

      switch (len) {
        case 1:
          return listeners.fn.call(listeners.context), true;
        case 2:
          return listeners.fn.call(listeners.context, a1), true;
        case 3:
          return listeners.fn.call(listeners.context, a1, a2), true;
        case 4:
          return listeners.fn.call(listeners.context, a1, a2, a3), true;
        case 5:
          return listeners.fn.call(listeners.context, a1, a2, a3, a4), true;
        case 6:
          return listeners.fn.call(listeners.context, a1, a2, a3, a4, a5), true;
      }

      for (i = 1, args = new Array(len - 1); i < len; i++) {
        args[i - 1] = arguments[i];
      }

      listeners.fn.apply(listeners.context, args);
    } else {
      var length = listeners.length,
          j;

      for (i = 0; i < length; i++) {
        if (listeners[i].once) this.removeListener(event, listeners[i].fn, undefined, true);

        switch (len) {
          case 1:
            listeners[i].fn.call(listeners[i].context);break;
          case 2:
            listeners[i].fn.call(listeners[i].context, a1);break;
          case 3:
            listeners[i].fn.call(listeners[i].context, a1, a2);break;
          case 4:
            listeners[i].fn.call(listeners[i].context, a1, a2, a3);break;
          default:
            if (!args) for (j = 1, args = new Array(len - 1); j < len; j++) {
              args[j - 1] = arguments[j];
            }

            listeners[i].fn.apply(listeners[i].context, args);
        }
      }
    }

    return true;
  };

  /**
   * Add a listener for a given event.
   *
   * @param {(String|Symbol)} event The event name.
   * @param {Function} fn The listener function.
   * @param {*} [context=this] The context to invoke the listener with.
   * @returns {EventEmitter} `this`.
   * @public
   */
  EventEmitter.prototype.on = function on(event, fn, context) {
    return addListener(this, event, fn, context, false);
  };

  /**
   * Add a one-time listener for a given event.
   *
   * @param {(String|Symbol)} event The event name.
   * @param {Function} fn The listener function.
   * @param {*} [context=this] The context to invoke the listener with.
   * @returns {EventEmitter} `this`.
   * @public
   */
  EventEmitter.prototype.once = function once(event, fn, context) {
    return addListener(this, event, fn, context, true);
  };

  /**
   * Remove the listeners of a given event.
   *
   * @param {(String|Symbol)} event The event name.
   * @param {Function} fn Only remove the listeners that match this function.
   * @param {*} context Only remove the listeners that have this context.
   * @param {Boolean} once Only remove one-time listeners.
   * @returns {EventEmitter} `this`.
   * @public
   */
  EventEmitter.prototype.removeListener = function removeListener(event, fn, context, once) {
    var evt = prefix ? prefix + event : event;

    if (!this._events[evt]) return this;
    if (!fn) {
      clearEvent(this, evt);
      return this;
    }

    var listeners = this._events[evt];

    if (listeners.fn) {
      if (listeners.fn === fn && (!once || listeners.once) && (!context || listeners.context === context)) {
        clearEvent(this, evt);
      }
    } else {
      for (var i = 0, events = [], length = listeners.length; i < length; i++) {
        if (listeners[i].fn !== fn || once && !listeners[i].once || context && listeners[i].context !== context) {
          events.push(listeners[i]);
        }
      }

      //
      // Reset the array, or remove it completely if we have no more listeners.
      //
      if (events.length) this._events[evt] = events.length === 1 ? events[0] : events;else clearEvent(this, evt);
    }

    return this;
  };

  /**
   * Remove all listeners, or those of the specified event.
   *
   * @param {(String|Symbol)} [event] The event name.
   * @returns {EventEmitter} `this`.
   * @public
   */
  EventEmitter.prototype.removeAllListeners = function removeAllListeners(event) {
    var evt;

    if (event) {
      evt = prefix ? prefix + event : event;
      if (this._events[evt]) clearEvent(this, evt);
    } else {
      this._events = new Events();
      this._eventsCount = 0;
    }

    return this;
  };

  //
  // Alias methods names because people roll like that.
  //
  EventEmitter.prototype.off = EventEmitter.prototype.removeListener;
  EventEmitter.prototype.addListener = EventEmitter.prototype.on;

  //
  // Expose the prefix.
  //
  EventEmitter.prefixed = prefix;

  //
  // Allow `EventEmitter` to be imported as module namespace.
  //
  EventEmitter.EventEmitter = EventEmitter;

  //
  // Expose the module.
  //
  {
    module.exports = EventEmitter;
  }
  });

  var _typeof$1 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

  var _get = function get(object, property, receiver) {
    if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {
      var parent = Object.getPrototypeOf(object);if (parent === null) {
        return undefined;
      } else {
        return get(parent, property, receiver);
      }
    } else if ("value" in desc) {
      return desc.value;
    } else {
      var getter = desc.get;if (getter === undefined) {
        return undefined;
      }return getter.call(receiver);
    }
  };

  var _createClass$7 = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _possibleConstructorReturn$1(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && ((typeof call === "undefined" ? "undefined" : _typeof$1(call)) === "object" || typeof call === "function") ? call : self;
  }

  function _inherits$1(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof$1(superClass)));
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  function _classCallCheck$8(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var DIRECT_EMIT_FLAG = '__TO__';

  var Context = function () {
    /**
     *
     * @param {*} player
     * @param {*} configs
     * @param {string[]}allowedEvents
     */
    function Context(player, configs) {
      var allowedEvents = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

      _classCallCheck$8(this, Context);

      this._emitter = new eventemitter3();
      if (!this._emitter.off) {
        this._emitter.off = this._emitter.removeListener;
      }

      this.mediaInfo = new MediaInfo();
      this._instanceMap = {}; // 所有的解码流程实例
      this._clsMap = {}; // 构造函数的map
      this._inited = false;
      this.allowedEvents = allowedEvents;
      this._configs = configs;
      this._player = player;
      this._hooks = {}; // 注册在事件前/后的钩子，例如 before('DEMUX_COMPLETE')
    }

    /**
     * 从上下文中获取解码流程实例，如果没有实例，构造一个
     * @param {string} tag
     * @returns {*}
     */

    _createClass$7(Context, [{
      key: 'getInstance',
      value: function getInstance(tag) {
        var instance = this._instanceMap[tag];
        if (instance) {
          return instance;
        } else {
          // throw new Error(`${tag}实例尚未初始化`)
          return null;
        }
      }

      /**
       * 初始化具体实例
       * @param {string} tag
       * @param {any[]}args
       */

    }, {
      key: 'initInstance',
      value: function initInstance(tag) {
        for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          args[_key - 1] = arguments[_key];
        }

        var a = args[0],
            b = args[1],
            c = args[2],
            d = args[3];

        if (this._clsMap[tag]) {
          var newInstance = new this._clsMap[tag](a, b, c, d);
          this._instanceMap[tag] = newInstance;
          if (newInstance.init) {
            newInstance.init(); // TODO: lifecircle
          }
          return newInstance;
        } else {
          throw new Error(tag + "\u672A\u5728context\u4E2D\u6CE8\u518C");
        }
      }

      /**
       * 避免大量的initInstance调用，初始化所有的组件
       * @param {*} config
       */

    }, {
      key: 'init',
      value: function init(config) {
        if (this._inited) {
          return;
        }
        for (var tag in this._clsMap) {
          // if not inited, init an instance
          if (this._clsMap.hasOwnProperty(tag) && !this._instanceMap[tag]) {
            this.initInstance(tag, config);
          }
        }
        this._inited = true;
      }

      /**
       * 注册一个上下文流程，提供安全的事件发送机制
       * @param {string} tag
       * @param {*} cls
       */

    }, {
      key: 'registry',
      value: function registry(tag, cls) {
        var _this2 = this;

        var emitter = this._emitter;
        var checkMessageName = this._isMessageNameValid.bind(this);
        var self = this;
        var enhanced = function (_cls) {
          _inherits$1(enhanced, _cls);

          function enhanced(a, b, c) {
            _classCallCheck$8(this, enhanced);

            var _this = _possibleConstructorReturn$1(this, (enhanced.__proto__ || Object.getPrototypeOf(enhanced)).call(this, a, b, c));

            _this.listeners = {};
            _this.onceListeners = {};
            _this.TAG = tag;
            _this._context = self;
            return _this;
          }

          /**
           * @param {string} messageName
           * @param {function} callback
           */

          _createClass$7(enhanced, [{
            key: 'on',
            value: function on(messageName, callback) {
              checkMessageName(messageName);

              if (this.listeners[messageName]) {
                this.listeners[messageName].push(callback);
              } else {
                this.listeners[messageName] = [callback];
              }

              emitter.on('' + messageName + DIRECT_EMIT_FLAG + tag, callback); // 建立定向通信监听
              return emitter.on(messageName, callback);
            }

            /**
             * @param {string} messageName
             * @param {function} callback
             */

          }, {
            key: 'before',
            value: function before(messageName, callback) {
              checkMessageName(messageName);
              if (self._hooks[messageName]) {
                self._hooks[messageName].push(callback);
              } else {
                self._hooks[messageName] = [callback];
              }
            }

            /**
             * @param {string} messageName
             * @param {function} callback
             */

          }, {
            key: 'once',
            value: function once(messageName, callback) {
              checkMessageName(messageName);

              if (this.onceListeners[messageName]) {
                this.onceListeners[messageName].push(callback);
              } else {
                this.onceListeners[messageName] = [callback];
              }

              emitter.once('' + messageName + DIRECT_EMIT_FLAG + tag, callback);
              return emitter.once(messageName, callback);
            }

            /**
             * @param {string} messageName
             * @param {any[]} args
             */

          }, {
            key: 'emit',
            value: function emit(messageName) {
              checkMessageName(messageName);
              // console.log('emit ', messageName);

              var beforeList = self._hooks ? self._hooks[messageName] : null;

              for (var _len2 = arguments.length, args = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
                args[_key2 - 1] = arguments[_key2];
              }

              if (beforeList) {
                for (var i = 0, len = beforeList.length; i < len; i++) {
                  var callback = beforeList[i];
                  // eslint-disable-next-line standard/no-callback-literal
                  callback.apply(undefined, args);
                }
              }
              return emitter.emit.apply(emitter, [messageName].concat(args));
            }

            /**
             * 定向发送给某个组件单例的消息
             * @param {string} messageName
             * @param {any[]} args
             */

          }, {
            key: 'emitTo',
            value: function emitTo(tag, messageName) {
              checkMessageName(messageName);

              for (var _len3 = arguments.length, args = Array(_len3 > 2 ? _len3 - 2 : 0), _key3 = 2; _key3 < _len3; _key3++) {
                args[_key3 - 2] = arguments[_key3];
              }

              return emitter.emit.apply(emitter, ['' + messageName + DIRECT_EMIT_FLAG + tag].concat(args));
            }

            /**
             * 定向发送给某个组件单例的消息
             * @param {string} messageName
             * @param {function} callback
             */

          }, {
            key: 'off',
            value: function off(messageName, callback) {
              checkMessageName(messageName);
              return emitter.off(messageName, callback);
            }
          }, {
            key: 'removeListeners',
            value: function removeListeners() {
              var hasOwn = Object.prototype.hasOwnProperty.bind(this.listeners);

              for (var messageName in this.listeners) {
                if (hasOwn(messageName)) {
                  var callbacks = this.listeners[messageName] || [];
                  for (var i = 0; i < callbacks.length; i++) {
                    var callback = callbacks[i];
                    emitter.off(messageName, callback);
                    emitter.off('' + messageName + DIRECT_EMIT_FLAG + tag, callback);
                  }
                }
              }

              for (var _messageName in this.onceListeners) {
                if (hasOwn(_messageName)) {
                  var _callbacks = this.onceListeners[_messageName] || [];
                  for (var _i = 0; _i < _callbacks.length; _i++) {
                    var _callback = _callbacks[_i];
                    emitter.off(_messageName, _callback);
                    emitter.off('' + _messageName + DIRECT_EMIT_FLAG + tag, _callback);
                  }
                }
              }
            }

            /**
             * 在组件销毁时，默认将它注册的事件全部卸载，确保不会造成内存泄漏
             */

          }, {
            key: 'destroy',
            value: function destroy() {
              // step1 unlisten events
              this.removeListeners();
              this.listeners = {};
              // step2 release from context
              delete self._instanceMap[tag];
              if (_get(enhanced.prototype.__proto__ || Object.getPrototypeOf(enhanced.prototype), 'destroy', this)) {
                return _get(enhanced.prototype.__proto__ || Object.getPrototypeOf(enhanced.prototype), 'destroy', this).call(this);
              }
              this._context = null;
            }
          }, {
            key: '_player',
            get: function get() {
              if (!this._context) {
                return null;
              }
              return this._context._player;
            },
            set: function set(v) {
              if (this._context) {
                this._context._player = v;
              }
            }
          }, {
            key: '_pluginConfig',
            get: function get() {
              if (!this._context) {
                return null;
              }
              return this._context._configs;
            }
          }]);

          return enhanced;
        }(cls);
        this._clsMap[tag] = enhanced;

        /**
         * get instance immediately
         * e.g const instance = context.registry(tag, Cls)(config)
         * */
        return function () {
          for (var _len4 = arguments.length, args = Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
            args[_key4] = arguments[_key4];
          }

          return _this2.initInstance.apply(_this2, [tag].concat(args));
        };
      }

      /**
       * 各个模块处理seek
       * @param {number} time
       */

    }, {
      key: 'seek',
      value: function seek(time) {
        this._emitter.emit(EVENTS.PLAYER_EVENTS.SEEK, time);
      }

      /**
       * 对存在的实例进行
       */

    }, {
      key: 'destroyInstances',
      value: function destroyInstances() {
        var _this3 = this;

        Object.keys(this._instanceMap).forEach(function (tag) {
          if (_this3._instanceMap[tag].destroy) {
            _this3._instanceMap[tag].destroy();
          }
        });
      }

      /**
       * 编解码流程无需关注事件的解绑
       */

    }, {
      key: 'destroy',
      value: function destroy() {
        this.destroyInstances();
        if (this._emitter) {
          this._emitter.removeAllListeners();
        }
        this._emitter = null;
        this.allowedEvents = [];
        this._clsMap = null;
        this._hooks = null;
        this._player = null;
        this._configs = null;
      }

      /**
       * 对信道进行收拢
       * @param {string} messageName
       * @private
       */

    }, {
      key: '_isMessageNameValid',
      value: function _isMessageNameValid(messageName) {
        if (!this.allowedEvents.indexOf(messageName) < 0) {
          throw new Error('unregistered message name: ' + messageName);
        }
      }
    }]);

    return Context;
  }();

  var _createClass$8 = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$9(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var MSE_EVENTS$1 = EVENTS.MSE_EVENTS;

  var MSE = function () {
    function MSE(configs, context) {
      _classCallCheck$9(this, MSE);

      if (context) {
        this._context = context;
        this.emit = context._emitter.emit.bind(context._emitter);
      }

      this.TAG = 'MSE';

      this.configs = Object.assign({}, configs);
      this.container = this.configs.container;
      this.format = this.configs.format; // hls | flv
      this.mediaSource = null;
      this.sourceBuffers = {};
      this.preloadTime = this.configs.preloadTime || 1;
      this.onSourceOpen = this.onSourceOpen.bind(this);
      this.onTimeUpdate = this.onTimeUpdate.bind(this);
      this.onUpdateEnd = this.onUpdateEnd.bind(this);
      this.onWaiting = this.onWaiting.bind(this);
      this.opened = false;
    }

    _createClass$8(MSE, [{
      key: 'init',
      value: function init() {
        // eslint-disable-next-line no-undef
        this.mediaSource = new self.MediaSource();
        this.mediaSource.addEventListener('sourceopen', this.onSourceOpen);
        this._url = null;
        this.container.addEventListener('timeupdate', this.onTimeUpdate);
        this.container.addEventListener('waiting', this.onWaiting);
      }
    }, {
      key: 'resetContext',
      value: function resetContext(newCtx, keepBuffer) {
        this._context = newCtx;
        this.emit = newCtx._emitter.emit.bind(newCtx._emitter);
        if (!keepBuffer) {
          for (var i = 0; i < Object.keys(this.sourceBuffers).length; i++) {
            var buffer = this.sourceBuffers[Object.keys(this.sourceBuffers)[i]];
            if (!buffer.updating) {
              MSE.clearBuffer(buffer);
            }
          }
        }
      }
    }, {
      key: 'onTimeUpdate',
      value: function onTimeUpdate() {
        this.emit('TIME_UPDATE', this.container);
      }
    }, {
      key: 'onWaiting',
      value: function onWaiting() {
        this.emit('WAITING', this.container);
      }
    }, {
      key: 'onSourceOpen',
      value: function onSourceOpen() {
        this.opened = true;
        this.addSourceBuffers();
      }
    }, {
      key: 'onUpdateEnd',
      value: function onUpdateEnd() {
        this.emit(MSE_EVENTS$1.SOURCE_UPDATE_END);
        this.doAppend();
      }
    }, {
      key: 'addSourceBuffers',
      value: function addSourceBuffers() {
        if (!this.mediaSource || this.mediaSource.readyState !== 'open' || !this.opened) {
          return;
        }
        var sources = this._context.getInstance('PRE_SOURCE_BUFFER');
        var tracks = this._context.getInstance('TRACKS');
        var track = void 0;
        if (!sources || !tracks) {
          return;
        }

        sources = sources.sources;
        var add = false;
        for (var i = 0, k = Object.keys(sources).length; i < k; i++) {
          var type = Object.keys(sources)[i];
          add = false;
          if (type === 'audio') {
            track = tracks.audioTrack;
          } else if (type === 'video') {
            track = tracks.videoTrack;
            // return;
          }
          if (track && sources[type].init !== null && sources[type].data.length) {
            add = true;
          }
        }

        if (add) {
          if (Object.keys(this.sourceBuffers).length > 1) {
            return;
          }
          for (var _i = 0, _k = Object.keys(sources).length; _i < _k; _i++) {
            var _type = Object.keys(sources)[_i];
            if (this.sourceBuffers[_type]) {
              continue;
            }
            var source = sources[_type];
            var mime = _type === 'video' ? 'video/mp4;codecs=' + source.mimetype : 'audio/mp4;codecs=' + source.mimetype;

            try {
              // console.log('add sourcebuffer', mime);
              var sourceBuffer = this.mediaSource.addSourceBuffer(mime);
              this.sourceBuffers[_type] = sourceBuffer;
              sourceBuffer.addEventListener('updateend', this.onUpdateEnd);
            } catch (e) {
              if (e.code === 22 && Object.keys(this.sourceBuffers).length > 0) {
                return this.emit(MSE_EVENTS$1.MSE_WRONG_TRACK_ADD, _type);
              }
              this.emit(MSE_EVENTS$1.MSE_ERROR, this.TAG, new Error(e.message));
            }
          }
          if (Object.keys(this.sourceBuffers).length === this.sourceBufferLen) {
            this.doAppend();
          }
        }
      }
    }, {
      key: 'doAppend',
      value: function doAppend() {
        if (!this.mediaSource || this.mediaSource.readyState === 'closed') return;
        this._doCleanupSourceBuffer();
        var sources = this._context.getInstance('PRE_SOURCE_BUFFER');
        if (!sources) return;
        if (Object.keys(this.sourceBuffers).length < this.sourceBufferLen) {
          return;
        }
        for (var i = 0; i < Object.keys(this.sourceBuffers).length; i++) {
          var type = Object.keys(this.sourceBuffers)[i];
          var sourceBuffer = this.sourceBuffers[type];
          if (sourceBuffer.updating) continue;
          var source = sources.sources[type];
          if (this['no' + type]) {
            source.data = [];
            source.init.buffer = null;
            continue;
          }
          if (source && !source.inited) {
            try {
              // console.log('append init buffer: ', type)
              sourceBuffer.appendBuffer(source.init.buffer.buffer);
              source.inited = true;
            } catch (e) {
              // DO NOTHING
            }
          } else if (source) {
            var data = source.data.shift();
            if (data) {
              try {
                // console.log('append buffer: ', type);
                sourceBuffer.appendBuffer(data.buffer.buffer);
              } catch (e) {
                source.data.unshift(data);
              }
            }
          }
        }
      }
    }, {
      key: 'endOfStream',
      value: function endOfStream() {
        try {
          var readyState = this.mediaSource.readyState;

          if (readyState === 'open') {
            this.mediaSource.endOfStream();
          }
        } catch (e) {}
      }
    }, {
      key: 'remove',
      value: function remove(end) {
        var start = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

        try {
          for (var i = 0; i < Object.keys(this.sourceBuffers).length; i++) {
            var buffer = this.sourceBuffers[Object.keys(this.sourceBuffers)[i]];
            if (!buffer.updating) {
              if (end > start) {
                buffer.remove(start, end);
              }
            }
          }
        } catch (e) {}
      }
    }, {
      key: '_doCleanupSourceBuffer',
      value: function _doCleanupSourceBuffer() {
        var currentTime = this.container.currentTime;
        var autoCleanupMinBackwardDuration = 60 * 3;
        var _pendingRemoveRanges = {
          video: [],
          audio: []
        };
        for (var i = 0; i < Object.keys(this.sourceBuffers).length; i++) {
          var type = Object.keys(this.sourceBuffers)[i];
          var sourceBuffer = this.sourceBuffers[type];
          var buffered = sourceBuffer.buffered;
          var doRemove = false;
          for (var j = 0; j < buffered.length; j++) {
            var start = buffered.start(j);
            var end = buffered.end(j);
            if (start <= currentTime && currentTime < end + 3) {
              if (currentTime - start >= autoCleanupMinBackwardDuration) {
                doRemove = true;
                var removeEnd = currentTime - autoCleanupMinBackwardDuration;
                _pendingRemoveRanges[type].push({ start: start, end: removeEnd });
              }
            } else if (end < currentTime && currentTime - start >= autoCleanupMinBackwardDuration) {
              doRemove = true;
              _pendingRemoveRanges[type].push({ start: start, end: end });
            }
          }
          if (doRemove && !sourceBuffer.updating) {
            this._doRemoveRanges(_pendingRemoveRanges);
          }
        }
      }
    }, {
      key: '_doRemoveRanges',
      value: function _doRemoveRanges(_pendingRemoveRanges) {
        for (var type in _pendingRemoveRanges) {
          if (!this.sourceBuffers[type] || this.sourceBuffers[type].updating) {
            continue;
          }
          var sb = this.sourceBuffers[type];
          var ranges = _pendingRemoveRanges[type];
          while (ranges.length && !sb.updating) {
            var range = ranges.shift();
            try {
              if (range && range.end > range.start) {
                sb.remove(range.start, range.end);
              }
            } catch (e) {}
          }
        }
      }
    }, {
      key: 'cleanBuffers',
      value: function cleanBuffers() {
        var _this = this;

        var taskList = [];

        var _loop = function _loop(i) {
          var buffer = _this.sourceBuffers[Object.keys(_this.sourceBuffers)[i]];

          var task = void 0;
          if (buffer.updating) {
            task = new Promise(function (resolve) {
              var doCleanBuffer = function doCleanBuffer() {
                var retryTime = 3;

                var clean = function clean() {
                  if (!buffer.updating) {
                    MSE.clearBuffer(buffer);
                    buffer.addEventListener('updateend', function () {
                      resolve();
                    });
                  } else if (retryTime > 0) {
                    setTimeout(clean, 200);
                    retryTime--;
                  } else {
                    resolve();
                  }
                };

                setTimeout(clean, 200);
                buffer.removeEventListener('updateend', doCleanBuffer);
              };
              buffer.addEventListener('updateend', doCleanBuffer);
            });
          } else {
            task = new Promise(function (resolve) {
              MSE.clearBuffer(buffer);
              buffer.addEventListener('updateend', function () {
                resolve();
              });
            });

            // task = Promise.resolve()
          }

          taskList.push(task);
        };

        for (var i = 0; i < Object.keys(this.sourceBuffers).length; i++) {
          _loop(i);
        }

        return Promise.all(taskList);
      }
    }, {
      key: 'removeBuffers',
      value: function removeBuffers() {
        var _this2 = this;

        var taskList = [];

        var _loop2 = function _loop2(i) {
          var buffer = _this2.sourceBuffers[Object.keys(_this2.sourceBuffers)[i]];
          buffer.removeEventListener('updateend', _this2.onUpdateEnd);

          var task = void 0;
          if (buffer.updating) {
            task = new Promise(function (resolve) {
              var doCleanBuffer = function doCleanBuffer() {
                var retryTime = 3;

                var clean = function clean() {
                  if (!buffer.updating) {
                    MSE.clearBuffer(buffer);
                    buffer.addEventListener('updateend', function () {
                      resolve();
                    });
                  } else if (retryTime > 0) {
                    setTimeout(clean, 200);
                    retryTime--;
                  } else {
                    resolve();
                  }
                };

                setTimeout(clean, 200);
                buffer.removeEventListener('updateend', doCleanBuffer);
              };
              buffer.addEventListener('updateend', doCleanBuffer);
            });
          } else {
            task = new Promise(function (resolve) {
              MSE.clearBuffer(buffer);
              buffer.addEventListener('updateend', function () {
                resolve();
              });
            });

            // task = Promise.resolve()
          }

          taskList.push(task);
        };

        for (var i = 0; i < Object.keys(this.sourceBuffers).length; i++) {
          _loop2(i);
        }

        return Promise.all(taskList);
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        var _this3 = this;

        if (!this.container) return Promise.resolve();
        this.container.removeEventListener('timeupdate', this.onTimeUpdate);
        this.container.removeEventListener('waiting', this.onWaiting);
        this.mediaSource.removeEventListener('sourceopen', this.onSourceOpen);
        return this.removeBuffers().then(function () {
          var sources = Object.keys(_this3.sourceBuffers);
          for (var i = 0; i < sources.length; i++) {
            var _buffer = _this3.sourceBuffers[sources[i]];
            delete _this3.sourceBuffers[sources[i]];

            if (_this3.mediaSource.readyState === 'open') {
              _this3.mediaSource.removeSourceBuffer(_buffer);
            }
          }

          _this3.endOfStream();
          try {
            window.URL.revokeObjectURL(_this3.url);
          } catch (e) {}

          _this3.url = null;
          _this3.configs = {};
          _this3.container = null;
          _this3.mediaSource = null;
          _this3.sourceBuffers = {};
          _this3.preloadTime = 1;

          _this3.onSourceOpen = null;
          _this3.onTimeUpdate = null;
          _this3.onUpdateEnd = null;
          _this3.onWaiting = null;
          _this3.noaudio = undefined;
          _this3.novideo = undefined;
        });
      }
    }, {
      key: 'sourceBufferLen',
      get: function get() {
        if (!this._context.mediaInfo) {
          if (this.noaudio || this.novideo) return 1;
          return 2;
        }
        return (!!this._context.mediaInfo.hasVideo && !this.novideo) + (!!this._context.mediaInfo.hasAudio && !this.noaudio);
      }
    }, {
      key: 'url',
      set: function set(val) {
        this._url = val;
      },
      get: function get() {
        if (!this._url) {
          try {
            this._url = window.URL.createObjectURL(this.mediaSource);
          } catch (e) {}
        }
        return this._url;
      }
    }], [{
      key: 'clearBuffer',
      value: function clearBuffer(buffer) {
        try {
          var buffered = buffer.buffered;
          var bEnd = 0.1;
          for (var i = 0, len = buffered.length; i < len; i++) {
            bEnd = buffered.end(i);
          }
          buffer.remove(0, bEnd);
        } catch (e) {
          // DO NOTHING
        }
      }
    }]);

    return MSE;
  }();

  var le = function () {
    var buf = new ArrayBuffer(2);
    new DataView(buf).setInt16(0, 256, true); // little-endian write
    return new Int16Array(buf)[0] === 256; // platform-spec read, if equal then LE
  }();

  var sniffer = {
    /**
     * get device type
     * @return {'pc'|'mobile'|'tablet'}
     */
    get device() {
      var r = sniffer.os;
      return r.isPc ? 'pc' : r.isTablet ? 'tablet' : 'mobile';
    },

    get browser() {
      var ua = navigator.userAgent.toLowerCase();
      var reg = {
        ie: /rv:([\d.]+)\) like gecko/,
        firfox: /firefox\/([\d.]+)/,
        chrome: /chrome\/([\d.]+)/,
        opera: /opera.([\d.]+)/,
        safari: /version\/([\d.]+).*safari/
      };
      return [].concat(Object.keys(reg).filter(function (key) {
        return reg[key].test(ua);
      }))[0];
    },
    get os() {
      var ua = navigator.userAgent;
      var isWindowsPhone = /(?:Windows Phone)/.test(ua);
      var isSymbian = /(?:SymbianOS)/.test(ua) || isWindowsPhone;
      var isAndroid = /(?:Android)/.test(ua);
      var isFireFox = /(?:Firefox)/.test(ua);
      var isTablet = /(?:iPad|PlayBook)/.test(ua) || isAndroid && !/(?:Mobile)/.test(ua) || isFireFox && /(?:Tablet)/.test(ua);
      var isPhone = /(?:iPhone)/.test(ua) && !isTablet;
      var isPc = !isPhone && !isAndroid && !isSymbian;
      return {
        isTablet: isTablet,
        isPhone: isPhone,
        isAndroid: isAndroid,
        isPc: isPc,
        isSymbian: isSymbian,
        isWindowsPhone: isWindowsPhone,
        isFireFox: isFireFox
      };
    },

    get isLe() {
      return le;
    }
  };

  var _createClass$9 = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$a(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var Speed = function () {
    function Speed() {
      _classCallCheck$a(this, Speed);

      // milliseconds
      this._firstCheckpoint = 0;
      this._lastCheckpoint = 0;
      this._intervalBytes = 0;
      this._lastSamplingBytes = 0;

      this._now = Date.now;
    }

    _createClass$9(Speed, [{
      key: "reset",
      value: function reset() {
        this._firstCheckpoint = this._lastCheckpoint = 0;
        this._intervalBytes = 0;
        this._lastSamplingBytes = 0;
      }
    }, {
      key: "addBytes",
      value: function addBytes(bytes) {
        var duration = this._now() - this._lastCheckpoint;
        if (this._firstCheckpoint === 0) {
          this._firstCheckpoint = this._now();
          this._lastCheckpoint = this._firstCheckpoint;
          this._intervalBytes += bytes;
        } else if (duration < 5000) {
          this._intervalBytes += bytes;
        } else {
          // duration >= 1000
          this._lastSamplingBytes = this._intervalBytes / (duration / 1000);
          this._intervalBytes = bytes;
          this._lastCheckpoint = this._now();
        }
      }
    }, {
      key: "currentKBps",
      get: function get() {
        this.addBytes(0);

        var durationSeconds = (this._now() - this._lastCheckpoint) / 1000;
        if (durationSeconds === 0) durationSeconds = 1;
        return this._intervalBytes / durationSeconds / 1024;
      }
    }, {
      key: "lastSamplingKBps",
      get: function get() {
        this.addBytes(0);

        if (this._lastSamplingBytes !== 0) {
          return this._lastSamplingBytes / 1024;
        } else {
          // lastSecondBytes === 0
          if (this._now() - this._lastCheckpoint >= 500) {
            // if time interval since last checkpoint has exceeded 500ms
            // the speed is nearly accurate
            return this.currentKBps;
          } else {
            // We don't know
            return 0;
          }
        }
      }
    }]);

    return Speed;
  }();

  var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

  var _typeof$2 = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
    return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
  } : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
  };

  var _createClass$a = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$b(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var LOADER_EVENTS$1 = EVENTS.LOADER_EVENTS;
  var READ_STREAM = 0;
  var READ_TEXT = 1;
  var READ_JSON = 2;
  var READ_BUFFER = 3;

  var FetchLoader = function () {
    function FetchLoader(configs) {
      _classCallCheck$b(this, FetchLoader);

      this.configs = Object.assign({}, configs);
      this.url = null;
      this.status = 0;
      this.error = null;
      this._reader = null;
      this._canceled = false;
      this._destroyed = false;
      this.readtype = this.configs.readtype;
      this.buffer = this.configs.buffer || 'LOADER_BUFFER';
      this._loaderTaskNo = 0;
      this._ttfb = 0;
      this._speed = new Speed();
      if (window.AbortController) {
        this.abortControllerEnabled = true;
      }
    }

    _createClass$a(FetchLoader, [{
      key: 'init',
      value: function init() {
        this.on(LOADER_EVENTS$1.LADER_START, this.load.bind(this));
      }
    }, {
      key: 'fetch',

      /**
       * @param {string}      url
       * @param {RequestInit} params
       * @param {number}      timeout
       * @return {Promise<unknown>}
       */
      value: function (_fetch) {
        function fetch(_x, _x2, _x3) {
          return _fetch.apply(this, arguments);
        }

        fetch.toString = function () {
          return _fetch.toString();
        };

        return fetch;
      }(function (url, params, timeout) {
        var _this2 = this;

        var timer = null;
        if (this.abortControllerEnabled) {
          this.abortController = new window.AbortController();
        }
        Object.assign(params, { signal: this.abortController ? this.abortController.signal : undefined });
        var start = new Date().getTime();
        return Promise.race([fetch(url, params), new Promise(function (resolve, reject) {
          timer = setTimeout(function () {
            /* eslint-disable-next-line */
            reject({
              code: 999,
              message: 'fetch timeout'
            });
            if (_this2.abortController) {
              _this2.abortController.abort();
            }
          }, timeout || 1e4); // 10s
        })]).then(function (response) {
          if (timer) {
            clearTimeout(timer);
            timer = null;
          }
          var end = new Date().getTime();
          _this2.emit(LOADER_EVENTS$1.LOADER_TTFB, {
            start: start,
            end: end,
            elapsed: end - start
          });
          return response;
        });
      })

      /**
       * @param {string}      url
       * @param {RequestInit} params
       * @param {number}      retryTimes
       * @param {number}      totalRetry
       * @param {number}      delayTime
       * @return {Promise<{ok} | minimist.Opts.unknown>}
       */

    }, {
      key: 'internalLoad',
      value: function internalLoad(url, params, retryTimes, totalRetry) {
        var _this3 = this;

        var delayTime = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 0;
        var loadTimeout = arguments[5];

        if (this._destroyed) return;
        this.loading = true;
        return this.fetch(this.url, params, loadTimeout).then(function (response) {
          !_this3._destroyed && _this3.emit(LOADER_EVENTS$1.LOADER_RESPONSE_HEADERS, _this3.TAG, response.headers);

          if (response.ok) {
            _this3.status = response.status;
            Promise.resolve().then(function () {
              _this3._onFetchResponse(response);
            });

            return Promise.resolve(response);
          }

          if (retryTimes-- > 0) {
            _this3._retryTimer = setTimeout(function () {
              _this3.emit(LOADER_EVENTS$1.LOADER_RETRY, _this3.TAG, {
                response: response,
                reason: 'response not ok',
                retryTime: totalRetry - retryTimes
              });
              return _this3.internalLoad(url, params, retryTimes, totalRetry, delayTime, loadTimeout);
            }, delayTime);
          } else {
            _this3.loading = false;
            _this3.emit(LOADER_EVENTS$1.LOADER_ERROR, _this3.TAG, {
              code: response.status || 21,
              message: response.status + ' (' + response.statusText + ')'
            });
          }
        }).catch(function (error) {
          if (_this3._destroyed) {
            _this3.loading = false;
            return;
          }

          if (retryTimes-- > 0) {
            _this3._retryTimer = setTimeout(function () {
              _this3.emit(LOADER_EVENTS$1.LOADER_RETRY, _this3.TAG, {
                error: error,
                reason: 'fetch error',
                retryTime: totalRetry - retryTimes
              });
              return _this3.internalLoad(url, params, retryTimes, totalRetry, delayTime, loadTimeout);
            }, delayTime);
          } else {
            _this3.loading = false;
            if (error && error.name === 'AbortError') {
              return;
            }
            _this3.emit(LOADER_EVENTS$1.LOADER_ERROR, _this3.TAG, Object.assign({ code: 21 }, error));
          }
        });
      }

      /**
       * @param {string}      url
       * @param {RequestInit} opts
       * @param {retryCount, retryDelay, loadTimeout}  pluginConfig
       * @return {Promise<{ok} | minimist.Opts.unknown>}
       */

    }, {
      key: 'load',
      value: function load(url) {
        var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        var _ref = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            retryCount = _ref.retryCount,
            retryDelay = _ref.retryDelay,
            loadTimeout = _ref.loadTimeout;

        retryCount = retryCount === undefined ? 3 : retryCount;
        this.url = url;
        this._canceled = false;

        // TODO: Add Ranges
        var params = this.getParams(opts);

        return this.internalLoad(url, params, retryCount, retryCount, retryDelay, loadTimeout);
      }
    }, {
      key: '_onFetchResponse',
      value: function _onFetchResponse(response) {
        var _this4 = this;

        var _this = this;
        var buffer = this._context.getInstance(this.buffer);
        this._loaderTaskNo++;
        var taskno = this._loaderTaskNo;
        if (response.ok === true) {
          switch (this.readtype) {
            case READ_JSON:
              response.json().then(function (data) {
                _this.loading = false;
                if (!_this._canceled && !_this._destroyed) {
                  if (buffer) {
                    buffer.push(data);
                    _this.emit(LOADER_EVENTS$1.LOADER_COMPLETE, buffer);
                  } else {
                    _this.emit(LOADER_EVENTS$1.LOADER_COMPLETE, data);
                  }
                }
              });
              break;
            case READ_TEXT:
              response.text().then(function (data) {
                _this.loading = false;
                if (!_this._canceled && !_this._destroyed) {
                  if (buffer) {
                    buffer.push(data);
                    _this.emit(LOADER_EVENTS$1.LOADER_COMPLETE, buffer);
                  } else {
                    _this.emit(LOADER_EVENTS$1.LOADER_COMPLETE, data);
                  }
                }
              });
              break;
            case READ_BUFFER:
              response.arrayBuffer().then(function (data) {
                _this.loading = false;
                if (!_this._canceled && !_this._destroyed) {
                  if (buffer) {
                    buffer.push(new Uint8Array(data));
                    _this4._speed.addBytes(data.byteLength);
                    _this.emit(LOADER_EVENTS$1.LOADER_COMPLETE, buffer);
                  } else {
                    _this.emit(LOADER_EVENTS$1.LOADER_COMPLETE, data);
                  }
                }
              }).catch(function () {});
              break;
            case READ_STREAM:
            default:
              return this._onReader(response.body.getReader(), taskno);
          }
        }
      }
    }, {
      key: '_onReader',
      value: function _onReader(reader, taskno) {
        var _this5 = this;

        var buffer = this._context.getInstance(this.buffer);
        if (!buffer && this._reader || this._destroyed) {
          try {
            this._reader.cancel();
          } catch (e) {
            // DO NOTHING
          }
        }

        this._reader = reader;
        if (this.loading === false) {
          return;
        }

        // reader read function returns a Promise. get data when callback and has value.done when disconnected.
        // read方法返回一个Promise. 回调中可以获取到数据。当value.done存在时，说明链接断开。
        this._noDataTimer = setTimeout(function () {
          if (_this5.loading === false) return;
          _this5.emit(LOADER_EVENTS$1.LOADER_COMPLETE);
        }, 3000);
        this._reader && this._reader.read().then(function (val) {
          clearTimeout(_this5._noDataTimer);
          if (_this5._canceled || _this5._destroyed) {
            if (_this5._reader) {
              try {
                _this5._reader.cancel();
              } catch (e) {
                // DO NOTHING
              }
            }
            return;
          }
          if (val.done) {
            _this5.loading = false;
            _this5.status = 0;
            Promise.resolve().then(function () {
              _this5.emit(LOADER_EVENTS$1.LOADER_COMPLETE, buffer);
            });
            return;
          }

          buffer.push(val.value);
          _this5._speed.addBytes(val.value.byteLength);
          Promise.resolve().then(function () {
            _this5.emit(LOADER_EVENTS$1.LOADER_DATALOADED, buffer);
          });
          return _this5._onReader(reader, taskno);
        }).catch(function (error) {
          clearTimeout(_this5._noDataTimer);
          _this5.loading = false;
          if (error && error.name === 'AbortError') return;
          _this5.emit(LOADER_EVENTS$1.LOADER_ERROR, _this5.TAG, error);
        });
      }

      /**
       *
       * @param {RequestInit} opts
       * @return {{mode: string, headers: Headers, cache: string, method: string}}
       */

    }, {
      key: 'getParams',
      value: function getParams(opts) {
        var options = Object.assign({}, opts);
        var headers = new Headers();

        var params = {
          method: 'GET',
          headers: headers,
          mode: 'cors',
          cache: 'default'

          // add custmor headers
          // 添加自定义头
        };if (_typeof$2(this.configs.headers) === 'object') {
          var configHeaders = this.configs.headers;
          for (var key in configHeaders) {
            if (configHeaders.hasOwnProperty(key)) {
              headers.append(key, configHeaders[key]);
            }
          }
        }

        if (_typeof$2(options.headers) === 'object') {
          var optHeaders = options.headers;
          for (var _key in optHeaders) {
            if (optHeaders.hasOwnProperty(_key)) {
              headers.append(_key, optHeaders[_key]);
            }
          }
        }

        if (options.cors === false) {
          params.mode = 'same-origin';
        }

        // withCredentials is disabled by default
        // withCredentials 在默认情况下不被使用。
        if (options.withCredentials) {
          params.credentials = 'include';
        }

        // TODO: Add ranges;
        return params;
      }

      // in KB/s

    }, {
      key: 'cancel',
      value: function cancel() {
        if (this._reader) {
          try {
            this._reader.cancel();
          } catch (e) {
            // 防止failed: 200错误被打印到控制台上
          }
          this._reader = null;
          this.loading = false;
        }
        clearTimeout(this._noDataTimer);
        this._canceled = true;
        if (this.abortController) {
          this.abortController.abort();
        }
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        this._destroyed = true;
        clearTimeout(this._retryTimer);
        clearTimeout(this._noDataTimer);
        this.cancel();
        this._speed.reset();
      }
    }, {
      key: 'currentSpeed',
      get: function get() {
        return this._speed.lastSamplingKBps;
      }
    }], [{
      key: 'isSupported',
      value: function isSupported() {
        return !!window.fetch;
      }
    }, {
      key: 'type',
      get: function get() {
        return 'loader';
      }
    }]);

    return FetchLoader;
  }();

  var _createClass$b = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _toConsumableArray(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
        arr2[i] = arr[i];
      }return arr2;
    } else {
      return Array.from(arr);
    }
  }

  function _classCallCheck$c(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var DevLogger = function () {
    function DevLogger() {
      var _this = this;

      _classCallCheck$c(this, DevLogger);

      try {
        var matched = /xgd=(\d)/.exec(document.cookie);
        this._status = !!matched;
        this._level = matched && matched[1];
      } catch (e) {
        this._status = false;
      }

      ['group', 'groupEnd', 'log', 'warn', 'error'].forEach(function (funName) {
        _this[funName] = function (arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10) {
          var _console;

          if (!_this._status) return;
          var tagName = arg1;
          var args = [arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10].filter(function (x) {
            return x !== undefined;
          });
          (_console = console)[funName].apply(_console, ['[' + tagName + ']:'].concat(_toConsumableArray(args)));
        };
      });
    }

    /**
     * @return {*|boolean|boolean}
     */

    _createClass$b(DevLogger, [{
      key: 'enable',
      get: function get() {
        return this._status;
      }

      /**
       * @return {boolean}
       */

    }, {
      key: 'long',
      get: function get() {
        return this._level === '2';
      }
    }]);

    return DevLogger;
  }();

  var logger = new DevLogger();

  var _createClass$c = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$d(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var AAC = function () {
    function AAC() {
      _classCallCheck$d(this, AAC);
    }

    _createClass$c(AAC, null, [{
      key: 'getSilentFrame',
      value: function getSilentFrame(codec, channelCount) {
        if (codec === 'mp4a.40.2') {
          // handle LC-AAC
          if (channelCount === 1) {
            return new Uint8Array([0x00, 0xc8, 0x00, 0x80, 0x23, 0x80]);
          } else if (channelCount === 2) {
            return new Uint8Array([0x21, 0x00, 0x49, 0x90, 0x02, 0x19, 0x00, 0x23, 0x80]);
          } else if (channelCount === 3) {
            return new Uint8Array([0x00, 0xc8, 0x00, 0x80, 0x20, 0x84, 0x01, 0x26, 0x40, 0x08, 0x64, 0x00, 0x8e]);
          } else if (channelCount === 4) {
            return new Uint8Array([0x00, 0xc8, 0x00, 0x80, 0x20, 0x84, 0x01, 0x26, 0x40, 0x08, 0x64, 0x00, 0x80, 0x2c, 0x80, 0x08, 0x02, 0x38]);
          } else if (channelCount === 5) {
            return new Uint8Array([0x00, 0xc8, 0x00, 0x80, 0x20, 0x84, 0x01, 0x26, 0x40, 0x08, 0x64, 0x00, 0x82, 0x30, 0x04, 0x99, 0x00, 0x21, 0x90, 0x02, 0x38]);
          } else if (channelCount === 6) {
            return new Uint8Array([0x00, 0xc8, 0x00, 0x80, 0x20, 0x84, 0x01, 0x26, 0x40, 0x08, 0x64, 0x00, 0x82, 0x30, 0x04, 0x99, 0x00, 0x21, 0x90, 0x02, 0x00, 0xb2, 0x00, 0x20, 0x08, 0xe0]);
          }
        } else {
          // handle HE-AAC (mp4a.40.5 / mp4a.40.29)
          if (channelCount === 1) {
            // ffmpeg -y -f lavfi -i "aevalsrc=0:d=0.05" -c:a libfdk_aac -profile:a aac_he -b:a 4k output.aac && hexdump -v -e '16/1 "0x%x," "\n"' -v output.aac
            return new Uint8Array([0x1, 0x40, 0x22, 0x80, 0xa3, 0x4e, 0xe6, 0x80, 0xba, 0x8, 0x0, 0x0, 0x0, 0x1c, 0x6, 0xf1, 0xc1, 0xa, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5e]);
          } else if (channelCount === 2) {
            // ffmpeg -y -f lavfi -i "aevalsrc=0|0:d=0.05" -c:a libfdk_aac -profile:a aac_he_v2 -b:a 4k output.aac && hexdump -v -e '16/1 "0x%x," "\n"' -v output.aac
            return new Uint8Array([0x1, 0x40, 0x22, 0x80, 0xa3, 0x5e, 0xe6, 0x80, 0xba, 0x8, 0x0, 0x0, 0x0, 0x0, 0x95, 0x0, 0x6, 0xf1, 0xa1, 0xa, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5e]);
          } else if (channelCount === 3) {
            // ffmpeg -y -f lavfi -i "aevalsrc=0|0|0:d=0.05" -c:a libfdk_aac -profile:a aac_he_v2 -b:a 4k output.aac && hexdump -v -e '16/1 "0x%x," "\n"' -v output.aac
            return new Uint8Array([0x1, 0x40, 0x22, 0x80, 0xa3, 0x5e, 0xe6, 0x80, 0xba, 0x8, 0x0, 0x0, 0x0, 0x0, 0x95, 0x0, 0x6, 0xf1, 0xa1, 0xa, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5a, 0x5e]);
          }
        }
        return null;
      }
    }]);

    return AAC;
  }();

  var _createClass$d = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$e(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var Golomb = function () {
    /**
     * @param {Uint8Array} uint8array
     */
    function Golomb(uint8array) {
      _classCallCheck$e(this, Golomb);

      this.TAG = 'Golomb';
      this._buffer = uint8array;
      this._bufferIndex = 0;
      this._totalBytes = uint8array.byteLength;
      this._totalBits = uint8array.byteLength * 8;
      this._currentWord = 0;
      this._currentWordBitsLeft = 0;
    }

    _createClass$d(Golomb, [{
      key: 'destroy',
      value: function destroy() {
        this._buffer = null;
      }
    }, {
      key: '_fillCurrentWord',
      value: function _fillCurrentWord() {
        var bufferBytesLeft = this._totalBytes - this._bufferIndex;

        var bytesRead = Math.min(4, bufferBytesLeft);
        var word = new Uint8Array(4);
        word.set(this._buffer.subarray(this._bufferIndex, this._bufferIndex + bytesRead));
        this._currentWord = new DataView(word.buffer).getUint32(0);

        this._bufferIndex += bytesRead;
        this._currentWordBitsLeft = bytesRead * 8;
      }

      /**
       * @param size
       * @return {number|*|number}
       */

    }, {
      key: 'readBits',
      value: function readBits(size) {
        var bits = Math.min(this._currentWordBitsLeft, size); // :uint
        var valu = this._currentWord >>> 32 - bits;
        if (size > 32) {
          throw new Error('Cannot read more than 32 bits at a time');
        }
        this._currentWordBitsLeft -= bits;
        if (this._currentWordBitsLeft > 0) {
          this._currentWord <<= bits;
        } else if (this._totalBytes - this._bufferIndex > 0) {
          this._fillCurrentWord();
        }

        bits = size - bits;
        if (bits > 0 && this._currentWordBitsLeft) {
          return valu << bits | this.readBits(bits);
        } else {
          return valu;
        }
      }

      /**
       * @return {boolean}
       */

    }, {
      key: 'readBool',
      value: function readBool() {
        return this.readBits(1) === 1;
      }

      /**
       * @return {*|number}
       */

    }, {
      key: 'readByte',
      value: function readByte() {
        return this.readBits(8);
      }
    }, {
      key: '_skipLeadingZero',
      value: function _skipLeadingZero() {
        var zeroCount = void 0;
        for (zeroCount = 0; zeroCount < this._currentWordBitsLeft; zeroCount++) {
          if ((this._currentWord & 0x80000000 >>> zeroCount) !== 0) {
            this._currentWord <<= zeroCount;
            this._currentWordBitsLeft -= zeroCount;
            return zeroCount;
          }
        }
        this._fillCurrentWord();
        return zeroCount + this._skipLeadingZero();
      }

      /**
       * @return {number}
       */

    }, {
      key: 'readUEG',
      value: function readUEG() {
        // unsigned exponential golomb
        var leadingZeros = this._skipLeadingZero();
        return this.readBits(leadingZeros + 1) - 1;
      }

      /**
       * @return {number}
       */

    }, {
      key: 'readSEG',
      value: function readSEG() {
        // signed exponential golomb
        var value = this.readUEG();
        if (value & 0x01) {
          return value + 1 >>> 1;
        } else {
          return -1 * (value >>> 1);
        }
      }
    }, {
      key: 'readSliceType',
      value: function readSliceType() {
        // skip NALu type Nal unit header 8bit
        this.readByte();
        // discard first_mb_in_slice
        this.readUEG();
        // return slice_type
        return this.readUEG();
      }
    }]);

    return Golomb;
  }();

  var _createClass$e = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$f(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var SPSParser = function () {
    function SPSParser() {
      _classCallCheck$f(this, SPSParser);
    }

    _createClass$e(SPSParser, null, [{
      key: '_ebsp2rbsp',

      /**
       * 0x00000300  -> 0x000000
       * 0x00000301  -> 0x000001
       * 0x00000302  -> 0x000002
       * 0x00000303  -> 0x000003
       */
      value: function _ebsp2rbsp(uint8array) {
        var src = uint8array;
        var srcLength = src.byteLength;
        var dst = new Uint8Array(srcLength);
        var dstIdx = 0;

        for (var i = 0; i < srcLength; i++) {
          if (i >= 2) {
            if (src[i] === 0x03 && src[i - 1] === 0x00 && src[i - 2] === 0x00) {
              continue;
            }
          }
          dst[dstIdx] = src[i];
          dstIdx++;
        }

        return new Uint8Array(dst.buffer, 0, dstIdx);
      }
    }, {
      key: 'parseSPS',
      value: function parseSPS(uint8array) {
        var rbsp = SPSParser._ebsp2rbsp(uint8array);
        var gb = new Golomb(rbsp);

        gb.readByte();
        var profileIdc = gb.readByte();
        gb.readByte();
        var levelIdc = gb.readByte();
        gb.readUEG();

        var profile_string = SPSParser.getProfileString(profileIdc);
        var level_string = SPSParser.getLevelString(levelIdc);
        var chroma_format_idc = 1;
        var chroma_format = 420;
        var chroma_format_table = [0, 420, 422, 444];
        var bit_depth = 8;

        if (profileIdc === 100 || profileIdc === 110 || profileIdc === 122 || profileIdc === 244 || profileIdc === 44 || profileIdc === 83 || profileIdc === 86 || profileIdc === 118 || profileIdc === 128 || profileIdc === 138 || profileIdc === 144) {
          chroma_format_idc = gb.readUEG();
          if (chroma_format_idc === 3) {
            gb.readBits(1);
          }
          if (chroma_format_idc <= 3) {
            chroma_format = chroma_format_table[chroma_format_idc];
          }

          bit_depth = gb.readUEG() + 8;
          gb.readUEG();
          gb.readBits(1);
          if (gb.readBool()) {
            var scaling_list_count = chroma_format_idc !== 3 ? 8 : 12;
            for (var i = 0; i < scaling_list_count; i++) {
              if (gb.readBool()) {
                if (i < 6) {
                  SPSParser._skipScalingList(gb, 16);
                } else {
                  SPSParser._skipScalingList(gb, 64);
                }
              }
            }
          }
        }
        gb.readUEG();
        var pic_order_cnt_type = gb.readUEG();
        if (pic_order_cnt_type === 0) {
          gb.readUEG();
        } else if (pic_order_cnt_type === 1) {
          gb.readBits(1);
          gb.readSEG();
          gb.readSEG();
          var num_ref_frames_in_pic_order_cnt_cycle = gb.readUEG();
          for (var _i = 0; _i < num_ref_frames_in_pic_order_cnt_cycle; _i++) {
            gb.readSEG();
          }
        }
        gb.readUEG();
        gb.readBits(1);

        var pic_width_in_mbs_minus1 = gb.readUEG();
        var pic_height_in_map_units_minus1 = gb.readUEG();

        var frame_mbs_only_flag = gb.readBits(1);
        if (frame_mbs_only_flag === 0) {
          gb.readBits(1);
        }
        gb.readBits(1);

        var frame_crop_left_offset = 0;
        var frame_crop_right_offset = 0;
        var frame_crop_top_offset = 0;
        var frame_crop_bottom_offset = 0;

        var frame_cropping_flag = gb.readBool();
        if (frame_cropping_flag) {
          frame_crop_left_offset = gb.readUEG();
          frame_crop_right_offset = gb.readUEG();
          frame_crop_top_offset = gb.readUEG();
          frame_crop_bottom_offset = gb.readUEG();
        }

        var par_width = 1;var par_height = 1;
        var fps = 0;var fps_fixed = true;var fps_num = 0;var fps_den = 0;

        var vui_parameters_present_flag = gb.readBool();
        if (vui_parameters_present_flag) {
          if (gb.readBool()) {
            // aspect_ratio_info_present_flag
            var aspect_ratio_idc = gb.readByte();
            var par_w_table = [1, 12, 10, 16, 40, 24, 20, 32, 80, 18, 15, 64, 160, 4, 3, 2];
            var par_h_table = [1, 11, 11, 11, 33, 11, 11, 11, 33, 11, 11, 33, 99, 3, 2, 1];

            if (aspect_ratio_idc > 0 && aspect_ratio_idc < 16) {
              par_width = par_w_table[aspect_ratio_idc - 1];
              par_height = par_h_table[aspect_ratio_idc - 1];
            } else if (aspect_ratio_idc === 255) {
              par_width = gb.readByte() << 8 | gb.readByte();
              par_height = gb.readByte() << 8 | gb.readByte();
            }
          }

          if (gb.readBool()) {
            gb.readBool();
          }
          if (gb.readBool()) {
            gb.readBits(4);
            if (gb.readBool()) {
              gb.readBits(24);
            }
          }
          if (gb.readBool()) {
            gb.readUEG();
            gb.readUEG();
          }
          if (gb.readBool()) {
            var num_units_in_tick = gb.readBits(32);
            var time_scale = gb.readBits(32);
            fps_fixed = gb.readBool();

            fps_num = time_scale;
            fps_den = num_units_in_tick * 2;
            fps = fps_num / fps_den;
          }
        }

        var parScale = 1;
        if (par_width !== 1 || par_height !== 1) {
          parScale = par_width / par_height;
        }

        var crop_unit_x = 0;var crop_unit_y = 0;
        if (chroma_format_idc === 0) {
          crop_unit_x = 1;
          crop_unit_y = 2 - frame_mbs_only_flag;
        } else {
          var sub_wc = chroma_format_idc === 3 ? 1 : 2;
          var sub_hc = chroma_format_idc === 1 ? 2 : 1;
          crop_unit_x = sub_wc;
          crop_unit_y = sub_hc * (2 - frame_mbs_only_flag);
        }

        var codec_width = (pic_width_in_mbs_minus1 + 1) * 16;
        var codec_height = (2 - frame_mbs_only_flag) * ((pic_height_in_map_units_minus1 + 1) * 16);

        codec_width -= (frame_crop_left_offset + frame_crop_right_offset) * crop_unit_x;
        codec_height -= (frame_crop_top_offset + frame_crop_bottom_offset) * crop_unit_y;

        var present_width = Math.ceil(codec_width * parScale);

        gb.destroy();
        gb = null;

        return {
          profile_string: profile_string,
          level_string: level_string,
          bit_depth: bit_depth,
          chroma_format: chroma_format,
          chroma_format_string: SPSParser.getChromaFormatString(chroma_format),

          frame_rate: {
            fixed: fps_fixed,
            fps: fps,
            fps_den: fps_den,
            fps_num: fps_num
          },

          par_ratio: {
            width: par_width,
            height: par_height
          },

          codec_size: {
            width: codec_width,
            height: codec_height
          },

          present_size: {
            width: present_width,
            height: codec_height
          }
        };
      }
    }, {
      key: '_skipScalingList',
      value: function _skipScalingList(gb, count) {
        var lastScale = 8;
        var nextScale = 8;
        var deltaScale = 0;
        for (var i = 0; i < count; i++) {
          if (nextScale !== 0) {
            deltaScale = gb.readSEG();
            nextScale = (lastScale + deltaScale + 256) % 256;
          }
          lastScale = nextScale === 0 ? lastScale : nextScale;
        }
      }
    }, {
      key: 'getProfileString',
      value: function getProfileString(profileIdc) {
        switch (profileIdc) {
          case 66:
            return 'Baseline';
          case 77:
            return 'Main';
          case 88:
            return 'Extended';
          case 100:
            return 'High';
          case 110:
            return 'High10';
          case 122:
            return 'High422';
          case 244:
            return 'High444';
          default:
            return 'Unknown';
        }
      }
    }, {
      key: 'getLevelString',
      value: function getLevelString(levelIdc) {
        return (levelIdc / 10).toFixed(1);
      }
    }, {
      key: 'getChromaFormatString',
      value: function getChromaFormatString(chroma) {
        switch (chroma) {
          case 420:
            return '4:2:0';
          case 422:
            return '4:2:2';
          case 444:
            return '4:4:4';
          default:
            return 'Unknown';
        }
      }
    }, {
      key: 'toVideoMeta',
      value: function toVideoMeta(spsConfig) {
        var meta = {};
        if (spsConfig && spsConfig.codec_size) {
          meta.codecWidth = spsConfig.codec_size.width;
          meta.codecHeight = spsConfig.codec_size.height;
          meta.presentWidth = spsConfig.present_size.width;
          meta.presentHeight = spsConfig.present_size.height;
        }

        meta.profile = spsConfig.profile_string;
        meta.level = spsConfig.level_string;
        meta.bitDepth = spsConfig.bit_depth;
        meta.chromaFormat = spsConfig.chroma_format;

        meta.parRatio = {
          width: spsConfig.par_ratio.width,
          height: spsConfig.par_ratio.height
        };

        meta.frameRate = spsConfig.frame_rate;

        var fpsDen = meta.frameRate.fps_den;
        var fpsNum = meta.frameRate.fps_num;
        meta.refSampleDuration = Math.floor(meta.timescale * (fpsDen / fpsNum));
        return meta;
      }
    }]);

    return SPSParser;
  }();

  var _createClass$f = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$g(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var RBSP = function () {
    function RBSP() {
      _classCallCheck$g(this, RBSP);
    }

    _createClass$f(RBSP, null, [{
      key: "EBSP2RBSP",

      /**
       * convert EBSP to RBSP
       * @param data {Uint8Array}
       * @returns {Uint8Array}
       * @constructor
       */
      value: function EBSP2RBSP(data) {
        return data.filter(function (el, idx) {
          if (idx < 2) {
            return true;
          } else {
            return !(data[idx - 2] === 0 && data[idx - 1] === 0 && el === 3);
          }
        });
      }

      /**
       * EBSP = SODB + '10000000'
       * @param data {Uint8Array}
       * @constructor
       */

    }, {
      key: "EBSP2SODB",
      value: function EBSP2SODB(data) {
        var lastByte = data[data.byteLength - 1];
        if (lastByte && lastByte === 128) {
          return data.slice(0, data.byteLength - 1);
        }

        return data;
      }
    }]);

    return RBSP;
  }();

  var _createClass$g = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$h(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var u8aToString = function u8aToString(data) {
    var result = '';
    for (var i = 0; i < data.byteLength; i++) {
      result += String.fromCharCode(data[i]);
    }
    return result;
  };

  var SEIParser = function () {
    function SEIParser() {
      _classCallCheck$h(this, SEIParser);
    }

    _createClass$g(SEIParser, null, [{
      key: '_resolveNalu',
      value: function _resolveNalu(data) {
        if (data.length >= 1) {
          return RBSP.EBSP2SODB(RBSP.EBSP2RBSP(data.slice(1)));
        }
        return null;
      }
      /**
       *
       * @param data {Uint8Array}
       */

    }, {
      key: 'parse',
      value: function parse(data) {
        var sodb = SEIParser._resolveNalu(data);

        var _SEIParser$switchPayl = SEIParser.switchPayloadType(sodb),
            payloadType = _SEIParser$switchPayl.payloadType,
            offset = _SEIParser$switchPayl.offset;

        var content = sodb.slice(offset);

        switch (payloadType) {
          case 5:
            return SEIParser.user_data_unregistered(content);
          default:
            return {
              code: payloadType,
              content: content
            };
        }
      }

      /**
       *
       * @param data
       * @returns {{payloadType: number, offset: number}}
       */

    }, {
      key: 'switchPayloadType',
      value: function switchPayloadType(data) {
        var dv = new DataView(data.buffer);
        var payloadType = 0;
        var offset = 0;
        while (dv.getUint8(offset) === 255) {
          offset++;
          payloadType += 255;
        }
        payloadType += dv.getUint8(offset++);

        return {
          payloadType: payloadType,
          offset: offset
        };
      }

      /**
       *
       * @param data {Uint8Array}
       * @return {{ payloadLength: number, offset: number }}
       */

    }, {
      key: 'getPayloadLength',
      value: function getPayloadLength(data) {
        var dv = new DataView(data.buffer);

        var payloadLength = 0;
        var offset = 0;
        while (dv.getUint8(offset) === 255) {
          offset++;
          payloadLength += 255;
        }
        payloadLength += dv.getUint8(offset++);

        return {
          payloadLength: payloadLength,
          offset: offset
        };
      }

      /**
       * resolve 0x05 user data unregistered
       * @param data {Uint8Array}
       */
      // eslint-disable-next-line camelcase

    }, {
      key: 'user_data_unregistered',
      value: function user_data_unregistered(data) {
        var _SEIParser$getPayload = SEIParser.getPayloadLength(data),
            payloadLength = _SEIParser$getPayload.payloadLength,
            offset = _SEIParser$getPayload.offset;

        if (payloadLength < 16) {
          return {
            uuid: '',
            content: null
          };
        }
        var payload = data.slice(offset);

        var uuid = u8aToString(payload.slice(0, 16));
        var content = u8aToString(payload.slice(16, payloadLength));

        return {
          code: 5, // for user data unregistered
          uuid: uuid,
          content: content
        };
      }
    }]);

    return SEIParser;
  }();

  var _createClass$h = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$i(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var Nalunit = function () {
    function Nalunit() {
      _classCallCheck$i(this, Nalunit);
    }

    _createClass$h(Nalunit, null, [{
      key: 'getNalunits',

      // https://en.wikipedia.org/wiki/Network_Abstraction_Layer
      value: function getNalunits(buffer) {
        if (buffer.length - buffer.position < 4) {
          return [];
        }

        var buf = buffer.dataview;
        var position = buffer.position;
        // 0x001 || 0x0001
        if (buf.getInt32(position) === 1 || buf.getInt16(position) === 0 && buf.getInt8(position + 2) === 1) {
          return Nalunit.getAnnexbNals(buffer);
        } else {
          return Nalunit.getAvccNals(buffer);
        }
      }
    }, {
      key: 'getAnnexbNals',
      value: function getAnnexbNals(buffer) {
        var nals = [];
        var position = Nalunit.getHeaderPositionAnnexB(buffer);
        var start = position.pos;
        var end = start;
        while (start < buffer.length - 4) {
          var header = buffer.buffer.slice(start, start + position.headerLength);
          if (position.pos === buffer.position) {
            buffer.skip(position.headerLength);
          }
          position = Nalunit.getHeaderPositionAnnexB(buffer);
          end = position.pos;
          var body = new Uint8Array(buffer.buffer.slice(start + header.byteLength, end));
          var unit = { header: header, body: body };
          Nalunit.analyseNal(unit);
          if (unit.type <= 9 && unit.type !== 0) {
            nals.push(unit);
          }
          buffer.skip(end - buffer.position);
          start = end;
        }
        return nals;
      }
    }, {
      key: 'getAvccNals',
      value: function getAvccNals(buffer) {
        // buffer.buffer = RBSP.EBSP2RBSP(new Uint8Array(buffer.buffer)).buffer;
        // buffer.dataview = new DataView(buffer.buffer)
        // buffer.dataview.position = 0;
        var nals = [];
        while (buffer.position < buffer.length - 4) {
          var length = buffer.dataview.getInt32(buffer.dataview.position);
          if (buffer.length - buffer.position >= length) {
            var header = buffer.buffer.slice(buffer.position, buffer.position + 4);
            buffer.skip(4);
            var body = new Uint8Array(buffer.buffer.slice(buffer.position, buffer.position + length));
            buffer.skip(length);
            var unit = { header: header, body: body };
            Nalunit.analyseNal(unit);
            if (unit.type <= 9 && unit.type !== 0) {
              nals.push(unit);
            }
          } else {
            break;
          }
        }
        return nals;
      }

      // * ISO-14496-10 7.3.1
      // *  forbidden_zero_bit  1bit
      // *  nal_ref_idc  2bit
      // *  nal_unit_type 5bit

    }, {
      key: 'analyseNal',
      value: function analyseNal(unit) {
        var type = unit.body[0] & 0x1f;
        unit.type = type;
        switch (type) {
          case 1:
            // NDR
            unit.ndr = true;
            break;
          case 5:
            // IDR
            unit.idr = true;
            break;
          case 6:
            // SEI
            try {
              unit.sei = SEIParser.parse(unit.body);
            } catch (e) {}
            break;
          case 7:
            // SPS
            unit.sps = SPSParser.parseSPS(unit.body);
            break;
          case 8:
            // PPS
            unit.pps = true;
            break;
        }
      }
    }, {
      key: 'getHeaderPositionAnnexB',
      value: function getHeaderPositionAnnexB(buffer) {
        // seperate
        var pos = buffer.position;
        var headerLength = 0;
        var bufferLen = buffer.length;
        while (headerLength !== 3 && headerLength !== 4 && pos < bufferLen - 4) {
          if (buffer.dataview.getInt16(pos) === 0) {
            if (buffer.dataview.getInt16(pos + 2) === 1) {
              // 0x000001
              headerLength = 4;
            } else if (buffer.dataview.getInt8(pos + 2) === 1) {
              headerLength = 3;
            } else {
              pos++;
            }
          } else {
            pos++;
          }
        }

        if (pos === bufferLen - 4) {
          if (buffer.dataview.getInt16(pos) === 0) {
            if (buffer.dataview.getInt16(pos + 2) === 1) {
              // 0x000001
              headerLength = 4;
            } else {
              pos = bufferLen;
            }
          } else {
            pos++;
            if (buffer.dataview.getInt16(pos) === 0 && buffer.dataview.getInt8(pos) === 1) {
              // 0x0000001
              headerLength = 3;
            } else {
              pos = bufferLen;
            }
          }
        }
        return { pos: pos, headerLength: headerLength };
      }

      // 组装AvcDecoderConfigurationRecord
      // *  configurationVerison = 1  uint(8)
      // *  avcProfileIndication      uint(8)
      // *  profile_compatibility     uint(8)
      // *  avcLevelIndication        uint(8)
      // *  reserved   `111111`       bit(6)
      // *  lengthSizeMinusOne        uint(2)
      // *  reserved   `111`          bit(3)
      // *  numOfSPS                  uint(5)
      // *  for(numOfSPS)
      // *    spsLength               uint(16)
      // *    spsNALUnit              spsLength个字节
      // *  numOfPPS                  uint(8)
      // *  for(numOfPPS)
      // *     ppsLength              uint(16)
      // *     ppsNALUnit             ppsLength个字节

    }, {
      key: 'getAvcc',
      value: function getAvcc(sps, pps) {
        var ret = new Uint8Array(sps.byteLength + pps.byteLength + 11);
        ret[0] = 0x01;
        ret[1] = sps[1];
        ret[2] = sps[2];
        ret[3] = sps[3];
        ret[4] = 255;
        ret[5] = 225; // 11100001

        var offset = 6;

        ret.set(new Uint8Array([sps.byteLength >>> 8 & 0xff, sps.byteLength & 0xff]), offset);
        offset += 2;
        ret.set(sps, offset);
        offset += sps.byteLength;

        ret[offset] = 1;
        offset++;

        ret.set(new Uint8Array([pps.byteLength >>> 8 & 0xff, pps.byteLength & 0xff]), offset);
        offset += 2;
        ret.set(pps, offset);
        return ret;
      }
    }]);

    return Nalunit;
  }();

  var _createClass$i = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$j(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var Golomb$1 = function () {
    /**
     * @param {Uint8Array} uint8array
     */
    function Golomb(uint8array) {
      _classCallCheck$j(this, Golomb);

      this.TAG = 'Golomb';
      this._buffer = uint8array;
      this._bufferIndex = 0;
      this._totalBytes = uint8array.byteLength;
      this._totalBits = uint8array.byteLength * 8;
      this._currentWord = 0;
      this._currentWordBitsLeft = 0;
    }

    _createClass$i(Golomb, [{
      key: 'destroy',
      value: function destroy() {
        this._buffer = null;
      }
    }, {
      key: '_fillCurrentWord',
      value: function _fillCurrentWord() {
        var bufferBytesLeft = this._totalBytes - this._bufferIndex;

        var bytesRead = Math.min(4, bufferBytesLeft);
        var word = new Uint8Array(4);
        word.set(this._buffer.subarray(this._bufferIndex, this._bufferIndex + bytesRead));
        this._currentWord = new DataView(word.buffer).getUint32(0);

        this._bufferIndex += bytesRead;
        this._currentWordBitsLeft = bytesRead * 8;
      }

      /**
       * @param size
       * @return {number|*|number}
       */

    }, {
      key: 'readBits',
      value: function readBits(size) {
        var bits = Math.min(this._currentWordBitsLeft, size); // :uint
        var valu = this._currentWord >>> 32 - bits;
        if (size > 32) {
          throw new Error('Cannot read more than 32 bits at a time');
        }
        this._currentWordBitsLeft -= bits;
        if (this._currentWordBitsLeft > 0) {
          this._currentWord <<= bits;
        } else if (this._totalBytes - this._bufferIndex > 0) {
          this._fillCurrentWord();
        }

        bits = size - bits;
        if (bits > 0 && this._currentWordBitsLeft) {
          return valu << bits | this.readBits(bits);
        } else {
          return valu;
        }
      }

      /**
       * @return {boolean}
       */

    }, {
      key: 'readBool',
      value: function readBool() {
        return this.readBits(1) === 1;
      }

      /**
       * @return {*|number}
       */

    }, {
      key: 'readByte',
      value: function readByte() {
        return this.readBits(8);
      }
    }, {
      key: '_skipLeadingZero',
      value: function _skipLeadingZero() {
        var zeroCount = void 0;
        for (zeroCount = 0; zeroCount < this._currentWordBitsLeft; zeroCount++) {
          if ((this._currentWord & 0x80000000 >>> zeroCount) !== 0) {
            this._currentWord <<= zeroCount;
            this._currentWordBitsLeft -= zeroCount;
            return zeroCount;
          }
        }
        this._fillCurrentWord();
        return zeroCount + this._skipLeadingZero();
      }

      /**
       * @return {number}
       */

    }, {
      key: 'readUEG',
      value: function readUEG() {
        // unsigned exponential golomb
        var leadingZeros = this._skipLeadingZero();
        return this.readBits(leadingZeros + 1) - 1;
      }

      /**
       * @return {number}
       */

    }, {
      key: 'readSEG',
      value: function readSEG() {
        // signed exponential golomb
        var value = this.readUEG();
        if (value & 0x01) {
          return value + 1 >>> 1;
        } else {
          return -1 * (value >>> 1);
        }
      }
    }, {
      key: 'readSliceType',
      value: function readSliceType() {
        // skip NALu type Nal unit header 8bit
        this.readByte();
        // discard first_mb_in_slice
        this.readUEG();
        // return slice_type
        return this.readUEG();
      }
    }]);

    return Golomb;
  }();

  var _createClass$j = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$k(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var SPSParser$1 = function () {
    function SPSParser() {
      _classCallCheck$k(this, SPSParser);
    }

    _createClass$j(SPSParser, null, [{
      key: '_ebsp2rbsp',

      /**
       *
       * @param {Uint8Array} uint8array
       * @return {Uint8Array}
       */
      value: function _ebsp2rbsp(uint8array) {
        var src = uint8array;
        var srcLength = src.byteLength;
        var dst = new Uint8Array(srcLength);
        var dstIdx = 0;

        for (var i = 0; i < srcLength; i++) {
          if (i >= 2) {
            if (src[i] === 0x03 && src[i - 1] === 0x00 && src[i - 2] === 0x00) {
              continue;
            }
          }
          dst[dstIdx] = src[i];
          dstIdx++;
        }

        return new Uint8Array(dst.buffer, 0, dstIdx);
      }

      /**
       * @param {Uint8Array} uint8array
       * @return {{width: *, general_profile_idc: number, chromaFormatIdc: *, general_level_idc: number, general_tier_flag: number, bitDepthLumaMinus8: (*|number), bitDepthChromaMinus8: (*|number), general_profile_space: number, height: *}}
       */

    }, {
      key: 'parseSPS',
      value: function parseSPS(uint8array) {
        var rbsp = SPSParser._ebsp2rbsp(uint8array);
        var gb = new Golomb$1(rbsp);

        var vpsId = 0;
        var maxSubLayersMinus1 = 0;
        var tINf = 0;
        var spsId = 0;
        var separate_colour_plane_flag = 0;
        var chromaFormatIdc = 0;
        var width = 0;
        var height = 0;
        var conf_win_left_offset = 0;
        var conf_win_right_offset = 0;
        var conf_win_top_offset = 0;
        var conf_win_bottom_offset = 0;
        var conformanceWindowFlag = 0;
        var bitDepthLumaMinus8 = 0;
        var bitDepthChromaMinus8 = 0;
        var sub_width_c = 0;
        var sub_height_c = 0;
        var profileTierLevel = {};

        gb.readByte(); // NAL header
        gb.readByte();

        vpsId = gb.readBits(4); // vps_id
        maxSubLayersMinus1 = gb.readBits(3); // max_sub_layers_minus1
        tINf = gb.readBits(1); // temporal_id_nesting_flag

        profileTierLevel = SPSParser._readProfileTierLevel(gb, maxSubLayersMinus1);

        spsId = gb.readUEG(); // sps id
        chromaFormatIdc = gb.readUEG();
        if (chromaFormatIdc === 3) {
          separate_colour_plane_flag = gb.readBits(1); // separate_colour_plane_flag
        }

        width = gb.readUEG(); // pic_width_in_luma_samples
        height = gb.readUEG(); // pic_height_in_luma_samples

        conformanceWindowFlag = gb.readBits(1);
        if (conformanceWindowFlag === 1) {
          conf_win_left_offset = gb.readUEG(); // conf_win_left_offset
          conf_win_right_offset = gb.readUEG(); // conf_win_right_offset
          conf_win_top_offset = gb.readUEG(); // conf_win_top_offset
          conf_win_bottom_offset = gb.readUEG(); // conf_win_bottom_offset
        }

        bitDepthLumaMinus8 = gb.readUEG(); // bit_depth_luma_minus8
        bitDepthChromaMinus8 = gb.readUEG(); // bit_depth_chroma_minus8

        if (conformanceWindowFlag === 1) {
          sub_width_c = (chromaFormatIdc === 1 || chromaFormatIdc === 2) && separate_colour_plane_flag === 0 ? 2 : 1;
          sub_height_c = chromaFormatIdc === 1 && separate_colour_plane_flag === 0 ? 2 : 1;
          width -= sub_width_c * conf_win_right_offset + sub_width_c * conf_win_left_offset;
          height -= sub_height_c * conf_win_bottom_offset + sub_height_c * conf_win_top_offset;
        }

        gb.destroy();
        gb = null;

        return { width: width,
          height: height,
          general_profile_space: profileTierLevel.general_profile_space,
          general_tier_flag: profileTierLevel.general_tier_flag,
          general_profile_idc: profileTierLevel.general_profile_idc,
          general_level_idc: profileTierLevel.general_level_idc,
          chromaFormatIdc: chromaFormatIdc,
          bitDepthLumaMinus8: bitDepthLumaMinus8,
          bitDepthChromaMinus8: bitDepthChromaMinus8 };
      }

      // static parseSPS (uint8array) {
      //   let rbsp = SPSParser._ebsp2rbsp(uint8array)
      //   let gb = new Golomb(rbsp)
      //
      //   gb.readByte()
      //   let profileIdc = gb.readByte()
      //   gb.readByte()
      //   let levelIdc = gb.readByte()
      //   gb.readUEG()
      //
      //   let profile_string = SPSParser.getProfileString(profileIdc)
      //   let level_string = SPSParser.getLevelString(levelIdc)
      //   let chroma_format_idc = 1
      //   let chroma_format = 420
      //   let chroma_format_table = [0, 420, 422, 444]
      //   let bit_depth = 8
      //
      //   if (profileIdc === 100 || profileIdc === 110 || profileIdc === 122 ||
      //     profileIdc === 244 || profileIdc === 44 || profileIdc === 83 ||
      //     profileIdc === 86 || profileIdc === 118 || profileIdc === 128 ||
      //     profileIdc === 138 || profileIdc === 144) {
      //     chroma_format_idc = gb.readUEG()
      //     if (chroma_format_idc === 3) {
      //       gb.readBits(1)
      //     }
      //     if (chroma_format_idc <= 3) {
      //       chroma_format = chroma_format_table[chroma_format_idc]
      //     }
      //
      //     bit_depth = gb.readUEG() + 8
      //     gb.readUEG()
      //     gb.readBits(1)
      //     if (gb.readBool()) {
      //       let scaling_list_count = (chroma_format_idc !== 3) ? 8 : 12
      //       for (let i = 0; i < scaling_list_count; i++) {
      //         if (gb.readBool()) {
      //           if (i < 6) {
      //             SPSParser._skipScalingList(gb, 16)
      //           } else {
      //             SPSParser._skipScalingList(gb, 64)
      //           }
      //         }
      //       }
      //     }
      //   }
      //   gb.readUEG()
      //   let pic_order_cnt_type = gb.readUEG()
      //   if (pic_order_cnt_type === 0) {
      //     gb.readUEG()
      //   } else if (pic_order_cnt_type === 1) {
      //     gb.readBits(1)
      //     gb.readSEG()
      //     gb.readSEG()
      //     let num_ref_frames_in_pic_order_cnt_cycle = gb.readUEG()
      //     for (let i = 0; i < num_ref_frames_in_pic_order_cnt_cycle; i++) {
      //       gb.readSEG()
      //     }
      //   }
      //   gb.readUEG()
      //   gb.readBits(1)
      //
      //   let pic_width_in_mbs_minus1 = gb.readUEG()
      //   let pic_height_in_map_units_minus1 = gb.readUEG()
      //
      //   let frame_mbs_only_flag = gb.readBits(1)
      //   if (frame_mbs_only_flag === 0) {
      //     gb.readBits(1)
      //   }
      //   gb.readBits(1)
      //
      //   let frame_crop_left_offset = 0
      //   let frame_crop_right_offset = 0
      //   let frame_crop_top_offset = 0
      //   let frame_crop_bottom_offset = 0
      //
      //   let frame_cropping_flag = gb.readBool()
      //   if (frame_cropping_flag) {
      //     frame_crop_left_offset = gb.readUEG()
      //     frame_crop_right_offset = gb.readUEG()
      //     frame_crop_top_offset = gb.readUEG()
      //     frame_crop_bottom_offset = gb.readUEG()
      //   }
      //
      //   let par_width = 1, par_height = 1
      //   let fps = 0, fps_fixed = true, fps_num = 0, fps_den = 0
      //
      //   let vui_parameters_present_flag = gb.readBool()
      //   if (vui_parameters_present_flag) {
      //     if (gb.readBool()) { // aspect_ratio_info_present_flag
      //       let aspect_ratio_idc = gb.readByte()
      //       let par_w_table = [1, 12, 10, 16, 40, 24, 20, 32, 80, 18, 15, 64, 160, 4, 3, 2]
      //       let par_h_table = [1, 11, 11, 11, 33, 11, 11, 11, 33, 11, 11, 33, 99, 3, 2, 1]
      //
      //       if (aspect_ratio_idc > 0 && aspect_ratio_idc < 16) {
      //         par_width = par_w_table[aspect_ratio_idc - 1]
      //         par_height = par_h_table[aspect_ratio_idc - 1]
      //       } else if (aspect_ratio_idc === 255) {
      //         par_width = gb.readByte() << 8 | gb.readByte()
      //         par_height = gb.readByte() << 8 | gb.readByte()
      //       }
      //     }
      //
      //     if (gb.readBool()) {
      //       gb.readBool()
      //     }
      //     if (gb.readBool()) {
      //       gb.readBits(4)
      //       if (gb.readBool()) {
      //         gb.readBits(24)
      //       }
      //     }
      //     if (gb.readBool()) {
      //       gb.readUEG()
      //       gb.readUEG()
      //     }
      //     if (gb.readBool()) {
      //       let num_units_in_tick = gb.readBits(32)
      //       let time_scale = gb.readBits(32)
      //       fps_fixed = gb.readBool()
      //
      //       fps_num = time_scale
      //       fps_den = num_units_in_tick * 2
      //       fps = fps_num / fps_den
      //     }
      //   }
      //
      //   let parScale = 1
      //   if (par_width !== 1 || par_height !== 1) {
      //     parScale = par_width / par_height
      //   }
      //
      //   let crop_unit_x = 0, crop_unit_y = 0
      //   if (chroma_format_idc === 0) {
      //     crop_unit_x = 1
      //     crop_unit_y = 2 - frame_mbs_only_flag
      //   } else {
      //     let sub_wc = (chroma_format_idc === 3) ? 1 : 2
      //     let sub_hc = (chroma_format_idc === 1) ? 2 : 1
      //     crop_unit_x = sub_wc
      //     crop_unit_y = sub_hc * (2 - frame_mbs_only_flag)
      //   }
      //
      //   let codec_width = (pic_width_in_mbs_minus1 + 1) * 16
      //   let codec_height = (2 - frame_mbs_only_flag) * ((pic_height_in_map_units_minus1 + 1) * 16)
      //
      //   codec_width -= (frame_crop_left_offset + frame_crop_right_offset) * crop_unit_x
      //   codec_height -= (frame_crop_top_offset + frame_crop_bottom_offset) * crop_unit_y
      //
      //   let present_width = Math.ceil(codec_width * parScale)
      //
      //   gb.destroy()
      //   gb = null
      //
      //   return {
      //     profile_string: profile_string,
      //     level_string: level_string,
      //     bit_depth: bit_depth,
      //     chroma_format: chroma_format,
      //     chroma_format_string: SPSParser.getChromaFormatString(chroma_format),
      //
      //     frame_rate: {
      //       fixed: fps_fixed,
      //       fps: fps,
      //       fps_den: fps_den,
      //       fps_num: fps_num
      //     },
      //
      //     par_ratio: {
      //       width: par_width,
      //       height: par_height
      //     },
      //
      //     codec_size: {
      //       width: codec_width,
      //       height: codec_height
      //     },
      //
      //     present_size: {
      //       width: present_width,
      //       height: codec_height
      //     }
      //   }
      // }
      /**
       * @param gb
       * @param maxSubLayersMinus1
       * @return {{general_profile_idc: (*|number), general_level_idc: (*|number), general_tier_flag: (*|number), general_profile_space: (*|number)}}
       */

    }, {
      key: '_readProfileTierLevel',
      value: function _readProfileTierLevel(gb, maxSubLayersMinus1) {
        var general_profile_space = 0;
        var general_tier_flag = 0;
        var general_profile_idc = 0;
        var general_level_idc = 0;
        general_profile_space = gb.readBits(2) || 0; // profile_space
        general_tier_flag = gb.readBits(1) || 0; // tierFlag
        general_profile_idc = gb.readBits(5) || 0; // profileIdc

        gb.readBits(16); // some 32bits
        gb.readBits(16);

        gb.readBits(1); // progressiveSourceFlag
        gb.readBits(1); // interlacedSourceFlag
        gb.readBits(1); // nonPackedConstraintFlag
        gb.readBits(1); // frameOnlyConstraintFlag

        gb.readBits(16); // reserved zero bits
        gb.readBits(16);
        gb.readBits(12);

        general_level_idc = gb.readBits(8) || 0; // level_idc

        var subLayerProfilePresentFlag = [];
        var subLayerLevelPresentFlag = [];
        for (var j = 0; j < maxSubLayersMinus1; j++) {
          subLayerProfilePresentFlag[j] = gb.readBits(1);
          subLayerLevelPresentFlag[j] = gb.readBits(1);
        }

        if (maxSubLayersMinus1 > 0) {
          gb.readBits((8 - maxSubLayersMinus1) * 2);
        }

        for (var i = 0; i < maxSubLayersMinus1; i++) {
          if (subLayerProfilePresentFlag[i] !== 0) {
            gb.readBits(2);
            gb.readBits(1);
            gb.readBits(5);

            gb.readBits(16);
            gb.readBits(16);

            gb.readBits(4);

            gb.readBits(16);
            gb.readBits(16);
            gb.readBits(12);
          }
          if (subLayerLevelPresentFlag[i] !== 0) {
            gb.readBits(8);
          }
        }

        return {
          general_profile_space: general_profile_space,
          general_tier_flag: general_tier_flag,
          general_profile_idc: general_profile_idc,
          general_level_idc: general_level_idc
        };
      }

      /**
       *
       *  @param {any} gb
       * @param {number}count
       */

    }, {
      key: '_skipScalingList',
      value: function _skipScalingList(gb, count) {
        var lastScale = 8;
        var nextScale = 8;
        var deltaScale = 0;
        for (var i = 0; i < count; i++) {
          if (nextScale !== 0) {
            deltaScale = gb.readSEG();
            nextScale = (lastScale + deltaScale + 256) % 256;
          }
          lastScale = nextScale === 0 ? lastScale : nextScale;
        }
      }

      /**
       *
       * @param {number} profileIdc
       * @return {string}
       */

    }, {
      key: 'getProfileString',
      value: function getProfileString(profileIdc) {
        switch (profileIdc) {
          case 66:
            return 'Baseline';
          case 77:
            return 'Main';
          case 88:
            return 'Extended';
          case 100:
            return 'High';
          case 110:
            return 'High10';
          case 122:
            return 'High422';
          case 244:
            return 'High444';
          default:
            return 'Unknown';
        }
      }

      /**
       * @param {number} levelIdc
       * @return {string}
       */

    }, {
      key: 'getLevelString',
      value: function getLevelString(levelIdc) {
        return (levelIdc / 10).toFixed(1);
      }

      /**
       * @param {number} chroma
       * @return {string}
       */

    }, {
      key: 'getChromaFormatString',
      value: function getChromaFormatString(chroma) {
        switch (chroma) {
          case 420:
            return '4:2:0';
          case 422:
            return '4:2:2';
          case 444:
            return '4:4:4';
          default:
            return 'Unknown';
        }
      }

      /**
       * @param {any} spsConfig
       * @return {any}
       */

    }, {
      key: 'toVideoMeta',
      value: function toVideoMeta(spsConfig) {
        var meta = {};
        if (spsConfig && spsConfig.codec_size) {
          meta.codecWidth = spsConfig.codec_size.width;
          meta.codecHeight = spsConfig.codec_size.height;
          meta.presentWidth = spsConfig.present_size.width;
          meta.presentHeight = spsConfig.present_size.height;
        } else if (spsConfig.width && spsConfig.height) {
          meta.codecWidth = spsConfig.width;
          meta.codecHeight = spsConfig.height;
          meta.presentWidth = spsConfig.width;
          meta.presentHeight = spsConfig.height;
        }

        meta.profile = spsConfig.profile_string;
        meta.level = spsConfig.level_string;
        meta.bitDepth = spsConfig.bit_depth;
        meta.chromaFormat = spsConfig.chroma_format;

        // meta.parRatio = {
        //   width: spsConfig.par_ratio.width,
        //   height: spsConfig.par_ratio.height
        // }

        // meta.frameRate = spsConfig.frame_rate

        // let fpsDen = meta.frameRate.fps_den
        // let fpsNum = meta.frameRate.fps_num
        // meta.refSampleDuration = Math.floor(meta.timescale * (fpsDen / fpsNum))
        return meta;
      }
    }]);

    return SPSParser;
  }();

  var _createClass$k = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$l(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var RBSP$1 = function () {
    function RBSP() {
      _classCallCheck$l(this, RBSP);
    }

    _createClass$k(RBSP, null, [{
      key: "EBSP2RBSP",

      /**
       * convert EBSP to RBSP
       * @param {Uint8Array} data
       * @returns {Uint8Array}
       * @constructor
       */
      value: function EBSP2RBSP(data) {
        return data.filter(function (el, idx) {
          if (idx < 2) {
            return true;
          } else {
            return !(data[idx - 2] === 0 && data[idx - 1] === 0 && el === 3);
          }
        });
      }

      /**
       * @param {Uint8Array} data
       * @constructor
       */

    }, {
      key: "EBSP2SODB",
      value: function EBSP2SODB(data) {
        var lastByte = data[data.byteLength - 1];
        if (lastByte && lastByte === 128) {
          return data.slice(0, data.byteLength - 1);
        }

        return data;
      }
    }]);

    return RBSP;
  }();

  var _createClass$l = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$m(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  /**
   * @param {Uint8Array} data
   * @return {string}
   */
  var u8aToString$1 = function u8aToString(data) {
    var result = '';
    for (var i = 0; i < data.byteLength; i++) {
      result += String.fromCharCode(data[i]);
    }
    return result;
  };

  var SEIParser$1 = function () {
    function SEIParser() {
      _classCallCheck$m(this, SEIParser);
    }

    _createClass$l(SEIParser, null, [{
      key: '_resolveNalu',
      value: function _resolveNalu(data) {
        if (data.length >= 1) {
          return RBSP$1.EBSP2SODB(RBSP$1.EBSP2RBSP(data.slice(1)));
        }
        return null;
      }

      /**
       * @param {Uint8Array} data
       * @return {{uuid: string, content: null}|{code: number, uuid: string, content: string}|{code: number, content: Uint8Array}}
       */

    }, {
      key: 'parse',
      value: function parse(data) {
        var sodb = SEIParser._resolveNalu(data);

        var _SEIParser$switchPayl = SEIParser.switchPayloadType(sodb),
            payloadType = _SEIParser$switchPayl.payloadType,
            offset = _SEIParser$switchPayl.offset;

        var content = sodb.slice(offset);

        switch (payloadType) {
          case 5:
            return SEIParser.user_data_unregistered(content);
          default:
            return {
              code: payloadType,
              content: content
            };
        }
      }

      /**
       *
       * @param {Uint8Array} data
       * @returns {{payloadType: number, offset: number}}
       */

    }, {
      key: 'switchPayloadType',
      value: function switchPayloadType(data) {
        var dv = new DataView(data.buffer);
        var payloadType = 0;
        var offset = 0;
        while (dv.getUint8(offset) === 255) {
          offset++;
          payloadType += 255;
        }
        payloadType += dv.getUint8(offset++);

        return {
          payloadType: payloadType,
          offset: offset
        };
      }

      /**
       *
       * @param {Uint8Array} data
       * @return {{ payloadLength: number, offset: number }}
       */

    }, {
      key: 'getPayloadLength',
      value: function getPayloadLength(data) {
        var dv = new DataView(data.buffer);

        var payloadLength = 0;
        var offset = 0;
        while (dv.getUint8(offset) === 255) {
          offset++;
          payloadLength += 255;
        }
        payloadLength += dv.getUint8(offset++);

        return {
          payloadLength: payloadLength,
          offset: offset
        };
      }

      /**
       * resolve 0x05 user data unregistered
       * @param {Uint8Array} data
       */
      // eslint-disable-next-line camelcase

    }, {
      key: 'user_data_unregistered',
      value: function user_data_unregistered(data) {
        var _SEIParser$getPayload = SEIParser.getPayloadLength(data),
            payloadLength = _SEIParser$getPayload.payloadLength,
            offset = _SEIParser$getPayload.offset;

        if (payloadLength < 16) {
          return {
            uuid: '',
            content: null
          };
        }
        var payload = data.slice(offset);

        var uuid = u8aToString$1(payload.slice(0, 16));
        var content = u8aToString$1(payload.slice(16, payloadLength));

        return {
          code: 5, // for user data unregistered
          uuid: uuid,
          content: content
        };
      }
    }]);

    return SEIParser;
  }();

  var _createClass$m = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$n(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var Nalunit$1 = function () {
    function Nalunit() {
      _classCallCheck$n(this, Nalunit);
    }

    _createClass$m(Nalunit, null, [{
      key: 'getNalunits',

      /**
       * @param {any} buffer
       * @return {[]|*[]}
       */
      value: function getNalunits(buffer) {
        if (buffer.length - buffer.position < 4) {
          return [];
        }

        var buf = buffer.dataview;
        var position = buffer.position;

        if (buf.getInt32(position) === 1 || buf.getInt16(position) === 0 && buf.getInt8(position + 2) === 1) {
          return Nalunit.getAnnexbNals(buffer);
        } else {
          return Nalunit.getHvccNals(buffer);
        }
      }

      /**
       * @param {any} buffer
       * @return {[]}
       */

    }, {
      key: 'getAnnexbNals',
      value: function getAnnexbNals(buffer) {
        var nals = [];
        var position = Nalunit.getHeaderPositionAnnexB(buffer);
        var start = position.pos;
        var end = start;
        while (start < buffer.length - 4) {
          var header = buffer.buffer.slice(start, start + position.headerLength);
          if (position.pos === buffer.position) {
            buffer.skip(position.headerLength);
          }
          position = Nalunit.getHeaderPositionAnnexB(buffer);
          end = position.pos;
          var body = new Uint8Array(buffer.buffer.slice(start + header.byteLength, end));
          var unit = { header: header, body: body };
          Nalunit.analyseNal(unit);
          if (unit.type <= 40) {
            nals.push(unit);
          }
          buffer.skip(end - buffer.position);
          start = end;
        }
        return nals;
      }

      // |四字节 nalSize| nalUnit |
      /**
       * @param {any} buffer
       * @return {[]}
       */

    }, {
      key: 'getHvccNals',
      value: function getHvccNals(buffer) {
        // console.log('getHvccNals')
        var nals = [];
        while (buffer.position < buffer.length - 4) {
          // console.log('buffer')
          // console.log(buffer)
          // console.log(buffer.position)
          // console.log(buffer.length)
          // console.log(buffer.dataview)
          // let length = buffer.dataview.getInt32();
          var length = buffer.dataview.getInt32(buffer.dataview.position);
          if (buffer.length - buffer.position >= length) {
            var header = buffer.buffer.slice(buffer.position, buffer.position + 4);
            buffer.skip(4);
            var body = new Uint8Array(buffer.buffer.slice(buffer.position, buffer.position + length));
            buffer.skip(length);
            var unit = { header: header, body: body };
            try {
              Nalunit.analyseNal(unit);
            } catch (e) {
              continue;
            }
            if (unit.type <= 40) {
              nals.push(unit);
            }
          } else {
            break;
          }
        }
        return nals;
      }

      /**
       * @param {any} unit
       */

    }, {
      key: 'analyseNal',
      value: function analyseNal(unit) {
        var type = unit.body[0] >>> 1 & 0x3f;
        unit.type = type;
        switch (type) {
          case 0:
            // SLICE_TRAIL_N
            unit.slice_trail_n = true;
            break;
          case 1:
            // SLICE_TRAIL_R
            unit.slice_trail_r = true;
            unit.key = true;
            break;
          case 2:
            // SLICE_TSA_N
            unit.slice_tsa_n = true;
            break;
          case 3:
            // SLICE_TSA_R
            unit.slice_tsa_r = true;
            unit.key = true;
            break;
          case 4:
            // SLICE_STSA_N
            unit.slice_stsa_n = true;
            break;
          case 5:
            // SLICE_STSA_R
            unit.slice_stsa_r = true;
            unit.key = true;
            break;
          case 6:
            // SLICE_RADL_N
            unit.slice_radl_n = true;
            break;
          case 7:
            // SLICE_RADL_R
            unit.slice_radl_r = true;
            unit.key = true;
            break;
          case 8:
            // SLICE_RASL_N
            unit.slice_rasl_n = true;
            break;
          case 9:
            // SLICE_RASL_R
            unit.slice_rasl_r = true;
            unit.key = true;
            break;
          case 16:
            // SLICE_BLA_W_LP
            unit.slice_bla_w_lp = true;
            break;
          case 17:
            // SLICE_BLA_W_RADL
            unit.slice_bla_w_radl = true;
            break;
          case 18:
            // SLICE_BLA_N_LP
            unit.slice_bla_n_lp = true;
            break;
          case 19:
            // SLICE_IDR_W_RADL
            unit.slice_idl_w_radl = true;
            unit.key = true;
            break;
          case 20:
            // SLICE_IDR_N_LP
            unit.slice_idr_n_lp = true;
            unit.key = true;
            break;
          case 21:
            // SLICE_CRA_NUT
            unit.slice_cra_nut = true;
            unit.key = true;
            break;
          case 32:
            // VPS
            unit.vps = true;
            break;
          case 33:
            // SPS
            unit.sps = SPSParser$1.parseSPS(unit.body);
            break;
          case 34:
            // PPS
            unit.pps = true;
            break;
          case 35:
            // AUD
            break;
          case 36:
            // EOS
            unit.aud = true;
            break;
          case 37:
            // EOB
            unit.eob = true;
            break;
          case 38:
            // FD
            unit.fd = true;
            break;
          case 39:
            // PREFIX_SEI
            // unit.prefix_sei = true;
            try {
              unit.sei = SEIParser$1.parse(unit.body.slice(1));
            } catch (e) {}
            break;
          case 40:
            // SUFFIX_SEI
            unit.sei = SEIParser$1.parse(unit.body.slice(1));
            break;
        }
      }
    }, {
      key: 'getHeaderPositionAnnexB',
      value: function getHeaderPositionAnnexB(buffer) {
        // seperate
        var pos = buffer.position;
        var headerLength = 0;
        var bufferLen = buffer.length;
        while (headerLength !== 3 && headerLength !== 4 && pos < bufferLen - 4) {
          if (buffer.dataview.getInt16(pos) === 0) {
            if (buffer.dataview.getInt16(pos + 2) === 1) {
              // 0x00000001
              headerLength = 4;
            } else if (buffer.dataview.getInt8(pos + 2) === 1) {
              headerLength = 3;
            } else {
              pos++;
            }
          } else {
            pos++;
          }
        }

        if (pos === bufferLen - 4) {
          if (buffer.dataview.getInt16(pos) === 0) {
            if (buffer.dataview.getInt16(pos + 2) === 1) {
              // 0x00000001
              headerLength = 4;
            }
          } else {
            pos++;
            if (buffer.dataview.getInt16(pos) === 0 && buffer.dataview.getInt8(pos) === 1) {
              // 0x000001
              headerLength = 3;
            } else {
              pos = bufferLen;
            }
          }
        }
        return { pos: pos, headerLength: headerLength };
      }
    }]);

    return Nalunit;
  }();

  var _createClass$n = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _toConsumableArray$1(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
        arr2[i] = arr[i];
      }return arr2;
    } else {
      return Array.from(arr);
    }
  }

  function _classCallCheck$o(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var REMUX_EVENTS$1 = EVENTS.REMUX_EVENTS;

  var Compatibility = function () {
    function Compatibility() {
      var _this = this;

      _classCallCheck$o(this, Compatibility);

      this.TAG = 'Compatibility';
      this.nextAudioDts = 0; // 模拟下一段音频数据的dts
      this.nextVideoDts = 0; // 模拟下一段视频数据的dts

      this.lastAudioSamplesLen = 0; // 上一段音频数据的长度
      this.lastVideoSamplesLen = 0; // 上一段视频数据的长度

      this.lastVideoDts = undefined; // 上一段音频数据的长度
      this.lastAudioDts = undefined; // 上一段视频数据的长度

      this.allAudioSamplesCount = 0; // 音频总数据量(原始帧)
      this.allVideoSamplesCount = 0; // 视频总数据量(原始帧)

      this._firstAudioSample = null;
      this._firstVideoSample = null;

      this.filledAudioSamples = []; // 补充音频帧（）
      this.filledVideoSamples = []; // 补充视频帧（）

      this.videoLastSample = null;
      this.audioLastSample = null; // stash last sample for duration compat

      Object.defineProperties(this, {
        _videoLargeGap: {
          set: function set(value) {
            _this.___videoLargeGap = value;
            if (value !== 0) {
              _this.emit(REMUX_EVENTS$1.DETECT_LARGE_GAP, 'video', value);
            }
          },
          get: function get() {
            return _this.___videoLargeGap || 0;
          }
        },
        _audioLargeGap: {
          set: function set(value) {
            _this.___audioLargeGap = value;
            if (value !== 0) {
              _this.emit(REMUX_EVENTS$1.DETECT_LARGE_GAP, 'audio', value);
            }
          },
          get: function get() {
            return _this.___audioLargeGap || 0;
          }
        }
      });
      this.audioUnsyncTime = 0;
    }

    _createClass$n(Compatibility, [{
      key: 'init',
      value: function init() {
        this.before(REMUX_EVENTS$1.REMUX_MEDIA, this.doFix.bind(this));
      }
    }, {
      key: 'reset',
      value: function reset() {
        this.nextAudioDts = null; // 估算下一段音频数据的dts
        this.nextVideoDts = null; // 估算下一段视频数据的dts

        this.lastAudioSamplesLen = 0; // 上一段音频数据的长度
        this.lastVideoSamplesLen = 0; // 上一段视频数据的长度

        this.lastVideoDts = undefined; // 上一段音频数据的dts
        this.lastAudioDts = undefined; // 上一段视频数据的dts
        this.lastVideoDuration = undefined;

        // this.allAudioSamplesCount = 0 // 音频总数据量(原始帧)
        // this.allVideoSamplesCount = 0 // 视频总数据量(原始帧)

        this._audioLargeGap = 0;
        this._videoLargeGap = 0;

        // this._firstAudioSample = null
        // this._firstVideoSample = null
        // this._firstAudioSample = null
        // this._firstVideoSample = null
        this.videoLastSample = null;
        this.audioLastSample = null;

        this.filledAudioSamples = []; // 补充音频帧（）
        this.filledVideoSamples = []; // 补充视频帧（）

        this.audioUnsyncTime = 0;
      }
    }, {
      key: 'doFix',
      value: function doFix() {
        var _getFirstSample = this.getFirstSample(),
            isFirstAudioSamples = _getFirstSample.isFirstAudioSamples,
            isFirstVideoSamples = _getFirstSample.isFirstVideoSamples;

        this.recordSamplesCount();

        if (this._firstVideoSample) {
          this.fixVideoRefSampleDuration(this.videoTrack.meta, this.videoTrack.samples);
        }

        var _Compatibility$detect = Compatibility.detectChangeStream(this.videoTrack.samples, isFirstVideoSamples),
            videoChanged = _Compatibility$detect.changed,
            videoChangedIdxes = _Compatibility$detect.changedIdxes;

        if (videoChanged) {
          var disContinue = false;
          for (var i = 0; i < videoChangedIdxes.length; i++) {
            if (this.fixChangeStreamVideo(videoChangedIdxes[i], isFirstVideoSamples)) {
              disContinue = true;
            }
          }
          if (!disContinue) {
            this.doFixVideo(isFirstVideoSamples);
          }
        } else {
          this.doFixVideo(isFirstVideoSamples);
        }

        var _Compatibility$detect2 = Compatibility.detectChangeStream(this.audioTrack.samples, isFirstAudioSamples),
            audioChanged = _Compatibility$detect2.changed,
            audioChangedIdxes = _Compatibility$detect2.changedIdxes;

        if (audioChanged) {
          var _disContinue = false;
          for (var _i = 0; _i < audioChangedIdxes.length; _i++) {
            if (this.fixChangeStreamAudio(audioChangedIdxes[_i], isFirstAudioSamples)) {
              _disContinue = true;
            }
          }
          if (!_disContinue) {
            this.doFixAudio(isFirstAudioSamples);
          } else {
            return;
          }
        } else {
          this.doFixAudio(isFirstAudioSamples);
        }

        this.removeInvalidSamples();
      }
    }, {
      key: 'doFixVideo',
      value: function doFixVideo(first, streamChangeStart) {
        var _videoTrack = this.videoTrack,
            videoSamples = _videoTrack.samples,
            meta = _videoTrack.meta;

        var len = videoSamples.length;

        for (var i = 0; i < len; i++) {
          var sample = videoSamples[i];
          sample.originDts = sample.dts;
          sample.originPts = sample.pts;
        }

        if (!videoSamples || !len || !this._firstVideoSample) {
          return;
        }

        var firstSample = videoSamples[0];

        logger.log(this.TAG, 'doFixVideo:: lastVideoDts: ' + this.lastVideoDts + ' ,  _videoLargeGap: ' + this._videoLargeGap + ' ,streamChangeStart:' + streamChangeStart + ', lastVideoSample:[dts=' + (this.videoLastSample && this.videoLastSample.dts) + ' , pts=' + (this.videoLastSample && this.videoLastSample.pts) + '] ,  firstDTS:' + firstSample.dts + ' ,firstPTS:' + firstSample.pts + ' ,lastDTS:' + videoSamples[len - 1].dts + ' , lastPTS: ' + videoSamples[len - 1].pts);

        // !first: 非首次加载的分片
        if (!first && this.videoLastSample === null && firstSample.options && firstSample.options.start) {
          if (streamChangeStart !== undefined) {
            streamChangeStart = firstSample.options.start;
          }
        }
        if (!first && streamChangeStart === undefined && this.videoLastSample && Compatibility.detectVideoLargeGap(this.videoLastSample ? this.videoLastSample.dts : 0, firstSample.dts + this._videoLargeGap)) {
          // large gap 不准确，出现了非换流场景的时间戳跳变
          this._videoLargeGap = this.videoLastSample.dts + meta.refSampleDuration - firstSample.dts;
        }
        // step0.修复hls流出现巨大gap，需要强制重定位的问题。
        if (this._videoLargeGap !== 0) {
          Compatibility.doFixLargeGap(videoSamples, this._videoLargeGap);
          if (this._videoLargeGap !== this.preVideoGap) {
            this.preVideoGap = this._videoLargeGap;
            this.emit(REMUX_EVENTS$1.DETECT_CHANGE_STREAM_DISCONTINUE, 'video', { prevDts: this.videoLastSample && this.videoLastSample.originDts, curDts: firstSample.originDts, duration: meta.refSampleDuration });
          }
        }

        if (!first && streamChangeStart !== undefined) {
          this._videoLargeGap = streamChangeStart - firstSample.dts;
          Compatibility.doFixLargeGap(videoSamples, this._videoLargeGap);
        }

        // step1. 修复与audio首帧差距太大的问题
        if (first && this._firstAudioSample) {
          var videoFirstDts = this._firstVideoSample.originDts;
          var audioFirstDts = this._firstAudioSample.originDts || this._firstAudioSample.dts;
          var gap = videoFirstDts - audioFirstDts;
          if (gap > 2 * meta.refSampleDuration && gap < 10 * meta.refSampleDuration) {
            var fillCount = Math.floor(gap / meta.refSampleDuration);

            for (var _i2 = 0; _i2 < fillCount; _i2++) {
              var clonedFirstSample = Object.assign({}, firstSample); // 视频头部帧缺失需要复制第一帧
              // 重新计算sample的dts和pts
              clonedFirstSample.dts = videoFirstDts - (_i2 + 1) * meta.refSampleDuration;
              clonedFirstSample.pts = clonedFirstSample.dts + clonedFirstSample.cts;

              videoSamples.unshift(clonedFirstSample);

              this.filledVideoSamples.push({
                dts: clonedFirstSample.dts,
                size: clonedFirstSample.data.byteLength
              });
            }
            this._firstVideoSample = this.filledVideoSamples[0] || this._firstVideoSample;
          } else if (Math.abs(gap) > 2 * meta.refSampleDuration && !this._videoLargeGap) {
            this._videoLargeGap = -1 * gap;
            Compatibility.doFixLargeGap(videoSamples, -1 * gap);
          }
        }

        var curLastSample = videoSamples.pop();
        if (videoSamples.length) {
          videoSamples[videoSamples.length - 1].duration = curLastSample.dts - videoSamples[videoSamples.length - 1].dts;
        }

        if (this.videoLastSample) {
          var videoLastSample = this.videoLastSample;
          videoLastSample.duration = firstSample.dts - videoLastSample.dts;
          videoSamples.unshift(this.videoLastSample);
        }

        // videoSamples.forEach((sample, idx) => {
        //   if (idx !== 0 && idx !== videoSamples.length - 1) {
        //     const pre = videoSamples[idx - 1];
        //     const next = videoSamples[idx + 1];
        //     if (sample.dts - pre.dts < 5) {
        //       sample.dts = (pre.dts + next.dts) / 2
        //       sample.pts = (pre.pts + next.pts) / 2
        //     }
        //   }
        // })

        this.videoLastSample = curLastSample;
        if (videoSamples[videoSamples.length - 1]) {
          this.lastVideoDuration = videoSamples[videoSamples.length - 1].duration;
          this.lastVideoDts = videoSamples[videoSamples.length - 1].dts;
        }
        this.videoTrack.samples = videoSamples;
      }
    }, {
      key: 'doFixAudio',
      value: function doFixAudio(first, streamChangeStart) {
        var _this2 = this;

        var _audioTrack = this.audioTrack,
            audioSamples = _audioTrack.samples,
            meta = _audioTrack.meta;

        if (!audioSamples || !audioSamples.length) {
          return;
        }

        this.fixAudioRefSampleDuration(meta);

        for (var i = 0, len = audioSamples.length; i < len; i++) {
          var sample = audioSamples[i];
          sample.originDts = sample.dts;
        }

        // console.log(`audio lastSample, ${audioSamples[audioSamples.length - 1].dts}`)

        var samplesLen = audioSamples.length;
        var silentFrame = AAC.getSilentFrame(meta.codec, meta.channelCount);
        var iRefSampleDuration = Math.floor(meta.refSampleDuration);

        var firstSample = this._firstAudioSample;

        var _firstSample = audioSamples[0];

        logger.log(this.TAG, 'doFixAudio:: audioDtsBase:' + this.audioDtsBase + ' ,  _audioLargeGap: ' + this._audioLargeGap + ', streamChangeStart:' + streamChangeStart + ' ,  nextAudioDts:' + this.nextAudioDts + ',  audio: firstDTS:' + _firstSample.dts + ' ,firstPTS:' + _firstSample.pts + ' ,lastDTS:' + audioSamples[samplesLen - 1].dts + ' , lastPTS: ' + audioSamples[samplesLen - 1].pts);

        if (!first && this.nextAudioDts === null && _firstSample.options && _firstSample.options.start) {
          if (streamChangeStart !== undefined) {
            streamChangeStart = _firstSample.options.start;
          }
        }

        if (!first && streamChangeStart === undefined && this.nextAudioDts && Compatibility.detectAudioLargeGap(this.nextAudioDts || 0, _firstSample.dts + this._audioLargeGap)) {
          // large gap 不准确，出现了非换流场景的时间戳跳变
          var _audioLargeGap = this.nextAudioDts - _firstSample.dts;
          this._audioLargeGap = Math.abs(_audioLargeGap - this._videoLargeGap) < 200 ? this._videoLargeGap : _audioLargeGap;
        }

        // 对audioSamples按照dts做排序
        if (this._audioLargeGap !== 0) {
          if (this._audioLargeGap > 0) {
            Compatibility.doFixLargeGap(audioSamples, this._audioLargeGap);
          }
          if (this._audioLargeGap !== this.preAudioGap) {
            this.preAudioGap = this._audioLargeGap;
            this.emit(REMUX_EVENTS$1.DETECT_CHANGE_STREAM_DISCONTINUE, 'audio', { prevDts: this.lastAudioOriginDts, curDts: _firstSample.originDts, duration: iRefSampleDuration });
          }
        } else if (!first && (streamChangeStart !== undefined || Compatibility.detectAudioLargeGap(this.nextAudioDts, _firstSample.dts))) {
          if (streamChangeStart !== undefined) {
            this.nextAudioDts = streamChangeStart; // FIX: Hls中途切codec，在如果直接seek到后面的点会导致largeGap计算失败
          }
          this._audioLargeGap = this.nextAudioDts - _firstSample.dts;

          if (_firstSample.options.start && !_firstSample.options.start.isContinue) {
            Compatibility.doFixLargeGap(audioSamples, this._audioLargeGap);
          }
        }
        // step0. 首帧与video首帧间距大的问题
        if (this._firstVideoSample && first) {
          var videoFirstPts = this._firstVideoSample.originDts || this._firstVideoSample.dts;
          var _gap = firstSample.dts - videoFirstPts;

          if (_gap === this._videoLargeGap) ; else if (_gap > meta.refSampleDuration && _gap < 10 * meta.refSampleDuration) {
            var silentSampleCount = Math.floor((firstSample.dts - videoFirstPts) / meta.refSampleDuration);

            for (var _i3 = 0; _i3 < silentSampleCount; _i3++) {
              var silentSample = {
                data: silentFrame,
                datasize: silentFrame.byteLength,
                dts: firstSample.dts - (_i3 + 1) * meta.refSampleDuration,
                filtered: 0
              };

              audioSamples.unshift(silentSample);

              this.filledAudioSamples.push({
                dts: silentSample.dts,
                size: silentSample.data.byteLength
              });
            }
            this._firstAudioSample = this.filledAudioSamples[0] || this._firstAudioSample;
          } else if (_gap < -1 * meta.refSampleDuration) {
            this._audioLargeGap = -1 * _gap;
            Compatibility.doFixLargeGap(audioSamples, -1 * _gap);
          }
        }

        var gap = void 0;
        var firstDts = audioSamples[0].dts;

        if (this.nextAudioDts) {
          // step1. 处理samples段之间的丢帧情况
          // 当发现duration差距大于1帧时进行补帧
          gap = firstDts - this.nextAudioDts;
          var absGap = Math.abs(gap);

          if (gap >= iRefSampleDuration && gap < 10000 && silentFrame) {
            var silentFrameCount = Math.ceil(gap / iRefSampleDuration);

            for (var _i4 = 0; _i4 < silentFrameCount; _i4++) {
              var computed = firstDts - (_i4 + 1) * iRefSampleDuration;
              var _silentSample = {
                dts: computed > this.nextAudioDts ? computed : this.nextAudioDts,
                pts: computed > this.nextAudioDts ? computed : this.nextAudioDts,
                datasize: silentFrame.byteLength,
                filtered: 0,
                data: silentFrame
              };

              this.filledAudioSamples.push({
                dts: _silentSample.dts,
                size: _silentSample.data.byteLength
              });
              this.audioTrack.samples.unshift(_silentSample);
              _firstSample = _silentSample;
            }
            this.emit(REMUX_EVENTS$1.DETECT_AUDIO_GAP, gap, silentFrameCount);
          } else if (absGap < meta.refSampleDuration && absGap > 0) {
            // 当差距比较小的时候将音频帧重定位
            // console.log('重定位音频帧dts', audioSamples[0].dts, this.nextAudioDts)
            _firstSample.dts = this.nextAudioDts;
            _firstSample.pts = this.nextAudioDts;
          } else if (gap < 0 && absGap < iRefSampleDuration) {
            Compatibility.doFixLargeGap(audioSamples, -1 * gap);
            this.emit(REMUX_EVENTS$1.DETECT_AUDIO_OVERLAP, gap);
          }
        }

        // 分片内采样间补帧
        var nextDts = audioSamples[0].dts + iRefSampleDuration;
        for (var _i5 = 1; _i5 < audioSamples.length;) {
          var _sample = audioSamples[_i5];
          var delta = _sample.dts - nextDts;
          if (delta <= -1 * iRefSampleDuration) {
            logger.warn('drop 1 audio sample for ' + delta + ' ms overlap');
            audioSamples.splice(_i5, 1);
            continue;
          }
          if (delta >= 10 * iRefSampleDuration) {
            var missingCount = Math.round(delta / iRefSampleDuration);
            if (missingCount > 1000) {
              break;
            }
            logger.warn(this.TAG, 'inject ' + missingCount + ' audio frame for ' + delta + ' ms gap');
            for (var j = 0; j < missingCount; j++) {
              var _silentSample2 = {
                data: silentFrame,
                datasize: silentFrame.byteLength,
                dts: nextDts,
                originDts: nextDts,
                filtered: 0
              };
              audioSamples.splice(_i5, 0, _silentSample2);
              nextDts += iRefSampleDuration;
              _i5++;
            }
            _sample.dts = _sample.pts = _sample.originDts = nextDts;
            nextDts += iRefSampleDuration;
            _i5++;
          } else {
            _sample.dts = _sample.pts = _sample.originDts = nextDts;
            nextDts += iRefSampleDuration;
            _i5++;
          }
        }

        var unSyncDuration = meta.refSampleDuration - iRefSampleDuration;
        audioSamples.forEach(function (sample, idx) {
          if (idx !== 0) {
            var _lastSample = audioSamples[idx - 1];
            sample.dts = sample.pts = _lastSample.dts + _lastSample.duration;
          }
          sample.duration = iRefSampleDuration;
          _this2.audioUnsyncTime = _this2.audioUnsyncTime + unSyncDuration;
          if (_this2.audioUnsyncTime >= 1) {
            sample.duration += 1;
            _this2.audioUnsyncTime -= 1;
          }
        });

        var lastSample = audioSamples[audioSamples.length - 1];
        this.lastAudioDts = lastSample.dts;
        var lastDuration = lastSample.duration;
        // const lastSampleDuration = audioSamples.length >= 2 ? lastOriginDts - audioSamples[audioSamples.length - 2].originDts : meta.refSampleDuration

        this.lastAudioSamplesLen = samplesLen;
        this.nextAudioDts = this.lastAudioDts + (lastDuration || iRefSampleDuration);
        this.lastAudioOriginDts = lastSample.originDts;

        this.audioTrack.samples = Compatibility.sortAudioSamples(audioSamples);
      }
    }, {
      key: 'fixChangeStreamVideo',
      value: function fixChangeStreamVideo(changeIdx) {
        logger.log(this.TAG, 'fixChangeStreamVideo(), changeIdx=', changeIdx);
        var _videoTrack2 = this.videoTrack,
            samples = _videoTrack2.samples,
            meta = _videoTrack2.meta;

        var preDuration = changeIdx === 0 ? this.lastVideoDuration ? this.lastVideoDuration : meta.refSampleDuration : meta.refSampleDuration;
        var prevDts = changeIdx === 0 ? this.lastVideoDts ? this.lastVideoDts : this.getStreamChangeStart(samples[0]) : samples[changeIdx - 1].dts;
        var curDts = samples[changeIdx].dts;
        var isContinue = Math.abs(prevDts - curDts) <= 10000;
        this.emit(REMUX_EVENTS$1.DETECT_CHANGE_STREAM, 'video', curDts);
        if (isContinue) {
          if (!samples[changeIdx].options) {
            samples[changeIdx].options = {
              isContinue: true
            };
          } else {
            samples[changeIdx].options.isContinue = true;
          }
          return false;
        }

        this.emit(REMUX_EVENTS$1.DETECT_CHANGE_STREAM_DISCONTINUE, 'video', { prevDts: prevDts, curDts: curDts, duration: preDuration });

        var firstPartSamples = samples.slice(0, changeIdx);
        var secondPartSamples = samples.slice(changeIdx);
        var changeSample = samples[changeIdx];

        var streamChangeStart = void 0;

        this._videoLargeGap = 0;
        this.videoLastSample = null;
        this.lastVideoDts = null;
        this.lastVideoDuration = null;
        if (changeSample.options && changeSample.options.start !== undefined) {
          streamChangeStart = changeSample.options.start;
        } else {
          streamChangeStart = prevDts - this.videoDtsBase;
        }

        this.videoTrack.samples = samples.slice(0, changeIdx);

        this.doFixVideo(false);

        this.videoTrack.samples = samples.slice(changeIdx);

        this.doFixVideo(false, streamChangeStart);

        this.videoTrack.samples = firstPartSamples.concat(secondPartSamples);

        return true;
      }
    }, {
      key: 'fixChangeStreamAudio',
      value: function fixChangeStreamAudio(changeIdx) {
        logger.log(this.TAG, 'fixChangeStreamAudio(), changeIdx=', changeIdx);
        var _audioTrack2 = this.audioTrack,
            samples = _audioTrack2.samples,
            meta = _audioTrack2.meta;

        var prevDts = changeIdx === 0 ? this.lastAudioDts : samples[changeIdx - 1].dts;
        var curDts = samples[changeIdx].dts;
        var isContinue = Math.abs(prevDts - curDts) <= 10000;
        this.emit(REMUX_EVENTS$1.DETECT_CHANGE_STREAM, 'audio', curDts);
        if (isContinue) {
          if (!samples[changeIdx].options) {
            samples[changeIdx].options = {
              isContinue: true
            };
          } else {
            samples[changeIdx].options.isContinue = true;
          }
          return false;
        }
        this.emit(REMUX_EVENTS$1.DETECT_CHANGE_STREAM_DISCONTINUE, 'audio', { prevDts: prevDts, curDts: curDts, duration: meta.refSampleDuration });
        this._audioLargeGap = 0;
        var cacheNextAudioDts = this.nextAudioDts;
        this.nextAudioDts = null;
        var firstPartSamples = samples.slice(0, changeIdx);
        var secondPartSamples = samples.slice(changeIdx);
        var changeSample = samples[changeIdx];

        var streamChangeStart = void 0;
        if (changeSample.options && changeSample.options.start !== undefined) {
          streamChangeStart = changeSample.options.start;
        } else {
          streamChangeStart = cacheNextAudioDts;
          changeSample.options.isContinue = true;
        }

        this.audioTrack.samples = firstPartSamples;

        this.doFixAudio(false);

        this.audioTrack.samples = secondPartSamples;

        this.doFixAudio(false, streamChangeStart);

        this.audioTrack.samples = firstPartSamples.concat(secondPartSamples);

        return true;
      }
    }, {
      key: 'getFirstSample',
      value: function getFirstSample() {
        // 获取video和audio的首帧数据
        var videoSamples = this.videoTrack.samples;
        var audioSamples = this.audioTrack.samples;

        var isFirstVideoSamples = false;
        var isFirstAudioSamples = false;

        if (!this._firstVideoSample && videoSamples.length) {
          this._firstVideoSample = Compatibility.findFirstVideoSample(videoSamples);
          this.removeInvalidSamples();
          isFirstVideoSamples = true;
        }

        if (!this._firstAudioSample && audioSamples.length) {
          this._firstAudioSample = Compatibility.findFirstAudioSample(audioSamples); // 寻找dts最小的帧作为首个音频帧
          this.removeInvalidSamples();
          isFirstAudioSamples = true;
        }

        return {
          isFirstVideoSamples: isFirstVideoSamples,
          isFirstAudioSamples: isFirstAudioSamples
        };
      }

      /**
       * 在没有refSampleDuration的问题流中，
       */

    }, {
      key: 'fixVideoRefSampleDuration',
      value: function fixVideoRefSampleDuration(meta, samples) {
        if (!meta) {
          return;
        }
        var allSamplesCount = this.allVideoSamplesCount;
        var firstDts = this._firstVideoSample.dts;
        var filledSamplesCount = this.filledVideoSamples.length;
        if (!Compatibility.isRefSampleDurationValid(meta.refSampleDuration)) {
          if (samples.length >= 1) {
            var lastDts = samples[samples.length - 1].dts;

            var fixed = Math.floor((lastDts - firstDts) / (allSamplesCount + filledSamplesCount - 1)); // 将refSampleDuration重置为计算后的平均值

            if (Compatibility.isRefSampleDurationValid(fixed)) {
              meta.refSampleDuration = fixed;
            }
          }
        } else if (meta.refSampleDuration) {
          if (samples.length >= 5) {
            var _lastDts = samples[samples.length - 1].dts;
            var _firstDts = samples[0].dts;
            var durationAvg = (_lastDts - _firstDts) / (samples.length - 1);
            if (durationAvg > 0 && durationAvg < 1000) {
              var _fixed = Math.floor(Math.abs(meta.refSampleDuration - durationAvg) <= 5 ? meta.refSampleDuration : durationAvg); // 将refSampleDuration重置为计算后的平均值
              if (Compatibility.isRefSampleDurationValid(_fixed)) {
                meta.refSampleDuration = _fixed;
              }
            }
          }
        }

        if (!Compatibility.isRefSampleDurationValid(meta.refSampleDuration)) {
          meta.refSampleDuration = 66;
        }
      }
    }, {
      key: 'fixAudioRefSampleDuration',
      value: function fixAudioRefSampleDuration(meta) {
        if (!meta) {
          return;
        }
        meta.refSampleDuration = meta.timescale * 1024 / meta.sampleRate;
      }

      /**
       * 记录截止目前一共播放了多少帧
       */

    }, {
      key: 'recordSamplesCount',
      value: function recordSamplesCount() {
        var audioTrack = this.audioTrack,
            videoTrack = this.videoTrack;

        this.allAudioSamplesCount += audioTrack.samples.length;
        this.allVideoSamplesCount += videoTrack.samples.length;
      }

      /**
       * 去除不合法的帧（倒退、重复帧）
       */

    }, {
      key: 'removeInvalidSamples',
      value: function removeInvalidSamples() {
        var firstAudioSample = this.audioTrack.samples[0];
        var firstVideoSample = this.videoTrack.samples[0];
        // const { _firstVideoSample, _firstAudioSample } = this

        if (firstAudioSample) {
          this.audioTrack.samples = this.audioTrack.samples.filter(function (sample, index) {
            if (sample === firstAudioSample) {
              return true;
            }
            return sample.dts >= firstAudioSample.dts;
          });
        }

        if (firstVideoSample) {
          this.videoTrack.samples = this.videoTrack.samples.filter(function (sample, index) {
            if (sample === firstVideoSample) {
              return true;
            }
            return sample.dts >= firstVideoSample.dts;
          });
        }
      }
    }, {
      key: 'getStreamChangeStart',
      value: function getStreamChangeStart(sample) {
        if (sample.options && sample.options.start) {
          return sample.options.start - this.dtsBase;
        }
        return Infinity;
      }
    }, {
      key: 'tracks',
      get: function get() {
        return this._context.getInstance('TRACKS');
      }
    }, {
      key: 'audioTrack',
      get: function get() {
        if (this.tracks && this.tracks.audioTrack) {
          return this.tracks.audioTrack;
        }
        return {
          samples: [],
          meta: {}
        };
      }
    }, {
      key: 'videoTrack',
      get: function get() {
        if (this.tracks && this.tracks.videoTrack) {
          return this.tracks.videoTrack;
        }
        return {
          samples: [],
          meta: {}
        };
      }
    }, {
      key: 'dtsBase',
      get: function get() {
        var remuxer = this._context.getInstance('MP4_REMUXER');
        if (remuxer) {
          return remuxer._dtsBase;
        }
        return 0;
      }
    }, {
      key: 'audioDtsBase',
      get: function get() {
        var remuxer = this._context.getInstance('MP4_REMUXER');
        if (remuxer && remuxer._audioDtsBase !== null) {
          return remuxer._audioDtsBase;
        }

        return this.dtsBase;
      }
    }, {
      key: 'videoDtsBase',
      get: function get() {
        var remuxer = this._context.getInstance('MP4_REMUXER');
        if (remuxer && remuxer._videoDtsBase !== null) {
          return remuxer._videoDtsBase;
        }

        return this.dtsBase;
      }
    }], [{
      key: 'sortAudioSamples',
      value: function sortAudioSamples(samples) {
        if (samples.length === 1) {
          return samples;
        }

        return [].concat(_toConsumableArray$1(samples)).sort(function (a, b) {
          return a.dts - b.dts;
        });
      }
    }, {
      key: 'isRefSampleDurationValid',
      value: function isRefSampleDurationValid(refSampleDuration) {
        return refSampleDuration && refSampleDuration > 0 && !Number.isNaN(refSampleDuration);
      }
      /**
       * 寻找dts最小的sample
       * @param samples
       */

    }, {
      key: 'findFirstAudioSample',
      value: function findFirstAudioSample(samples) {
        if (!samples || samples.length === 0) {
          return null;
        }

        return Compatibility.sortAudioSamples(samples)[0];
      }
    }, {
      key: 'findFirstVideoSample',
      value: function findFirstVideoSample(samples) {
        if (!samples.length) {
          return null;
        }

        var sorted = [].concat(_toConsumableArray$1(samples)).sort(function (a, b) {
          return a.dts - b.dts;
        });

        for (var i = 0, len = sorted.length; i < len; i++) {
          if (sorted[i].isKeyframe) {
            return sorted[i];
          }
        }
      }
    }, {
      key: 'detectVideoLargeGap',
      value: function detectVideoLargeGap(nextDts, firstSampleDts) {
        if (nextDts === null) {
          return;
        }
        var delta = 10000;
        return nextDts - firstSampleDts >= delta || firstSampleDts - nextDts >= delta; // fix hls流出现大量流dts间距问题
      }
    }, {
      key: 'detectAudioLargeGap',
      value: function detectAudioLargeGap(nextDts, firstSampleDts) {
        if (nextDts === null) {
          return;
        }
        return nextDts - firstSampleDts >= 1000 || firstSampleDts - nextDts >= 1000; // fix hls流出现大量流dts间距问题
      }
    }, {
      key: 'doFixLargeGap',
      value: function doFixLargeGap(samples, gap) {
        // console.log('fix large gap...... ', gap)
        for (var i = 0, len = samples.length; i < len; i++) {
          var sample = samples[i];
          sample.dts += gap;
          if (sample.pts) {
            sample.pts += gap;
          }
        }
      }

      /**
       * 中途换流
       *         |-------------------frag2---------------|
       * | frag1 | -----sample 0~n -------sample n+1 ~ m |
       *
       * 换流可能开始于新分片的第一帧 或者分片中间
       *
       */

    }, {
      key: 'detectChangeStream',
      value: function detectChangeStream(samples, isFirst) {
        var changed = false;
        var changedIdxes = [];
        for (var i = 0, len = samples.length; i < len; i++) {
          var sample = samples[i];
          if (sample.options && sample.options.meta && !(isFirst && i === 0)) {
            changed = true;
            changedIdxes.push(i);
            // break;
          }
        }

        return {
          changed: changed,
          changedIdxes: changedIdxes
        };
      }
    }]);

    return Compatibility;
  }();

  var _createClass$o = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$p(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var UTF8 = function () {
    function UTF8() {
      _classCallCheck$p(this, UTF8);
    }

    _createClass$o(UTF8, null, [{
      key: 'decode',

      /**
       *
       * @param {Uint8Array} uint8array
       * @return {string}
       */
      value: function decode(uint8array) {
        var out = [];
        var input = uint8array;
        var i = 0;
        var length = uint8array.length;

        while (i < length) {
          if (input[i] < 0x80) {
            out.push(String.fromCharCode(input[i]));
            ++i;
            continue;
          } else if (input[i] < 0xC0) ; else if (input[i] < 0xE0) {
            if (UTF8._checkContinuation(input, i, 1)) {
              var ucs4 = (input[i] & 0x1F) << 6 | input[i + 1] & 0x3F;
              if (ucs4 >= 0x80) {
                out.push(String.fromCharCode(ucs4 & 0xFFFF));
                i += 2;
                continue;
              }
            }
          } else if (input[i] < 0xF0) {
            if (UTF8._checkContinuation(input, i, 2)) {
              var _ucs = (input[i] & 0xF) << 12 | (input[i + 1] & 0x3F) << 6 | input[i + 2] & 0x3F;
              if (_ucs >= 0x800 && (_ucs & 0xF800) !== 0xD800) {
                out.push(String.fromCharCode(_ucs & 0xFFFF));
                i += 3;
                continue;
              }
            }
          } else if (input[i] < 0xF8) {
            if (UTF8._checkContinuation(input, i, 3)) {
              var _ucs2 = (input[i] & 0x7) << 18 | (input[i + 1] & 0x3F) << 12 | (input[i + 2] & 0x3F) << 6 | input[i + 3] & 0x3F;
              if (_ucs2 > 0x10000 && _ucs2 < 0x110000) {
                _ucs2 -= 0x10000;
                out.push(String.fromCharCode(_ucs2 >>> 10 | 0xD800));
                out.push(String.fromCharCode(_ucs2 & 0x3FF | 0xDC00));
                i += 4;
                continue;
              }
            }
          }
          out.push(String.fromCharCode(0xFFFD));
          ++i;
        }

        return out.join('');
      }
    }, {
      key: '_checkContinuation',
      value: function _checkContinuation(uint8array, start, checkLength) {
        var array = uint8array;
        if (start + checkLength < array.length) {
          while (checkLength--) {
            if ((array[++start] & 0xC0) !== 0x80) {
              return false;
            }
          }
          return true;
        } else {
          return false;
        }
      }
    }]);

    return UTF8;
  }();

  var _createClass$p = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$q(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var isLe = sniffer.isLe;

  var DATA_TYPES = {
    NUMBER: 0,
    BOOLEAN: 1,
    STRING: 2,
    OBJECT: 3,
    MIX_ARRAY: 8,
    OBJECT_END: 9,
    STRICT_ARRAY: 10,
    DATE: 11,
    LONE_STRING: 12

    /**
     * meta信息解析
     */
  };
  var AMFParser = function () {
    function AMFParser() {
      _classCallCheck$q(this, AMFParser);

      this.offset = 0;
      this.readOffset = this.offset;
    }

    _createClass$p(AMFParser, [{
      key: 'resolve',
      value: function resolve(meta, size) {
        if (size < 3) {
          throw new Error('not enough data for metainfo');
        }
        var metaData = {};
        var name = this.parseValue(meta);
        var value = this.parseValue(meta, size - name.bodySize);
        metaData[name.data] = value.data;

        this.resetStatus();
        return metaData;
      }
    }, {
      key: 'resetStatus',
      value: function resetStatus() {
        this.offset = 0;
        this.readOffset = this.offset;
      }
    }, {
      key: 'parseString',
      value: function parseString(buffer) {
        var dv = new DataView(buffer, this.readOffset);
        var strLen = dv.getUint16(0, !isLe);
        var str = '';
        if (strLen > 0) {
          str = UTF8.decode(new Uint8Array(buffer, this.readOffset + 2, strLen));
        } else {
          str = '';
        }
        var size = strLen + 2;
        this.readOffset += size;
        return {
          data: str,
          bodySize: strLen + 2
        };
      }
    }, {
      key: 'parseDate',
      value: function parseDate(buffer, size) {
        var dv = new DataView(buffer, this.readOffset, size);
        var ts = dv.getFloat64(0, !isLe);
        var timeOffset = dv.getInt16(8, !isLe);
        ts += timeOffset * 60 * 1000;

        this.readOffset += 10;
        return {
          data: new Date(ts),
          bodySize: 10
        };
      }
    }, {
      key: 'parseObject',
      value: function parseObject(buffer, size) {
        var name = this.parseString(buffer, size);
        var value = this.parseValue(buffer, size - name.bodySize);
        return {
          data: {
            name: name.data,
            value: value.data
          },
          bodySize: name.bodySize + value.bodySize,
          isObjEnd: value.isObjEnd
        };
      }
    }, {
      key: 'parseLongString',
      value: function parseLongString(buffer) {
        var dv = new DataView(buffer, this.readOffset);
        var strLen = dv.getUint32(0, !isLe);
        var str = '';
        if (strLen > 0) {
          str = UTF8.decode(new Uint8Array(buffer, this.readOffset + 2, strLen));
        } else {
          str = '';
        }
        // const size = strLen + 4;
        this.readOffset += strLen + 4;
        return {
          data: str,
          bodySize: strLen + 4
        };
      }

      /**
       * 解析meta中的变量
       */

    }, {
      key: 'parseValue',
      value: function parseValue(data, size) {
        var buffer = new ArrayBuffer();
        if (data instanceof ArrayBuffer) {
          buffer = data;
        } else {
          buffer = data.buffer;
        }
        var NUMBER = DATA_TYPES.NUMBER,
            BOOLEAN = DATA_TYPES.BOOLEAN,
            STRING = DATA_TYPES.STRING,
            OBJECT = DATA_TYPES.OBJECT,
            MIX_ARRAY = DATA_TYPES.MIX_ARRAY,
            OBJECT_END = DATA_TYPES.OBJECT_END,
            STRICT_ARRAY = DATA_TYPES.STRICT_ARRAY,
            DATE = DATA_TYPES.DATE,
            LONE_STRING = DATA_TYPES.LONE_STRING;

        var dataView = new DataView(buffer, this.readOffset, size);
        var isObjEnd = false;
        var type = dataView.getUint8(0);
        var offset = 1;
        this.readOffset += 1;
        var value = null;

        switch (type) {
          case NUMBER:
            {
              value = dataView.getFloat64(1, !isLe);
              this.readOffset += 8;
              offset += 8;
              break;
            }
          case BOOLEAN:
            {
              var boolNum = dataView.getUint8(1);
              value = !!boolNum;
              this.readOffset += 1;
              offset += 1;
              break;
            }
          case STRING:
            {
              var str = this.parseString(buffer);
              value = str.data;
              offset += str.bodySize;
              break;
            }
          case OBJECT:
            {
              value = {};
              var objEndSize = 0;
              if (dataView.getUint32(size - 4, !isLe) & 0x00FFFFFF) {
                objEndSize = 3;
              }
              // this.readOffset += offset - 1;
              while (offset < size - 4) {
                var amfObj = this.parseObject(buffer, size - offset - objEndSize);
                if (amfObj.isObjectEnd) {
                  break;
                }
                value[amfObj.data.name] = amfObj.data.value;
                offset += amfObj.bodySize;
              }
              if (offset <= size - 3) {
                var mark = dataView.getUint32(offset - 1, !isLe) & 0x00FFFFFF;
                if (mark === 9) {
                  this.readOffset += 3;
                  offset += 3;
                }
              }
              break;
            }
          case MIX_ARRAY:
            {
              value = {};
              offset += 4;
              this.readOffset += 4;
              var _objEndSize = 0;
              if ((dataView.getUint32(size - 4, !isLe) & 0x00FFFFFF) === 9) {
                _objEndSize = 3;
              }

              while (offset < size - 8) {
                var amfVar = this.parseObject(buffer, size - offset - _objEndSize);
                if (amfVar.isObjectEnd) {
                  break;
                }
                value[amfVar.data.name] = amfVar.data.value;
                offset += amfVar.bodySize;
              }
              if (offset <= size - 3) {
                var marker = dataView.getUint32(offset - 1, !isLe) & 0x00FFFFFF;
                if (marker === 9) {
                  offset += 3;
                  this.readOffset += 3;
                }
              }
              break;
            }

          case OBJECT_END:
            {
              value = null;
              isObjEnd = true;
              break;
            }

          case STRICT_ARRAY:
            {
              value = [];
              var arrLength = dataView.getUint32(1, !isLe);
              offset += 4;
              this.readOffset += 4;
              for (var i = 0; i < arrLength; i++) {
                var script = this.parseValue(buffer, size - offset);
                value.push(script.data);
                offset += script.bodySize;
              }
              break;
            }

          case DATE:
            {
              var date = this.parseDate(buffer, size - 1);
              value = date.data;
              offset += date.bodySize;
              break;
            }

          case LONE_STRING:
            {
              var longStr = this.parseLongString(buffer, size - 1);
              value = longStr.data;
              offset += longStr.bodySize;
              break;
            }

          default:
            {
              offset = size;
            }
        }

        return {
          data: value,
          bodySize: offset,
          isObjEnd: isObjEnd
        };
      }
    }]);

    return AMFParser;
  }();

  var eventemitter3$1 = createCommonjsModule(function (module) {

  var has = Object.prototype.hasOwnProperty,
      prefix = '~';

  /**
   * Constructor to create a storage for our `EE` objects.
   * An `Events` instance is a plain object whose properties are event names.
   *
   * @constructor
   * @private
   */
  function Events() {}

  //
  // We try to not inherit from `Object.prototype`. In some engines creating an
  // instance in this way is faster than calling `Object.create(null)` directly.
  // If `Object.create(null)` is not supported we prefix the event names with a
  // character to make sure that the built-in object properties are not
  // overridden or used as an attack vector.
  //
  if (Object.create) {
    Events.prototype = Object.create(null);

    //
    // This hack is needed because the `__proto__` property is still inherited in
    // some old browsers like Android 4, iPhone 5.1, Opera 11 and Safari 5.
    //
    if (!new Events().__proto__) prefix = false;
  }

  /**
   * Representation of a single event listener.
   *
   * @param {Function} fn The listener function.
   * @param {*} context The context to invoke the listener with.
   * @param {Boolean} [once=false] Specify if the listener is a one-time listener.
   * @constructor
   * @private
   */
  function EE(fn, context, once) {
    this.fn = fn;
    this.context = context;
    this.once = once || false;
  }

  /**
   * Add a listener for a given event.
   *
   * @param {EventEmitter} emitter Reference to the `EventEmitter` instance.
   * @param {(String|Symbol)} event The event name.
   * @param {Function} fn The listener function.
   * @param {*} context The context to invoke the listener with.
   * @param {Boolean} once Specify if the listener is a one-time listener.
   * @returns {EventEmitter}
   * @private
   */
  function addListener(emitter, event, fn, context, once) {
    if (typeof fn !== 'function') {
      throw new TypeError('The listener must be a function');
    }

    var listener = new EE(fn, context || emitter, once),
        evt = prefix ? prefix + event : event;

    if (!emitter._events[evt]) emitter._events[evt] = listener, emitter._eventsCount++;else if (!emitter._events[evt].fn) emitter._events[evt].push(listener);else emitter._events[evt] = [emitter._events[evt], listener];

    return emitter;
  }

  /**
   * Clear event by name.
   *
   * @param {EventEmitter} emitter Reference to the `EventEmitter` instance.
   * @param {(String|Symbol)} evt The Event name.
   * @private
   */
  function clearEvent(emitter, evt) {
    if (--emitter._eventsCount === 0) emitter._events = new Events();else delete emitter._events[evt];
  }

  /**
   * Minimal `EventEmitter` interface that is molded against the Node.js
   * `EventEmitter` interface.
   *
   * @constructor
   * @public
   */
  function EventEmitter() {
    this._events = new Events();
    this._eventsCount = 0;
  }

  /**
   * Return an array listing the events for which the emitter has registered
   * listeners.
   *
   * @returns {Array}
   * @public
   */
  EventEmitter.prototype.eventNames = function eventNames() {
    var names = [],
        events,
        name;

    if (this._eventsCount === 0) return names;

    for (name in events = this._events) {
      if (has.call(events, name)) names.push(prefix ? name.slice(1) : name);
    }

    if (Object.getOwnPropertySymbols) {
      return names.concat(Object.getOwnPropertySymbols(events));
    }

    return names;
  };

  /**
   * Return the listeners registered for a given event.
   *
   * @param {(String|Symbol)} event The event name.
   * @returns {Array} The registered listeners.
   * @public
   */
  EventEmitter.prototype.listeners = function listeners(event) {
    var evt = prefix ? prefix + event : event,
        handlers = this._events[evt];

    if (!handlers) return [];
    if (handlers.fn) return [handlers.fn];

    for (var i = 0, l = handlers.length, ee = new Array(l); i < l; i++) {
      ee[i] = handlers[i].fn;
    }

    return ee;
  };

  /**
   * Return the number of listeners listening to a given event.
   *
   * @param {(String|Symbol)} event The event name.
   * @returns {Number} The number of listeners.
   * @public
   */
  EventEmitter.prototype.listenerCount = function listenerCount(event) {
    var evt = prefix ? prefix + event : event,
        listeners = this._events[evt];

    if (!listeners) return 0;
    if (listeners.fn) return 1;
    return listeners.length;
  };

  /**
   * Calls each of the listeners registered for a given event.
   *
   * @param {(String|Symbol)} event The event name.
   * @returns {Boolean} `true` if the event had listeners, else `false`.
   * @public
   */
  EventEmitter.prototype.emit = function emit(event, a1, a2, a3, a4, a5) {
    var evt = prefix ? prefix + event : event;

    if (!this._events[evt]) return false;

    var listeners = this._events[evt],
        len = arguments.length,
        args,
        i;

    if (listeners.fn) {
      if (listeners.once) this.removeListener(event, listeners.fn, undefined, true);

      switch (len) {
        case 1:
          return listeners.fn.call(listeners.context), true;
        case 2:
          return listeners.fn.call(listeners.context, a1), true;
        case 3:
          return listeners.fn.call(listeners.context, a1, a2), true;
        case 4:
          return listeners.fn.call(listeners.context, a1, a2, a3), true;
        case 5:
          return listeners.fn.call(listeners.context, a1, a2, a3, a4), true;
        case 6:
          return listeners.fn.call(listeners.context, a1, a2, a3, a4, a5), true;
      }

      for (i = 1, args = new Array(len - 1); i < len; i++) {
        args[i - 1] = arguments[i];
      }

      listeners.fn.apply(listeners.context, args);
    } else {
      var length = listeners.length,
          j;

      for (i = 0; i < length; i++) {
        if (listeners[i].once) this.removeListener(event, listeners[i].fn, undefined, true);

        switch (len) {
          case 1:
            listeners[i].fn.call(listeners[i].context);break;
          case 2:
            listeners[i].fn.call(listeners[i].context, a1);break;
          case 3:
            listeners[i].fn.call(listeners[i].context, a1, a2);break;
          case 4:
            listeners[i].fn.call(listeners[i].context, a1, a2, a3);break;
          default:
            if (!args) for (j = 1, args = new Array(len - 1); j < len; j++) {
              args[j - 1] = arguments[j];
            }

            listeners[i].fn.apply(listeners[i].context, args);
        }
      }
    }

    return true;
  };

  /**
   * Add a listener for a given event.
   *
   * @param {(String|Symbol)} event The event name.
   * @param {Function} fn The listener function.
   * @param {*} [context=this] The context to invoke the listener with.
   * @returns {EventEmitter} `this`.
   * @public
   */
  EventEmitter.prototype.on = function on(event, fn, context) {
    return addListener(this, event, fn, context, false);
  };

  /**
   * Add a one-time listener for a given event.
   *
   * @param {(String|Symbol)} event The event name.
   * @param {Function} fn The listener function.
   * @param {*} [context=this] The context to invoke the listener with.
   * @returns {EventEmitter} `this`.
   * @public
   */
  EventEmitter.prototype.once = function once(event, fn, context) {
    return addListener(this, event, fn, context, true);
  };

  /**
   * Remove the listeners of a given event.
   *
   * @param {(String|Symbol)} event The event name.
   * @param {Function} fn Only remove the listeners that match this function.
   * @param {*} context Only remove the listeners that have this context.
   * @param {Boolean} once Only remove one-time listeners.
   * @returns {EventEmitter} `this`.
   * @public
   */
  EventEmitter.prototype.removeListener = function removeListener(event, fn, context, once) {
    var evt = prefix ? prefix + event : event;

    if (!this._events[evt]) return this;
    if (!fn) {
      clearEvent(this, evt);
      return this;
    }

    var listeners = this._events[evt];

    if (listeners.fn) {
      if (listeners.fn === fn && (!once || listeners.once) && (!context || listeners.context === context)) {
        clearEvent(this, evt);
      }
    } else {
      for (var i = 0, events = [], length = listeners.length; i < length; i++) {
        if (listeners[i].fn !== fn || once && !listeners[i].once || context && listeners[i].context !== context) {
          events.push(listeners[i]);
        }
      }

      //
      // Reset the array, or remove it completely if we have no more listeners.
      //
      if (events.length) this._events[evt] = events.length === 1 ? events[0] : events;else clearEvent(this, evt);
    }

    return this;
  };

  /**
   * Remove all listeners, or those of the specified event.
   *
   * @param {(String|Symbol)} [event] The event name.
   * @returns {EventEmitter} `this`.
   * @public
   */
  EventEmitter.prototype.removeAllListeners = function removeAllListeners(event) {
    var evt;

    if (event) {
      evt = prefix ? prefix + event : event;
      if (this._events[evt]) clearEvent(this, evt);
    } else {
      this._events = new Events();
      this._eventsCount = 0;
    }

    return this;
  };

  //
  // Alias methods names because people roll like that.
  //
  EventEmitter.prototype.off = EventEmitter.prototype.removeListener;
  EventEmitter.prototype.addListener = EventEmitter.prototype.on;

  //
  // Expose the prefix.
  //
  EventEmitter.prefixed = prefix;

  //
  // Allow `EventEmitter` to be imported as module namespace.
  //
  EventEmitter.EventEmitter = EventEmitter;

  //
  // Expose the module.
  //
  {
    module.exports = EventEmitter;
  }
  });

  var _typeof$3 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

  var _createClass$q = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  var _get$1 = function get(object, property, receiver) {
    if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {
      var parent = Object.getPrototypeOf(object);if (parent === null) {
        return undefined;
      } else {
        return get(parent, property, receiver);
      }
    } else if ("value" in desc) {
      return desc.value;
    } else {
      var getter = desc.get;if (getter === undefined) {
        return undefined;
      }return getter.call(receiver);
    }
  };

  function _classCallCheck$r(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn$2(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && ((typeof call === "undefined" ? "undefined" : _typeof$3(call)) === "object" || typeof call === "function") ? call : self;
  }

  function _inherits$2(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof$3(superClass)));
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }
  var SpsParser = SPSParser,
      NalUnit = Nalunit;
  var SpsParserHEVC = SPSParser$1,
      NalUnitHEVC = Nalunit$1;

  var FlvDemuxer = function (_EventEmitter) {
    _inherits$2(FlvDemuxer, _EventEmitter);

    function FlvDemuxer() {
      _classCallCheck$r(this, FlvDemuxer);

      /** @type {boolean} */
      var _this = _possibleConstructorReturn$2(this, (FlvDemuxer.__proto__ || Object.getPrototypeOf(FlvDemuxer)).call(this));

      _this.headerParsed = false;
      /** @type {number} */
      _this.trackNum = 0;
      /** @type {boolean} */
      _this.hasScript = false;
      /** @type {boolean} */
      _this._videoMetaChange = false;
      /** @type {boolean} */
      _this._audioMetaChange = false;
      /** @type {number} */
      _this.gopId = 0;
      /** @type {*}  */
      _this.onMetaData = null;
      return _this;
    }

    /**
     *
     * @return {{FILE_HEADER_PARSED: string, AUDIO_META_PARSED: string, SCRIPT_TAG_PARSED: string, AUDIO_SAMPLE_PARSED: string, VIDEO_META_PARSED: string, VIDEO_SAMPLE_PARSED: string, VIDEO_SEI_PARSED: string}}
     * @constructor
     */

    _createClass$q(FlvDemuxer, [{
      key: 'demux',
      value: function demux(buffer) {
        if (!this.headerParsed) {
          if (buffer.length < 13) {
            return;
          }
          var header = buffer.shift(13);
          this.parseFlvHeader(header);
          this.demux(buffer); // recursive invoke
        } else {
          if (buffer.length < 11) {
            return;
          }
          var flvTag = void 0;

          var loopMax = 10000; // avoid Infinite loop
          do {
            flvTag = this._parseFlvTag(buffer);
          } while (flvTag && loopMax-- > 0);
        }
      }
    }, {
      key: 'parseFlvHeader',
      value: function parseFlvHeader(header) {
        if (!FlvDemuxer.isFlvFile(header)) {
          throw new Error('invalid flv file,' + header.join(','));
        } else {
          this.headerParsed = true;

          var hasAudio = header[4] >>> 2 === 1;
          var hasVideo = (header[4] & 1) === 1;
          this.emit(FlvDemuxer.EVENTS.FILE_HEADER_PARSED, {
            hasVideo: hasVideo,
            hasAudio: hasAudio
          });
        }
      }

      /**
       * Package the data as the following data structure
       * {
       *    data: Uint8Array. the Stream data.
       *    info: The first byte info of the Tag.
       *    tagType: 8、9、18
       *    timeStamp: the timestemp
       * }
       */

    }, {
      key: '_parseFlvTag',
      value: function _parseFlvTag(buffer) {
        var tagSize = buffer.toInt(1, 3);
        if (buffer.length < 11 + tagSize + 4) {
          // no enough data for tag parsing
          return null;
        }
        var flvTag = this._parseFlvTagHeader(buffer);
        if (flvTag) {
          this._processTagData(flvTag, buffer);
          if (!this._datasizeValidator(flvTag.datasize, buffer)) {
            throw new Error('TAG length error at ' + flvTag.datasize);
          }
        }
        return flvTag;
      }

      /**
       * Parse the 11 byte tag Header
       */

    }, {
      key: '_parseFlvTagHeader',
      value: function _parseFlvTagHeader(buffer) {
        var offset = 0;
        var flvTag = new FlvTag();

        var tagType = buffer.toInt(offset, 1);
        offset += 1;

        // 2 bit FMS reserved, 1 bit filtered, 5 bit tag type
        flvTag.filtered = (tagType & 32) >>> 5;
        flvTag.tagType = tagType & 31;

        // 3 Byte datasize
        flvTag.datasize = buffer.toInt(offset, 3);
        offset += 3;

        if (flvTag.tagType !== 8 && flvTag.tagType !== 9 && flvTag.tagType !== 11 && flvTag.tagType !== 18) {
          if (buffer && buffer.length > 0) {
            buffer.shift(1);
          }
          throw new Error('tagType ' + flvTag.tagType);
        }

        if (buffer.length < flvTag.datasize + 15) {
          return null;
        }

        // read the data.
        buffer.shift(4);

        // 3 Byte timestamp
        var timestamp = buffer.toInt(0, 3);
        buffer.shift(3);

        // 1 Byte timestampExt
        var timestampExt = buffer.shift(1)[0];
        if (timestampExt > 0) {
          timestamp += timestampExt * 0x1000000;
        }

        flvTag.dts = timestamp;

        // streamId
        buffer.shift(3);

        // 4 + 3 + 3 + 1 = 11 字节 TagHeader
        return flvTag;
      }
    }, {
      key: '_processTagData',
      value: function _processTagData(flvTag, buffer) {
        switch (flvTag.tagType) {
          case 18:
            this._parseScriptData(flvTag, buffer);
            break;
          case 8:
            this._parseAudioTag(flvTag, buffer);
            break;
          case 9:
            this._parseVideoData(flvTag, buffer);
            break;
          case 11:
            // for some CDN that did not process the currect RTMP messages
            buffer.shift(3);
            break;
          default:
            buffer.shift(1);
        }
      }

      /**
       * parse flv script data
       * @param flvTag
       * @private
       */

    }, {
      key: '_parseScriptData',
      value: function _parseScriptData(flvTag, buffer) {
        flvTag.data = buffer.shift(flvTag.datasize);
        var info = new AMFParser().resolve(flvTag.data, flvTag.data.length);

        this.onMetaData = info ? info.onMetaData : undefined;
        this.emit(FlvDemuxer.EVENTS.SCRIPT_TAG_PARSED, this.onMetaData);
      }

      // ISO中定义的AudioSpecificConfig
      // *  audioObjectType    5bit
      // *  samplingFrquecyIndex   4bit
      // *  if(samplingFrquencyIndex === 0xf)
      // *     samplingFrequency   24bit
      // *  channelConfiguration   4bit |01111000|

    }, {
      key: '_aacSequenceHeaderParser',
      value: function _aacSequenceHeaderParser(data) {
        var ret = {};
        ret.hasSpecificConfig = true;
        ret.objectType = data[1] >>> 3;
        ret.originObjectType = ret.objectType;
        ret.sampleRateIndex = (data[1] & 7) << 1 | data[2] >>> 7;
        ret.audiosamplerate = this._switchAudioSampleRate(ret.sampleRateIndex);
        ret.channelCount = (data[2] & 120) >>> 3;
        ret.frameLength = (data[2] & 4) >>> 2;
        ret.dependsOnCoreCoder = (data[2] & 2) >>> 1;
        ret.extensionFlagIndex = data[2] & 1;

        var userAgent = window.navigator.userAgent.toLowerCase();
        var extensionSamplingIndex = void 0;

        var config = void 0;
        var samplingIndex = ret.sampleRateIndex;

        if (userAgent.indexOf('firefox') !== -1) {
          // 火狐下 HE-AACv2编码方式 采样率是22050时候也要使用 LC-AAC
          // firefox: use SBR (HE-AAC) if freq less than 24kHz
          if (ret.sampleRateIndex >= 8) {
            ret.objectType = 5;
            config = new Array(4);
            extensionSamplingIndex = samplingIndex - 3;
          } else {
            // use LC-AAC
            ret.objectType = 2;
            config = new Array(2);
            extensionSamplingIndex = samplingIndex;
          }
        } else if (userAgent.indexOf('android') !== -1 || userAgent.indexOf('safari') !== -1 || userAgent.indexOf('iphone') !== -1) {
          // android: always use LC-AAC
          ret.objectType = 2;
          config = new Array(2);
          extensionSamplingIndex = samplingIndex;
        } else {
          // for other browsers, e.g. chrome...
          // Always use HE-AAC to make it easier to switch aac codec profile
          ret.objectType = 5;
          extensionSamplingIndex = ret.sampleRateIndex;
          config = new Array(4);

          if (ret.sampleRateIndex >= 6) {
            extensionSamplingIndex = ret.sampleRateIndex - 3;
          } else if (ret.channelCount === 1) {
            // Mono channel
            ret.objectType = 2;
            config = new Array(2);
            extensionSamplingIndex = ret.sampleRateIndex;
          }
        }
        ret.codec = 'mp4a.40.' + ret.objectType;
        config[0] = ret.objectType << 3;
        config[0] |= (ret.sampleRateIndex & 0x0F) >>> 1;
        config[1] = (ret.sampleRateIndex & 0x0F) << 7;
        config[1] |= (ret.channelCount & 0x0F) << 3;
        if (ret.objectType === 5) {
          config[1] |= (extensionSamplingIndex & 0x0F) >>> 1;
          config[2] = (extensionSamplingIndex & 0x01) << 7;
          // extended audio object type: force to 2 (LC-AAC)
          config[2] |= 2 << 2;
          config[3] = 0;
        }
        ret.config = config;
        return ret;
      }

      //   TagType == 8时
      //  *                     header.filter == 1  header.filter == 1
      //  *    | AudioTagHeader | EncryptionHeader? | FilterParams? | AUDIODATA |
      //  *
      //  * AudioTagHeader结构 (1或2字节):
      //  * SoundFormat : bit1-4 表示音频格式。 0:行内pcm数据，平台字节序 3:行内pcm,小端序 10:aac格式
      //  * SoundRate : bit5-6  采样率 0=5.5kHz 1=11kHz 2=22kHz  3=44kHz
      //  * SoundSize : 采样大小 0=8bit 1采样 1=16bit 1采样
      //  * SoundType : 声道类型 0=单声道 1=立体声
      //  * AACPacketType : 1字节 如果soundFormat==10 及aac编码格式,则存在这个字节
      //  *                      0=aac 序列header , 1=aac元数据

    }, {
      key: '_parseAudioTag',
      value: function _parseAudioTag(tag, buffer) {
        var meta = new AudioTrackMeta();
        var info = buffer.shift(1)[0];

        tag.data = buffer.shift(tag.datasize - 1);

        var format = (info & 240) >>> 4;

        if (format !== 10) {
          throw new Error('invalid audio format: ' + format);
        }

        if (format === 10) {
          meta.sampleRate = this._switchAudioSamplingFrequency(info);
          meta.sampleRateIndex = (info & 12) >>> 2;
          meta.frameLenth = (info & 2) >>> 1;
          meta.channelCount = info & 1;
          meta.refSampleDuration = Math.floor(1024 / meta.audioSampleRate * meta.timescale);
        }

        var audioSampleRate = meta.audioSampleRate;
        var audioSampleRateIndex = meta.sampleRateIndex;
        var refSampleDuration = meta.refSampleDuration;

        // AACPacketType
        if (tag.data[0] === 0) {
          // AAC Sequence Header
          var aacHeader = this._aacSequenceHeaderParser(tag.data);
          audioSampleRate = aacHeader.audiosamplerate || meta.audioSampleRate;
          audioSampleRateIndex = aacHeader.sampleRateIndex || meta.sampleRateIndex;
          refSampleDuration = Math.floor(1024 / audioSampleRate * meta.timescale);

          meta.channelCount = aacHeader.channelCount;
          meta.sampleRate = audioSampleRate;
          meta.sampleRateIndex = audioSampleRateIndex;
          meta.refSampleDuration = refSampleDuration;
          meta.duration = this.onMetaData.duration * meta.timescale;
          meta.config = aacHeader.config;
          meta.objectType = aacHeader.objectType;
          meta.originObjectType = aacHeader.originObjectType;
          this.emit(FlvDemuxer.EVENTS.AUDIO_META_PARSED, meta);
        } else {
          tag.data = tag.data.slice(1, tag.data.length);
          this.emit(FlvDemuxer.EVENTS.AUDIO_SAMPLE_PARSED, tag);
        }
      }

      // parse hevc/avc video data
      // *  TagType == 9时
      // *                      filter == 1         filter == 1
      // *    | VideoTagHeader | EncryptionHeader? | FilterParams? | VideoDATA |
      // *
      // * VideoTagHeader结构(AVC格式 5字节):
      // * FrameType : bit 1-4  1=keyFrame  2=inter frame
      // * CodecId : bit 5-8  编码器信息  7=AVC
      // * AvcPacketType :  1字节  codecId==7时存在   0=avc序列header 1=avc nalU 2=avc end of sequence
      // * CompositionTime  : 3字节 codecId==7时存在  取值 0 或者 具体值 单位ms

    }, {
      key: '_parseVideoData',
      value: function _parseVideoData(flvTag, buffer) {
        // header
        var header = buffer.shift(1)[0];
        var vSample = new VideoSample(flvTag);
        var frameType = (header & 0xf0) >>> 4;
        vSample.isKeyframe = frameType === 1;
        var codecID = header & 0x0f;

        // hevc和avc的header解析方式一样
        flvTag.avcPacketType = buffer.shift(1)[0];
        vSample.cts = buffer.toInt(0, 3);
        vSample.cts = vSample.cts << 8 >> 8;
        buffer.shift(3);

        // 12 for hevc, 7 for avc
        if (codecID === 7 || codecID === 12) {
          var _hevc = codecID === 12;
          var data = buffer.shift(flvTag.datasize - 5); // 减5字节header信息
          if (data[4] === 0 && data[5] === 0 && data[6] === 0 && data[7] === 1) {
            var avcclength = 0;
            for (var i = 0; i < 4; i++) {
              avcclength = avcclength * 256 + data[i];
            }
            avcclength -= 4;
            data = data.slice(4, data.length);
            data[3] = avcclength % 256;
            avcclength = (avcclength - data[3]) / 256;
            data[2] = avcclength % 256;
            avcclength = (avcclength - data[2]) / 256;
            data[1] = avcclength % 256;
            data[0] = (avcclength - data[1]) / 256;
          }

          vSample.data = data;

          var videoMeta = void 0;
          if (flvTag.avcPacketType === 0) {
            if (_hevc) {
              videoMeta = this._hevcSequenceHeaderParser(data);
            } else {
              videoMeta = this._avcSequenceHeaderParser(data);
            }
            videoMeta.codecID = codecID;
            this.emit(FlvDemuxer.EVENTS.VIDEO_META_PARSED, videoMeta);
          } else {
            var dv = new XGDataView(data.buffer);
            var nals = _hevc ? NalUnitHEVC.getHvccNals(dv) : NalUnit.getAvccNals(dv);
            var keyTypes = _hevc ? [19, 20, 21] : [5];
            for (var _i = 0; _i < nals.length; _i++) {
              var unit = nals[_i];
              _hevc ? NalUnitHEVC.analyseNal(unit) : NalUnit.analyseNal(unit);

              if (unit.sei) {
                this.emit(FlvDemuxer.EVENTS.VIDEO_SEI_PARSED, Object.assign(unit.sei, {
                  dts: flvTag.dts
                }));
              }
              if (keyTypes.indexOf(unit.type) > -1) {
                vSample.firstInGop = true;
                this.gopId++;
              }
            }

            vSample.gopId = this.gopId;
            vSample.nals = nals;

            this.emit(FlvDemuxer.EVENTS.VIDEO_SAMPLE_PARSED, vSample);
          }
        } else {
          throw new Error('video codeid is ' + codecID);
        }
      }

      /**
       * parse avc metadata
       *  configurationVerison = 1  uint(8)
       *  avcProfileIndication      uint(8)
       *  profile_compatibility     uint(8)
       *  avcLevelIndication        uint(8)
       *  reserved   `111111`       bit(6)
       *  lengthSizeMinusOne        uint(2)
       *  reserved   `111`          bit(3)
       *  numOfSPS                  uint(5)
       *  for(numOfSPS)
       *    spsLength               uint(16)
       *    spsNALUnit              spsLength个字节
       *  numOfPPS                  uint(8)
       *  for(numOfPPS)
       *     ppsLength              uint(16)
       *     ppsNALUnit             ppsLength个字节
       */

    }, {
      key: '_avcSequenceHeaderParser',
      value: function _avcSequenceHeaderParser(data) {
        var offset = 0;

        var meta = new VideoTrackMeta();

        meta.configurationVersion = data[0];
        meta.avcProfileIndication = data[1];
        meta.profileCompatibility = data[2];
        meta.avcLevelIndication = data[3] / 10;
        meta.nalUnitLength = (data[4] & 0x03) + 1;

        var numOfSps = data[5] & 0x1f;
        offset = 6;
        var config = {};

        // parse SPS
        for (var i = 0; i < numOfSps; i++) {
          var size = data[offset] * 255 + data[offset + 1];
          offset += 2;

          var sps = new Uint8Array(size);
          for (var j = 0; j < size; j++) {
            sps[j] = data[offset + j];
          }

          // codec string
          var codecString = 'avc1.';
          for (var _j = 1; _j < 4; _j++) {
            var h = sps[_j].toString(16);
            if (h.length < 2) {
              h = '0' + h;
            }
            codecString += h;
          }

          meta.codec = codecString;

          offset += size;
          meta.sps = sps;
          config = SpsParser.parseSPS(sps);
        }

        var numOfPps = data[offset];

        offset++;

        for (var _i2 = 0; _i2 < numOfPps; _i2++) {
          var _size = data[offset] * 255 + data[offset + 1];
          offset += 2;
          var pps = new Uint8Array(_size);
          for (var _j2 = 0; _j2 < _size; _j2++) {
            pps[_j2] = data[offset + _j2];
          }
          offset += _size;
          meta.pps = pps;
        }

        Object.assign(meta, SpsParser.toVideoMeta(config));

        meta.duration = this.onMetaData.duration * meta.timescale;
        meta.avcc = new Uint8Array(data.length);
        meta.avcc.set(data);
        meta.streamType = 0x1b;

        return meta;
      }

      /**
       * parse hevc metadata
       *  configurationVerison = 1  uint(8)
       *  generalProfileSpace       uint(2)
       *  generalTierFlag           uint(1)
       *  generalProfileIdc         uint(5)
       *  generalProfileCompatibilityFlags uint(32)
       *  generalConstraintIndicatorFlags  uint(48)
       *  generalLevelIdc           uint(8)
       *  complete_representation   bit(1)
       *  reserved   `111`          bit(3)
       *  segmentationIdc           uint(12)
       *  reserved `111111`         bit(6)
       *  parallelismType           uint(2)
       *  reserved `111111`         bit(6)
       *  chromaFormat              uint(2)
       *  reserved `11111`          bit(5)
       *  bitDepthLumaMinus8        uint(3)
       *  reserved `11111`          bit(5)
       *  bitDepthChromaMinus8      uint(3)
       *  avgFrameRate              bit(16)
       *  constantFrameRate         bit(2)
       *  numTemporalLayers         bit(3)
       *  temporalIdNested          bit(1)
       *  lengthSizeMinusOne        uint(2)
       *  numOfArrays               uint(8)
       *  for(numOfArrays)
       *    arrayCompleteness       bit(1)
       *    reserved                uint(1)
       *    nalUnitType             uint(16)
       *    numNals                 uint(16)
       *    for(numNals)
       *       nalUintLength        uint(16)
       *       nalUint              bit(8 * nalUintLength)
       */

    }, {
      key: '_hevcSequenceHeaderParser',
      value: function _hevcSequenceHeaderParser(data) {
        var meta = new VideoTrackMeta();

        var offset = 0;

        meta.configurationVersion = data[0];
        meta.hevcProfileSpace = (data[1] & 0xc0) >>> 6;
        meta.hevcTierFlag = (data[1] & 0x20) >>> 5;
        meta.hevcProfileIdc = data[1] & 0x1f;
        meta.hevcProfileCompatibilityFlags = [data[2], data[3], data[4], data[5]];
        meta.hevcConstraintIndicatorFlags = [data[6], data[7], data[8], data[9], data[10], data[11]];
        meta.hevcLevelIdc = data[12];
        meta.minSpatialSegmentationIdc = data[13] & 0x0f + data[14] << 4;
        meta.parallelismType = data[15] & 0x03;
        meta.chromaFormat = data[16] & 0x03;
        meta.bitDepthLumaMinus8 = data[17] & 0x07;
        meta.bitDepthChromaMinus8 = data[18] & 0x07;
        meta.avgFrameRate = data[19] * 256 + data[20];
        meta.constantFrameRate = (data[21] & 0xc0) >>> 6;
        meta.numTemporalLayers = (data[21] & 0x38) >>> 3;
        meta.temporalIdNested = (data[21] & 0x04) >>> 2;
        meta.lengthSizeMinusOne = data[21] & 0x03;
        var numOfArrays = data[22];

        offset = 23;
        var config = {};
        var nalUnitType = 0;
        var numNalus = 0;
        var nalUnitSize = 0;
        var hasVPS = false;
        var hasSPS = false;
        var hasPPS = false;
        var vps = void 0,
            sps = void 0,
            pps = void 0;
        for (var i = 0; i < numOfArrays; i++) {
          nalUnitType = data[offset] & 0x3f;
          numNalus = data[offset + 1] * 256 + data[offset + 2];
          offset += 3;
          for (var j = 0; j < numNalus; j++) {
            nalUnitSize = data[offset] * 256 + data[offset + 1];
            switch (nalUnitType) {
              case 32:
                if (!hasVPS) {
                  hasVPS = true;
                  vps = data.slice(offset + 2, offset + 2 + nalUnitSize);
                  meta.vps = SpsParserHEVC._ebsp2rbsp(vps);
                  meta.rawVps = vps;
                }
                break;
              case 33:
                if (!hasSPS) {
                  hasSPS = true;
                  sps = data.slice(offset + 2, offset + 2 + nalUnitSize);
                  meta.sps = SpsParserHEVC._ebsp2rbsp(sps);
                  meta.rawSps = sps;
                  meta.codec = 'hev1.1.6.L93.B0';
                  config = SpsParserHEVC.parseSPS(sps);
                }
                break;
              case 34:
                if (!hasPPS) {
                  hasPPS = true;
                  pps = data.slice(offset + 2, offset + 2 + nalUnitSize);
                  meta.pps = SpsParserHEVC._ebsp2rbsp(pps);
                  meta.rawPps = pps;
                }
                break;
            }
            offset += 2 + nalUnitSize;
          }
        }

        Object.assign(meta, SpsParserHEVC.toVideoMeta(config));

        meta.streamType = 0x24;

        return meta;
      }

      /**
       * choose audio sample rate
       * @param samplingFrequencyIndex
       * @returns {number}
       * @private
       */

    }, {
      key: '_switchAudioSampleRate',
      value: function _switchAudioSampleRate(samplingFrequencyIndex) {
        var samplingFrequencyList = [96000, 88200, 64000, 48000, 44100, 32000, 24000, 22050, 16000, 12000, 11025, 8000, 7350];
        return samplingFrequencyList[samplingFrequencyIndex];
      }

      /**
       * choose audio sampling frequence
       * @param info
       * @returns {number}
       * @private
       */

    }, {
      key: '_switchAudioSamplingFrequency',
      value: function _switchAudioSamplingFrequency(info) {
        var samplingFrequencyIndex = (info & 12) >>> 2;
        var samplingFrequencyList = [5500, 11025, 22050, 44100, 48000];
        return samplingFrequencyList[samplingFrequencyIndex];
      }

      /**
       * choose audio channel count
       * @param info
       * @returns {number}
       * @private
       */

    }, {
      key: '_switchAudioChannel',
      value: function _switchAudioChannel(info) {
        var sampleTrackNumIndex = info & 1;
        var sampleTrackNumList = [1, 2];
        return sampleTrackNumList[sampleTrackNumIndex];
      }

      /**
       * check datasize is valid use 4 Byte after current tag
       * @param datasize
       * @returns {boolean}
       * @private
       */

    }, {
      key: '_datasizeValidator',
      value: function _datasizeValidator(datasize, buffer) {
        var datasizeConfirm = buffer.toInt(0, 4);
        buffer.shift(4);
        return datasizeConfirm === datasize + 11;
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        _get$1(FlvDemuxer.prototype.__proto__ || Object.getPrototypeOf(FlvDemuxer.prototype), 'removeAllListeners', this).call(this);
      }
    }], [{
      key: 'isFlvFile',

      /**
       * if the flv head is valid
       * @param data
       * @returns {boolean}
       */
      value: function isFlvFile(data) {
        return !(data[0] !== 0x46 || data[1] !== 0x4C || data[2] !== 0x56 || data[3] !== 0x01);
      }

      /**
       * If the stream has audio or video.
       * @param {number} streamFlag - Data from the stream which is define whether the audio / video track is exist.
       */

    }, {
      key: 'getPlayType',
      value: function getPlayType(streamFlag) {
        var result = {
          hasVideo: false,
          hasAudio: false
        };

        if (streamFlag & 0x01 > 0) {
          result.hasVideo = true;
        }

        if (streamFlag & 0x04 > 0) {
          result.hasAudio = true;
        }

        return result;
      }
    }, {
      key: 'EVENTS',
      get: function get() {
        return {
          FILE_HEADER_PARSED: 'FILE_HEADER_PARSED',
          SCRIPT_TAG_PARSED: 'SCRIPT_TAG_PARSED',
          AUDIO_META_PARSED: 'AUDIO_META_PARSED',
          VIDEO_META_PARSED: 'VIDEO_META_PARSED',
          VIDEO_SAMPLE_PARSED: 'VIDEO_SAMPLE_PARSED',
          AUDIO_SAMPLE_PARSED: 'AUDIO_SAMPLE_PARSED',
          VIDEO_SEI_PARSED: 'VIDEO_SEI_PARSED'
        };
      }
    }]);

    return FlvDemuxer;
  }(eventemitter3$1);

  var _createClass$r = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$s(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var DEMUX_EVENTS$1 = EVENTS.DEMUX_EVENTS;
  var INTERNAL_EVENTS = FlvDemuxer.EVENTS;

  var FlvDemuxer$1 = function () {
    function FlvDemuxer$1() {
      _classCallCheck$s(this, FlvDemuxer$1);

      this.TAG = 'FLV_DEMUXER';
      this._firstFragmentLoaded = false;
      this._trackNum = 0;
      this._hasScript = false;
      this._videoMetaChange = false;
      this._audioMetaChange = false;
      this._hasVideoSequence = false;
      this._hasAudioSequence = false;
      this.demuxer = new FlvDemuxer();
    }

    _createClass$r(FlvDemuxer$1, [{
      key: 'init',
      value: function init() {
        this.on(DEMUX_EVENTS$1.DEMUX_START, this.demux.bind(this));
        this.demuxer.on(INTERNAL_EVENTS.FILE_HEADER_PARSED, this.handleFileHeaderParsed.bind(this));
        this.demuxer.on(INTERNAL_EVENTS.SCRIPT_TAG_PARSED, this.handleScriptTagParsed.bind(this));
        this.demuxer.on(INTERNAL_EVENTS.AUDIO_META_PARSED, this.handleAudioMetaParsed.bind(this));
        this.demuxer.on(INTERNAL_EVENTS.VIDEO_META_PARSED, this.handleVideoMetaParsed.bind(this));
        this.demuxer.on(INTERNAL_EVENTS.VIDEO_SAMPLE_PARSED, this.handleVideoSampleParsed.bind(this));
        this.demuxer.on(INTERNAL_EVENTS.AUDIO_SAMPLE_PARSED, this.handleAudioSampleParsed.bind(this));
        this.demuxer.on(INTERNAL_EVENTS.VIDEO_SEI_PARSED, this.handleSeiParsed.bind(this));
      }
    }, {
      key: 'handleAudioMetaParsed',
      value: function handleAudioMetaParsed(meta) {
        if (!this.tracks || !this.tracks.audioTrack) {
          return;
        }
        this._context.mediaInfo.hasAudio = true;
        this.tracks.audioTrack.meta = meta;
        if (!this._hasAudioSequence) {
          this.emit(DEMUX_EVENTS$1.METADATA_PARSED, 'audio');
        } else {
          this.emit(DEMUX_EVENTS$1.AUDIO_METADATA_CHANGE);
        }
      }
    }, {
      key: 'handleVideoMetaParsed',
      value: function handleVideoMetaParsed(meta) {
        if (!this.tracks || !this.tracks.videoTrack) {
          return;
        }
        this._context.mediaInfo.hasVideo = true;
        this.tracks.videoTrack.meta = meta;
        if (!this._hasVideoSequence) {
          this.emit(DEMUX_EVENTS$1.METADATA_PARSED, 'video');
        } else {
          this.emit(DEMUX_EVENTS$1.VIDEO_METADATA_CHANGE);
        }
      }
    }, {
      key: 'handleSeiParsed',
      value: function handleSeiParsed(seiObj) {
        this.emit(DEMUX_EVENTS$1.SEI_PARSED, seiObj);
      }
    }, {
      key: 'handleVideoSampleParsed',
      value: function handleVideoSampleParsed(sample) {
        if (!this.tracks || !this.tracks.videoTrack) {
          return;
        }
        if (sample.isKeyframe) {
          this.emit(DEMUX_EVENTS$1.ISKEYFRAME, sample.dts + sample.cts);
        }
        this.tracks.videoTrack.samples.push(sample);
      }
    }, {
      key: 'handleAudioSampleParsed',
      value: function handleAudioSampleParsed(sample) {
        if (!this.tracks || !this.tracks.videoTrack) {
          return;
        }
        this.tracks.audioTrack.samples.push(sample);
      }
    }, {
      key: 'handleScriptTagParsed',
      value: function handleScriptTagParsed(onMetaData) {
        var videoTrack = this.videoTrack,
            audioTrack = this.audioTrack;
        // fill mediaInfo

        this._context.mediaInfo.duration = onMetaData.duration;
        if (typeof onMetaData.hasAudio === 'boolean') {
          this._context.mediaInfo.hsaAudio = onMetaData.hasAudio;
        }
        if (typeof onMetaData.hasVideo === 'boolean') {
          this._context.mediaInfo.hasVideo = onMetaData.hasVideo;
        }
        this.emit(DEMUX_EVENTS$1.MEDIA_INFO);
        this._hasScript = true;

        // Edit default meta.
        if (audioTrack && !audioTrack.hasSpecificConfig) {
          var meta = audioTrack.meta;
          if (onMetaData.audiosamplerate) {
            meta.sampleRate = onMetaData.audiosamplerate;
          }

          if (onMetaData.audiochannels) {
            meta.channelCount = onMetaData.audiochannels;
          }

          switch (onMetaData.audiosamplerate) {
            case 44100:
              meta.sampleRateIndex = 4;
              break;
            case 22050:
              meta.sampleRateIndex = 7;
              break;
            case 11025:
              meta.sampleRateIndex = 10;
              break;
          }
        }
        if (videoTrack && !videoTrack.hasSpecificConfig) {
          var _meta = videoTrack.meta;
          if (typeof onMetaData.framerate === 'number') {
            var fpsNum = Math.floor(onMetaData.framerate * 1000);
            if (fpsNum > 0) {
              var fps = fpsNum / 1000;
              if (!_meta.frameRate) {
                _meta.frameRate = {};
              }
              _meta.frameRate.fixed = true;
              _meta.frameRate.fps = fps;
              _meta.frameRate.fps_num = fpsNum;
              _meta.frameRate.fps_den = 1000;
            }
          }
        }
      }
    }, {
      key: 'handleFileHeaderParsed',
      value: function handleFileHeaderParsed(_ref) {
        var hasVideo = _ref.hasVideo,
            hasAudio = _ref.hasAudio;

        this._context.mediaInfo.hasVideo = hasVideo;
        this._context.mediaInfo.hasAudio = hasAudio;

        this.initVideoTrack();
        this.initAudioTrack();
      }
    }, {
      key: 'demux',
      value: function demux() {
        if (this.loaderBuffer) {
          try {
            this.demuxer.demux(this.loaderBuffer);
          } catch (e) {
            // this.emit(DEMUX_EVENTS.DEMUX_ERROR, this.TAG, e)
          }
          this.emit(DEMUX_EVENTS$1.DEMUX_COMPLETE);
        }
      }
      /**
       * If the stream has audio or video.
       * @param {number} streamFlag - Data from the stream which is define whether the audio / video track is exist.
       */

    }, {
      key: 'initVideoTrack',

      /**
       * init default video track configs
       */
      value: function initVideoTrack() {
        this._trackNum++;
        var videoTrack = new VideoTrack();
        videoTrack.meta = new VideoTrackMeta();
        videoTrack.id = videoTrack.meta.id = this._trackNum;

        this.tracks.videoTrack = videoTrack;
      }

      /**
       * init default audio track configs
       */

    }, {
      key: 'initAudioTrack',
      value: function initAudioTrack() {
        this._trackNum++;
        var audioTrack = new AudioTrack();
        audioTrack.meta = new AudioTrackMeta();
        audioTrack.id = audioTrack.meta.id = this._trackNum;

        this.tracks.audioTrack = audioTrack;
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        if (this.demuxer) {
          this.demuxer.destroy();
        }
      }
    }, {
      key: 'loaderBuffer',
      get: function get() {
        var buffer = this._context.getInstance('LOADER_BUFFER');
        if (buffer) {
          return buffer;
        } else {
          this.emit(DEMUX_EVENTS$1.DEMUX_ERROR, this.TAG, new Error('找不到 loaderBuffer 实例'));
        }
      }
    }, {
      key: 'tracks',
      get: function get() {
        return this._context.getInstance('TRACKS');
      }
    }, {
      key: 'logger',
      get: function get() {
        return this._context.getInstance('LOGGER');
      }
    }], [{
      key: 'getPlayType',
      value: function getPlayType(streamFlag) {
        var result = {
          hasVideo: false,
          hasAudio: false
        };

        if (streamFlag & 0x01 > 0) {
          result.hasVideo = true;
        }

        if (streamFlag & 0x04 > 0) {
          result.hasAudio = true;
        }

        return result;
      }
    }]);

    return FlvDemuxer$1;
  }();

  var concat = createCommonjsModule(function (module, exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  exports.default = function (ResultConstructor) {
    var totalLength = 0;

    for (var _len = arguments.length, arrays = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      arrays[_key - 1] = arguments[_key];
    }

    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = arrays[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var arr = _step.value;

        totalLength += arr.length;
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }

    var result = new ResultConstructor(totalLength);
    var offset = 0;
    var _iteratorNormalCompletion2 = true;
    var _didIteratorError2 = false;
    var _iteratorError2 = undefined;

    try {
      for (var _iterator2 = arrays[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
        var _arr = _step2.value;

        result.set(_arr, offset);
        offset += _arr.length;
      }
    } catch (err) {
      _didIteratorError2 = true;
      _iteratorError2 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion2 && _iterator2.return) {
          _iterator2.return();
        }
      } finally {
        if (_didIteratorError2) {
          throw _iteratorError2;
        }
      }
    }

    return result;
  };
  });

  unwrapExports(concat);

  var lib = createCommonjsModule(function (module) {



  var _concat2 = _interopRequireDefault(concat);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
  }

  module.exports = _concat2.default;
  });

  var Concat = unwrapExports(lib);

  var _createClass$s = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$t(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var Buffer = function () {
    function Buffer(buffer) {
      _classCallCheck$t(this, Buffer);

      this.buffer = buffer || new Uint8Array(0);
    }

    _createClass$s(Buffer, [{
      key: 'write',
      value: function write() {
        var _this = this;

        for (var _len = arguments.length, buffer = Array(_len), _key = 0; _key < _len; _key++) {
          buffer[_key] = arguments[_key];
        }

        buffer.forEach(function (item) {
          _this.buffer = Concat(Uint8Array, _this.buffer, item);
        });
      }
    }], [{
      key: 'writeUint32',
      value: function writeUint32(value) {
        return new Uint8Array([value >> 24, value >> 16 & 0xff, value >> 8 & 0xff, value & 0xff]);
      }
    }, {
      key: 'readAsInt',
      value: function readAsInt(arr) {
        var temp = '';

        function padStart4Hex(hexNum) {
          var hexStr = hexNum.toString(16);
          return hexStr.padStart(2, '0');
        }

        arr.forEach(function (num) {
          temp += padStart4Hex(num);
        });
        return parseInt(temp, 16);
      }
    }]);

    return Buffer;
  }();

  var _createClass$t = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$u(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  // const UINT32_MAX = Math.pow(2, 32) - 1;

  var Fmp4 = function () {
    function Fmp4() {
      _classCallCheck$u(this, Fmp4);
    }

    _createClass$t(Fmp4, null, [{
      key: 'size',
      value: function size(value) {
        return Buffer.writeUint32(value);
      }
    }, {
      key: 'initBox',
      value: function initBox(size, name) {
        var buffer = new Buffer();

        for (var _len = arguments.length, content = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
          content[_key - 2] = arguments[_key];
        }

        buffer.write.apply(buffer, [Fmp4.size(size), Fmp4.type(name)].concat(content));
        return buffer.buffer;
      }
    }, {
      key: 'extension',
      value: function extension(version, flag) {
        return new Uint8Array([version, flag >> 16 & 0xff, flag >> 8 & 0xff, flag & 0xff]);
      }
    }, {
      key: 'ftyp',
      value: function ftyp() {
        return Fmp4.initBox(24, 'ftyp', new Uint8Array([0x69, 0x73, 0x6F, 0x6D, // isom,
        0x0, 0x0, 0x00, 0x01, // minor_version: 0x01
        0x69, 0x73, 0x6F, 0x6D, // isom
        0x61, 0x76, 0x63, 0x31 // avc1
        ]));
      }
    }, {
      key: 'ftypHEVC',
      value: function ftypHEVC() {
        return Fmp4.initBox(24, 'ftyp', new Uint8Array([0x69, 0x73, 0x6F, 0x6D, // isom,
        0x0, 0x0, 0x00, 0x01, // minor_version: 0x01
        0x69, 0x73, 0x6F, 0x6D, // isom
        0x64, 0x61, 0x73, 0x68 // hev1
        ]));
      }
    }, {
      key: 'moov',
      value: function moov(_ref) {
        var type = _ref.type,
            meta = _ref.meta;

        var size = 8;
        var mvhd = Fmp4.mvhd(meta.duration, meta.timescale);
        var trak = void 0;

        if (type === 'video') {
          trak = Fmp4.videoTrak(meta);
        } else {
          trak = Fmp4.audioTrak(meta);
        }

        var mvex = Fmp4.mvex(meta.duration, meta.timescale || 1000, meta.id);
        [mvhd, trak, mvex].forEach(function (item) {
          size += item.byteLength;
        });
        return Fmp4.initBox(size, 'moov', mvhd, trak, mvex);
      }
    }, {
      key: 'mvhd',
      value: function mvhd(duration) {
        var timescale = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1000;

        // duration *= timescale;
        var bytes = new Uint8Array([0x00, 0x00, 0x00, 0x00, // version(0) + flags     1位的box版本+3位flags   box版本，0或1，一般为0。（以下字节数均按version=0）
        0x00, 0x00, 0x00, 0x00, // creation_time    创建时间  （相对于UTC时间1904-01-01零点的秒数）
        0x00, 0x00, 0x00, 0x00, // modification_time   修改时间

        /**
               * timescale: 4 bytes文件媒体在1秒时间内的刻度值，可以理解为1秒长度
               */
        timescale >>> 24 & 0xFF, timescale >>> 16 & 0xFF, timescale >>> 8 & 0xFF, timescale & 0xFF,

        /**
               * duration: 4 bytes该track的时间长度，用duration和time scale值可以计算track时长，比如audio track的time scale = 8000,
               * duration = 560128，时长为70.016，video track的time scale = 600, duration = 42000，时长为70
               */
        duration >>> 24 & 0xFF, duration >>> 16 & 0xFF, duration >>> 8 & 0xFF, duration & 0xFF, 0x00, 0x01, 0x00, 0x00, // Preferred rate: 1.0   推荐播放速率，高16位和低16位分别为小数点整数部分和小数部分，即[16.16] 格式，该值为1.0（0x00010000）表示正常前向播放
        /**
               * PreferredVolume(1.0, 2bytes) + reserved(2bytes)
               * 与rate类似，[8.8] 格式，1.0（0x0100）表示最大音量
               */
        0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //  reserved: 4 + 4 bytes保留位
        0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, // ----begin composition matrix----
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 视频变换矩阵   线性代数
        0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, // ----end composition matrix----
        0x00, 0x00, 0x00, 0x00, // ----begin pre_defined 6 * 4 bytes----
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // pre-defined 保留位
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ----end pre_defined 6 * 4 bytes----
        0xFF, 0xFF, 0xFF, 0xFF // next_track_ID 下一个track使用的id号
        ]);
        return Fmp4.initBox(8 + bytes.length, 'mvhd', new Uint8Array(bytes));
      }
    }, {
      key: 'videoTrak',
      value: function videoTrak(data) {
        var size = 8;

        var tkhd = Fmp4.tkhd({
          id: 1,
          duration: data.duration,
          timescale: data.timescale || 1000,
          width: data.presentWidth,
          height: data.presentHeight,
          type: 'video'
        });
        var mdia = Fmp4.mdia({
          type: 'video',
          timescale: data.timescale || 1000,
          duration: data.duration,
          avcc: data.avcc,
          parRatio: data.parRatio,
          width: data.presentWidth,
          height: data.presentHeight,
          streamType: data.streamType
        });
        [tkhd, mdia].forEach(function (item) {
          size += item.byteLength;
        });
        return Fmp4.initBox(size, 'trak', tkhd, mdia);
      }
    }, {
      key: 'audioTrak',
      value: function audioTrak(data) {
        var size = 8;
        var tkhd = Fmp4.tkhd({
          id: 2,
          duration: data.duration,
          timescale: data.timescale || 1000,
          width: 0,
          height: 0,
          type: 'audio'
        });
        var mdia = Fmp4.mdia({
          type: 'audio',
          timescale: data.timescale || 1000,
          duration: data.duration,
          channelCount: data.channelCount,
          samplerate: data.sampleRate,
          config: data.config
        });
        [tkhd, mdia].forEach(function (item) {
          size += item.byteLength;
        });
        return Fmp4.initBox(size, 'trak', tkhd, mdia);
      }
    }, {
      key: 'tkhd',
      value: function tkhd(data) {
        var id = data.id;
        var duration = data.duration;
        var width = data.width;
        var height = data.height;
        var content = new Uint8Array([0x00, 0x00, 0x00, 0x07, // version(0) + flags 1位版本 box版本，0或1，一般为0。（以下字节数均按version=0）按位或操作结果值，预定义如下：
        // 0x000001 track_enabled，否则该track不被播放；
        // 0x000002 track_in_movie，表示该track在播放中被引用；
        // 0x000004 track_in_preview，表示该track在预览时被引用。
        // 一般该值为7，1+2+4 如果一个媒体所有track均未设置track_in_movie和track_in_preview，将被理解为所有track均设置了这两项；对于hint track，该值为0
        // hint track 这个特殊的track并不包含媒体数据，而是包含了一些将其他数据track打包成流媒体的指示信息。
        0x00, 0x00, 0x00, 0x00, // creation_time创建时间（相对于UTC时间1904-01-01零点的秒数）
        0x00, 0x00, 0x00, 0x00, // modification time 修改时间
        id >>> 24 & 0xFF, // track_ID: 4 bytes id号，不能重复且不能为0
        id >>> 16 & 0xFF, id >>> 8 & 0xFF, id & 0xFF, 0x00, 0x00, 0x00, 0x00, // reserved: 4 bytes    保留位
        duration >>> 24 & 0xFF, // duration: 4 bytes track的时间长度
        duration >>> 16 & 0xFF, duration >>> 8 & 0xFF, duration & 0xFF, 0x00, 0x00, 0x00, 0x00, // reserved: 2 * 4 bytes    保留位
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // layer(2bytes) + alternate_group(2bytes)  视频层，默认为0，值小的在上层.track分组信息，默认为0表示该track未与其他track有群组关系
        0x00, 0x00, 0x00, 0x00, // volume(2bytes) + reserved(2bytes)    [8.8] 格式，如果为音频track，1.0（0x0100）表示最大音量；否则为0   +保留位
        0x00, 0x01, 0x00, 0x00, // ----begin composition matrix----
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, // 视频变换矩阵
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, // ----end composition matrix----
        width >>> 8 & 0xFF, // //宽度
        width & 0xFF, 0x00, 0x00, height >>> 8 & 0xFF, // 高度
        height & 0xFF, 0x00, 0x00]);
        return Fmp4.initBox(8 + content.byteLength, 'tkhd', content);
      }
    }, {
      key: 'edts',
      value: function edts(data) {
        var buffer = new Buffer();
        var duration = data.duration;
        var mediaTime = data.mediaTime;
        buffer.write(Fmp4.size(36), Fmp4.type('edts'));
        // elst
        buffer.write(Fmp4.size(28), Fmp4.type('elst'));
        buffer.write(new Uint8Array([0x00, 0x00, 0x00, 0x01, // entry count
        duration >> 24 & 0xff, duration >> 16 & 0xff, duration >> 8 & 0xff, duration & 0xff, mediaTime >> 24 & 0xff, mediaTime >> 16 & 0xff, mediaTime >> 8 & 0xff, mediaTime & 0xff, 0x00, 0x00, 0x00, 0x01 // media rate
        ]));
        return buffer.buffer;
      }
    }, {
      key: 'mdia',
      value: function mdia(data) {
        var size = 8;
        var mdhd = Fmp4.mdhd(data.timescale, data.duration);
        var hdlr = Fmp4.hdlr(data.type);
        var minf = Fmp4.minf(data);
        [mdhd, hdlr, minf].forEach(function (item) {
          size += item.byteLength;
        });
        return Fmp4.initBox(size, 'mdia', mdhd, hdlr, minf);
      }
    }, {
      key: 'mdhd',
      value: function mdhd() {
        var timescale = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1000;
        var duration = arguments[1];

        var content = new Uint8Array([0x00, 0x00, 0x00, 0x00, // creation_time    创建时间
        0x00, 0x00, 0x00, 0x00, // modification_time修改时间
        timescale >>> 24 & 0xFF, // timescale: 4 bytes    文件媒体在1秒时间内的刻度值，可以理解为1秒长度
        timescale >>> 16 & 0xFF, timescale >>> 8 & 0xFF, timescale & 0xFF, duration >>> 24 & 0xFF, // duration: 4 bytes  track的时间长度
        duration >>> 16 & 0xFF, duration >>> 8 & 0xFF, duration & 0xFF, 0x55, 0xC4, // language: und (undetermined) 媒体语言码。最高位为0，后面15位为3个字符（见ISO 639-2/T标准中定义）
        0x00, 0x00 // pre_defined = 0
        ]);
        return Fmp4.initBox(12 + content.byteLength, 'mdhd', Fmp4.extension(0, 0), content);
      }
    }, {
      key: 'hdlr',
      value: function hdlr(type) {
        var value = [0x00, // version 0
        0x00, 0x00, 0x00, // flags
        0x00, 0x00, 0x00, 0x00, // pre_defined
        0x76, 0x69, 0x64, 0x65, // handler_type: 'vide'
        0x00, 0x00, 0x00, 0x00, // reserved
        0x00, 0x00, 0x00, 0x00, // reserved
        0x00, 0x00, 0x00, 0x00, // reserved
        0x56, 0x69, 0x64, 0x65, 0x6f, 0x48, 0x61, 0x6e, 0x64, 0x6c, 0x65, 0x72, 0x00 // name: 'VideoHandler'
        ];
        if (type === 'audio') {
          value.splice.apply(value, [8, 4].concat([0x73, 0x6f, 0x75, 0x6e]));
          value.splice.apply(value, [24, 13].concat([0x53, 0x6f, 0x75, 0x6e, 0x64, 0x48, 0x61, 0x6e, 0x64, 0x6c, 0x65, 0x72, 0x00]));
        }
        return Fmp4.initBox(8 + value.length, 'hdlr', new Uint8Array(value));
      }
    }, {
      key: 'minf',
      value: function minf(data) {
        var size = 8;
        var vmhd = data.type === 'video' ? Fmp4.vmhd() : Fmp4.smhd();
        var dinf = Fmp4.dinf();
        var stbl = Fmp4.stbl(data);
        [vmhd, dinf, stbl].forEach(function (item) {
          size += item.byteLength;
        });
        return Fmp4.initBox(size, 'minf', vmhd, dinf, stbl);
      }
    }, {
      key: 'vmhd',
      value: function vmhd() {
        return Fmp4.initBox(20, 'vmhd', new Uint8Array([0x00, // version
        0x00, 0x00, 0x01, // flags
        0x00, 0x00, // graphicsmode
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00 // opcolor
        ]));
      }
    }, {
      key: 'smhd',
      value: function smhd() {
        return Fmp4.initBox(16, 'smhd', new Uint8Array([0x00, // version
        0x00, 0x00, 0x00, // flags
        0x00, 0x00, // balance
        0x00, 0x00 // reserved
        ]));
      }
    }, {
      key: 'dinf',
      value: function dinf() {
        var buffer = new Buffer();
        var dref = [0x00, // version 0
        0x00, 0x00, 0x00, // flags
        0x00, 0x00, 0x00, 0x01, // entry_count
        0x00, 0x00, 0x00, 0x0c, // entry_size
        0x75, 0x72, 0x6c, 0x20, // 'url' type
        0x00, // version 0
        0x00, 0x00, 0x01 // entry_flags
        ];
        buffer.write(Fmp4.size(36), Fmp4.type('dinf'), Fmp4.size(28), Fmp4.type('dref'), new Uint8Array(dref));
        return buffer.buffer;
      }
    }, {
      key: 'stbl',
      value: function stbl(data) {
        var size = 8;
        var stsd = Fmp4.stsd(data);
        var stts = Fmp4.stts();
        var stsc = Fmp4.stsc();
        var stsz = Fmp4.stsz();
        var stco = Fmp4.stco();
        [stsd, stts, stsc, stsz, stco].forEach(function (item) {
          size += item.byteLength;
        });
        return Fmp4.initBox(size, 'stbl', stsd, stts, stsc, stsz, stco);
      }
    }, {
      key: 'stsd',
      value: function stsd(data) {
        var content = void 0;
        if (data.type === 'audio') {
          // if (!data.isAAC && data.codec === 'mp4') {
          //     content = FMP4.mp3(data);
          // } else {
          //
          // }

          // 支持mp4a
          content = Fmp4.mp4a(data);
        } else {
          if (data.streamType === 0x24) {
            content = Fmp4.hvc1(data);
          } else {
            content = Fmp4.avc1(data);
          }
        }
        return Fmp4.initBox(16 + content.byteLength, 'stsd', Fmp4.extension(0, 0), new Uint8Array([0x00, 0x00, 0x00, 0x01]), content);
      }
    }, {
      key: 'mp4a',
      value: function mp4a(data) {
        var content = new Uint8Array([0x00, 0x00, 0x00, // reserved
        0x00, 0x00, 0x00, // reserved
        0x00, 0x01, // data_reference_index
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // reserved
        0x00, data.channelCount, // channelcount
        0x00, 0x10, // sampleSize:16bits
        0x00, 0x00, 0x00, 0x00, // reserved2
        data.samplerate >> 8 & 0xff, data.samplerate & 0xff, //
        0x00, 0x00]);
        var esds = Fmp4.esds(data.config);
        return Fmp4.initBox(8 + content.byteLength + esds.byteLength, 'mp4a', content, esds);
      }
    }, {
      key: 'esds',
      value: function esds() {
        var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [43, 146, 8, 0];

        var configlen = config.length;
        var buffer = new Buffer();
        var content = new Uint8Array([0x00, // version 0
        0x00, 0x00, 0x00, // flags

        0x03, // descriptor_type
        0x17 + configlen, // length
        0x00, 0x01, // es_id
        0x00, // stream_priority

        0x04, // descriptor_type
        0x0f + configlen, // length
        0x40, // codec : mpeg4_audio
        0x15, // stream_type
        0x00, 0x00, 0x00, // buffer_size
        0x00, 0x00, 0x00, 0x00, // maxBitrate
        0x00, 0x00, 0x00, 0x00, // avgBitrate

        0x05 // descriptor_type
        ].concat([configlen]).concat(config).concat([0x06, 0x01, 0x02]));
        buffer.write(Fmp4.size(8 + content.byteLength), Fmp4.type('esds'), content);
        return buffer.buffer;
      }
    }, {
      key: 'avc1',
      value: function avc1(data) {
        var buffer = new Buffer();
        var size = 40; // 8(avc1)+8(avcc)+8(btrt)+16(pasp)
        // let sps = data.sps
        // let pps = data.pps
        var width = data.width;
        var height = data.height;
        var hSpacing = data.parRatio.width;
        var vSpacing = data.parRatio.height;
        // let avccBuffer = new Buffer()
        // avccBuffer.write(new Uint8Array([
        //   0x01, // version
        //   sps[1], // profile
        //   sps[2], // profile compatible
        //   sps[3], // level
        //   0xfc | 3,
        //   0xE0 | 1 // 目前只处理一个sps
        // ].concat([sps.length >>> 8 & 0xff, sps.length & 0xff])))
        // avccBuffer.write(sps, new Uint8Array([1, pps.length >>> 8 & 0xff, pps.length & 0xff]), pps)

        var avcc = data.avcc;
        var avc1 = new Uint8Array([0x00, 0x00, 0x00, // reserved
        0x00, 0x00, 0x00, // reserved
        0x00, 0x01, // data_reference_index
        0x00, 0x00, // pre_defined
        0x00, 0x00, // reserved
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // pre_defined
        width >> 8 & 0xff, width & 0xff, // width
        height >> 8 & 0xff, height & 0xff, // height
        0x00, 0x48, 0x00, 0x00, // horizresolution
        0x00, 0x48, 0x00, 0x00, // vertresolution
        0x00, 0x00, 0x00, 0x00, // reserved
        0x00, 0x01, // frame_count
        0x12, 0x64, 0x61, 0x69, 0x6C, // dailymotion/hls.js
        0x79, 0x6D, 0x6F, 0x74, 0x69, 0x6F, 0x6E, 0x2F, 0x68, 0x6C, 0x73, 0x2E, 0x6A, 0x73, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // compressorname
        0x00, 0x18, // depth = 24
        0x11, 0x11]); // pre_defined = -1
        var btrt = new Uint8Array([0x00, 0x1c, 0x9c, 0x80, // bufferSizeDB
        0x00, 0x2d, 0xc6, 0xc0, // maxBitrate
        0x00, 0x2d, 0xc6, 0xc0 // avgBitrate
        ]);
        var pasp = new Uint8Array([hSpacing >> 24, // hSpacing
        hSpacing >> 16 & 0xff, hSpacing >> 8 & 0xff, hSpacing & 0xff, vSpacing >> 24, // vSpacing
        vSpacing >> 16 & 0xff, vSpacing >> 8 & 0xff, vSpacing & 0xff]);

        buffer.write(Fmp4.size(size + avc1.byteLength + avcc.byteLength + btrt.byteLength), Fmp4.type('avc1'), avc1, Fmp4.size(8 + avcc.byteLength), Fmp4.type('avcC'), avcc, Fmp4.size(20), Fmp4.type('btrt'), btrt, Fmp4.size(16), Fmp4.type('pasp'), pasp);
        return buffer.buffer;
      }
    }, {
      key: 'hvc1',
      value: function hvc1(track) {
        var buffer = new Buffer();
        var content = new Uint8Array([0x00, 0x00, 0x00, // reserved
        0x00, 0x00, 0x00, // reserved
        0x00, 0x01, // data_reference_index
        0x00, 0x00, // pre_defined
        0x00, 0x00, // reserved
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // pre_defined
        track.width >> 8 & 0xFF, track.width & 0xff, // width
        track.height >> 8 & 0xFF, track.height & 0xff, // height
        0x00, 0x48, 0x00, 0x00, // horizresolution
        0x00, 0x48, 0x00, 0x00, // vertresolution
        0x00, 0x00, 0x00, 0x00, // reserved
        0x00, 0x01, // frame_count
        0x00, 0x00, 0x00, 0x00, 0x00, // dailymotion/hls.js
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // compressorname
        0x00, 0x18, // depth = 24
        0xFF, 0xFF, 0x00, 0x00, 0x00, 0x7A, 0x68, 0x76, 0x63, 0x43, 0x01, // configurationVersion
        0x01, // profile_space + tier_flag + profile_idc
        0x60, 0x00, 0x00, 0x00, // general_profile_compatibility
        0x90, 0x00, 0x00, 0x00, 0x00, 0x00, // constraint_indicator_flags
        0x5D, // level_idc=90
        0xF0, 0x00, 0xFC, 0xFD, // profile_compatibility_indications
        0xF8, // ‘11111’b + bitDepthLumaMinus8
        0xF8, // ‘11111’b + bitDepthChromaMinus8
        0x00, 0x00, // avgFrameRate
        0x0F, // constantFrameRate + numTemporalLayers + ‘1’b + lengthSizeMinusOne
        0x03, // numOfArrays

        // vps
        0xA0, 0x00, 0x01, // array_completeness + ‘0’b + NAL_unit_type + numNalus
        0x00, 0x18, // nalUnitLength
        0x40, 0x01, 0x0C, 0x01, 0xFF, 0xFF, 0x01, 0x60, 0x00, 0x00, 0x03, 0x00, 0x90, 0x00, 0x00, 0x03, 0x00, 0x00, 0x03, 0x00, 0x5D, 0x99, 0x98, 0x09,

        // sps
        0xA1, 0x00, 0x01, // array_completeness + ‘0’b + NAL_unit_type + numNalus
        0x00, 0x2D, // nalUnitLength
        0x42, 0x01, 0x01, 0x01, 0x60, 0x00, 0x00, 0x03, 0x00, 0x90, 0x00, 0x00, 0x03, 0x00, 0x00, 0x03, 0x00, 0x5D, 0xA0, 0x02, 0x80, 0x80, 0x2D, 0x16, 0x59, 0x99, 0xA4, 0x93, 0x2B, 0x9A, 0x80, 0x80, 0x80, 0x82, 0x00, 0x00, 0x03, 0x00, 0x02, 0x00, 0x00, 0x03, 0x00, 0x32, 0x10,

        // pps
        0xA2, 0x00, 0x01, // array_completeness + ‘0’b + NAL_unit_type + numNalus
        0x00, 0x07, // nalUnitLength
        0x44, 0x01, 0xC1, 0x72, 0xB4, 0x62, 0x40]);
        buffer.write(Fmp4.size(8 + content.byteLength + 10), Fmp4.type('hvc1'), content, Fmp4.size(10), Fmp4.type('fiel'), new Uint8Array([0x01, 0x00]));
        return buffer.buffer;
      }
    }, {
      key: 'stts',
      value: function stts() {
        var content = new Uint8Array([0x00, // version
        0x00, 0x00, 0x00, // flags
        0x00, 0x00, 0x00, 0x00 // entry_count
        ]);
        return Fmp4.initBox(16, 'stts', content);
      }
    }, {
      key: 'stsc',
      value: function stsc() {
        var content = new Uint8Array([0x00, // version
        0x00, 0x00, 0x00, // flags
        0x00, 0x00, 0x00, 0x00 // entry_count
        ]);
        return Fmp4.initBox(16, 'stsc', content);
      }
    }, {
      key: 'stco',
      value: function stco() {
        var content = new Uint8Array([0x00, // version
        0x00, 0x00, 0x00, // flags
        0x00, 0x00, 0x00, 0x00 // entry_count
        ]);
        return Fmp4.initBox(16, 'stco', content);
      }
    }, {
      key: 'stsz',
      value: function stsz() {
        var content = new Uint8Array([0x00, // version
        0x00, 0x00, 0x00, // flags
        0x00, 0x00, 0x00, 0x00, // sample_size
        0x00, 0x00, 0x00, 0x00 // sample_count
        ]);
        return Fmp4.initBox(20, 'stsz', content);
      }
    }, {
      key: 'mvex',
      value: function mvex(duration) {
        var trackID = arguments[2];

        var buffer = new Buffer();
        var mehd = Buffer.writeUint32(duration);
        buffer.write(Fmp4.size(56), Fmp4.type('mvex'), Fmp4.size(16), Fmp4.type('mehd'), Fmp4.extension(0, 0), mehd, Fmp4.trex(trackID));
        return buffer.buffer;
      }
    }, {
      key: 'trex',
      value: function trex(id) {
        var content = new Uint8Array([0x00, // version 0
        0x00, 0x00, 0x00, // flags
        id >> 24, id >> 16 & 0xff, id >> 8 & 0xff, id & 0xff, // track_ID
        0x00, 0x00, 0x00, 0x01, // default_sample_description_index
        0x00, 0x00, 0x00, 0x00, // default_sample_duration
        0x00, 0x00, 0x00, 0x00, // default_sample_size
        0x00, 0x01, 0x00, 0x01 // default_sample_flags
        ]);
        return Fmp4.initBox(8 + content.byteLength, 'trex', content);
      }
    }, {
      key: 'moof',
      value: function moof(data) {
        var size = 8;
        var mfhd = Fmp4.mfhd();
        var traf = Fmp4.traf(data);
        [mfhd, traf].forEach(function (item) {
          size += item.byteLength;
        });
        return Fmp4.initBox(size, 'moof', mfhd, traf);
      }
    }, {
      key: 'mfhd',
      value: function mfhd() {
        var content = Buffer.writeUint32(Fmp4.sequence);
        Fmp4.sequence += 1;
        return Fmp4.initBox(16, 'mfhd', Fmp4.extension(0, 0), content);
      }
    }, {
      key: 'traf',
      value: function traf(data) {
        var size = 8;
        var tfhd = Fmp4.tfhd(data.id);
        var tfdt = Fmp4.tfdt(data.time);
        var sdtp = Fmp4.sdtp(data);
        var trun = Fmp4.trun(data, sdtp.byteLength);

        [tfhd, tfdt, trun, sdtp].forEach(function (item) {
          size += item.byteLength;
        });
        return Fmp4.initBox(size, 'traf', tfhd, tfdt, trun, sdtp);
      }
    }, {
      key: 'tfhd',
      value: function tfhd(id) {
        var content = Buffer.writeUint32(id);
        return Fmp4.initBox(16, 'tfhd', Fmp4.extension(0, 0), content);
      }
    }, {
      key: 'tfdt',
      value: function tfdt(time) {
        // let upper = Math.floor(time / (UINT32_MAX + 1)),
        //     lower = Math.floor(time % (UINT32_MAX + 1));
        return Fmp4.initBox(16, 'tfdt', Fmp4.extension(0, 0), Buffer.writeUint32(time));
      }
    }, {
      key: 'trun',
      value: function trun(data, sdtpLength) {
        // let id = data.id;
        // let ceil = id === 1 ? 16 : 12;
        var buffer = new Buffer();
        var sampleCount = Buffer.writeUint32(data.samples.length);
        // mdat-header 8
        // moof-header 8
        // mfhd 16
        // traf-header 8
        // thhd 16
        // tfdt 20
        // trun-header 12
        // sampleCount 4
        // data-offset 4
        // samples.length
        var offset = Buffer.writeUint32(8 + 8 + 16 + 8 + 16 + 16 + 12 + 4 + 4 + 16 * data.samples.length + sdtpLength);
        buffer.write(Fmp4.size(20 + 16 * data.samples.length), Fmp4.type('trun'), new Uint8Array([0x00, 0x00, 0x0F, 0x01]), sampleCount, offset);

        // let size = buffer.buffer.byteLength
        // let writeOffset = 0
        // data.samples.forEach(() => {
        //   size += 16
        // })
        //
        // let trunBox = new Uint8Array(size)

        // trunBox.set(buffer.buffer, 0)

        data.samples.forEach(function (item) {
          var flags = item.flags;
          // console.log(item.type, item.dts, item.duration)

          buffer.write(new Uint8Array([item.duration >>> 24 & 0xFF, // sample_duration
          item.duration >>> 16 & 0xFF, item.duration >>> 8 & 0xFF, item.duration & 0xFF, item.size >>> 24 & 0xFF, // sample_size
          item.size >>> 16 & 0xFF, item.size >>> 8 & 0xFF, item.size & 0xFF, flags.isLeading << 2 | flags.dependsOn, // sample_flags
          flags.isDependedOn << 6 | flags.hasRedundancy << 4 | flags.isNonSync, 0x00, 0x00, // sample_degradation_priority
          item.cts >>> 24 & 0xFF, // sample_composition_time_offset
          item.cts >>> 16 & 0xFF, item.cts >>> 8 & 0xFF, item.cts & 0xFF]));
          // writeOffset += 16
          // buffer.write(Buffer.writeUint32(0));
        });
        return buffer.buffer;
      }
    }, {
      key: 'sdtp',
      value: function sdtp(data) {
        var buffer = new Buffer();
        buffer.write(Fmp4.size(12 + data.samples.length), Fmp4.type('sdtp'), Fmp4.extension(0, 0));
        data.samples.forEach(function (item) {
          var flags = item.flags;
          var num = flags.isLeading << 6 | // is_leading: 2 (bit)
          flags.dependsOn << 4 | // sample_depends_on
          flags.isDependedOn << 2 | // sample_is_depended_on
          flags.hasRedundancy; // sample_has_redundancy

          buffer.write(new Uint8Array([num]));
        });
        return buffer.buffer;
      }
    }, {
      key: 'mdat',
      value: function mdat(data) {
        var buffer = new Buffer();
        var size = 8;
        data.samples.forEach(function (item) {
          size += item.size;
        });
        buffer.write(Fmp4.size(size), Fmp4.type('mdat'));
        var mdatBox = new Uint8Array(size);
        var offset = 0;
        mdatBox.set(buffer.buffer, offset);
        offset += 8;
        data.samples.forEach(function (item) {
          item.buffer.forEach(function (unit) {
            mdatBox.set(unit, offset);
            offset += unit.byteLength;
            // buffer.write(unit.data);
          });
        });
        return mdatBox;
      }
    }]);

    return Fmp4;
  }();

  Fmp4.type = function (name) {
    return new Uint8Array([name.charCodeAt(0), name.charCodeAt(1), name.charCodeAt(2), name.charCodeAt(3)]);
  };
  Fmp4.sequence = 1;

  var _typeof$4 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

  var _createClass$u = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$v(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn$3(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && ((typeof call === "undefined" ? "undefined" : _typeof$4(call)) === "object" || typeof call === "function") ? call : self;
  }

  function _inherits$3(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof$4(superClass)));
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  /**
   * @typedef { import('xgplayer-helper-models').VideoTrack } VideoTrack
   */
  /**
   * @typedef { import('xgplayer-helper-models').AudioTrack } AudioTrack
   */

  var Mp4Remuxer = function (_EventEmitter) {
    _inherits$3(Mp4Remuxer, _EventEmitter);

    function Mp4Remuxer(_ref) {
      var videoMeta = _ref.videoMeta,
          audioMeta = _ref.audioMeta,
          curTime = _ref.curTime;

      _classCallCheck$v(this, Mp4Remuxer);

      var _this = _possibleConstructorReturn$3(this, (Mp4Remuxer.__proto__ || Object.getPrototypeOf(Mp4Remuxer)).call(this));

      _this.TAG = 'Fmp4Remuxer';
      _this._dtsBase = curTime * 1000;

      _this._videoMeta = videoMeta;
      _this._audioMeta = audioMeta;

      _this._audioDtsBase = null;
      _this._videoDtsBase = null;
      _this._isDtsBaseInited = false;

      _this.isFirstVideo = true;
      _this.isFirstAudio = true;

      _this.videoAllDuration = 0;
      _this.audioAllDuration = 0;

      _this.audioRemuxed = 0;
      _this.videoRemuxed = 0;
      _this.mp4Durations = {
        keys: []
      };
      return _this;
    }

    _createClass$u(Mp4Remuxer, [{
      key: 'destroy',
      value: function destroy() {
        this._dtsBase = -1;
        this._isDtsBaseInited = false;
        this.mp4Durations = {
          keys: []
        };

        this.removeAllListeners();
      }

      /**
       * @param {AudioTrack} audioTrack
       * @param {VideoTrack} videoTrack
       */

    }, {
      key: 'remux',
      value: function remux(audioTrack, videoTrack) {
        !this._isDtsBaseInited && this.calcDtsBase(audioTrack, videoTrack);
        this.remuxVideo(videoTrack);
        this.remuxAudio(audioTrack);

        logger.groupEnd();
      }
    }, {
      key: 'resetDtsBase',
      value: function resetDtsBase() {
        this._dtsBase = 0;
      }
    }, {
      key: 'seek',
      value: function seek(time) {
        logger.log(this.TAG, 'set dtsBase for seek(),time=', time);
        if (!this._isDtsBaseInited) {
          this._dtsBase = time * 1000;
        } else {
          this._isDtsBaseInited = false;
          this._dtsBase = time * 1000;
        }

        this._audioDtsBase = this._videoDtsBase = null;
      }

      /**
       * @param {'video' | 'audio' } type
       * @param {*} meta
       * @return {Buffer}
       */

    }, {
      key: 'remuxInitSegment',
      value: function remuxInitSegment(type, meta) {
        logger.log(this.TAG, 'remuxInitSegment: type=', type);
        var initSegment = new Buffer();
        var ftyp = meta.streamType === 0x24 ? Fmp4.ftypHEVC() : Fmp4.ftyp();
        var moov = Fmp4.moov({ type: type, meta: meta });

        initSegment.write(ftyp, moov);
        return initSegment;
      }

      /**
       * @param {AudioTrack} audioTrack
       * @param {VideoTrack} videoTrack
       */

    }, {
      key: 'calcDtsBase',
      value: function calcDtsBase(audioTrack, videoTrack) {
        if (!audioTrack && videoTrack.samples.length) {
          var firstSample = videoTrack.samples[0];
          var _start = void 0;
          if (firstSample.options && firstSample.options.start) {
            _start = firstSample.options.start;
          }
          this._videoDtsBase = firstSample.dts - (_start || this._dtsBase);
          this._isDtsBaseInited = true;
          return;
        }

        if ((!audioTrack || !audioTrack.samples.length) && (!videoTrack || !videoTrack.samples.length)) {
          return;
        }

        var audioBase = null;
        var videoBase = null;
        var start = null;
        if (audioTrack && audioTrack.samples && audioTrack.samples.length) {
          var _firstSample = audioTrack.samples[0];
          audioBase = _firstSample.dts;
          if (_firstSample.options && _firstSample.options.start) {
            start = _firstSample.options.start;
          }
        }
        if (videoTrack && videoTrack.samples && videoTrack.samples.length) {
          var _firstSample2 = videoTrack.samples[0];
          videoBase = _firstSample2.dts;
          if (_firstSample2.options && _firstSample2.options.start) {
            start = _firstSample2.options.start;
          }
        }

        var delta = audioBase - videoBase;
        // 临时兼容逻辑， hls 考虑上av之间的差值
        if (delta < 0 && start !== null) {
          videoTrack.samples.forEach(function (x) {
            x.dts -= delta;
            if (x.pts) {
              x.pts -= delta;
            }
          });
        }

        this._videoDtsBase = (videoBase || audioBase) - (start || this._dtsBase);
        this._audioDtsBase = (audioBase || videoBase) - (start || this._dtsBase);
        this._dtsBase = Math.min(videoBase, audioBase);
        this._isDtsBaseInited = true;

        logger.log(this.TAG, 'calcDtsBase');
        logger.log(this.TAG, 'video first dts: ' + videoBase + ' , start:' + start + ' , _videoDtsBase:' + this._videoDtsBase + ' , _dtsBase:' + this._dtsBase);
        logger.log(this.TAG, 'audio first dts: ' + audioBase + ' , start:' + start + ' , _audioDtsBase:' + this._audioDtsBase + ', _dtsBase:' + this._dtsBase);
      }

      /**
       * @param {VideoTrack}videoTrack
       * @return {*}
       * @private
       */

    }, {
      key: 'remuxVideo',
      value: function remuxVideo(videoTrack) {
        var _this2 = this;

        var track = videoTrack || {};

        if (!videoTrack || !videoTrack.samples || !videoTrack.samples.length) {
          return;
        }

        var samples = track.samples,
            meta = track.meta;

        if (!meta) return;

        var firstDts = -1;

        var initSegment = null;
        var mp4Samples = [];
        var mdatBox = {
          samples: []
        };

        var maxLoop = 10000;
        while (samples.length && maxLoop-- > 0) {
          var avcSample = samples.shift();

          var isKeyframe = avcSample.isKeyframe,
              options = avcSample.options;

          if (!this.isFirstVideo && options && options.meta) {
            initSegment = this.remuxInitSegment('video', options.meta);
            options.meta = null;
            samples.unshift(avcSample);
            if (!options.isContinue) {
              this._videoDtsBase = 0;
            }
            break;
          }
          var dts = Math.max(0, avcSample.dts - this.videoDtsBase);
          if (firstDts === -1) {
            firstDts = dts;
          }

          var cts = void 0;
          var pts = void 0;
          if (avcSample.pts !== undefined) {
            pts = avcSample.pts - this._dtsBase;
            cts = pts - dts;
          }
          if (avcSample.cts !== undefined) {
            pts = avcSample.cts + dts;
            cts = avcSample.cts;
          }

          var mdatSample = {
            buffer: [],
            size: 0
          };

          var sampleDuration = 0;
          if (avcSample.duration) {
            sampleDuration = avcSample.duration;
          } else if (samples.length >= 1) {
            var nextDts = samples[0].dts - this.videoDtsBase;
            sampleDuration = nextDts - dts;
          } else {
            if (mp4Samples.length >= 1) {
              // lastest sample, use second last duration
              sampleDuration = mp4Samples[mp4Samples.length - 1].duration;
            } else {
              // the only one sample, use reference duration
              sampleDuration = this._videoMeta.refSampleDuration;
            }
          }
          this.videoAllDuration += sampleDuration;
          if (logger.long) {
            logger.log(this.TAG, 'video dts ' + dts, 'pts ' + pts, 'cts: ' + cts, isKeyframe, 'originDts ' + avcSample.originDts, 'duration ' + sampleDuration);
          }
          if (sampleDuration >= 0) {
            mdatBox.samples.push(mdatSample);
            mdatSample.buffer.push(avcSample.data);
            mdatSample.size += avcSample.data.byteLength;
            //  manage same pts sample
            if (this.mp4Durations[pts]) {
              pts += this.mp4Durations[pts];
              cts = pts - dts;
            }
            mp4Samples.push({
              dts: dts,
              cts: cts,
              pts: pts,
              data: avcSample.data,
              size: avcSample.data.byteLength,
              isKeyframe: isKeyframe,
              duration: sampleDuration,
              flags: {
                isLeading: 0,
                dependsOn: isKeyframe ? 2 : 1,
                isDependedOn: isKeyframe ? 1 : 0,
                hasRedundancy: 0,
                isNonSync: isKeyframe ? 0 : 1
              },
              originDts: dts,
              type: 'video'
            });
            this.mp4Durations[pts] = sampleDuration;
            this.mp4Durations.keys.push(pts);
          }

          if (isKeyframe) {
            this.emit(Mp4Remuxer.EVENTS.RANDOM_ACCESS_POINT, pts);
          }
        }
        // delete too much data
        if (this.mp4Durations.keys.length > 1e4) {
          var tmp = this.mp4Durations;
          this.mp4Durations = {};
          this.mp4Durations.keys = tmp.keys.slice(-100);
          this.mp4Durations.keys.forEach(function (key) {
            _this2.mp4Durations[key] = tmp[key];
          });
        }
        if (mp4Samples.length) {
          logger.log(this.TAG, 'remux to mp4 video:', [firstDts / 1000, mp4Samples[mp4Samples.length - 1].dts / 1000]);
        }

        var moofMdat = new Buffer();
        if (mp4Samples.length && track.meta) {
          var moof = Fmp4.moof({
            id: track.meta.id,
            time: firstDts,
            samples: mp4Samples
          });
          var mdat = Fmp4.mdat(mdatBox);
          moofMdat.write(moof, mdat);

          this.segmentRemuxed('video', moofMdat, mp4Samples[mp4Samples.length - 1].pts - mp4Samples[0].pts);
        }

        if (initSegment) {
          this.segmentRemuxed('video', initSegment);

          if (samples.length) {
            // second part of stream change
            track.samples = samples;
            return this.remuxVideo(track);
          }
        }

        this.isFirstVideo = false;
        this.emit(Mp4Remuxer.EVENTS.TRACK_REMUXED, 'video', moofMdat);

        track.samples = [];
        track.length = 0;
      }

      /**
       *
       * @param {AudioTrack} track
       * @return {*}
       * @private
       */

    }, {
      key: 'remuxAudio',
      value: function remuxAudio(track) {
        var _ref2 = track || {},
            samples = _ref2.samples;

        var firstDts = -1;
        var mp4Samples = [];

        var initSegment = null;
        var mdatBox = {
          samples: []
        };
        if (!samples || !samples.length) {
          return;
        }

        var maxLoop = 10000;
        var isFirstDtsInited = false;
        while (samples.length && maxLoop-- > 0) {
          var sample = samples.shift();
          var data = sample.data,
              options = sample.options;

          if (!this.isFirstAudio && options && options.meta) {
            initSegment = this.remuxInitSegment('audio', options.meta);
            options.meta = null;
            samples.unshift(sample);
            if (!options.isContinue) {
              this._audioDtsBase = 0;
            }
            break;
          }

          var dts = Math.max(0, sample.dts - this.audioDtsBase);
          var originDts = sample.originDts;
          if (!isFirstDtsInited) {
            firstDts = dts;
            isFirstDtsInited = true;
          }

          var sampleDuration = 0;
          if (sample.duration) {
            sampleDuration = sample.duration;
          } else if (this._audioMeta.refSampleDurationFixed) {
            sampleDuration = this._audioMeta.refSampleDurationFixed;
          } else if (samples.length >= 1) {
            var nextDts = samples[0].dts - this.audioDtsBase;
            sampleDuration = nextDts - dts;
          } else {
            if (mp4Samples.length >= 1) {
              // use second last sample duration
              sampleDuration = mp4Samples[mp4Samples.length - 1].duration;
            } else {
              // the only one sample, use reference sample duration
              sampleDuration = this._audioMeta.refSampleDuration;
            }
          }

          if (logger.long) {
            logger.log(this.TAG, 'audio dts ' + dts, 'pts ' + dts, 'originDts ' + originDts, 'duration ' + sampleDuration);
          }
          this.audioAllDuration += sampleDuration;
          var mp4Sample = {
            dts: dts,
            pts: dts,
            cts: 0,
            size: data.byteLength,
            duration: sample.duration ? sample.duration : sampleDuration,
            flags: {
              isLeading: 0,
              dependsOn: 1,
              isDependedOn: 0,
              hasRedundancy: 0,
              isNonSync: 0
            },
            isKeyframe: true,
            originDts: originDts,
            type: 'audio'
          };

          var mdatSample = {
            buffer: [],
            size: 0
          };

          if (sampleDuration >= 0) {
            mdatSample.buffer.push(data);
            mdatSample.size += data.byteLength;

            mdatBox.samples.push(mdatSample);
            mp4Samples.push(mp4Sample);
          }
        }

        var moofMdat = new Buffer();

        if (mp4Samples.length && track.meta) {
          logger.log(this.TAG, 'remux to mp4 audio:', [firstDts / 1000, mp4Samples[mp4Samples.length - 1].dts / 1000]);
          var moof = Fmp4.moof({
            id: track.meta.id,
            time: firstDts,
            samples: mp4Samples
          });
          var mdat = Fmp4.mdat(mdatBox);
          moofMdat.write(moof, mdat);

          this.segmentRemuxed('audio', moofMdat, mp4Samples[mp4Samples.length - 1].dts - mp4Samples[0].dts);
        }

        if (initSegment) {
          this.segmentRemuxed('audio', initSegment);
          if (samples.length) {
            // second part of stream change
            track.samples = samples;
            return this.remuxAudio(track);
          }
        }

        this.isFirstAudio = false;
        this.emit(Mp4Remuxer.EVENTS.TRACK_REMUXED, 'audio', moofMdat);

        track.samples = [];
        track.length = 0;
      }
    }, {
      key: 'segmentRemuxed',
      value: function segmentRemuxed(type, buffer, bufferDuration) {
        this.emit(Mp4Remuxer.EVENTS.MEDIA_SEGMENT, type, buffer, bufferDuration);
      }
    }, {
      key: 'videoDtsBase',
      get: function get() {
        if (this._videoDtsBase !== null) {
          return this._videoDtsBase;
        }
        return this._dtsBase;
      },
      set: function set(value) {
        this._videoDtsBase = value;
      }
    }, {
      key: 'audioDtsBase',
      get: function get() {
        if (this._audioDtsBase !== null) {
          return this._audioDtsBase;
        }
        return this._dtsBase;
      }
    }], [{
      key: 'EVENTS',
      get: function get() {
        return {
          MEDIA_SEGMENT: 'MEDIA_SEGMENT',
          INIT_SEGMENT: 'INIT_SEGMENT',
          RANDOM_ACCESS_POINT: 'RANDOM_ACCESS_POINT',
          TRACK_REMUXED: 'TRACK_REMUXED'
        };
      }
    }]);

    return Mp4Remuxer;
  }(eventemitter3$1);

  var _createClass$v = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$w(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var REMUX_EVENTS$2 = EVENTS.REMUX_EVENTS;
  var PLAYER_EVENTS$1 = EVENTS.PLAYER_EVENTS;

  var Mp4Remuxer$1 = function () {
    function Mp4Remuxer$1() {
      var curTime = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

      _classCallCheck$w(this, Mp4Remuxer$1);

      this.TAG = 'Mp4Remuxer';
      this._curTime = curTime;
      if (!this.remuxer) {
        this.initRemuxer();
      }
    }

    _createClass$v(Mp4Remuxer$1, [{
      key: 'init',
      value: function init() {
        this.on(REMUX_EVENTS$2.REMUX_MEDIA, this.remux.bind(this));
        this.on(REMUX_EVENTS$2.REMUX_METADATA, this.onMetaDataReady.bind(this));
        this.on(REMUX_EVENTS$2.DETECT_CHANGE_STREAM, this.resetDtsBase.bind(this));
        this.on(REMUX_EVENTS$2.DETECT_FRAG_ID_DISCONTINUE, this.seek.bind(this));
        this.on(PLAYER_EVENTS$1.SEEK, this.seek.bind(this));
      }
    }, {
      key: 'initRemuxer',
      value: function initRemuxer() {
        this.remuxer = new Mp4Remuxer({
          audioMeta: null,
          videoMeta: null,
          curTime: this._curTime
        });
        this.remuxer.on(Mp4Remuxer.EVENTS.MEDIA_SEGMENT, this.writeToSource.bind(this));
        this.remuxer.on(Mp4Remuxer.EVENTS.TRACK_REMUXED, this.onTrackRemuxed.bind(this));
      }
    }, {
      key: 'remux',
      value: function remux() {
        if (!this.remuxer._videoMeta) {
          this.remuxer._videoMeta = this.videoMeta;
          this.remuxer._audioMeta = this.audioMeta;
        }

        var _context$getInstance = this._context.getInstance('TRACKS'),
            audioTrack = _context$getInstance.audioTrack,
            videoTrack = _context$getInstance.videoTrack;

        return this.remuxer.remux(audioTrack, videoTrack);
      }
    }, {
      key: 'resetDtsBase',
      value: function resetDtsBase() {
        this.remuxer && this.remuxer.resetDtsBase();
      }
    }, {
      key: 'seek',
      value: function seek(time) {
        this.remuxer && this.remuxer.seek(time);
      }
    }, {
      key: 'onMetaDataReady',
      value: function onMetaDataReady(type) {
        if (!this.remuxer) {
          this.initRemuxer();
        }
        var track = void 0;

        if (type === 'audio') {
          var _context$getInstance2 = this._context.getInstance('TRACKS'),
              audioTrack = _context$getInstance2.audioTrack;

          track = audioTrack;
        } else {
          var _context$getInstance3 = this._context.getInstance('TRACKS'),
              videoTrack = _context$getInstance3.videoTrack;

          track = videoTrack;
        }

        var presourcebuffer = this._context.getInstance('PRE_SOURCE_BUFFER');
        var source = presourcebuffer.getSource(type);
        if (!source) {
          source = presourcebuffer.createSource(type);
        }

        source.mimetype = track.meta.codec;
        source.init = this.remuxer.remuxInitSegment(type, track.meta);

        this.emit(REMUX_EVENTS$2.INIT_SEGMENT, type);
      }
    }, {
      key: 'onTrackRemuxed',
      value: function onTrackRemuxed(track) {
        this.emit(REMUX_EVENTS$2.MEDIA_SEGMENT, track);
      }
    }, {
      key: 'writeToSource',
      value: function writeToSource(type, buffer, bufferDuration) {
        var presourcebuffer = this._context.getInstance('PRE_SOURCE_BUFFER');
        var source = presourcebuffer.getSource(type);
        if (!source) {
          source = presourcebuffer.createSource(type);
        }
        source.data.push(buffer);
        if (bufferDuration) {
          source.bufferDuration = bufferDuration + (source.bufferDuration || 0);
        }
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        if (this.remuxer) {
          this.remuxer.destroy();
        }
        this.remuxer = null;
      }
    }, {
      key: 'videoMeta',
      get: function get() {
        return this._context.getInstance('TRACKS').videoTrack.meta;
      }
    }, {
      key: 'audioMeta',
      get: function get() {
        return this._context.getInstance('TRACKS').audioTrack.meta;
      }
    }]);

    return Mp4Remuxer$1;
  }();

  var _createClass$w = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$x(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var REMUX_EVENTS$3 = EVENTS.REMUX_EVENTS;
  var DEMUX_EVENTS$2 = EVENTS.DEMUX_EVENTS;
  var LOADER_EVENTS$2 = EVENTS.LOADER_EVENTS;
  var MSE_EVENTS$2 = EVENTS.MSE_EVENTS;

  var Tag = 'FLVController';

  var Logger = function () {
    function Logger() {
      _classCallCheck$x(this, Logger);
    }

    _createClass$w(Logger, [{
      key: 'warn',
      value: function warn() {}
    }]);

    return Logger;
  }();

  var FLV_ERROR = 'FLV_ERROR';

  var FlvController = function () {
    function FlvController(player) {
      var configs = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var mse = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;

      _classCallCheck$x(this, FlvController);

      this.TAG = Tag;
      this.state = {
        initSegmentArrived: false,
        randomAccessPoints: []
      };
      this.configs = Object.assign({
        retryTimes: 3
      }, configs);

      this.mse = mse;

      this.bufferClearTimer = null;

      this._handleTimeUpdate = this._handleTimeUpdate.bind(this);
    }

    _createClass$w(FlvController, [{
      key: 'init',
      value: function init() {
        if (!this.mse) {
          this.mse = new MSE({ container: this._player.video }, this._context);
          this.mse.init();
        }

        this.initComponents();
        this.initListeners();
      }
    }, {
      key: 'initComponents',
      value: function initComponents() {
        this._context.registry('FETCH_LOADER', FetchLoader);
        this._context.registry('LOADER_BUFFER', XgBuffer);

        this._context.registry('FLV_DEMUXER', FlvDemuxer$1);
        this._context.registry('TRACKS', Track);

        this._context.registry('MP4_REMUXER', Mp4Remuxer$1)(this._player.currentTime);

        this._context.registry('PRE_SOURCE_BUFFER', RemuxedBufferManager);

        if (this._player.config.compatibility !== false) {
          this._context.registry('COMPATIBILITY', Compatibility);
        }

        this._context.registry('LOGGER', Logger);
      }
    }, {
      key: 'initListeners',
      value: function initListeners() {
        this.on(LOADER_EVENTS$2.LOADER_DATALOADED, this._handleLoaderDataLoaded.bind(this));
        this.on(LOADER_EVENTS$2.LOADER_ERROR, this._handleNetworkError.bind(this));
        this.on(LOADER_EVENTS$2.LOADER_RETRY, this._handleFetchRetry.bind(this));

        this.on(DEMUX_EVENTS$2.MEDIA_INFO, this._handleMediaInfo.bind(this));
        this.on(DEMUX_EVENTS$2.METADATA_PARSED, this._handleMetadataParsed.bind(this));
        this.on(DEMUX_EVENTS$2.DEMUX_COMPLETE, this._handleDemuxComplete.bind(this));
        this.on(DEMUX_EVENTS$2.DEMUX_ERROR, this._handleDemuxError.bind(this));
        this.on(DEMUX_EVENTS$2.SEI_PARSED, this._handleSEIParsed.bind(this));

        this.on(REMUX_EVENTS$3.INIT_SEGMENT, this._handleAppendInitSegment.bind(this));
        this.on(REMUX_EVENTS$3.MEDIA_SEGMENT, this._handleMediaSegment.bind(this));
        this.on(REMUX_EVENTS$3.RANDOM_ACCESS_POINT, this._handleAddRAP.bind(this));
        this.on(REMUX_EVENTS$3.REMUX_ERROR, this._handleRemuxError.bind(this));
        this.on(MSE_EVENTS$2.SOURCE_UPDATE_END, this._handleSourceUpdateEnd.bind(this));
        this.on(MSE_EVENTS$2.MSE_ERROR, this._handleMseError.bind(this));

        this._player.on('timeupdate', this._handleTimeUpdate);
      }
    }, {
      key: '_handleMediaInfo',
      value: function _handleMediaInfo() {
        if (!this._context.mediaInfo) {
          this.emit(DEMUX_EVENTS$2.DEMUX_ERROR, new Error('failed to get mediainfo'));
        }
      }
    }, {
      key: '_handleLoaderDataLoaded',
      value: function _handleLoaderDataLoaded() {
        this.emitTo('FLV_DEMUXER', DEMUX_EVENTS$2.DEMUX_START);
      }
    }, {
      key: '_handleMetadataParsed',
      value: function _handleMetadataParsed(type) {
        this.emit(REMUX_EVENTS$3.REMUX_METADATA, type);
      }
    }, {
      key: '_handleSEIParsed',
      value: function _handleSEIParsed(sei) {
        this._player.emit('SEI_PARSED', sei);
      }
    }, {
      key: '_handleDemuxComplete',
      value: function _handleDemuxComplete() {
        this.emit(REMUX_EVENTS$3.REMUX_MEDIA, 'flv');
      }
    }, {
      key: '_handleRemuxError',
      value: function _handleRemuxError(tag, err, fatal) {
        if (fatal === undefined) {
          fatal = false;
        }
        this._player.emit('error', {
          errorType: 'parse',
          ex: '[' + tag + ']: ' + err.message,
          errd: {}
        });
        this._onError(REMUX_EVENTS$3.REMUX_ERROR, tag, err, fatal);
      }
    }, {
      key: '_handleAppendInitSegment',
      value: function _handleAppendInitSegment() {
        this.state.initSegmentArrived = true;
        this.mse.addSourceBuffers();
      }
    }, {
      key: '_handleMediaSegment',
      value: function _handleMediaSegment() {
        this.mse.addSourceBuffers();
        this.mse.doAppend();
      }
    }, {
      key: '_handleSourceUpdateEnd',
      value: function _handleSourceUpdateEnd() {
        var time = this._player.currentTime;
        var video = this._player.video;
        var preloadTime = this._player.config.preloadTime || 5;

        var length = video.buffered.length;

        if (length === 0) {
          return;
        }

        var bufferEnd = video.buffered.end(length - 1);
        if (bufferEnd - time > preloadTime * 2 && !this._player.paused) {
          this._player.currentTime = bufferEnd - preloadTime;
        }
        this.mse.doAppend();
        if (this._player.paused) {
          this._handleTimeUpdate();
        }
      }
    }, {
      key: '_handleTimeUpdate',
      value: function _handleTimeUpdate() {
        var _this = this;

        var time = this._player.currentTime;

        var video = this._player.video;
        var buffered = video.buffered;

        if (!buffered || !buffered.length) {
          return;
        }

        var range = [0, 0];
        var currentTime = video.currentTime;
        if (buffered) {
          for (var i = 0, len = buffered.length; i < len; i++) {
            range[0] = buffered.start(i);
            range[1] = buffered.end(i);
            if (range[0] <= currentTime && currentTime <= range[1]) {
              break;
            }
          }
        }

        var bufferStart = range[0];
        var bufferEnd = range[1];

        if (currentTime < bufferStart) {
          video.currentTime = bufferStart;
          return;
        }

        if (time - bufferStart > 10 || buffered.length > 1) {
          // 在直播时及时清空buffer，降低直播内存占用
          if (this.bufferClearTimer || !this.state.randomAccessPoints.length) {
            return;
          }
          var rap = Infinity;
          for (var _i = 0; _i < this.state.randomAccessPoints.length; _i++) {
            var temp = Math.ceil(this.state.randomAccessPoints[_i] / 1000);
            if (temp > time - 10) {
              break;
            } else {
              rap = temp;
            }
          }

          // console.log('rap', rap, `time ${time}`, `bufferEnd ${bufferEnd}`,`clean ${Math.min(rap, time - 10, bufferEnd - 10)}`)
          this.mse.remove(Math.max(Math.min(rap - 1, time - 10, bufferEnd - 10), 0.1), 0);

          this.bufferClearTimer = setTimeout(function () {
            _this.bufferClearTimer = null;
          }, 5000);
        }
      }
    }, {
      key: '_handleNetworkError',
      value: function _handleNetworkError(tag, err) {
        this._player.emit('error', {
          code: err.code,
          errorType: 'network',
          ex: '[' + tag + ']: ' + err.message,
          errd: {}
        });
        this._onError(LOADER_EVENTS$2.LOADER_ERROR, tag, err, true);
      }
    }, {
      key: '_handleFetchRetry',
      value: function _handleFetchRetry(tag, info) {
        this._player.emit('retry', Object.assign({
          tag: tag
        }, info));
      }
    }, {
      key: '_handleDemuxError',
      value: function _handleDemuxError(tag, err, fatal) {
        if (fatal === undefined) {
          fatal = false;
        }
        this._player.emit('error', {
          errorType: 'parse',
          ex: '[' + tag + ']: ' + err.message,
          errd: {}
        });
        this._onError(DEMUX_EVENTS$2.DEMUX_ERROR, tag, err, fatal);
      }
    }, {
      key: '_handleMseError',
      value: function _handleMseError(tag, err, fatal) {
        if (fatal === undefined) {
          fatal = false;
        }
        this._onError(MSE_EVENTS$2.MSE_ERROR, tag, err, fatal);
      }
    }, {
      key: '_handleAddRAP',
      value: function _handleAddRAP(rap) {
        if (this.state.randomAccessPoints) {
          this.state.randomAccessPoints.push(rap);
        }
      }
    }, {
      key: '_onError',
      value: function _onError(type, mod, err, fatal) {
        var error = {
          errorType: type,
          errorDetails: '[' + mod + ']: ' + (err ? err.message : ''),
          errorFatal: fatal || false
        };
        this._player.emit(FLV_ERROR, error);
      }
    }, {
      key: 'seek',
      value: function seek() {
        if (!this.state.initSegmentArrived) {
          this.loadData();
        }
      }
    }, {
      key: 'loadData',
      value: function loadData() {
        var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this._player.config.url;

        var _ref = this._player.config.retry || {},
            times = _ref.count,
            delayTime = _ref.delay;

        this.emit(LOADER_EVENTS$2.LADER_START, url, {}, times, delayTime);
      }
    }, {
      key: 'pause',
      value: function pause() {
        var loader = this._context.getInstance('FETCH_LOADER');

        if (loader) {
          loader.cancel();
        }
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        this._player.off('timeupdate', this._handleTimeUpdate);
        this.mse = null;
        this.state.randomAccessPoints = [];
      }
    }]);

    return FlvController;
  }();

  var defaultConfig = {
    Mp4Remuxer: Mp4Remuxer$1,
    FlvDemuxer: FlvDemuxer$1,
    FetchLoader: FetchLoader,
    Tracks: Track,
    RemuxedBufferManager: RemuxedBufferManager,
    XgBuffer: XgBuffer,
    Compatibility: Compatibility,
    Mse: MSE
  };

  var _typeof$5 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

  var _get$2 = function get(object, property, receiver) {
    if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {
      var parent = Object.getPrototypeOf(object);if (parent === null) {
        return undefined;
      } else {
        return get(parent, property, receiver);
      }
    } else if ("value" in desc) {
      return desc.value;
    } else {
      var getter = desc.get;if (getter === undefined) {
        return undefined;
      }return getter.call(receiver);
    }
  };

  var _createClass$x = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$y(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn$4(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && ((typeof call === "undefined" ? "undefined" : _typeof$5(call)) === "object" || typeof call === "function") ? call : self;
  }

  function _inherits$4(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof$5(superClass)));
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }
  var flvAllowedEvents = EVENTS.FlvAllowedEvents;

  var FlvPlayer = function (_Player) {
    _inherits$4(FlvPlayer, _Player);

    _createClass$x(FlvPlayer, null, [{
      key: 'isSupported',
      value: function isSupported() {
        return window.MediaSource && window.MediaSource.isTypeSupported('video/mp4; codecs="avc1.42E01E,mp4a.40.2"');
      }
    }, {
      key: 'install',
      value: function install(name, plugin) {
        return Player.install(name, plugin);
      }
    }]);

    function FlvPlayer(config) {
      _classCallCheck$y(this, FlvPlayer);

      var _this = _possibleConstructorReturn$4(this, (FlvPlayer.__proto__ || Object.getPrototypeOf(FlvPlayer)).call(this, config));

      _this.config = Object.assign({}, defaultConfig, _this.config);
      _this.initEvents();
      _this.loaderCompleteTimer = null;
      _this.hasPlayed = false;
      // const preloadTime = player.config.preloadTime || 15
      return _this;
    }

    _createClass$x(FlvPlayer, [{
      key: 'start',
      value: function start() {
        if (this.started) {
          return;
        }
        if (this.context) {
          this.context.destroy();
        }
        this.context = new Context(this, this.config, flvAllowedEvents);
        this.initFlv();
        this.context.init();
        _get$2(FlvPlayer.prototype.__proto__ || Object.getPrototypeOf(FlvPlayer.prototype), 'start', this).call(this, this.flv.mse.url);
        this.loadData();
        this.started = true;
      }
    }, {
      key: 'initFlvEvents',
      value: function initFlvEvents(flv) {
        var _this2 = this;

        var player = this;

        flv.once(EVENTS.REMUX_EVENTS.INIT_SEGMENT, function () {
          Player.util.addClass(player.root, 'xgplayer-is-live');
        });

        flv.once(EVENTS.LOADER_EVENTS.LOADER_COMPLETE, function () {
          // 直播完成，待播放器播完缓存后发送关闭事件
          if (!player.paused) {
            if (_this2.video) {
              var end = _this2.getBufferedRange()[1];
              var restTime = end - _this2.currentTime;
              setTimeout(function () {
                _this2.emit('ended');
              }, restTime * 1000);
            }
          } else {
            player.emit('ended');
          }
        });
      }
    }, {
      key: 'initFlvBackupEvents',
      value: function initFlvBackupEvents(flv, ctx) {
        var _this3 = this;

        var mediaLength = 3;
        flv.on(EVENTS.REMUX_EVENTS.MEDIA_SEGMENT, function () {
          mediaLength -= 1;
          if (mediaLength === 0) {
            // ensure switch smoothly
            _this3.flv = flv;
            _this3.mse.resetContext(ctx);
            _this3.context.destroy();
            _this3.context = ctx;
            if (_this3.paused) {
              _get$2(FlvPlayer.prototype.__proto__ || Object.getPrototypeOf(FlvPlayer.prototype), 'play', _this3).call(_this3);
            }
          }
        });

        flv.once(EVENTS.LOADER_EVENTS.LOADER_COMPLETE, function () {
          // 直播完成，待播放器播完缓存后发送关闭事件
          if (!_this3.paused) {
            if (_this3.video) {
              var end = _this3.getBufferedRange()[1];
              var restTime = end - _this3.currentTime;
              setTimeout(function () {
                _this3.emit('ended');
              }, restTime * 1000);
            }
          } else {
            _this3.emit('ended');
          }
        });

        flv.once(EVENTS.LOADER_EVENTS.LOADER_ERROR, function () {
          ctx.destroy();
        });
      }
    }, {
      key: 'initEvents',
      value: function initEvents() {
        var _this4 = this;

        this.on('seeking', function () {
          var time = _this4.currentTime;
          var range = _this4.getBufferedRange();
          if (time > range[1] || time < range[0]) {
            _this4.flv.seek(_this4.currentTime);
          }
        });

        // support for old version xgplayer
        this.once('play', function () {
          _this4.hasPlayed = true;
        });
      }
    }, {
      key: 'initFlv',
      value: function initFlv() {
        var flv = this.context.registry('FLV_CONTROLLER', FlvController)(this, { retryTimes: this.config.retryTimes });
        this.initFlvEvents(flv);
        this.flv = flv;
        this.mse = flv.mse;
        return flv;
      }
    }, {
      key: 'play',
      value: function play() {
        var _this5 = this;

        if (this.paused && this.hasPlayed) {
          return this.mse.cleanBuffers().then(function () {
            _this5.started = false;
            _this5.start();
            return _get$2(FlvPlayer.prototype.__proto__ || Object.getPrototypeOf(FlvPlayer.prototype), 'play', _this5).call(_this5);
          });
        } else {
          this.hasPlayed = true;
          return _get$2(FlvPlayer.prototype.__proto__ || Object.getPrototypeOf(FlvPlayer.prototype), 'play', this).call(this);
        }
      }
    }, {
      key: 'pause',
      value: function pause() {
        _get$2(FlvPlayer.prototype.__proto__ || Object.getPrototypeOf(FlvPlayer.prototype), 'pause', this).call(this);
        if (this.flv) {
          this.flv.pause();
          this.hasPlayed = true;
        }
      }
    }, {
      key: 'loadData',
      value: function loadData() {
        var time = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.currentTime;

        if (this.flv) {
          this.flv.seek(time);
        }
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        var _this6 = this;

        var prom = new Promise(function (resolve) {
          _this6._destroy().then(resolve).catch(resolve);
          setTimeout(function () {
            resolve();
          }, 50);
        });
        _get$2(FlvPlayer.prototype.__proto__ || Object.getPrototypeOf(FlvPlayer.prototype), 'destroy', this).call(this);
        return prom;
      }
    }, {
      key: '_destroy',
      value: function _destroy() {
        var _this7 = this;

        if (this.context) {
          this.context.destroy();
        }
        if (this.newContext) {
          this.newContext.destroy();
        }
        var work = function work() {
          _this7.flv = null;
          _this7.context = null;
          _this7.newFlv = null;
          _this7.newContext = null;
        };
        if (this.flv && this.flv.mse) {
          return this.flv.mse.destroy().then(function () {
            work();
          });
        }
        return Promise.resolve().then(function () {
          work();
        });
      }
    }, {
      key: 'switchURL',

      /**
       * switch to another flv live url
       * @param url
       * @param seamless
       */
      value: function switchURL(url) {
        var seamless = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

        if (!seamless) {
          this.pause();
          this.context.destroy();
        }
        var context = new Context(this, this.config, flvAllowedEvents);
        var flv = context.registry('FLV_CONTROLLER', FlvController)(this, { retryTimes: this.config.retryTimes }, this.mse);
        this.newContext = context;
        this.newFlv = flv;
        context.init();
        this.initFlvBackupEvents(flv, context);
        flv.loadData(url);
      }
    }, {
      key: 'src',
      get: function get() {
        return this.currentSrc;
      },
      set: function set(url) {
        this.switchURL(url);
      }
    }, {
      key: 'core',
      get: function get() {
        return this.flv;
      }
    }]);

    return FlvPlayer;
  }(Player);

  var _createClass$y = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$z(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var NalUnit$1 = Nalunit;
  var NalUnitHEVC$1 = Nalunit$1;

  var DEMUX_EVENTS$3 = EVENTS.DEMUX_EVENTS;
  var LOADER_EVENTS$3 = EVENTS.LOADER_EVENTS;

  var Tag$1 = 'FLVController';

  var FLV_ERROR$1 = 'FLV_ERROR';

  var FlvController$1 = function () {
    function FlvController(player) {
      _classCallCheck$z(this, FlvController);

      this.TAG = Tag$1;

      this.state = {
        initSegmentArrived: false,
        randomAccessPoints: []
      };

      this.bufferClearTimer = null;
    }

    _createClass$y(FlvController, [{
      key: 'init',
      value: function init() {
        this.initComponents();
        this.initListeners();
      }
    }, {
      key: 'initComponents',
      value: function initComponents() {
        var _player$config = this._player.config,
            FetchLoader = _player$config.FetchLoader,
            XgBuffer = _player$config.XgBuffer,
            FlvDemuxer = _player$config.FlvDemuxer,
            Tracks = _player$config.Tracks,
            Logger = _player$config.Logger;

        this._context.registry('FETCH_LOADER', FetchLoader);
        this._context.registry('LOADER_BUFFER', XgBuffer);

        this._context.registry('FLV_DEMUXER', FlvDemuxer);
        this._context.registry('TRACKS', Tracks);

        this._context.registry('LOGGER', Logger);
        // this._context.registry('PAGE_VISIBILITY', PageVisibility)
      }
    }, {
      key: 'initListeners',
      value: function initListeners() {
        this.on(LOADER_EVENTS$3.LOADER_DATALOADED, this._handleLoaderDataLoaded.bind(this));
        this.on(LOADER_EVENTS$3.LOADER_ERROR, this._handleNetworkError.bind(this));

        this.on(DEMUX_EVENTS$3.MEDIA_INFO, this._handleMediaInfo.bind(this));
        this.on(DEMUX_EVENTS$3.METADATA_PARSED, this._handleMetadataParsed.bind(this));
        this.on(DEMUX_EVENTS$3.DEMUX_COMPLETE, this._handleDemuxComplete.bind(this));
        this.on(DEMUX_EVENTS$3.DEMUX_ERROR, this._handleDemuxError.bind(this));
        this.on(DEMUX_EVENTS$3.SEI_PARSED, this._handleSEIParsed.bind(this));
        // this.on(BROWSER_EVENTS.VISIBILITY_CHANGE, this._handleVisibilityChange.bind(this))
      }
    }, {
      key: '_handleMediaInfo',
      value: function _handleMediaInfo() {
        // meta extracted from flv scriptData
        if (!this._context.mediaInfo) {
          this.emit(DEMUX_EVENTS$3.DEMUX_ERROR, new Error('failed to get mediainfo'));
        } else {
          if (this._player.video) {
            this._player.video.handleMediaInfo();
          }
        }
      }
    }, {
      key: '_handleLoaderDataLoaded',
      value: function _handleLoaderDataLoaded() {
        this.emitTo('FLV_DEMUXER', DEMUX_EVENTS$3.DEMUX_START);
      }
    }, {
      key: '_handleSEIParsed',
      value: function _handleSEIParsed(sei) {
        this._player.emit('SEI_PARSED', sei);
      }
    }, {
      key: '_handleDemuxComplete',
      value: function _handleDemuxComplete() {
        var _this = this;

        if (this._player.video) {
          var _context$getInstance = this._context.getInstance('TRACKS'),
              videoTrack = _context$getInstance.videoTrack,
              audioTrack = _context$getInstance.audioTrack;

          videoTrack.samples.forEach(function (sample) {
            if (sample.analyzed) {
              return;
            }

            sample.analyzed = true;
            var buffer = new XGDataView(sample.data.buffer);
            var nals = void 0;
            if (_this._isHEVC(videoTrack.meta)) {
              nals = NalUnitHEVC$1.getHvccNals(buffer);
            } else {
              nals = NalUnit$1.getNalunits(buffer);
            }
            var nalsLength = nals.reduce(function (len, current) {
              return len + 4 + current.body.byteLength;
            }, 0);
            var newData = new Uint8Array(nalsLength);
            var offset = 0;

            // byte-stream nalunit to AnnexB
            nals.forEach(function (nal) {
              newData.set([0, 0, 0, 1], offset);
              offset += 4;
              newData.set(new Uint8Array(nal.body), offset);
              offset += nal.body.byteLength;
            });

            sample.data = newData;
          });
          this._player.video.onDemuxComplete(videoTrack, audioTrack);
        }
      }
    }, {
      key: '_handleVisibilityChange',
      value: function _handleVisibilityChange(hidden) {
        var visible = !hidden;
        if (!visible && !this._player.paused) {
          this._player.pause();
        } else if (visible && this._player.paused) {
          this._player.play();
        }
      }
    }, {
      key: '_handleMetadataParsed',
      value: function _handleMetadataParsed(type) {
        if (type === 'audio') {
          // 将音频meta信息交给audioContext，不走remux封装
          var _context$getInstance2 = this._context.getInstance('TRACKS'),
              audioTrack = _context$getInstance2.audioTrack;

          if (audioTrack && audioTrack.meta) {
            this._setMetaToAudio(audioTrack.meta);
          }
        } else {
          var _context$getInstance3 = this._context.getInstance('TRACKS'),
              videoTrack = _context$getInstance3.videoTrack;

          if (videoTrack && videoTrack.meta) {
            this._setMetaToVideo(videoTrack.meta);
          }
        }
      }
    }, {
      key: '_setMetaToAudio',
      value: function _setMetaToAudio(audioMeta) {
        if (this._player.video) {
          this._player.video.setAudioMeta(audioMeta);
        }
      }
    }, {
      key: '_setMetaToVideo',
      value: function _setMetaToVideo(videoMeta) {
        if (this._player.video) {
          this._player.video.setVideoMeta(videoMeta);
        }
      }
    }, {
      key: '_handleAppendInitSegment',
      value: function _handleAppendInitSegment() {
        this.state.initSegmentArrived = true;
      }
    }, {
      key: '_handleNetworkError',
      value: function _handleNetworkError(tag, err) {
        this._player.emit('error', new Player.Errors('network', this._player.config.url));
        this._onError(LOADER_EVENTS$3.LOADER_ERROR, tag, err, true, 'MEDIA_ERR_NETWORK');
      }
    }, {
      key: '_handleDemuxError',
      value: function _handleDemuxError(tag, err, fatal) {
        if (fatal === undefined) {
          fatal = false;
        }
        this._player.emit('error', new Player.Errors('parse', this._player.config.url));
        this._onError(DEMUX_EVENTS$3.DEMUX_ERROR, tag, err, fatal, 'MEDIA_ERR_DECODE');
      }
    }, {
      key: '_handleAddRAP',
      value: function _handleAddRAP(rap) {
        if (this.state.randomAccessPoints) {
          this.state.randomAccessPoints.push(rap);
        }
      }
    }, {
      key: '_onError',
      value: function _onError(type, mod, err, fatal, code) {
        var detail = '[' + mod + ']: ' + err.message;
        var error = {
          errorType: type,
          errorDetails: detail,
          errorFatal: fatal || false
        };
        this._player.emit(FLV_ERROR$1, error);
        if (this._player.video) {
          this._player.video.handleErr(code, detail);
        }
      }
    }, {
      key: '_isHEVC',
      value: function _isHEVC(meta) {
        return meta && meta.codec === 'hev1.1.6.L93.B0';
      }
    }, {
      key: 'seek',
      value: function seek() {
        if (!this.state.initSegmentArrived) {
          this.loadData();
        }
      }
    }, {
      key: 'loadData',
      value: function loadData() {
        var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this._player.config.url;

        this.emit(LOADER_EVENTS$3.LADER_START, url);
      }
    }, {
      key: 'pause',
      value: function pause() {
        var loader = this._context.getInstance('FETCH_LOADER');

        if (loader) {
          loader.cancel();
        }
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        this.state.randomAccessPoints = [];
      }
    }]);

    return FlvController;
  }();

  var _createClass$z = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$A(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var Logger$1 = function () {
    function Logger() {
      _classCallCheck$A(this, Logger);
    }

    _createClass$z(Logger, [{
      key: 'warn',
      value: function warn() {}
    }]);

    return Logger;
  }();

  var defaultConfig$1 = {
    FlvDemuxer: FlvDemuxer$1,
    FetchLoader: FetchLoader,
    Tracks: Track,
    XgBuffer: XgBuffer,
    Logger: Logger$1
  };

  var _typeof$6 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

  var _get$3 = function get(object, property, receiver) {
    if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {
      var parent = Object.getPrototypeOf(object);if (parent === null) {
        return undefined;
      } else {
        return get(parent, property, receiver);
      }
    } else if ("value" in desc) {
      return desc.value;
    } else {
      var getter = desc.get;if (getter === undefined) {
        return undefined;
      }return getter.call(receiver);
    }
  };

  var _createClass$A = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$B(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn$5(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && ((typeof call === "undefined" ? "undefined" : _typeof$6(call)) === "object" || typeof call === "function") ? call : self;
  }

  function _inherits$5(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof$6(superClass)));
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }
  // import 'xgplayer-mobilevideo'
  var flvAllowedEvents$1 = EVENTS.FlvAllowedEvents;

  var FlvPlayer$1 = function (_Player) {
    _inherits$5(FlvPlayer, _Player);

    _createClass$A(FlvPlayer, null, [{
      key: 'isSupported',
      value: function isSupported() {
        var wasmSupported = 'WebAssembly' in window;
        var WebComponentSupported = 'customElements' in window && window.customElements.define;
        var isComponentDefined = false;
        if (WebComponentSupported) {
          isComponentDefined = window.customElements.get('live-video');
        }
        return wasmSupported && isComponentDefined;
      }
    }]);

    function FlvPlayer(config) {
      _classCallCheck$B(this, FlvPlayer);

      if (!config.mediaType) {
        config.mediaType = 'live-video';
        if (config.videoConfig) {
          config.videoConfig.preloadtime = config.preloadTime || 5;
        } else {
          config.videoConfig = {
            preloadtime: config.preloadTime || 5
          };
        }
      }

      var _this = _possibleConstructorReturn$5(this, (FlvPlayer.__proto__ || Object.getPrototypeOf(FlvPlayer)).call(this, config));

      _this.config = Object.assign({}, defaultConfig$1, _this.config);
      if (!_this.playerInited) {
        _this.initPlayer(config);
      }
      return _this;
    }

    _createClass$A(FlvPlayer, [{
      key: 'initPlayer',
      value: function initPlayer() {
        this.video.width = Number.parseInt(this.config.width || 600);
        this.video.height = Number.parseInt(this.config.height || 337.5);
        this.video.style.outline = 'none';
        this.context = new Context(flvAllowedEvents$1);
        this.playerInited = true;
      }
    }, {
      key: 'start',
      value: function start() {
        if (!this.playerInited) {
          this.initPlayer();
        }
        this.initFlv();
        this.context.init();
        this.flv.seek(0);
        _get$3(FlvPlayer.prototype.__proto__ || Object.getPrototypeOf(FlvPlayer.prototype), 'start', this).call(this, this.config.url);
        // this.play();
      }
    }, {
      key: 'initFlvEvents',
      value: function initFlvEvents(flv) {
        var player = this;

        flv.once(EVENTS.LOADER_EVENTS.LOADER_COMPLETE, function () {
          // 直播完成，待播放器播完缓存后发送关闭事件
          if (!player.paused) {
            var timer = setInterval(function () {
              var end = player.getBufferedRange()[1];
              if (Math.abs(player.currentTime - end) < 0.5) {
                // player.emit('ended')
                if (player.video) {
                  player.video.handleEnded();
                }
                window.clearInterval(timer);
              }
            }, 200);
          }
        });
      }
    }, {
      key: 'initFlv',
      value: function initFlv() {
        var flv = this.context.registry('FLV_CONTROLLER', FlvController$1)(this);
        this.initFlvEvents(flv);
        this.flv = flv;
      }
    }, {
      key: 'play',
      value: function play() {
        if (this._hasStart && this.paused) {
          this._destroy();
          this.context = new Context(flvAllowedEvents$1);
          var flv = this.context.registry('FLV_CONTROLLER', FlvController$1)(this);
          this.initFlvEvents(flv);
          this.flv = flv;
          this.context.init();
          this.loadData();
          _get$3(FlvPlayer.prototype.__proto__ || Object.getPrototypeOf(FlvPlayer.prototype), 'start', this).call(this);
          _get$3(FlvPlayer.prototype.__proto__ || Object.getPrototypeOf(FlvPlayer.prototype), 'play', this).call(this);
        } else {
          _get$3(FlvPlayer.prototype.__proto__ || Object.getPrototypeOf(FlvPlayer.prototype), 'play', this).call(this);
          this.addLiveFlag();
        }
      }
    }, {
      key: 'pause',
      value: function pause() {
        _get$3(FlvPlayer.prototype.__proto__ || Object.getPrototypeOf(FlvPlayer.prototype), 'pause', this).call(this);
        if (this.flv) {
          this.flv.pause();
        }
      }
    }, {
      key: 'loadData',
      value: function loadData() {
        var time = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.currentTime;

        if (this.flv) {
          this.flv.seek(time);
        }
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        var isDelDOM = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

        this._destroy();
        var video = this.video,
            root = this.root;

        _get$3(FlvPlayer.prototype.__proto__ || Object.getPrototypeOf(FlvPlayer.prototype), 'destroy', this).call(this, isDelDOM);
        if (video && video.remove) {
          video.remove();
        } else if (root) {
          root.removeChild(video);
        }
      }
    }, {
      key: 'addLiveFlag',
      value: function addLiveFlag() {
        var player = this;
        Player.util.addClass(player.root, 'xgplayer-is-live');
        if (!Player.util.findDom(this.root, 'xg-live')) {
          var live = Player.util.createDom('xg-live', '正在直播', {}, 'xgplayer-live');
          player.controls.appendChild(live);
        }
      }
    }, {
      key: '_destroy',
      value: function _destroy() {
        this.context.destroy();
        this.flv = null;
        this.context = null;
      }
    }, {
      key: 'src',
      get: function get() {
        return this.currentSrc;
      },
      set: function set(url) {
        this.config.url = url;
        if (!this.paused) {
          this.currentTime = 0;
          this.pause();
          // this.once('pause', () => {
          //   this.start(url)
          // })
          this.play();
        } else {
          this.start(url);
        }
      }
    }]);

    return FlvPlayer;
  }(Player);

  var _createClass$B = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$C(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var FlvLivePlayer = function () {
    function FlvLivePlayer(config) {
      _classCallCheck$C(this, FlvLivePlayer);

      if (FlvPlayer$1.isSupported() && config.useWASM) {
        return new FlvPlayer$1(config);
      }if (FlvPlayer.isSupported()) {
        return new FlvPlayer(config);
      } else {
        return new Player(config);
      }
    }

    _createClass$B(FlvLivePlayer, null, [{
      key: 'install',
      value: function install(name, plugin) {
        return Player.install(name, plugin);
      }
    }]);

    return FlvLivePlayer;
  }();

  FlvLivePlayer.EVENTS = EVENTS;

  var _createClass$C = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$D(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var REMUX_EVENTS$4 = EVENTS.REMUX_EVENTS;
  var DEMUX_EVENTS$4 = EVENTS.DEMUX_EVENTS;
  var LOADER_EVENTS$4 = EVENTS.LOADER_EVENTS;
  var MSE_EVENTS$3 = EVENTS.MSE_EVENTS;

  var Tag$2 = 'FLVController';

  var Logger$2 = function () {
    function Logger() {
      _classCallCheck$D(this, Logger);
    }

    _createClass$C(Logger, [{
      key: 'warn',
      value: function warn() {}
    }]);

    return Logger;
  }();

  var FLV_ERROR$2 = 'FLV_ERROR';

  var FlvController$2 = function () {
    function FlvController(player, mse) {
      _classCallCheck$D(this, FlvController);

      this.TAG = Tag$2;

      this.mse = mse;
      this.state = {
        initSegmentArrived: false,
        range: {
          start: 0,
          end: ''
        },
        rangeSupport: true
      };
    }

    _createClass$C(FlvController, [{
      key: 'init',
      value: function init() {
        this._context.registry('FETCH_LOADER', FetchLoader);
        this._context.registry('LOADER_BUFFER', XgBuffer);

        this._context.registry('FLV_DEMUXER', FlvDemuxer$1);
        this._context.registry('TRACKS', Track);

        this._context.registry('MP4_REMUXER', Mp4Remuxer$1)(this._player.currentTime);
        this._context.registry('PRE_SOURCE_BUFFER', RemuxedBufferManager);

        this._context.registry('COMPATIBILITY', Compatibility);

        this._context.registry('LOGGER', Logger$2);
        if (!this.mse) {
          this.mse = new MSE({ container: this._player.video }, this._context);
          this.mse.init();
        }

        this.initListeners();
      }
    }, {
      key: 'initListeners',
      value: function initListeners() {
        this.on(LOADER_EVENTS$4.LOADER_DATALOADED, this._handleLoaderDataLoaded.bind(this));
        this.on(LOADER_EVENTS$4.LOADER_ERROR, this._handleNetworkError.bind(this));

        this.on(DEMUX_EVENTS$4.MEDIA_INFO, this._handleMediaInfo.bind(this));
        this.on(DEMUX_EVENTS$4.METADATA_PARSED, this._handleMetadataParsed.bind(this));
        this.on(DEMUX_EVENTS$4.DEMUX_COMPLETE, this._handleDemuxComplete.bind(this));
        this.on(DEMUX_EVENTS$4.DEMUX_ERROR, this._handleDemuxError.bind(this));
        this.on(DEMUX_EVENTS$4.SEI_PARSED, this._handleSEIParsed.bind(this));
        this.on(MSE_EVENTS$3.MSE_ERROR, this._handleMseError.bind(this));
        this.on(REMUX_EVENTS$4.INIT_SEGMENT, this._handleAppendInitSegment.bind(this));
        this.on(REMUX_EVENTS$4.MEDIA_SEGMENT, this._handleMediaSegment.bind(this));
      }
    }, {
      key: '_handleMediaInfo',
      value: function _handleMediaInfo() {
        var _this = this;

        if (!this._context.onMetaData) {
          this.emit(DEMUX_EVENTS$4.DEMUX_ERROR, new Error('failed to get mediainfo'));
        }
        var buffer = this._context.getInstance('LOADER_BUFFER');
        var loader = this._context.getInstance('FETCH_LOADER');
        if (this.isSeekable) {
          loader.cancel();
          // 初始化点播的range
          this.state.range = {
            start: 0,
            end: buffer.historyLen - 1
          };
          setTimeout(function () {
            _this.loadNext(0);
          });
        }
      }
    }, {
      key: '_handleLoaderDataLoaded',
      value: function _handleLoaderDataLoaded() {
        this.emitTo('FLV_DEMUXER', DEMUX_EVENTS$4.DEMUX_START);
      }
    }, {
      key: '_handleMetadataParsed',
      value: function _handleMetadataParsed(type) {
        this.emit(REMUX_EVENTS$4.REMUX_METADATA, type);
      }
    }, {
      key: '_handleSEIParsed',
      value: function _handleSEIParsed(sei) {
        this._player.emit('SEI_PARSED', sei);
      }
    }, {
      key: '_handleDemuxComplete',
      value: function _handleDemuxComplete() {
        this.emit(REMUX_EVENTS$4.REMUX_MEDIA, 'flv');
      }
    }, {
      key: '_handleAppendInitSegment',
      value: function _handleAppendInitSegment() {
        this.state.initSegmentArrived = true;
        this.mse.addSourceBuffers();
      }
    }, {
      key: '_handleMediaSegment',
      value: function _handleMediaSegment() {
        this.mse.addSourceBuffers();
        this.mse.doAppend();
      }
    }, {
      key: '_handleNetworkError',
      value: function _handleNetworkError(tag, err) {
        this._player.emit('error', new Player.Errors('network', this._player.config.url));
        this._onError(LOADER_EVENTS$4.LOADER_ERROR, tag, err, true);
      }
    }, {
      key: '_handleDemuxError',
      value: function _handleDemuxError(tag, err, fatal) {
        if (fatal === undefined) {
          fatal = false;
        }
        this._player.emit('error', new Player.Errors('parse', this._player.config.url));
        this._onError(LOADER_EVENTS$4.LOADER_ERROR, tag, err, fatal);
      }
    }, {
      key: '_handleMseError',
      value: function _handleMseError(tag, err, fatal) {
        if (fatal === undefined) {
          fatal = false;
        }
        this._onError(MSE_EVENTS$3.MSE_ERROR, tag, err, fatal);
      }
    }, {
      key: '_onError',
      value: function _onError(type, mod, err, fatal) {
        var error = {
          errorType: type,
          errorDetails: '[' + mod + ']: ' + (err ? err.message : ''),
          errorFatal: fatal || false
        };
        this._player.emit(FLV_ERROR$2, error);
      }
    }, {
      key: 'seek',
      value: function seek(time) {
        if (!this._context.onMetaData) {
          this.loadMeta();
          return;
        }
        if (!this.isSeekable) {
          return;
        }

        var buffer = this._context.getInstance('LOADER_BUFFER');
        buffer.clear();

        var _player$config$preloa = this._player.config.preloadTime,
            preloadTime = _player$config$preloa === undefined ? 15 : _player$config$preloa;

        var range = this.getSeekRange(time, preloadTime);
        this.state.range = range;

        if (this.compat) {
          this.compat.reset();
        }

        this.loader.destroy();
        this._context.registry('FETCH_LOADER', FetchLoader)();
        this.loadData();
      }
    }, {
      key: 'loadNext',
      value: function loadNext(curTime) {
        if (!this._context.onMetaData) {
          return;
        }

        if (this.loader.loading) {
          return;
        }

        if (this.getNextRange(curTime)) {
          this.loadData();
        }
      }
    }, {
      key: 'loadData',
      value: function loadData() {
        var _state$range = this.state.range,
            start = _state$range.start,
            end = _state$range.end;

        this.emit(LOADER_EVENTS$4.LADER_START, this._player.config.url, {
          headers: {
            method: 'get',
            Range: 'bytes=' + start + '-' + end
          }
        });
      }
    }, {
      key: 'loadMeta',
      value: function loadMeta() {
        var _this2 = this;

        this.loader.load(this._player.config.url, {
          headers: {
            Range: 'bytes=0-'
          }
        }).catch(function () {
          // 在尝试获取视频数据失败时，尝试使用直播方式加载整个视频
          _this2.state.rangeSupport = false;
          _this2.loadFallback();
        });
      }
    }, {
      key: 'loadFallback',
      value: function loadFallback() {
        var _this3 = this;

        this.loader.load(this._player.config.url).catch(function () {
          // 在尝试获取视频数据失败时，尝试使用直播方式加载整个视频
          _this3._player.emit('error', new Player.Errors('network', _this3._player.config.url));
        });
      }
    }, {
      key: 'getSeekRange',
      value: function getSeekRange(time, preloadTime) {
        var keyframes = this._context.onMetaData.keyframes;

        var duration = this._context.mediaInfo.duration;
        var seekStartTime = time;
        var seekEndTime = time + preloadTime;

        var seekStartFilePos = FlvController.findFilePosition(seekStartTime, keyframes);

        if (seekEndTime >= duration || seekStartTime >= duration) {
          return {
            start: seekStartFilePos,
            end: ''
          };
        }
        var seekEndFilePos = FlvController.findFilePosition(seekEndTime, keyframes);

        return {
          start: seekStartFilePos,
          end: seekEndFilePos
        };
      }
    }, {
      key: 'getNextRange',
      value: function getNextRange(time) {
        if (this.state.range.end === '') {
          return;
        }

        var _getSeekRange = this.getSeekRange(time, this.config.preloadTime || 15),
            end = _getSeekRange.end;

        if (end <= this.state.range.end && end !== '') {
          return;
        }

        this.state.range = {
          start: this.state.range.end + 1,
          end: end
        };
        return true;
      }
    }, {
      key: 'destroy',
      value: function destroy() {
        this.mse = null;
        this.state = {
          initSegmentArrived: false,
          range: {
            start: 0,
            end: ''
          },
          rangeSupport: true
        };
      }
    }, {
      key: 'isSeekable',
      get: function get() {
        if (!this.state.rangeSupport || !this._context) {
          return false;
        }

        return this._context.onMetaData.keyframes !== null && this._context.onMetaData.keyframes !== undefined;
      }
    }, {
      key: 'config',
      get: function get() {
        return this._player.config;
      }
    }, {
      key: 'loader',
      get: function get() {
        return this._context.getInstance('FETCH_LOADER');
      }
    }, {
      key: 'compat',
      get: function get() {
        return this._context.getInstance('COMPATIBILITY');
      }
    }, {
      key: 'loadBuffer',
      get: function get() {
        return this._context.getInstance('LOADER_BUFFER');
      }
    }], [{
      key: 'findFilePosition',
      value: function findFilePosition(time, keyframes) {
        for (var i = 0, len = keyframes.times.length; i < len; i++) {
          var currentKeyframeTime = keyframes.times[i];
          var nextKeyframeTime = i + 1 < len ? keyframes.times[i + 1] : Number.MAX_SAFE_INTEGER;

          if (currentKeyframeTime <= time && time <= nextKeyframeTime) {
            return keyframes.filepositions[i];
          }
        }

        return '';
      }
    }]);

    return FlvController;
  }();

  var _typeof$7 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

  var _get$4 = function get(object, property, receiver) {
    if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {
      var parent = Object.getPrototypeOf(object);if (parent === null) {
        return undefined;
      } else {
        return get(parent, property, receiver);
      }
    } else if ("value" in desc) {
      return desc.value;
    } else {
      var getter = desc.get;if (getter === undefined) {
        return undefined;
      }return getter.call(receiver);
    }
  };

  var _createClass$D = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
      }
    }return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
  }();

  function _classCallCheck$E(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn$6(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && ((typeof call === "undefined" ? "undefined" : _typeof$7(call)) === "object" || typeof call === "function") ? call : self;
  }

  function _inherits$6(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof$7(superClass)));
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  var debounce$1 = debounce;

  var flvAllowedEvents$2 = EVENTS.FlvAllowedEvents;

  var isEnded = function isEnded(player, flv) {
    if (!player.config.isLive) {
      if (player.duration - player.currentTime < 2) {
        var range = player.getBufferedRange();
        if (player.currentTime - range[1] < 0.1) {
          player.emit('ended');
          flv.mse.endOfStream();
        }
      }
    }
  };

  var FlvVodPlayer = function (_Player) {
    _inherits$6(FlvVodPlayer, _Player);

    _createClass$D(FlvVodPlayer, null, [{
      key: 'isSupported',
      value: function isSupported() {
        return window.MediaSource && window.MediaSource.isTypeSupported('video/mp4; codecs="avc1.42E01E,mp4a.40.2"');
      }
    }, {
      key: 'install',
      value: function install(name, plugin) {
        return Player.install(name, plugin);
      }
    }]);

    function FlvVodPlayer(config) {
      _classCallCheck$E(this, FlvVodPlayer);

      var _this = _possibleConstructorReturn$6(this, (FlvVodPlayer.__proto__ || Object.getPrototypeOf(FlvVodPlayer)).call(this, config));

      _this.initEvents();
      // const preloadTime = player.config.preloadTime || 15
      // this.started = false;
      return _this;
    }

    _createClass$D(FlvVodPlayer, [{
      key: 'start',
      value: function start() {
        if (this.started) {
          return;
        }
        if (!this.context) {
          this.context = new Context(this, this.config, flvAllowedEvents$2);
        }
        var flv = this.initFlv();
        flv.loadMeta();
        _get$4(FlvVodPlayer.prototype.__proto__ || Object.getPrototypeOf(FlvVodPlayer.prototype), 'start', this).call(this, flv.mse.url);
        this.started = true;
      }
    }, {
      key: 'initFlv',
      value: function initFlv() {
        var flv = this.context.registry('FLV_CONTROLLER', FlvController$2)(this);
        this.context.init();
        this.flv = flv;
        this.mse = flv.mse;
        return flv;
      }
    }, {
      key: 'initFlvBackupEvents',
      value: function initFlvBackupEvents(flv, ctx) {
        var _this2 = this;

        flv.once(EVENTS.REMUX_EVENTS.INIT_SEGMENT, function () {
          var mediaLength = 3;
          flv.on(EVENTS.REMUX_EVENTS.MEDIA_SEGMENT, function () {
            mediaLength -= 1;
            if (mediaLength === 0) {
              // ensure switch smoothly
              _this2.flv = flv;
              _this2.mse.resetContext(ctx);
              _this2.context.destroy();
              _this2.context = ctx;
            }
          });
        });

        flv.once(EVENTS.LOADER_EVENTS.LOADER_ERROR, function () {
          ctx.destroy();
        });
      }
    }, {
      key: 'initEvents',
      value: function initEvents() {
        this.on('timeupdate', this.handleTimeUpdate.bind(this));

        this.on('seeking', this.handleSeek.bind(this));

        this.once('destroy', this._destroy.bind(this));
      }
    }, {
      key: 'handleTimeUpdate',
      value: function handleTimeUpdate() {
        this.loadData();
        isEnded(this, this.flv);
      }
    }, {
      key: 'handleSeek',
      value: function handleSeek() {
        var time = this.currentTime;
        var range = this.getBufferedRange();
        if (time > range[1] || time < range[0]) {
          debounce$1(this.flv.seek(this.currentTime), 200);
        }
      }
    }, {
      key: '_destroy',
      value: function _destroy() {
        if (this.context) {
          this.context.destroy();
        }
        this.context = null;
        this.flv = null;
      }
    }, {
      key: 'loadData',
      value: function loadData() {
        var time = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.currentTime;

        var range = this.getBufferedRange();
        if (range[1] - time < (this.config.preloadTime || 15) - 5) {
          this.flv.loadNext(range[1] + 1);
        }
      }
    }, {
      key: 'switchURL',
      value: function switchURL(url) {
        this.config.url = url;
        var context = new Context(this, this.config, flvAllowedEvents$2);
        var flv = context.registry('FLV_CONTROLLER', FlvController$2)(this, this.mse);
        this.newContext = context;
        this.newFlv = flv;
        context.init();
        var remuxer = context.getInstance('MP4_REMUXER');
        remuxer._dtsBase = 0;
        this.initFlvBackupEvents(flv, context);
        flv.loadMeta();
      }
    }, {
      key: 'remuxer',
      get: function get() {
        return this.context.getInstance('MP4_REMUXER');
      }
    }, {
      key: 'src',
      get: function get() {
        return this.currentSrc;
      },
      set: function set(url) {
        return this.switchURL(url);
      }
    }]);

    return FlvVodPlayer;
  }(Player);

  var _createClass$E = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

  function _classCallCheck$F(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var FlvPlayer$2 = function () {
    function FlvPlayer(config) {
      _classCallCheck$F(this, FlvPlayer);

      if (config.isLive) {
        return new FlvLivePlayer(config);
      } else {
        return new FlvVodPlayer(config);
      }
    }

    _createClass$E(FlvPlayer, null, [{
      key: 'isSupported',
      value: function isSupported() {
        return window.MediaSource && window.MediaSource.isTypeSupported('video/mp4; codecs="avc1.42E01E,mp4a.40.2"');
      }
    }, {
      key: 'install',
      value: function install(name, plugin) {
        return Player.install(name, plugin);
      }
    }]);

    return FlvPlayer;
  }();

  return FlvPlayer$2;

})));
//# sourceMappingURL=index.js.map
