import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import pages from '@/views/pages'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/doc',
    name: 'Doc',
    component: () => import('@/views/Doc.vue'),
  },
  {
    path: '/mapTest',
    name: 'MapTest',
    component: () => import('@/views/MapTest.vue'),
  },
  {
    path: '/MyUITest',
    name: 'MyUITest',
    component: () => import('@/views/MyUITest.vue'),
  },
  {
    path: '/DragTest',
    name: 'DragTest',
    component: () => import('@/views/DragTest.vue'),
  },
  {
    path: '/GridLayout',
    name: 'GridLayout',
    component: () => import('@/views/GridLayout.vue'),
  },
  {
    path: '/ResizeTest',
    name: 'ResizeTest',
    component: () => import('@/views/ResizeTest.vue'),
  },
  {
    path: '/GiantTreeTest',
    name: 'GiantTreeTest',
    component: () => import('@/views/GiantTreeTest.vue'),
  },
  {
    path: '/FlvDemo',
    name: 'FlvDemo',
    component: () => import('@/views/FlvDemo.vue'),
  },
  {
    path: '/VueDPlayerDemo',
    name: 'VueDPlayerDemo',
    component: () => import('@/views/VueDPlayerDemo.vue'),
  },
  {
    path: '/DPlayerDemo',
    name: 'DPlayerDemo',
    component: () => import('@/views/DPlayerDemo.vue'),
  },
  {
    path: '/ClusterDemo',
    name: 'ClusterDemo',
    component: () => import('@/views/ClusterDemo.vue'),
  },
  {
    path: '/MyCluster',
    name: 'MyCluster',
    component: () => import('@/views/MyCluster.vue'),
  },
  {
    path: '/OceCluster',
    name: 'OceCluster',
    component: () => import('@/views/OceClusterDemo.vue'),
  },
  {
    path: '/WebSocket',
    name: 'WebSocket',
    component: () => import('@/views/WebSocketDemo.vue'),
  },
  {
    path: '/RowCol',
    name: 'RowCol',
    component: () => import('@/views/RowCol.vue'),
  },
  {
    path: '/GridDemo',
    name: 'GridDemo',
    component: () => import('@/views/GridDemo.vue'),
  },
  {
    path: '/DrawShapesDemo',
    name: 'DrawShapesDemo',
    component: () => import('@/views/DrawShapesDemo.vue'),
  },
  {
    path: '/XlsxDemo',
    name: 'XlsxDemo',
    component: () => import('@/views/XlsxDemo.vue'),
  },
  // {
  //   path: '/webWorkerDemo',
  //   name: 'webWorkerDemo',
  //   component: () => import('@/views/webWorkerDemo.vue'),
  // },
  {
    path: '/EventParamTest',
    name: 'EventParamTest',
    component: () => import('@/views/EventParamTest.vue'),
  },
  {
    path: '/FlvPlayer',
    name: 'FlvPlayer',
    component: () => import('@/views/FlvPlayer.vue'),
  },
  {
    path: '/EventsDemo',
    name: 'EventsDemo',
    component: () => import('@/views/EventsDemo.vue'),
  },
  {
    path: '/XgPlayer',
    name: 'XgPlayer',
    component: () => import('@/views/XgPlayer.vue'),
  },
  {
    path: '/SelfPlayer',
    name: 'SelfPlayer',
    component: () => import('@/views/SelfPlayer.vue'),
  },
  {
    path: '/ExtendDemo',
    name: 'ExtendDemo',
    component: () => import('@/views/ExtendDemo.vue'),
  },
]


const router = new VueRouter({
  routes,
})

pages.pages.forEach(item => {
  let newRoute = {
    path: item.path,
    name: item.name,
    component: (resolve) => require([`@/views/${item.component}`], resolve),
  }
  router.addRoute(newRoute)
})

export default router
