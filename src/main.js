import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import Ocean from '../components/index'
// import Ocean from '../../ocean-v2/dist/ocean'
import Ocean from '../../ocean-v2/components/index'
import App from './App.vue'

import 'normalize.css/normalize.css'
import router from './router'
// import clickoutside from './utils/clickoutside'

// Vue.directive('clickoutside', clickoutside)
Vue.use(ElementUI)
Vue.use(Ocean, {
  ComponentPrefix: 'Oce',
})

Vue.config.productionTip = false

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app')
