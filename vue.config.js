module.exports = {
  lintOnSave: false,
  transpileDependencies: ['@xdh/my'],
  chainWebpack(config) {
    config.resolve.alias.set('$ui', '@xdh/my/ui/lib')

    let date = new Date()
    let month = date.getMonth() + 1
    let D = date.getDate()
    let h = date.getHours()
    let m = date.getMinutes()
    let versionDate = `${JSON.stringify(date.getFullYear())}${
      month < 10 ? '0' + month : month
    }${D < 10 ? '0' + D : D}${h < 10 ? '0' + h : h}${m < 10 ? '0' + m : m}`
    // 添加环境变量
    config.plugin('define').tap((args) => {
      args[0]['process.env']['VERSION'] = JSON.stringify(
        'v0.1.0.' + versionDate + ''
      )
      return args
    })
  },
  // configureWebpack: (config) => {
  //   config.module.rules.push({
  //     test: /\.worker.js$/,
  //     use: [
  //       {
  //         loader: 'worker-loader',
  //         // options: {
  //         //   inline: true,
  //         //   // filename: '[hash].worker.js'
  //         // }
  //       },
  //     ],
  //   })
  // },
  configureWebpack: {
    resolve: {
      extensions: ['.js', '.vue', '.json', '.ts'],
    },
    module: {
      rules: [
        {
          test: /\.worker.js$/,
          use: [
            {
              loader: 'worker-loader',
              // options: {
              //   inline: true,
              //   // filename: '[hash].worker.js'
              // }
            },
          ],
        },
      ],
    },
  },
}
