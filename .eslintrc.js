module.exports = {
  root: true,
  env: {
    browser: true,
    es2021: true,
    node: true,
    es6: true,
  },
  extends: ['airbnb-base', 'plugin:vue/essential'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: ['vue'],
  rules: {
    // indent: ["error", "tab"],
    'linebreak-style': ['off', 'windows'],
    // quotes: ['error', 'double'],
    semi: ['error', 'never'],
    'operator-linebreak': ['error', 'after'],
    eqeqeq: 'off',
    // 'max-len': 250,
  },
}
