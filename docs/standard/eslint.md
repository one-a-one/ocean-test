# eslint配置

- 保存自动格式化代码风格

## 构建配置

- 安装eslint 并初始化

```shell
yarn add eslint -G
eslint --init
```



- 安装prettier-eslint

```shell
yarn add prettier-eslint -D
```



## 使用配置

- vscode商店中安装  eslint  与  Prettier - Code formatter  两个插件
- vscode配置文件中添加下列配置

```js
// setting.json
"eslint.validate": [
    "javascript",
    "javascriptreact",
    {
        "language": 'html',
        "autoFix": true
    },
    {
        "language": 'vue',
        "autoFix": true
    },
],
"vetur.format.defaultFormatter.js": "prettier-eslint",
"prettier.trailingComma": "es5",
"prettier.semi": false,
"prettier.jsxSingleQuote": true,
"prettier.singleQuote": true,
"prettier.eslintIntegration": true,
"editor.formatOnPaste": true,
"editor.formatOnSave": true
```

- 右键修改默认格式文档为Prettier - Code formatter

## vue模板

- 文件 - 首选项 - 用户片段 vue.json文件 添加下面模板

```json
{
	"Create vue template": {
		"prefix": "vue",
		"body": [
			"<template>",
			"  <div class=\"container\">\n",
			"  </div>",
			"</template>\n",
			"<script>",
			"export default {",
			"  name: 'c',",
			"  data() {",
			"    return {\n",
			"    }",
			"  },",
			"  props: {},",
			"  components: {},",
			"  watch: {},",
			"  computed: {},",
			"  methods: {},",
			"  created() {},",
			"  mounted() {},",
			"}",
			"</script>\n",
			"<style scoped lang=\"stylus\">\n",
			"</style>",
			"$2"
		],
		"description": "Create vue template"
	}
}
```

