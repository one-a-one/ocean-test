# Avue 升级注意事项

- avue项目由2.6.18 升级至2.8.22，已知以下用法需要调整。
- 新版本才用extends的方式自定义crud组件，保留原有的avue-crud组件。
- 新增自定义组件oce-crud组件，原框架所有avue-crud均需要替换为oce-crud组件。



## 自定义crud组件说明

- column.overHidden 默认值修改为true
- 调整功能列位置
- 重写getTableHeight方法，使用时可不需要在配置calcHeight，实现高度自定义
- css-列显隐控制到crud组件范围内，删除el-drawer组件上append-to-body属性
- css-分页栏居中
- css-添加高度修改过渡效果
- 默认开启条纹 stripe
- 表单名称不加冒号（改avue-from源码）
- menuRight 插槽位置（无明确需求，暂不修改）
- 修改新增按钮样式
- 默认组件尺寸改为 mini （改avue源码）
- 自动计算默认最小宽度 setMinWidth
- input-tree组件点击事件改造(改avue源码，同步伟榕修改，在src\views\system\user\index.vue中使用，后续修改可取消)
- dialogMenuPosition默认值改为center
- 抽屉样式同步伟榕修改



## CRUD组件新版已知必要变更

### option部分

```js
option: {
    column: [
        {
            prop:'stationCode/stationName', // porp字段不能用“/”命名，可以用“_”代替
            showColumn: false, // 2.8.22中只能表示该列是否允许动态控制，表格中默认显示，如需要在表格中隐藏需用hide字段控制
            hide: true,
            title: '表格标题', // 与excel导出组件功能重合，新版若不用必须去除
            overHidden: true, // 文字超出隐藏 默认值改为true(官方文档为false)
        }
    ]
}
```



### template部分

```html
<template>
    <!-- 表单项自定义插槽 数据scope模型发生变化 -->
    <template slot-scope="scope" slot="longitudeForm">
        <!-- {{ scope }} -->
        <el-input
                  v-model="scope.value"
                  type="number"
                  class="logLatInput"
                  placeholder="有效值（-180 ~ 180）"
                  />
    </template>
</template>
```

### scope结构

```js
{
    "value":106.57042,
    "column":{
        "label":"经度",
        "prop":"longitude",
        "formslot":true,
        "minWidth":"100px",
        "rules":[
            {
                "required":true,
                "message":"请选择 经度",
                "trigger":"blur"
            }
        ],
        "index":"",
        "boxType":"edit",
        "dicFlag":false
    },
    "size":"small",
    "disabled":false,
    "type":"edit"
}
```

### 特别说明

- 请勿在使用ref获取crud实例后，继续获取其子组件操作的方式。

```js
 // /system/user/index.vue 436行
this.$refs.crud.$children[6].clearSelection();
this.$refs.crud.$children[4].columnBox = true;
```



