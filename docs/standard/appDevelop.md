# APP 开发说明

> 安卓 PDA 终端 App 开发，常用开发资源：
>
> - [已联网安卓 PDA](https://baike.baidu.com/item/%E6%8E%8C%E4%B8%8A%E7%94%B5%E8%84%91/576782?fromtitle=pda&fromid=111022&fr=aladdin)
> - [HBuilderx 3.3.10 - IDE 工具](https://www.dcloud.io/hbuilderx.html)
> - [apifox - mock 工具](https://www.apifox.cn/)
> - [uni-app - 移动端跨平台框架](https://uniapp.dcloud.io/)
>   - [UI组件文档](https://uniapp.dcloud.io/component/README)
>   - [UI组件示例](https://hellouniapp.dcloud.net.cn/pages/component/view/view)
> - [vue3 - JS 核心框架](https://v3.cn.vuejs.org/)
> - [typeScript - JS 超集](https://www.tslang.cn/)
> - [vuex - vue 状态管理库](https://vuex.vuejs.org/zh/)
> - [vite - 构建工具](https://vitejs.cn/)

## 环境搭建

### 拉取代码

> 开发使用develop分支，或创建独立分支。

```shell
# 拉取代码
git clone ssh://git@10.10.10.132:8222/mom-java/sx-app.git
git clone http://10.10.10.132:8888/mom-java/sx-app.git

# 切分支
git checkout -b develop origin/develop
```

### 安装 mock 工具

> 该工具主要用于模拟测试页面接口数据，根据实际开发情况选择安装。

- 下载安装；[apifox - mock 工具](https://www.apifox.cn/)

- 注册账号并发送至项目管理员，加入项目组；<img src="http://ocean-v2.top:8899/img/OceanDoc/001.png" style="zoom:50%;" />

- 开启 mock 服务，参考开发文档使用；<img src="http://ocean-v2.top:8899/img/OceanDoc/002.png" style="zoom:50%;" />

- 修改代码中测试接口根路径

  ```js
  // mock接口路径 '@/api/test.ts'
  const baseURL = `http://${本机IP}:4523/mock/634142`;
  ```

  

### 安装 HBuilderx 

> 建议使用HBuilderx v3.3.10版本。

- 下载安装[HBuilderx 3.3.10](https://www.dcloud.io/hbuilderx.html)
- 使用HB打开项目代码
- 将PDA连接至PC，并确认开启USB调试，连接网络
- 如图运行项目，首次编译会安装多个插件<img src="http://ocean-v2.top:8899/img/OceanDoc/003.png" style="zoom:50%;" />

## 调试技巧

### 调试服务

> 配合输出语句，方便开发过程查看开发数据；
>
> 运行成功后点击调试按钮，即可进入调试服务。

<img src="http://ocean-v2.top:8899/img/OceanDoc/004.png"  />

### WebView调试控制台

> - 项目运行后可进行单页调试
> - 开启WebView调试控制台（运行 - 运行手机或模拟器 - 显示WebView调试控制台 ）
> - 控制台点击打开具体调试页面，具体如下图
> - 与浏览器控制台功能基本相同
> - 该功能不稳定，容易闪退，酌情使用

![](http://ocean-v2.top:8899/img/OceanDoc/005.png)

## 路由跳转

> - 对[uni-app原生路由跳转](https://uniapp.dcloud.io/api/router?id=navigateto)进行二次封装
> - 并挂载至vue全局属性
> - 源码位置（@/utils/router.ts）

### 页面跳转

```js
this.$router.push(path,option = {
    type,
    param,
    animationType,
    animationDuration,
    receiveParam 
})
```

- path { String } ：需要跳转的应用页面的路径，不可以带参数，如：'/pages/text/WorkOrder'。
- option { Object }：配置项
  - type { String }：跳转类型，默认值‘navigateTo’，其余可选值‘redirectTo’、‘reLaunch’、‘switchTab’。
  - param { Object }：传递至下个页面的参数
  - receiveParam { Function } ：下个页面传递参数至本页面的回调函数
  - animationType { String }：窗口显示的动画效果，[详见](https://uniapp.dcloud.io/api/router?id=animation)
  - animationDuration { Number }：窗口动画持续时间，单位为 ms

> 说明跳转类型
>
> - navigateTo 保留当前页面，跳转到应用内的某个页面
> - redirectTo 关闭当前页面，跳转到应用内的某个页面。
> - reLaunch 关闭所有页面，打开到应用内的某个页面。
> - switchTab 跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面。 不能带参数。

### 向上个页面传参

```js
this.$router.sendUpParam.call(this, { data: '参数内容' })
```

> - 配合$router.push中option.receiveParam函数使用
> - 注意this指向

### 返回页面

```js
this.$router.back({delta: 1})
```

- option { Object } ：配置项
  - delta： 返回的页面数，如果 delta 大于现有页面数，则返回到首页。

## api 管理

> - 二次封装[uni.request](https://uniapp.dcloud.io/api/request/request?id=request)方法，实现类似axios的API管理方法
> - 使用 Glob 导入自动整合所有，并挂载至vue全局属性
> - 可以不用各页面分别引入api

### 添加api

```js
import { request } from '@/utils/request';

const api = {
  // 登陆
  login(data) {
    return request({
      url: '/auth/login',
      method: 'POST',
      data
    });
  },
};

export default api;
```

### 调用api

```js
// 根目录下的api 
// fileName = 文件名
this.$api.fileName.workOrderInfo().then(res => {
    console.log('返回数据：', res)
})
// 多级目录下的api
// mkdirName_fileName = 目录名_文件名
this.$api.mkdirName_fileName.workOrderInfo().then(res => {
    console.log('返回数据：', res)
})
```
