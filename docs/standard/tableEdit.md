# 表格配置使用说明

## 数据准备工作

- 创建数据源(数据库或视图)
- 创建所需字典数据

## 新增表及标签

- 填写表、标签基本配置及数据源
- 最少创建一个标签，数据源与标签关联
- 数据源来源与数据库表，多表需求通过视图实现

<img src="http://ocean-v2.top:8899/img/OceanDoc/006.png" style="zoom:50%;" />

<img src="http://ocean-v2.top:8899/img/OceanDoc/007.png" style="zoom:50%;" />

## 标签列配置

<img src="http://ocean-v2.top:8899/img/OceanDoc/008.png" style="zoom:50%;" />

### 1、普通列数据配置

> - 可以设置不同列类型获得不一样的展现形式
> - 部分列类型有专属配置项（如数值列 可配置精度等）
> - 列数据支持字典映射功能，字典数据来源通过后台数据提前配置即可

<img src="http://ocean-v2.top:8899/img/OceanDoc/009.png" style="zoom:50%;" />

### 2、搜索项配置

> - 打开启用后，在打开搜索项即可使用
> - 有多种搜索类型可选，不同类型有不同专有配置项
> - 新搜索编号为提交后自动生成，代码中插槽需要使用复制生成后的即可

<img src="http://ocean-v2.top:8899/img/OceanDoc/010.png" style="zoom:50%;" />

### 3、顺序调整

> 方案一：操作列直接调整
>
> 方案二：快速调整页拖拽调整

<img src="http://ocean-v2.top:8899/img/OceanDoc/011.png" style="zoom:50%;" />

<img src="http://ocean-v2.top:8899/img/OceanDoc/012.png" style="zoom:50%;" />

### 4、批量修改

​	勾选启用按钮： 快速勾选已启用列

​	批量修改按钮： 对勾选列进行批量修改

<img src="http://ocean-v2.top:8899/img/OceanDoc/013.png" style="zoom:50%;" />

> - 批量修改页顶部项目需要勾选后生效
> - 表格内带编辑标识列点击后可编辑

<img src="http://ocean-v2.top:8899/img/OceanDoc/014.png" style="zoom:50%;" />

### 5、其他标签配置

> - 除了与表编辑页相同的几个配置项以为，该页面新增标识字段、视图条件、排序条件配置项
> - SQL编辑框右下角按钮可进行格式化

<img src="http://ocean-v2.top:8899/img/OceanDoc/015.png" style="zoom:50%;" />

## 

## 代码生成器 开发中

<img src="http://ocean-v2.top:8899/img/OceanDoc/016.png" style="zoom:50%;" />