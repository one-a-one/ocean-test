module.exports = {
  // base: '/ocean/',
  title: 'Ocean',
  head: [['link', { rel: 'icon', href: '/o-logo.png' }]],
  plugins: [
    'demo-container',
    '@vuepress-reco/extract-code',
    '@vuepress/back-to-top',
    // [
    //   'vuepress-plugin-comment',
    //   {
    //     choosen: 'valine',
    //     // options选项中的所有参数，会传给Valine的配置
    //     options: {
    //       el: '#valine-vuepress-comment',
    //       appId: 'SFutH4Id7Ll4eerGweqqoKYH-gzGzoHsz',
    //       appKey: 'VFFAQ0mB0iHb7WvE2nFJHM2P',
    //       path: '<%- frontmatter.commentid || frontmatter.permalink %>',
    //     },
    //   },
    // ],
  ],
  themeConfig: {
    logo: '/o-logo.png',
    lastUpdated: 'Last Updated',
    nav: [
      {
        text: '首页',
        link: '/',
      },
      {
        text: '工具',
        ariaLabel: '工具',
        items: [
          { text: 'lodop模板生成器', link: '/components/tool/LodopBuilder' },
        ],
      },
      {
        text: '指南',
        ariaLabel: '指南',
        items: [
          { text: 'Oce组件', link: '/components/ocean/general/doc' },
          // { text: 'Oce组件', link: '/components/ocean/Toolbar' },
          // { text: 'Xny组件', link: '/components/xy/SplitPane' },
          { text: 'APP组件', link: '/components/app/Form' },
          { text: '工具类', link: '/api/lodop' },
          { text: '其他案例', link: '/components/rests/checkAllselect' },
          { text: '更新日志', link: '/components/ChangeLog' },
        ],
      },
      {
        text: '文档',
        ariaLabel: '文档',
        items: [
          { text: '提交规范', link: '/standard/commitNorme.md' },
          { text: 'avue升级注意事项', link: '/standard/avue.md' },
          { text: 'eslint自动格式化说明', link: '/standard/eslint.md' },
          { text: 'App开发说明', link: '/standard/appDevelop.md' },
          { text: '表格配置使用说明', link: '/standard/tableEdit.md' },
          // { text: '依赖文档', link: '/prepare.md' },
          // { text: 'vsCode配置', link: '/prepare.md' },
          // { text: 'icon管理', link: '/prepare.md' },
        ],
      },
    ],
    sidebar: {
      '/standard': [
        '/standard/commitNorme',
        '/standard/avue',
        '/standard/eslint',
        '/standard/appDevelop',
      ],
      '/components/ocean': [
        {
          title: '通用表格 General',
          collapsable: false,
          children: [
            {
              title: '综合案例及说明',
              path: '/components/ocean/general/doc',
            },
            {
              title: '行展开',
              path: '/components/ocean/general/expand',
            },
            {
              title: '单选框',
              path: '/components/ocean/general/radio',
            },
            {
              title: '多选框回显',
              path: '/components/ocean/general/checkboxEcho',
            },
          ],
        },
        {
          title: 'CRUD组件',
          collapsable: false,
          children: [
            {
              title: '基础案例',
              path: '/components/ocean/CRUD/basics',
            },
            {
              title: '导出excel',
              path: '/components/ocean/CRUD/export',
            },
          ],
        },
        {
          title: 'CRUD单组件',
          collapsable: false,
          children: [
            {
              title: '搜索框 search',
              path: '/components/ocean/alone/Search',
            },
            {
              title: '工具栏 toolbar',
              path: '/components/ocean/alone/Toolbar',
            },
            {
              title: '表格 Vxe Table',
              path: '/components/ocean/alone/VxeTable',
            },
            {
              title: '分页栏 pagination',
              path: '/components/ocean/alone/Pagination',
            },
            {
              title: '组合示例',
              path: '/components/ocean/alone/Group',
            },
          ],
        },
        '/components/ocean/SplitPane',
        // '/components/ocean/BigTable',
      ],
      '/components/app': [
        {
          title: '表单 form',
          path: '/components/app/Form',
        },
      ],
      '/components/rests': [
        '/components/rests/CheckAllselect',
        '/components/rests/TableSelect',
        '/components/rests/DialogDrag',
      ],
      '/api': ['/api/Lodop'],
    },
  },
  chainWebpack: (config) => {
    config.globalObject = 'this'
  },
}
