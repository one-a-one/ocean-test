// import ElementUI from './public/element-ui/elementUI.js'
// import './public/element-ui/element-variables.scss'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

// import '../../lib/ocean-all.css'

import './styles/index.scss'

export default async ({ Vue, isServer }) => {
  Vue.use(ElementUI)
  if (!isServer) {
    await import('../../../ocean-v2/dist/ocean.js').then((Ocean) => {
      // await import('../../../ocean-v2/components/index.js').then((Ocean) => {
        Vue.use(Ocean.default)
        // console.log('ocean:',Ocean)
      })
  }
}
