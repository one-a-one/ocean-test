# 窗口拖动

## el-dialog 方案

> - 使用v-dialogDrag指令，配合el-dialog实现
> - 可以用于快速替换原有方案

::: demo

```vue

<template>
  <section class="ElDialogDragDemo">
    <el-button @click="elDialog = true">el窗口</el-button>
    <el-dialog v-dialogDrag title="el窗口" :visible.sync="elDialog">
      el窗口
    </el-dialog>
  </section>
</template>

<script>
export default {
  name: 'ElDialogDragDemo',
  data() {
    return {
      elDialog: false,
    }
  },
}
</script>


```

:::

## vxe-modal 方案

> - 使用vxe-modal组件 [参考文档](https://vxetable.cn/v3/#/table/module/modal)
> - 功能更加强大，但对现有代码改动较大


::: demo

```vue

<template>
  <section class="VxeDialogDragDemo">
    <el-button @click="vxeDialog = true">vxe窗口</el-button>
    <vxe-modal v-model="vxeDialog" show-zoom resize width="50%" height="50%">
      vxe窗口-带拖拽调整、全屏功能
    </vxe-modal>
  </section>
</template>

<script>
export default {
  name: 'VxeDialogDragDemo',
  data() {
    return {
      vxeDialog: false,
    }
  },
}
</script>

```

:::