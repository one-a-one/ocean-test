# 全选下拉框

> - [参考文档- #Column-Select ](https://avuejs.com/form/form-doc/)

::: demo

```vue
<template>
  <section class="CheckAllSelectDemo">
    <avue-select :dic="options" multiple tags all v-model="value"></avue-select>
  </section>
</template>

<script>
export default {
  name: 'CheckAllSelectDemo',
  data() {
    return {
      options: [
        {
          value: '选项1',
          label: '黄金糕',
        },
        {
          value: '选项2',
          label: '双皮奶',
        },
        {
          value: '选项3',
          label: '蚵仔煎',
        },
        {
          value: '选项4',
          label: '龙须面',
        },
        {
          value: '选项5',
          label: '北京烤鸭',
        },
        {
          value: '选项6',
          label: '黄金糕2',
        },
        {
          value: '选项7',
          label: '双皮奶2',
        },
        {
          value: '选项8',
          label: '蚵仔煎2',
        },
        {
          value: '选项9',
          label: '龙须面2',
        },
        {
          value: '选项10',
          label: '北京烤鸭2',
        },
      ],
      value: ['选项8'],
    }
  },
}
</script>
```

:::
