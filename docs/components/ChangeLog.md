---
sidebar: auto
---

# 更新日志

---

> <span class="updateLog">该颜色的更新日志可能会影响旧代码运行。</span>

<!-- ## 未发布更新 -->

## 1.5.4
`2023 年 3 月 15 日`
- 新增 crud组件导出方案

## 1.5.0
`2023 年 3 月 1 日`
- 修复 NumberPicker组件Bug

## 1.4.9
`2023 年 2 月 27 日`
- 新增 表格组件rowStyle配置
## 1.4.8
`2023 年 2 月 23 日`
- 新增 search组件 searchMenuAlign 配置
- 新增 pagination组件 justifyContent 配置，left right 插槽
- 新增 表格部分组件 showFooter footerMethod 配置， 表尾插槽

## 1.4.7
`2023 年 2 月 20 日`
- 修复 formDialog组件部分bug

## 1.4.6
`2023 年 2 月 16 日`
- 新增 表格tab颜色配置项
- 修改 tableEdit组件顺序调整依赖

## 1.4.5
`2023 年 2 月 8 日`
- 新增 formDialog组件
- 修复 搜索框盒模型bug

## 1.4.4
`2023 年 1 月 9 日`
- 新增 小数加法工具类
- 修复 选择器值为数值时的bug

## 1.4.3
`2023 年 1 月 5 日`
- 新增 General组件refreshTable方法

## 1.4.2
`2023 年 1 月 5 日`
- 修复 重新获取表格配置后默认搜索项数据bug

## 1.4.1
`2023 年 1 月 4 日`
- 修改 表格序号列默认宽度
- 修改 搜索框精确匹配组件及提示文本

## 1.4.0
`2022 年 12 月 28 日`
- 修复 导出弹窗样式
## 1.3.9
`2022 年 12 月 28 日`
- 新增 导出功能
- 新增 表格配置模糊文本框搜索类型

## 1.3.8
`2022 年 12 月 20 日`
- 修复 修复NumberPicker Bug
- 新增 General 表格方法入口

## 1.3.7
`2022 年 12 月 19 日`
- 修复 表格字典映射bug

## 1.3.6
`2022 年 12 月 19 日`
- 修复 表格数据绑定bug

## 1.3.5
`2022 年 12 月 15 日`
- 修复 表格字典无法及时更新bug

## 1.3.4
`2022 年 12 月 13 日`
- 修复 NumberPicker组件浮点数校验无法输入负号bug

## 1.3.3
`2022 年 12 月 13 日`
- 修改 NumberPicker组件校验浮点数

## 1.3.2
`2022 年 12 月 12 日`
- 修复 vexTable 数值列补零逻辑bug

## 1.3.1
`2022 年 12 月 12 日`
- 新增 General组件列配置动态存储

## 1.3.0
`2022 年 12 月 8 日`
- 修复 表格构建组件前缀bug

## 1.2.9
`2022 年 12 月 8 日`
- 修复 表格构建组件前缀bug

## 1.2.8
`2022 年 12 月 8 日`
- 新增 表格构建组件表列顺序、搜索项顺序调整功能

## 1.2.6
`2022 年 12 月 6 日`
- 新增 表格构建组件

## 1.2.5
`2022 年 11 月 25 日`
- 新增 表格maxHeight支持

## 1.2.4
`2022 年 11 月 21 日`
- 新增 general兼容多选搜索项类型MultiSelFindIn

## 1.2.3
`2022 年 11 月 18 日`
- 修改 表格展开按钮样式
- 新增 虚拟滚动控制入口

## 1.2.2
`2022 年 11 月 15 日`
- 修改 项目依赖
- 修改 表格数据列字典映射忽略大小写

## 1.2.1
`2022 年 11 月 15 日`
- 修改 General组件搜索ID取值类型
- 修复 NumberPicker组件前缀兼容异常

## 1.2.0
`2022 年 11 月 14 日`
- 修复 表格数据列字典映射逻辑

## 1.1.9
`2022 年 11 月 9 日`
- 新增 表格列 number 类型及相关属性配置
- 修复 组件前缀配置bug

## 1.1.8
`2022 年 11 月 7 日`
- 新增 搜索表单-多选下拉框-全选及合并展示形式
- 新增 组件前缀配置入口
- 修改 工具类specialStrFilter 特殊字符详情

## 1.1.7
`2022 年 11 月 3 日`
- 修复 search-chang事件bug

## 1.1.6
`2022 年 10 月 31 日`
- 修改 VxeTable数据列映射逻辑

## 1.1.5
`2022 年 10 月 27 日`
- 新增 General组件search-change事件

## 1.1.4
`2022 年 10 月 25 日`
- 修改 General配置初始化逻辑;
- 新增 VxeTable组件行内编辑支持

## 1.1.3
`2022 年 10 月 20 日`
- 修复 General组件-默认不加载时搜索条件异常

## 1.1.2
`2022 年 10 月 19 日`
- 新增 General组件-搜索框-下拉项-搜索功能
- 新增 Crud组件searchLabel配置
- 新增 Crud组件表单弹窗功能
- 新增 Crud组件page属性适配.sync语法糖
- 优化 General组件表格大量数据加载速度
- 修改 General组件表格滚动条尺寸

## 1.1.1
`2022 年 7 月 11 日`
- 修复 Array.sort方法兼容性问题
- 新增 表格editBtn delBtn 按钮配置
- 更新 avue 至 2.9.12
- 新增 动态修改General样式入口
- 删除 General 修改行高属性

## 1.1.0 
`2022 年 6 月 14 日`
- 新增 General组件单选列
- 新增 行展开按钮对是否有展开插槽判断
- 修改 validatenull工具类function判断
- 修复 search搜索项必填异常

## 1.0.9 
`2022 年 6 月 8 日`
- 新增 工具类contentClone内容拷贝方法
- 新增 crud组件treeConfig配置

## 1.0.8

`2022 年 5 月 25 日`

- 新增 crud组件列配置treeNode
- 完善 crud组件表格配置
- 修改 lodop打印二维码编码类型
- 修改 vxeTable表格排序逻辑


## 1.0.7

`2022 年 5 月 6 日`

- 新增SPC组件
- General 组件 新增行展开功能

## 1.0.6

`2022 年 4 月 7 日`

- 新增 lodop打印插件Demo
- 新增 lodop模板生成器
- 修复 SplitPane 组件出滚动条后，拖拽定位异常 Bug

## 1.0.5

`2022 年 3 月 15 日`

- General 组件

  - 新增 generalOption.tableOption.height 取值 none

  - 修改 <span class="updateLog">generalOption.tableOption.checkbox（复选框） 默认取值为 true</span>

  - 修改 <span class="updateLog">generalOption.tableOption.isCurrent（单选高亮行） 默认取值为 true</span>

  - 修复 headerAlign 不生效 Bug

## 1.0.4

`2022 年 3 月 8 日`

- General 组件

  - 新增 option 接口首次加载完成事件（option-init）

  - 新增 info 接口搜索栏配置 searchSpan 属性，动态配置搜索项宽度占比

  - 修改 表格数据功能，无法匹配映射值时默认显示原始值

  - 修复 搜索栏多行文本类型默认值为 "-1" 的 Bug

## 1.0.3

`2022 年 3 月 7 日`

- General 组件

  - 新增 表格操作列插槽 (table-menu-right/table-menu-left)

  - 新增 显示表格 tab 切换菜单配置 (tableTabMenu)

  - 新增 表格顶部插槽 (table-top)

  - 新增 表格高度配置(height),可选值 auto, %, px,默认值 auto 自适应与父组件

  - 新增 搜索栏显隐过渡动画

  - 新增 标题溢出省略配置(showHeaderOverflow), 取值同 showOverflow

  - 新增 加载数据更新回调(update-data)

  - 新增 刷新 option 的方法(getOption)

  - 新增 表格行高配置（lineHeight）默认值 40

  - 修改 <span class="updateLog">原 toolbar-menu-Left 插槽为 toolbar-menu-left</span>

  - 修复 搜索栏单选（SingleSelection）、多选（MultiSelection）项目类型

  - 修复 滚动后顶部分页栏有横线样式问题

  - 修复 首次加载会使用默认搜索参数请求

## 1.0.2

`2022 年 3 月 2 日`

- General 组件

  - 新增 单元格溢出省略配置，info 接口返回 showOverflow 列配置

    - false - 无操作
    - ellipsis - 只显示省略号
    - tooltip - 默认值，显示省略号并且显示为 tooltip 提示

  - 新增 搜索框按钮宽度配置（searchMenuSpan )

  - 新增 行高配置（lineHeight ），最小值 30 ，默认值 40

  - 新增 全选事件 checkbox-all

  - 新增 获取表格数据方法（getData）

  - 新增 行复选框选中状态切换方法（setCheckboxRow）

  - 新增 表格配置栏显示方式（configLocalDisplay）

  - 修改 斑马线条纹、选中行颜色加深

  - 修改 <span class="updateLog">handleUpdate、handleDelete、tabChange 事件名为 handle-update、handle-delete、tab-change </span>

  - 修复 数据总数异常 bug

## 1.0.1

`2022 年 2 月 28 日`

- General 组件

  - 新增 字典映射功能，需配合 info 接口 dictList、dictCode 字段使用

  - 新增 initialLoad 属性控制是否默认加载

  - 新增 配置项 menuFixed 取值为'none',表格操作列取消冻结

  - 修改 <span class="updateLog">tabChange 回调值，取值调整为 info 接口 fieldName 字段</span>

  - 修复 搜索栏隐藏时，调用搜索方法抛出错误 Bug
