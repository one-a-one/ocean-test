# 分割面板 Split Pane

::: demo

```vue
<template>
  <div class="SplitPaneDemo">
    <div style="margin: 15px">
      指定宽度：
      <el-input v-model="width" />
      <el-button
        circle
        type="success"
        icon="el-icon-check"
        style="margin-left: 20px"
        @click="cut"
      ></el-button>
    </div>
    <section class="box">
      <oce-split-pane
        split="vertical"
        ref="splitPane"
        :min-percent="20"
        :default-percent="30"
        @resize="resize"
      >
        <template slot="paneL">
          <div class="boxItem boxA">A</div>
        </template>
        <template slot="paneR">
          <oce-split-pane split="horizontal">
            <template slot="paneL">
              <div class="boxItem boxB">B</div>
            </template>
            <template slot="paneR">
              <div class="boxItem boxC">C</div>
            </template>
          </oce-split-pane>
        </template>
      </oce-split-pane>
    </section>
  </div>
</template>

<script>
export default {
  name: 'SplitPaneDemo',
  data() {
    return {
      width: null,
    }
  },
  props: {},
  watch: {},
  computed: {},
  methods: {
    resize(percent) {
      console.log('宽度变化值：', percent)
    },
    cut() {
      if (this.width) this.$refs.splitPane.percent = this.width
    },
  },
  created() {},
  mounted() {},
}
</script>

<style>
.SplitPaneDemo {
  width: 100%;
  text-align: center;
}
.SplitPaneDemo .box {
  width: 800px;
  height: 500px;
  margin: auto;
  border: 1px #ccc solid;
}
.SplitPaneDemo .boxItem {
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 30px;
  overflow: hidden;
}
.SplitPaneDemo .boxA {
  background: #e1f3d8;
}
.SplitPaneDemo .boxB {
  background: #faecd8;
}
.SplitPaneDemo .boxC {
  background: #fde2e2;
}
.SplitPaneDemo .el-input {
  width: 180px;
}
</style>
```

:::

## Attributes

| 参数            | 说明       | 类型   | 可选择              | 默认值 |
| :-------------- | :--------- | :----- | :------------------ | :----- |
| split           | 分割方向   | String | horizontal,vertical | -      |
| min-percent     | 最小百分比 | Number | -                   | 10     |
| default-percent | 默认百分比 | Number | -                   | 50     |

## Slots

| name  | 说明        |
| :---- | :---------- |
| paneL | 左/上侧区域 |
| paneR | 右/下侧区域 |

## Events

| 事件名称 | 说明           | 回调参数 |
| :------- | :------------- | :------- |
| resize   | 尺寸发生变化时 | percent  |
