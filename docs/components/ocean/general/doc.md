## 综合案例

::: demo

```vue
<template>
  <div class="XnyGeneralDoc" style="height: 600px; position: relative">
    <!-- 实际开发中需要传入request方法 -->
    <oce-general
      ref="xnyGeneral"
      :urlInfo="urlInfo"
      :option="generalOption"
      :custom-request-param="customRequestParam"
      :search.sync="search"
      @checkbox-change="checkboxChange"
      @current-change="currentChange"
      @add="add"
      @remove="remove"
      @handle-update="handleUpdate"
      @handle-delete="handleDelete"
      @tab-change="tabChange"
      @checkbox-all="checkboxAll"
      @update-data="updateData"
      @option-init="optionInit"
    >
      <!-- 搜索项插槽 -->
      <template #search-2="scope">
        <div :style="slot">
          <span>搜索项插槽 {{ $ocean.VERSION }}</span>
          <!-- <span>搜索项插槽 {{ scope }}</span> -->
          <!-- <el-input v-model="search[2]"></el-input> -->
        </div>
      </template>
      <!-- 工具栏左侧插槽 -->
      <template #toolbar-menu-left>
        <span :style="slot">
          <el-button size="mini">按钮</el-button>
          <el-button size="mini" @click="refresh">刷新</el-button>
          <el-button size="mini" @click="searchChange">搜索</el-button>
          <el-button size="mini" @click="checkRow">选中第一行</el-button>
          <el-button size="mini" @click="getOption">加载表格配置</el-button>
          按钮栏插槽
        </span>
      </template>
      <!-- 表格顶部插槽 -->
      <template #table-top>
        <oce-general-test :option.sync="generalOption" :style="slot">表格顶部插槽</oce-general-test>
      </template>
      <!-- 表格列插槽 -->
      <template #table-test_tab-id="scope">
        <div
          :style="slot"
          v-if="scope.rowIndex == 0"
          @click.stop="tableMenuTest(scope)"
        >
          表格列插槽 {{ scope.row }}
        </div>
        <div :style="slot" v-else>{{ scope.row[scope.column.property] }}</div>
      </template>
      <!-- 表格操作列插槽 -->
      <template #table-menu-left="scope">
        <el-button
          :style="slot"
          size="mini"
          type="text"
          icon="el-icon-folder"
          @click.stop="tableMenuTest(scope)"
        >
          左侧按钮
        </el-button>
      </template>
      <template #table-menu-right="scope">
        <el-button
          :style="slot"
          size="mini"
          type="text"
          icon="el-icon-folder"
          @click.stop="tableMenuTest(scope)"
        >
          右侧按钮
        </el-button>
      </template>
    </oce-general>
  </div>
</template>

<script>
// import request from '@/utils/request'

export default {
  name: 'XnyGeneralDoc',
  data() {
    return {
      // request,
      search: {},
      activeTab: '测试表格',
      urlInfo: {
        optionUrl: {
          url: '/system/webTable/tableInfo',
          method: 'post',
          webTableName: 'testTable',
          // webTableName: "pgFacCapacityA",
        },
        dataUrl: {
          url: '/system/webTable/tableData',
          method: 'post',
        },
      },
      customRequestParam: {
        webTableParamQueryList: [],
      },
      generalOption: {
        showSearch: true, // 显示搜索栏 （true）
        showToolbar: true, // 显示工具栏 （true）
        showPage: true, // 显示分页栏 （true）
        addBtn: true, // 新增按钮 （true）
        delBtn: true, // 删除按钮 （true）
        editBtn: true, // 编辑按钮 （true）
        refreshBtn: true, // 刷新按钮 （true）
        searchMenuSpan: 6, // 搜索框按钮宽度(4)
        initialLoad: true, // 是否默认加载
        configLocalDisplay: true, // 配置抽屉局部显示 （true）
        tableTabMenu: true, // 显示表格tab切换菜单 （true）
        tableOption: {
          height: 'auto', // 表格高度 auto, %, px, none
          lineHeight: 45, // 行高，最小30（40）
          seq: true, // 序号列 （true）
          checkbox: true, // 复选框 （true）
          menuWidth: 300, // 表格操作列宽度 （200）
          menuFixed: 'right', // 表格操作列冻结 （right）
          menu: true, // 显示操作栏 （true）
          resizable: true, // 列宽度拖拽 （true）
          stripe: true, // 斑马线条纹 （true）
          isHover: true, // 鼠标悬浮高亮 （true）
          isCurrent: true, // 单选高亮行 （true） 回调current-change
          align: 'left', // 列的对齐方式 (center)
          headerAlign: 'center', // 表头列的对齐方式 (center) 继承 align
        },
      },
      slot: { background: '#fde2e2 !important' },
    }
  },
  props: {},
  components: {},
  watch: {},
  computed: {},
  methods: {
    // option接口首次加载完成
    optionInit() {
      console.log('option接口首次加载完成')
    },
    // 加载表格配置
    getOption() {
      this.$refs.xnyGeneral.getOption()
    },
    /**
     * 加载数据回调
     *
     * @param {Array} data 接口获取数据
     * @param {Function} done done方法不带参正常渲染数据,带参按参数数据渲染表格
     * @param {Function} loading loading方法仅关闭loading动画,不修改数据
     * @param {Boolean} first 是否首次加载数据
     *
     * done、loading方法必须调用一个
     */
    updateData({ data, done, loading, first }) {
      console.log('是否首次加载:', first)
      console.log('加载数据:', data)
      data[0].rowList[0].fieldList[0].textValue = '自定义内容'
      done(data)
    },
    tableMenuTest(scope) {
      console.log('点击自定义按钮', scope)
    },
    // 获取指定行数据，切换行选择状态
    checkRow() {
      let row = this.$refs.xnyGeneral.getData(0)
      this.$refs.xnyGeneral.setCheckboxRow([row], true)
    },
    // 列全选回调
    checkboxAll({ checked, records }) {
      console.log('全选', checked, records)
    },
    // tab切换回调
    tabChange(tab) {
      // console.log(tab);
      this.activeTab = tab
    },
    checkboxChange(param) {
      console.log('复选框勾选回调：', param)
      console.log('记录集合：', param.records)
    },
    currentChange(param) {
      console.log('行点击：', param)
    },
    add() {
      console.log('工具栏-新增')
    },
    remove() {
      console.log('工具栏-删除')
    },
    handleUpdate(row) {
      console.log('行-编辑：', row)
    },
    handleDelete(row) {
      console.log('行-删除：', row)
    },
    // 刷新数据
    refresh() {
      this.$refs.xnyGeneral.onLoad()
    },
    // 搜索
    searchChange() {
      this.$refs.xnyGeneral.searchData({ 1: '11,23' })
    },
  },
  created() {},
  mounted() {},
}
</script>

<style scoped lang="scss">
.XnyGeneralDoc {
  // .editorBox {
  //   height: 100%;
  //   width: 100%;
  //   overflow: hidden;
  // }
  // .btn {
  //   background: #3883fa;
  //   width: 100%;
  //   height: 40px;
  //   text-align: center;
  //   line-height: 40px;
  //   color: #fff;
  // }
  ::v-deep .el-drawer__body {
    height: calc(100% - 78px);
  }
}
</style>
```

:::

## 说明

<general-doc/>
