> 配合 tableInfo 接口设置列 expand 属性为 true,开启下拉按钮。

::: demo

```vue
<template>
  <div class="XnyGeneralExpand" style="height: 600px; position: relative">
    <!-- 实际开发中需要传入request方法 -->
    <oce-general
      ref="xnyGeneral"
      :urlInfo="urlInfo"
      :option="generalOption"
      @toggle-row-expand="toggleRowExpand"
    >
      <!-- 展开行插槽 -->
      <template #expand="{ row, rowIndex, $rowIndex, column, prop }">
        <h2>这是来自{{ prop }}的插槽</h2>
        <p>row:{{ row }}</p>
        <p>rowIndex:{{ rowIndex }}</p>
        <p>$rowIndex:{{ $rowIndex }}</p>
        <p>column:{{ column }}</p>
        <p>prop: {{ prop }}</p>
      </template>
       <!-- 表格顶部插槽 -->
      <template #table-top>
        <oce-general-test :option.sync="generalOption"></oce-general-test>
      </template>
    </oce-general>
  </div>
</template>

<script>
export default {
  name: 'XnyGeneralExpand',
  data() {
    return {
      urlInfo: {
        optionUrl: {
          url: '/system/webTable/tableInfo',
          method: 'post',
          webTableName: 'testTable',
        },
        dataUrl: {
          url: '/system/webTable/tableData',
          method: 'post',
        },
      },
      generalOption: {
        tableOption: {
          align: 'left', // 列的对齐方式 (center)
          // accordion: false, // 每次只能展开一行
        },
      },
    }
  },
  props: {},
  components: {},
  watch: {},
  computed: {},
  methods: {
    // 当行展开或收起时会触发该事件
    toggleRowExpand(e) {
      console.log(e)
      console.log(e.expanded ? '展开' : '收起')
    },
  },
  created() {},
  mounted() {},
}
</script>

<style scoped lang="scss"></style>
```

:::
