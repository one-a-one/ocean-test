::: demo

```vue

<template>
  <div
    class="CheckboxEchoDemo"
    style="margin: auto; max-width: 1600px; height: 600px; position: relative"
  >
    <!-- 实际开发中需要传入request方法 -->
    <oce-general
      ref="xnyGeneral"
      :urlInfo="urlInfo"
      :option="generalOption"
      @tab-change="tabChange"
    >
      <!-- 表格顶部插槽 -->
      <template #table-top>
        <oce-general-test :option.sync="generalOption">
          <el-button size="mini" @click="setCheckboxRow"
            >选中数据</el-button
          ></oce-general-test
        >
      </template>
    </oce-general>
  </div>
</template>

<script>
export default {
  name: 'CheckboxEchoDemo',
  data() {
    return {
      urlInfo: {
        optionUrl: {
          url: '/system/webTable/tableInfo',
          method: 'post',
          webTableName: 'testTable',
        },
        dataUrl: {
          url: '/system/webTable/tableData',
          method: 'post',
        },
      },
      generalOption: {
        searchMenuSpan: 6, // 搜索框按钮宽度(4)
        tableOption: {
          align: 'left', // 列的对齐方式 (center)
          headerAlign: 'center', // 表头列的对齐方式 (center) 继承 align
        },
      },
    }
  },
  props: {},
  components: {},
  watch: {},
  computed: {},
  methods: {
    setCheckboxRow() {
      const el = this.$refs.xnyGeneral
      const row = el.getData(0)
      el.setCheckboxRow([row], true)
    },
    tabChange(tab) {
      if(tab == 'test_tab') {
        setTimeout(this.setCheckboxRow,1)
      }
    },
  },
  created() {},
  mounted() {},
}
</script>

<style scoped lang="scss">
.CheckboxEchoDemo {
}
</style>

```

:::
