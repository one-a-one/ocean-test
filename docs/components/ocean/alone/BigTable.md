# 虚拟表格 bigTable

::: demo

```vue
<template>
  <oce-big-table
    :column="tableColumn"
    :data="tableData"
    :option="tableOption"
    @tree-load="treeLoad"
    ref="bigTable"
  />
</template>

<script>
export default {
  data() {
    return {
      tableOption: {
        rowKey: 'id',
        menuWidth: 150, //操作栏宽度，设定后会固定在右侧
        lazy: true,
      },
      tableColumn: [
        {
          label: '列1',
          prop: 'column1',
        },
        {
          label: '列2',
          prop: 'column2',
        },
        {
          label: '列3',
          prop: 'column3',
        },
        {
          label: '列4',
          prop: 'column4',
        },
      ],
      tableData: [
        {
          column1: '1-1',
          column2: '1-2',
          column3: '1-3',
          column4: '1-4',
          id: 1,
          hasChildren: 1,
        },
        {
          column1: '2-1',
          column2: '2-2',
          column3: '2-3',
          column4: '2-4',
          id: 2,
        },
        {
          column1: '3-1',
          column2: '3-2',
          column3: '3-3',
          column4: '3-4',
          id: 3,
          hasChildren: 1,
        },
        {
          column1: '4-1',
          column2: '4-2',
          column3: '4-3',
          column4: '4-4',
          id: 4,
        },
        {
          column1: '5-1',
          column2: '5-2',
          column3: '5-3',
          column4: '5-4',
          id: 5,
        },
        {
          column1: '6-1',
          column2: '6-2',
          column3: '6-3',
          column4: '6-4',
          id: 6,
        },
        {
          column1: '7-1',
          column2: '7-2',
          column3: '7-3',
          column4: '7-4',
          id: 7,
        },
        {
          column1: '8-1',
          column2: '8-2',
          column3: '8-3',
          column4: '8-4',
          id: 8,
        },
        {
          column1: '9-1',
          column2: '9-2',
          column3: '9-3',
          column4: '9-4',
          id: 9,
        },
      ],
    }
  },
  methods: {
    treeLoad(tree, resolve) {
      let pId = tree.id
      let data = []
      for (let i = 0; i < 10000; i++) {
        let item = {
          column1: i + '-' + '1',
          column2: i + '-' + '2',
          column3: i + '-' + '3',
          column4: i + '-' + '4',
          id: pId + '-' + i,
        }
        data.push(item)
      }
      setTimeout(() => {
        resolve(data)
      }, 2000)
    },
  },
}
</script>
```

:::

## Attributes

| 参数              | 说明                                                                                       | 类型                      | 可选择     | 默认值 |
| :---------------- | :----------------------------------------------------------------------------------------- | :------------------------ | :--------- | :----- |
| data              | 表格数据                                                                                   | Array                     | -          | -      |
| option            | 表格配置属性，详情见 Option 属性                                                           | Object                    | -          | -      |
| column            | 表格列配置属性，详情见 column 属性                                                         | Array                     | -          | -      |
| page              | 分页数据（仅用到 currentPage、pageSize，说明同分页栏组件），continuousIndex 为 true 时使用 | Object                    | -          | -      |
| continuousIndex   | 连续分页行号（开启虚拟渲染后不可用）                                                       | Boolean                   | true/false | false  |
| tableRowClassName | 行的 className 的回调方法，也可以使用字符串为所有行设置一个固定的 className。              | Function({row, rowIndex}) | -          | -      |

- 兼容 element-ui 2.9.1 table 组件，未在 option、Attributes 中声明所有的属性

### Option Attributes

| 参数                | 说明                                     | 类型    | 可选择                                                       | 默认值                                               |
| :------------------ | :--------------------------------------- | :------ | :----------------------------------------------------------- | :--------------------------------------------------- |
| border              | 表格边框                                 | Boolean | true/false                                                   | false                                                |
| stripe              | 表格条纹                                 | Boolean | true/false                                                   | true                                                 |
| height              | 表格高度                                 | String  | -                                                            | -                                                    |
| showSummary         | 是否在表尾显示合计行                     | Boolean | true/false                                                   | false                                                |
| showHeader          | 是否显示表格的表头                       | Boolean | true/false                                                   | true                                                 |
| isBigData           | 大数据滚动渲染                           | Boolean | true/false                                                   | true                                                 |
| isTreeTable         | 树形表格                                 | Boolean | true/false                                                   | true                                                 |
| lazy                | 是否懒加载子节点数据                     | Boolean | true/false                                                   | -                                                    |
| rowKey              | 行数据的 Key 的主键，用于其他相关操作    | String  | -                                                            | -                                                    |
| treeProps           | 渲染嵌套数据的配置选项                   | Object  | -                                                            | { hasChildren: 'hasChildren', children: 'children' } |
| scrollYRenderConfig | 大数据滚动的配置选项                     | Object  | renderSize：一次渲染多少条数据；offsetSize：预渲染多少条数据 | { renderSize: 30, offsetSize: 10 }                   |
| selection           | 表格勾选列                               | Boolean | true/false                                                   | false                                                |
| align               | 列的对齐方式                             | String  | left/center/right                                            | center                                               |
| headerAlign         | 表头对齐方式                             | String  | left/center/right                                            | center                                               |
| index               | 是否显示表格序号（开启虚拟渲染后不可用） | Boolean | true/false                                                   | false                                                |
| menuWidth           | 操作菜单栏的宽度                         | Number  | -                                                            | 200                                                  |
| menuFixed           | 操作列是否冻结                           | Boolean | true/false                                                   | false                                                |
| menu                | 是否显示操作菜单栏                       | Boolean | true/false                                                   | true                                                 |
| editBtn             | 行内编辑按钮                             | Boolean | true/false                                                   | true                                                 |
| delBtn              | 行内删除按钮                             | Boolean | true/false                                                   | true                                                 |

### Column Attributes

| 参数       | 说明               | 类型                                    | 可选择            | 默认值  |
| :--------- | :----------------- | :-------------------------------------- | :---------------- | :------ |
| type       | 字段类型           | String                                  | "radio", "select" | 'input' |
| label      | 标题名称           | String                                  | -                 | -       |
| prop       | 列字段(唯一不重复) | String                                  | -                 | -       |
| minWidth   | 列最小宽度         | Number                                  | -                 | auto    |
| sortable   | 开启排序           | Boolean                                 | true/false        | false   |
| fixed      | 冻结列             | Boolean                                 | true/false        | false   |
| align      | 列的对其方式       | String                                  | left/center/right | center  |
| formatter  | 用来格式化内容     | Function(row, column, cellValue, index) | -                 | -       |
| showColumn | 是否加入动态现隐列 | Boolean                                 | true/false        | true    |
| hide       | 隐藏列             | Boolean                                 | true/false        | false   |

## Slots

| name           | 说明       |
| :------------- | :--------- |
| 字段名（prop） | -          |
| menuLeft       | 操作栏左侧 |
| menuRight      | 操作栏右侧 |

## Events

| 事件名称          | 说明                                      | 回调参数       |
| :---------------- | :---------------------------------------- | :------------- |
| tree-load         | 加载子节点数据的函数，lazy 为 true 时生效 | row, resolve   |
| handleUpdate      | 点击行修改按钮                            | row            |
| handleDelete      | 点击行删除按钮                            | row            |
| handleRowClick    | 点击行                                    | row            |
| handleCellClick   | 点击单元格                                | { row, value } |
| HandleRowDblClick | 行双击                                    | row            |
| setCurrentRow     | 设置滚动到当前                            | row            |
| selection-change  | 多选项发送变化                            | val            |

## Methods

| 方法名             | 说明                                                  | 参数 |
| :----------------- | :---------------------------------------------------- | :--- |
| clearExpanded      | 收起所有展开行,并清空展开状态（下次展开重新请求数据） | -    |
| toggleAllSelection | 选中所有行                                            | -    |
