# 工具栏 toolbar

::: demo

```vue
<template>
  <oce-toolbar :option="tableOption" :column.sync="tableColumn"> </oce-toolbar>
</template>

<script>
export default {
  data() {
    return {
      tableOption: {
        addBtn: true,
        editBtn: true,
        delBtn: true,
        importBtn: true,
        exportBtn: true,
        dowTempBtn: true,
        refreshBtn: true,
      },
      tableColumn: [
        {
          label: '列1',
          prop: 'column1',
        },
        {
          label: '列2',
          prop: 'column2',
        },
        {
          label: '列3',
          prop: 'column3',
        },
        {
          label: '列4',
          prop: 'column4',
        },
      ],
    }
  },
}
</script>
```

:::

## Attributes

| 参数        | 说明                                                                    | 类型    | 可选择 | 默认值 |
| :---------- | :---------------------------------------------------------------------- | :------ | :----- | :----- |
| option      | 组件配置属性，详情见 Option 属性                                        | Object  | -      | -      |
| column      | 显隐列配置属性，配合表格组件使用，详情见 column 属性，支持 .sync 修饰符 | Array   | -      | -      |
| show-search | 是否显示搜索栏，配合搜索框使用，支持 .sync 修饰符                       | Boolean | -      | -      |

### Option Attributes

| 参数       | 说明             | 类型    | 可选择     | 默认值 |
| :--------- | :--------------- | :------ | :--------- | :----- |
| addBtn     | 显示新增按钮     | Boolean | true/false | true   |
| editBtn    | 显示修改按钮     | Boolean | true/false | false  |
| delBtn     | 显示删除按钮     | Boolean | true/false | true   |
| importBtn  | 显示导入按钮     | Boolean | true/false | false  |
| exportBtn  | 显示导出按钮     | Boolean | true/false | false  |
| dowTempBtn | 显示模板下载按钮 | Boolean | true/false | false  |
| refreshBtn | 显示刷新按钮     | Boolean | true/false | false  |

### Column Attributes

| 参数       | 说明               | 类型    | 可选择     | 默认值 |
| :--------- | :----------------- | :------ | :--------- | :----- |
| label      | 标题名称           | String  | -          | -      |
| prop       | 列字段(唯一不重复) | String  | -          | -      |
| hide       | 隐藏列             | Boolean | true/false | false  |
| fixed      | 冻结列             | Boolean | true/false | false  |
| sortable   | 开启排序           | Boolean | true/false | false  |
| showColumn | 是否加入动态现隐列 | Boolean | true/false | true   |

## Slots

| name      | 说明             |
| :-------- | :--------------- |
| menuLeft  | 左侧普通按钮尾部 |
| menuRight | 右侧圆形按钮尾部 |

## Events

| 事件名称          | 说明             | 回调参数 |
| :---------------- | :--------------- | :------- |
| refresh           | 刷新按钮事件     | -        |
| add               | 新增按钮事件     | -        |
| edit              | 修改按钮事件     | -        |
| remove            | 删除按钮事件     | -        |
| import-excel      | 导入按钮事件     | -        |
| export-excel      | 导出按钮事件     | -        |
| download-template | 模板下载按钮事件 | -        |

## Methods

| 方法名 | 说明 | 参数 |
| :----- | :--- | :--- |
| -      | -    | -    |
