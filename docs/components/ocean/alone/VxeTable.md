# Vxe表格 Vxe Table

::: demo

```vue

<template>
  <div class="XnyVxeTableDemo">
    <oce-vxe-table
      :data="tableData"
      :column="tableColumn"
      :option="tableOption"
    ></oce-vxe-table>
  </div>
</template>

<script>
export default {
  name: 'XnyVxeTableDemo',
  data() {
    return {
      tableColumn: [
        {
          label: '列1',
          prop: 'column1',
        },
        {
          label: '列2',
          prop: 'column2',
        },
        {
          label: '列3',
          prop: 'column3',
        },
        {
          label: '列4',
          prop: 'column4',
        },
      ],
      tableData: [
        {
          column1: '1-1',
          column2: '1-2',
          column3: '1-3',
          column4: '1-4',
          id: 1,
        },
        {
          column1: '2-1',
          column2: '2-2',
          column3: '2-3',
          column4: '2-4',
          id: 2,
        },
        {
          column1: '3-1',
          column2: '3-2',
          column3: '3-3',
          column4: '3-4',
          id: 3,
        },
        {
          column1: '4-1',
          column2: '4-2',
          column3: '4-3',
          column4: '4-4',
          id: 4,
        },
      ],
      tableOption: {},
    }
  },
}
</script>

```

:::