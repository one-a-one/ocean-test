# 组合示例

::: demo

```vue

<template>
  <section class="container">
    <oce-search
      :option="tableOption"
      :column.sync="tableColumn"
      :show-search="showSearch"
      v-model="search"
    ></oce-search>
    <oce-toolbar
      :option="tableOption"
      :column.sync="tableColumn"
      :show-search.sync="showSearch"
      @add="handleAdd"
      @remove="handleRemove"
      @export-excel="handleEexportExcel"
      @refresh="handleRefresh"
      @update-column="updateColumn"
    >
    </oce-toolbar>
    <oce-vxe-table
      ref="vxeTable"
      v-if="showTable"
      :data="tableData"
      :column="tableColumn"
      :option="tableOption"
    ></oce-vxe-table>
    <oce-pagination :page.sync="page" />
  </section>
</template>

<script>
export default {
  name: 'aloneDemo',
  data() {
    return {
      search: {},
      showTable: true,
      tableColumn: [
        {
          label: '列1',
          prop: 'column1',
          search: true,
          searchslot: false,
          searchLabelWidth: 80, // 默认 90
          searchSpan: 6, // 默认6
          searchPlaceholder: '自定义提示文字',
        },
        {
          label: '列2',
          prop: 'column2',
          search: true,
        },
        {
          label: '列3',
          prop: 'column3',
        },
        {
          label: '列4',
          prop: 'column4',
        },
      ],
      tableData: [
        {
          column1: '1-1',
          column2: '1-2',
          column3: '1-3',
          column4: '1-4',
          id: 1,
        },
        {
          column1: '2-1',
          column2: '2-2',
          column3: '2-3',
          column4: '2-4',
          id: 2,
        },
        {
          column1: '3-1',
          column2: '3-2',
          column3: '3-3',
          column4: '3-4',
          id: 3,
        },
        {
          column1: '4-1',
          column2: '4-2',
          column3: '4-3',
          column4: '4-4',
          id: 4,
        },
      ],
      tableOption: {
        addBtn: true,
        delBtn: true,
        exportBtn: true,
      },
      showSearch: true,
      page: {
        total: 75, // 总数
        currentPage: 1, // 当前页码
        pageSize: 10, // 分页数
        pagerCount: 5, // 分页按钮数量
      },
    }
  },
  props: {},
  components: {},
  watch: {},
  computed: {},
  methods: {
    handleAdd() {
      // 新增按钮回调
    },
    handleRemove() {
      // 删除按钮回调
    },
    handleEexportExcel() {
      // 导出按钮回调
    },
    handleRefresh() {
      // 刷新按钮回调
    },
    updateColumn(param) {
      if (!param.prop || !param.attrs) return
      this.tableColumn.forEach((el) => {
        if (el.prop == param.prop) {
          // eslint-disable-next-line no-param-reassign
          el[param.attrs] = param.val
        }
      })
      this.showTable = false
      this.$nextTick(() => {
        this.showTable = true
      })
    },
  },
  created() {},
  mounted() {},
}
</script>

<style scoped lang="scss"></style>



```

:::