# 搜索框 search

::: demo

```vue
<template>
  <oce-search :option="tableOption" :column.sync="tableColumn" v-model="search">
    <template slot-scope="scope" slot="search-search2">
      <el-select v-model="scope.row['search2']" placeholder="请选择">
        <el-option
          v-for="item in options"
          :key="item.value"
          :label="item.label"
          :value="item.value"
        >
        </el-option>
      </el-select>
    </template>
  </oce-search>
</template>

<script>
export default {
  data() {
    return {
      search: {},
      tableOption: {
        searchMenuSpan: 6,
      },
      tableColumn: [
        {
          label: '搜索项1',
          prop: 'search1',
          search: true,
          searchslot: false,
          searchLabelWidth: 80, // 默认 90
          searchSpan: 6, // 默认6
          searchPlaceholder: '自定义提示文字',
        },
        {
          label: '搜索项2',
          prop: 'search2',
          search: true,
          searchslot: true,
        },
      ],
      options: [
        {
          value: '选项1',
          label: '黄金糕',
        },
        {
          value: '选项2',
          label: '双皮奶',
        },
        {
          value: '选项3',
          label: '蚵仔煎',
        },
        {
          value: '选项4',
          label: '龙须面',
        },
        {
          value: '选项5',
          label: '北京烤鸭',
        },
      ],
    }
  },
}
</script>
```

:::

## Attributes

| 参数        | 说明                               | 类型    | 可选择 | 默认值 |
| :---------- | :--------------------------------- | :------ | :----- | :----- |
| option      | 组件配置属性，详情见 Option 属性   | Object  | -      | -      |
| column      | 搜索项配置属性，详情见 column 属性 | Array   | -      | -      |
| show-search | 是否显示搜索栏，配合工具栏使用     | Boolean | -      | -      |
| v-model     | 搜索项值                           | Object  | -      | -      |

### Option Attributes

| 参数           | 说明           | 类型   | 可选择 | 默认值 |
| :------------- | :------------- | :----- | :----- | :----- |
| searchMenuSpan | 搜索框按钮栅列 | Number | -      | 4      |

### Column Attributes

| 参数              | 说明                 | 类型    | 可选择 | 默认值 |
| :---------------- | :------------------- | :------ | :----- | :----- |
| label             | 标题名称             | String  | -      | -      |
| prop              | 列字段(唯一不重复)   | String  | -      | -      |
| search            | 是否为搜索项         | Boolean | -      | false  |
| searchslot        | 表格搜索选项的自定义 | Boolean | -      | false  |
| searchLabelWidth  | 搜索框的标题宽度     | Number  | -      | 80     |
| searchSpan        | 搜索框栅列           | Number  | -      | 6      |
| searchPlaceholder | 搜索框辅助文字       | String  | -      | -      |

## Slots

| name                      | 说明         |
| :------------------------ | :----------- |
| search - 字段名（prop） | 自定义搜索项 |

## Events

| 事件名称      | 说明                 | 回调参数    |
| :------------ | :------------------- | :---------- |
| search-change | 点击搜索后触发该事件 | searchParam |
| search-reset  | 清空搜索回调方法     | -           |

## Methods

| 方法名      | 说明         | 参数 |
| :---------- | :----------- | :--- |
| searchReset | 清空搜索方法 | -    |
