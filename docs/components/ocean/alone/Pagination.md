# 分页栏 pagination

::: demo

```vue
<template>
  <oce-pagination :page.sync="page" />
</template>

<script>
export default {
  data() {
    return {
      page: {
        total: 75, // 总数 初始化建议为0
        currentPage: 1, // 当前页码
        pageSize: 10, // 分页数
        pagerCount: 5, // 分页按钮数量
      },
    }
  },
}
</script>
```

:::

## Attributes

| 参数    | 说明                       | 类型   | 可选择            | 默认值 |
| :------ | :------------------------- | :----- | :---------------- | :----- |
| page    | 分页数据(需要 sync 修饰符) | Object | -                 | -      |
| content | 对齐方式                   | String | left/center/right | center |

### Page Attributes

| 参数        | 说明                                     | 类型     | 可选择                          | 默认值                    |
| :---------- | :--------------------------------------- | :------- | :------------------------------ | :------------------------ |
| total       | 总条目数                                 | number   | -                               | -                         |
| currentPage | 当前页数                                 | number   | -                               | -                         |
| pageSize    | 每页显示条目个数                         | number   | 10, 20, 30, 40, 50, 100         | 10                        |
| pageSizes   | 每页显示个数选择器的选项设置             | number[] | -                               | [10, 20, 30, 40, 50, 100] |
| pagerCount  | 页码按钮的数量，当总页数超过该值时会折叠 | number   | 大于等于 5 且小于等于 21 的奇数 | 5                         |

## Slots

| name | 说明 |
| :--- | :--- |
| -    | -    |

## Events

| 事件名称       | 说明                     | 回调参数 |
| :------------- | :----------------------- | :------- |
| current-change | currentPage 改变时会触发 | 当前页   |
| size-change    | pageSize 改变时会触发    | 每页条数 |

## Methods

| 方法名 | 说明 | 参数 |
| :----- | :--- | :--- |
| -      | -    | -    |
