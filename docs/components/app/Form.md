## 案例源码
```vue
<template>
  <scroll-view :scroll-y="true" style="height: 100%;">
    <xny-form
      v-model="form"
      :column="formColumn"
      :option="formOption"
    ></xny-form>
  </scroll-view>
</template>

<script lang="ts">
import XnyForm from '@/components/XnyForm/XnyForm.vue';
import dicMock from '../assets/dicMock'

export default {
  name: 'BasicsForm',
  components: { XnyForm },
  setup() {
    return {};
  },
  data() {
    return {
      form: {},
      formColumn: [
        {
          label: '输入框',
          prop: 'input',
          type: 'input',
        },
        {
          label: '扫码输入框',
          prop: 'scanCode',
          type: 'scanCode',
        },
        {
          label: '多行输入框',
          prop: 'textarea',
          type: 'textarea',
        },
        {
          label: '数字输入框',
          prop: 'number',
          type: 'number',
        },
        {
          label: '时间选择器',
          prop: 'time',
          type: 'time',
        },
        {
          label: '日期选择器',
          prop: 'date',
          type: 'date',
        },
        {
          label: '日期时间',
          prop: 'datetime',
          type: 'datetime',
        },
        {
          label: '日期范围',
          prop: 'daterange',
          type: 'daterange',
        },
        {
          label: '日期时间范围',
          prop: 'datetimerange',
          type: 'datetimerange',
        },
        {
          label: '数据选择器',
          prop: 'dataPicker',
          type: 'dataPicker',
          dicData: dicMock.treeDic,
        },
        {
          label: '普通选择器',
          prop: 'selector',
          type: 'selector',
          dicData: dicMock.objListDic,
        },
        {
          label: '数字框',
          prop: 'numberBox',
          type: 'numberBox'
        },
        {
          label: '组合框',
          prop: 'combox',
          type: 'combox',
          dicData: dicMock.strListDic
        },
        {
          label: '开关选择器',
          prop: 'switch',
          type: 'switch',
        },
        {
          label: '多选框',
          prop: 'checkbox',
          type: 'checkbox',
          dicData: dicMock.objListDic,
        },
        {
          label: '单选框',
          prop: 'radio',
          type: 'radio',
          dicData: dicMock.objListDic,
        },
        {
          label: '滑动选择器',
          prop: 'slider',
          type: 'slider'
        },
      ],
      formOption: {
        labelWidth: 90,
        labelAlign: 'left',
        submitBtn: true,
        resetBtn: true,
        button: true
      }
    };
  },
  onLoad() {},
  onShow() {}
};
</script>

<style lang="scss" scoped></style>

```

## 说明文档

<app-form-doc/>
<app-demo src="http://ocean-v2.top:9090/#/pages/text/basicsForm/basicsForm"/>

<!-- <app-demo src="http://localhost:3000/xy-app-demo/#/pages/text/XnyFormDemo"/> -->
