# Lodop 打印

::: demo

```vue
<template>
  <section class="LodopDemo">
    <el-button @click="preview">预览</el-button>
    <el-button @click="print">打印</el-button>
  </section>
</template>

<script>
const template = `
  LODOP.PRINT_INIT('')
  LODOP.SET_PRINT_PAGESIZE(0, 800, 1100, '')
  LODOP.SET_PRINT_STYLE('fontName', '宋体')
  LODOP.SET_PRINT_STYLE('fontSize', '9')
  LODOP.SET_PRINT_STYLE('fontColor', '#000000')
  LODOP.SET_PRINT_STYLE('bold', '0')
  LODOP.SET_PRINT_STYLE('italic', '0')
  LODOP.SET_PRINT_STYLE('underline', '0')
  LODOP.SET_PRINT_STYLE('alignment', '1')
  LODOP.SET_PRINT_STYLE('angle', '0')
  LODOP.SET_PRINT_STYLE('itemType', '0')
  LODOP.SET_PRINT_STYLE('hOrient', '0')
  LODOP.SET_PRINT_STYLE('vOrient', '0')
  LODOP.SET_PRINT_STYLE('penWidth', '1')
  LODOP.SET_PRINT_STYLE('penStyle', '0')
  LODOP.SET_PRINT_STYLE('stretch', '0')
  LODOP.SET_PRINT_STYLE('previewOnly', '0')
  LODOP.SET_PRINT_STYLE('readOnly', 'true')
  LODOP.SET_PRINT_STYLE('fontSize', '19.84251968503937')
  LODOP.SET_PRINT_STYLE('alignment', '2')
  LODOP.ADD_PRINT_TEXT('3mm', '1mm', '108mm', '11mm', ( form && form['title'] ) ? form['title'] : 'L700过程标签')
  LODOP.SET_PRINT_STYLE('fontSize', '11.905511811023624')
  LODOP.SET_PRINT_STYLE('alignment', '1')
  LODOP.ADD_PRINT_TEXT('14mm', '5mm', '30mm', '5mm', ( form && form['label1'] ) ? form['label1'] : '物料名称:')
  LODOP.ADD_PRINT_TEXT('14mm', '30mm', '30mm', '5mm', ( form && form['name'] ) ? form['name'] : 'L700大粒度烧结料')
  LODOP.ADD_PRINT_TEXT('24mm', '5mm', '30mm', '5mm', ( form && form['label2'] ) ? form['label2'] : '物料批号:')
  LODOP.ADD_PRINT_TEXT('24mm', '30mm', '50mm', '5mm', ( form && form['batch'] ) ? form['batch'] : 'XMA-LDS-21000001-03')
  LODOP.ADD_PRINT_BARCODE('14mm', '74mm', '35mm', '35mm', 'QRCode', ( form && form['QRCode'] ) ? form['QRCode'] : '12345临兵斗者皆阵列前行abcDEF!@#$%^&*()_+=-[]\|}{;:,./<>?~')
  LODOP.SET_PRINT_STYLEA(0,"DataCharset","UTF-8");
  LODOP.ADD_PRINT_RECT('1mm', '1mm', '108mm', '76mm', '0', '1')`

export default {
  name: 'LodopDemo',
  data() {
    return {}
  },
  props: {},
  watch: {},
  computed: {},
  methods: {
    preview() {
      try {
        this.$ocean.lodopPreview(template, {
          title: '自定义标题',
          name: '自定义物料名称',
          batch: '自定义物料批号',
        })
      } catch (err) {
        this.$message({
          type: 'error',
          dangerouslyUseHTMLString: true,
          message: err.message,
          duration: 10000,
          showClose: true,
        })
      }
    },
    print() {
      try {
        this.$ocean.lodopPrint(template, {
          title: '自定义标题',
          name: '自定义物料名称',
          batch: '自定义物料批号',
        })
      } catch (err) {
        this.$message({
          type: 'error',
          dangerouslyUseHTMLString: true,
          message: err.message,
          duration: 10000,
          showClose: true,
        })
      }
    },
  },
  created() {},
  mounted() {},
}
</script>

<style></style>
```

:::

## 环境搭建

- 本地调试打印机正常使用
- 下载安装[Lodop](http://www.lodop.net/download/Lodop6.226Clodop4.158.zip)
- Lodop[官方网站](http://www.lodop.net/index.html)（包含资料及各种 Demo）

## 模板设计

- 使用模板生成器生成所需模板

- 存储为 JSON、js 静态文件或写数据库使用接口调用模板字符串,待业务开发调用

  ```js
  const template = `
    LODOP.PRINT_INIT('')
    LODOP.SET_PRINT_PAGESIZE(0, 800, 1100, '')
    LODOP.SET_PRINT_STYLE('fontName', '宋体')
    LODOP.SET_PRINT_STYLE('fontSize', '9')
    LODOP.SET_PRINT_STYLE('fontColor', '#000000')
    LODOP.SET_PRINT_STYLE('bold', '0')
    LODOP.SET_PRINT_STYLE('italic', '0')
    LODOP.SET_PRINT_STYLE('underline', '0')
    LODOP.SET_PRINT_STYLE('alignment', '1')
    LODOP.SET_PRINT_STYLE('angle', '0')
    LODOP.SET_PRINT_STYLE('itemType', '0')
    LODOP.SET_PRINT_STYLE('hOrient', '0')
    LODOP.SET_PRINT_STYLE('vOrient', '0')
    LODOP.SET_PRINT_STYLE('penWidth', '1')
    LODOP.SET_PRINT_STYLE('penStyle', '0')
    LODOP.SET_PRINT_STYLE('stretch', '0')
    LODOP.SET_PRINT_STYLE('previewOnly', '0')
    LODOP.SET_PRINT_STYLE('readOnly', 'true')
    LODOP.SET_PRINT_STYLE('fontSize', '19.84251968503937')
    LODOP.SET_PRINT_STYLE('alignment', '2')
    LODOP.ADD_PRINT_TEXT('3mm', '1mm', '108mm', '11mm', ( form && form['title'] ) ? form['title'] : 'L700过程标签')
    LODOP.SET_PRINT_STYLE('fontSize', '11.905511811023624')
    LODOP.SET_PRINT_STYLE('alignment', '1')
    LODOP.ADD_PRINT_TEXT('14mm', '5mm', '30mm', '5mm', ( form && form['label1'] ) ? form['label1'] : '物料名称:')
    LODOP.ADD_PRINT_TEXT('14mm', '30mm', '30mm', '5mm', ( form && form['name'] ) ? form['name'] : 'L700大粒度烧结料')
    LODOP.ADD_PRINT_TEXT('24mm', '5mm', '30mm', '5mm', ( form && form['label2'] ) ? form['label2'] : '物料批号:')
    LODOP.ADD_PRINT_TEXT('24mm', '30mm', '50mm', '5mm', ( form && form['batch'] ) ? form['batch'] : 'XMA-LDS-21000001-03')
    LODOP.ADD_PRINT_BARCODE('14mm', '74mm', '35mm', '35mm', 'QRCode', ( form && form['QRCode'] ) ? form['QRCode'] : '12345临兵斗者皆阵列前行abcDEF!@#$%^&*()_+=-[]\|}{;:,./<>?~')
    LODOP.SET_PRINT_STYLEA(0,"DataCharset","UTF-8");
    LODOP.ADD_PRINT_RECT('1mm', '1mm', '108mm', '76mm', '0', '1')`
  ```

## 业务使用

- 预览标签`this.$ocean.lodopPreview(template, form)`
- 直接打印`this.$ocean.lodopPrint(template, form)`
