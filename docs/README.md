---
sidebar: false

home: true
heroImage: /o-logo.png
heroText: Ocean
tagline: Welcome to your Ocean site
actionText: 快速上手 →
actionLink: /components/xy/SplitPane
features:
  - title: private
    details: 组件案例、开发规范说明文档
  - title: element-ui
    details: 基础UI风格基于element-ui
  - title: avue
    details: 兼容avue配置项及使用风格
custom: null
content: null
theme: null
footer: MIT Licensed | Copyright © 2021-present Evan You
---
